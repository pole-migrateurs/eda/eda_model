# Nom fichier :        fn_sql_0
# Projet :             controle migrateur / traitement
# Organisme :          IAV
# Auteur :             C�dric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :   06/02/2007 10:58:37
# Compatibilite :      R 2.4.0
# Etat :               OK
# Description          fonction g�n�rique de connection � la base sans date
#**********************************************************************


# la fonction essaye d'�tablir une connection
# revoit une erreur en l'absence de connection
# puis teste la requ�te et renvoit une erreur en cas de pb de requete
# pour test   baseODBC="BD_CONTMIG.centraldata"

fn_sql_query<-function(channel, sql,silent=TRUE){
     
        if (!silent) print(paste("Try query"))
        e=expression(query<-sqlQuery(channel,sql,errors=TRUE))
        resultatRequete<-tryCatch(eval(e),error = function(e) e)
        # teste le resultat, si il y a erreur
        #classe avec plusieurs variables erreur , condition..
        # d'ou le [1]
        #sinon classe data.frame
        if ((class(resultatRequete)=="data.frame")[1]) { if (!silent) print("query ok")} else
        print(resultatRequete)
        return(list("query"=query,"sql"=sql))
        }



