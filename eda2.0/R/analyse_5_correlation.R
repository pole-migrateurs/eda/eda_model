# Etudes des corr�lations avec le coefficient de spearman
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#Coefficient de corr�lation de spearman (la distribution des variables ne suit pas une loi normale)
#Etude de la corr�lation entre les diff�rentes variables
corers=ers[,c(
				"op_surfaceechantillon" ,
				"alt_gradie",
				"distance_sea",
				"slope_mean",
				"elev_mean",
				"rain_mean",
				"temp_mean",
				"relative_distance","distance_source",
				"work",
				"c_height","nbdams","cs_nbdams","cs_height",
				"strahlermax","shree",
				"catchment_area","art_11_13","art_14","arable_21",
				"permcrop_22","pasture_23","hetagr_24","forest_31",
				"natural_32_33","wetlands_4","inwat_51","marwat_52",
				"up_catchment_area","up_art_11_13","up_art_14","up_arable_21",
				"up_permcrop_22","up_pasture_23","up_hetagr_24","up_forest_31",
				"up_natural_32_33","up_wetlands_4","up_inwat_51","up_marwat_52",
				"ef_wetted_area")]
cor(corers,use = "complete")
cor=symnum(clS <-cor(corers,use ="complete",method="spearman"))
print(cor)


#Avec les variables s�lectionn�es d'apr�s l'ACP
corers=ers[,c(
				"op_surfaceechantillon",
				"alt_gradie",
				"slope_mean",
				"elev_mean",
				"rain_mean",
				"temp_mean",
				"distance_sea","relative_distance","distance_source",
				"work",
				"cs_nbdams","cs_height",
				"up_area","up_catchment_area",
				"p_up_art_11_13","p_up_arable_21",
				"p_up_pasture_23","p_up_hetagr_24","p_up_forest_31",
				"p_up_natural_32_33",
				"ef_wetted_area","strahlermax","shree")]
for(i in 1:(length(corers)-1)){
	print(paste(names(corers[,i]),paste(names(corers[,i+1]),cor.test(corers[,i],corers[,i+1],method="spearman",use="complete")$p.value)))
}
cor(corers,use = "complete",method="spearman")
corsp<-round(cor(corers,use = "complete"),3)
cor=symnum(clS <-cor(corers,use ="complete",method="spearman")) #�tranges les r�sultats avec les corr�lations n�gatives je comprend pas � quoi �a sert
print(cor)


#Pour les mod�les :
# ne pas utiliser simultan�ment : 
# summer et winter
# slope_mean et alt_gradie et elev_mean
# distance_sea et relative_distance

# Corr�lation pente-altitude-gradient
x11();hist(ers$elev_mean)  #non normal
x11();hist(ers$temp_mean) #approx normal
x11();plot(ers$elev_mean~ers$slope_mean)
cor.test(ers$elev_mean,ers$slope_mean,method="spearman",exact=TRUE)
x11();plot(ers$elev_mean~ers$alt_gradie)
cor.test(ers$elev_mean,ers$alt_gradie,method="spearman",exact=TRUE)
x11();plot(ers$slope_mean~ers$alt_gradie)
cor.test(ers$slope_mean,ers$alt_gradie,method="spearman",exact=TRUE)

#Corr�lation elev_mean - temp_mean
x11();plot(ers$elev_mean~ers$temp_mean)
cor.test(ers$elev_mean,ers$temp_mean,method="spearman",exact=TRUE)
#Corr�lation elev_mean - rain_mean
x11();plot(ers$elev_mean~ers$rain_mean)
cor.test(ers$elev_mean,ers$rain_mean,method="spearman",exact=TRUE)

#Corr�lation alt_gradie - temp_mean
x11();plot(ers$alt_gradie~ers$temp_mean)
cor.test(ers$alt_gradie,ers$temp_mean,method="spearman",exact=TRUE)
#Corr�lation alt_gradie - rain_mean
x11();plot(ers$alt_gradie~ers$rain_mean)
cor.test(ers$alt_gradie,ers$rain_mean,method="spearman",exact=TRUE)


# Corr�lation slope_mean - temp_mean
x11();plot(ers$slope_mean~ers$temp_mean)
cor.test(ers$slope_mean,ers$temp_mean,method="spearman",exact=TRUE)
# Corr�lation slope_mean - rain_mean
x11();plot(ers$slope_mean~ers$rain_mean)
cor.test(ers$slope_mean,ers$rain_mean,method="spearman",exact=TRUE)

#Corr�lation elev_mean - temp_mean
x11();plot(ers$elev_mean~ers$temp_mean)
cor.test(ers$elev_mean,ers$temp_mean,method="spearman",exact=TRUE)
#Corr�lation elev_mean - rain_mean
x11();plot(ers$elev_mean~ers$rain_mean)
cor.test(ers$elev_mean,ers$rain_mean,method="spearman",exact=TRUE)

#Corr�lation distance relative - distance source - distance_sea (distance � la mer) - work
x11();plot(ers$relative_distance~ers$distance_source)
cor.test(ers$relative_distance,ers$distance_source,method="spearman",exact=TRUE)
x11();plot(ers$relative_distance~ers$distance_sea)
x11();plot(ers$distance_source~ers$distance_sea)


x11()
par(fig=c(0,0.8,0,0.8),new=TRUE)
plot(ers$distance_source,ers$elev_mean, xlab="distance_source",ylab="elev_mean")
par(fig=c(0,0.8,0.55,1), new=TRUE)
boxplot(ers$distance_source,horizontal=TRUE,axes=FALSE)
par(fig=c(0.65,1,0,0.8),new=TRUE)
boxplot(ers$elev_mean,axes=FALSE)

x11();plot(ers$distance_sea~ers$elev_mean)
x11();plot(ers$elev_mean~ers$distance_sea)

cor.test(ers$relative_distance,ers$distance_sea,method="spearman",exact=TRUE)
x11();plot(ers$relative_distance~ers$work)
x11();plot(ers$work~ers$distance_source)
cor.test(ers$work,ers$distance_source,method="spearman",exact=TRUE)

cor.test(ers$relative_distance,ers$work,method="spearman",exact=TRUE)
cordist=ers[,c("distance_sea","relative_distance","distance_source","work")]
cor(cordist,use = "complete")
symnum(clS <-cor(cordist,use ="complete",method="spearman"))


# Corr�lation distance � la mer - altitude
x11();plot(ers$distance_sea~ers$alt_gradie)
cor.test(ers$distance_sea,ers$alt_gradie,method="spearman",exact=TRUE)

# Corr�lation distance � la mer - slope_mean
x11();plot(ers$distance_sea~ers$slope_mean)
cor.test(ers$distance_sea,ers$slope_mean,method="spearman",exact=TRUE)
# Corr�lation distance � la mer - alt_gradie
x11();plot(ers$distance_sea~ers$alt_gradie)
cor.test(ers$distance_sea,ers$alt_gradie,method="spearman",exact=TRUE)
# Corr�lation distance � la mer - slope_mean
x11();plot(ers$distance_source~ers$slope_mean)
cor.test(ers$distance_source,ers$slope_mean,method="spearman",exact=TRUE)
# Corr�lation distance � la mer - alt_gradie
x11();plot(ers$distance_source~ers$alt_gradie)
cor.test(ers$distance_source,ers$alt_gradie,method="spearman",exact=TRUE)


# Corr�lation cs_height - rain_mean
x11();plot(ers$cs_height~ers$rain_mean)
cor.test(ers$cs_height,ers$rain_mean,method="spearman",exact=TRUE)
cor.test(ers$elev_mean,ers$p_up_forest_31,method="spearman",exact=TRUE)

x11();plot(ers$cs_height~ers$cs_nbdams)
#Variables CLC
x11();par(mfrow=c(1,2));hist(ers$p_hetagr_24,main=NULL);hist(ers$p_up_hetagr_24,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_arable_21,main=NULL);hist(ers$p_up_arable_21,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_inwat_51,main=NULL);hist(ers$p_up_inwat_51,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_natural_32_33,main=NULL);hist(ers$p_up_natural_32_33,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_pasture_23,main=NULL);hist(ers$p_up_pasture_23,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_permcrop_22,main=NULL);hist(ers$p_up_permcrop_22,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_wetlands_4,main=NULL);hist(ers$p_up_wetlands_4,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_art_11_13,main=NULL);hist(ers$p_up_art_11_13,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_art_14,main=NULL);hist(ers$p_up_art_14,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$p_forest_31,main=NULL);hist(ers$p_up_forest_31,main=NULL)

boxplot(ers$p_up_forest)
x11();par(mfrow=c(5,4),mar=c(4.2,1.9,0.5,0.5));hist(ers$p_hetagr_24,main=NULL);hist(ers$p_up_hetagr_24,main=NULL);hist(ers$p_arable_21,main=NULL);hist(ers$p_up_arable_21,main=NULL);hist(ers$p_inwat_51,main=NULL);hist(ers$p_up_inwat_51,main=NULL);hist(ers$p_natural_32_33,main=NULL);hist(ers$p_up_natural_32_33,main=NULL);hist(ers$p_pasture_23,main=NULL);hist(ers$p_up_pasture_23,main=NULL);hist(ers$p_permcrop_22,main=NULL);hist(ers$p_up_permcrop_22,main=NULL);hist(ers$p_wetlands_4,main=NULL);hist(ers$p_up_wetlands_4,main=NULL);hist(ers$p_art_11_13,main=NULL);hist(ers$p_up_art_11_13,main=NULL);hist(ers$p_art_14,main=NULL);hist(ers$p_up_art_14,main=NULL)


#Variables barrages
x11();par(mfrow=c(1,2));hist(ers$nbdams,main=NULL);hist(ers$cs_nbdams,main=NULL)
x11();par(mfrow=c(1,2));hist(ers$cs_height,main=NULL);hist(ers$cs_height,main=NULL)

x11();par(mfrow=c(2,2),mar=c(4.5,1.9,0.8,0.5));hist(ers$nbdams,main=NULL);hist(ers$cs_nbdams,main=NULL);hist(ers$cs_height,main=NULL);hist(ers$cs_height,main=NULL)

x11()
boxplot(ers$nbdams)
summary(ers$nbdams)
ers$clnbdmas<-cut(ers$nb_barrages,c(0,25,50,100,300))
#Corr�lation distance � la mer - nombre de barrages
plot(ers$distance_sea~ers$nbdams)
cor.test(ers$distance_sea,ers$nbdams,method="spearman",exact=TRUE)

par(mfrow=c(1,3))
hist(ers$distance_sea)
hist(ers$relative_distance)
hist(ers$st_distancesource)

for(i in c("n150","n150_300","n300_450","n450_600","n600_750","n750_900","n900","totalnumber")){
	x11()
	print(plot(ers$distance_sea,ers$alt_gradie,col=ifelse(ers[,i]==0,"black","blue"),pch="+"))
}

plot(ers$totalnumber~ers$annee)






