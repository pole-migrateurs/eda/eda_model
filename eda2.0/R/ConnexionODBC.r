# Nom fichier :        ConnexionODBC (classe)
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

#fonction pour valider les acces ODBC
validite_ODBC=function(object)
{
	rep1= class(object@baseODBC[1])=="Character"
	rep2=class(object@baseODBC[2])=="Character"
	rep3=class(object@baseODBC[3])=="ANY"
	rep4=length(object@baseODBC)==3
	return(ifelse(rep1 & rep2 & rep3 & rep4,TRUE,c(1:4)[!c(rep1, rep2, rep3, rep4)]))
}

#' @title ConnexionODBC class 
#' @note Mother class for connection, opens the connection but does not shut it
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @slot baseODBC="vector" (of length 3, character)
#' @slot silent="logical"
#' @slot etat="ANY" # can be -1 or string
#' @slot connexion="ANY" # could be both string or S3
#' @slot sql="character"
#' @slot query="data.frame"
#' @return connexionODBC an S4 object of class connexionODBC
#' @example 
#' object=new("ConnexionODBC")
#' object@baseODBC=baseODBC"
#' object@silent=FALSE
#' object<-connect(object)
#' # odbcCloseAll()
#' odbcClose(object@connexion)
#' @exportClass
setClass(Class="ConnexionODBC",
		representation= representation(baseODBC="vector",silent="logical",etat="ANY",connexion="ANY"),
		prototype = list(silent=TRUE),
		validity=validite_ODBC)


#' connect method for ConnexionODBC class
#' @returnType ConnectionODBC S4 object
#' @return a connexion with slot filled
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @example object=new("ConnexionODBC")
#' object@baseODBC=baseODBC
#' connect(object)
setMethod("connect",signature=signature("ConnexionODBC"),definition=function(object) {     
		      if (length(object@baseODBC)!=3)  {
       if (exists("baseODBC",envir=.GlobalEnv)){ 
        object@baseODBC<-get("baseODBC",envir=.GlobalEnv) 
        } else {
          stop("Il faut definir un vecteur baseODBC avec le lien ODBC, l'utilistateur et le mot de passe")
        }
      }
      e=expression(channel <-odbcConnect(object@baseODBC[1],
							uid = object@baseODBC[2],
							pwd = object@baseODBC[3],
							case = "tolower",
							believeNRows = FALSE))
			if (!exists("odbcConnect")) stop("La librairie RODBC est necessaire, chargez le package ! \n")
			if (!object@silent) print(paste("Essai de connexion :",object@baseODBC[1]))
			# renvoit du resultat d'un try catch expression dans
			#l'ojet Connexion courante, soit un vecteur caractere
			connexionCourante<-tryCatch(eval(e), error=paste("Connexion impossible :",object@baseODBC[1])) 
			if (class(connexionCourante)=="RODBC") {
				if (!object@silent) print("Connexion �tablie")
				object@connexion=connexionCourante  # un object S3 RODBC
				object@etat="Connexion en cours"
			} else {
				print(connexionCourante)
				object@etat=connexionCourante # report de l'erreur
			}
			return(object)
		})

#' utility function allowing to remove factors from ODBC queries
#' @param df 
#' @returnType df
#' @return df
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
killfactor=function(df){
	for (i in 1:ncol(df))
	{
		if(is.factor(df[,i])) df[,i]=as.character(df[,i])
	}
	return(df)
}
