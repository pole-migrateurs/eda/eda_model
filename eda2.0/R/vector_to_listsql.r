#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

vector_to_listsql<-function(vect){
# vect = un vecteur caractere
  if (length(vect)==1) {
  listsql=paste("(","'",vect,"'",")",sep="")
  }  else if  (length(vect)==2){
   listsql=paste("(","'",vect[1],"'",",", sep="")
   listsql=paste(listsql,"'",vect[length(vect)],"'",")", sep="") 
  } else if (length(vect)>2&length(vect)<1000000){
    listsql=paste("(","'",vect[1],"'",",", sep="")
      for(j in 2:(length(vect)-1)){
        listsql=paste(listsql,"'",vect[j],"'",",",sep="")
       }
    listsql=paste(listsql,"'",vect[length(vect)],"'",")", sep="")
  } else {
    n1000=round(length(vect)/1000000)
    listsql=list()
    for (k in 1:n1000){
        progress(k,n1000)
        listsql[[k]]=paste("(","'",vect[1+(k-1)*1000000],"'",",", sep="")
        if (k<n1000){
        for(j in 2:999999){
            #progress(j,999)
            #cat(paste((j+(k-1)*1000),"\n"))
            listsql[[k]]=paste(listsql[[k]],"'",vect[j+(k-1)*1000000],"'",",",sep="")
            }
        listsql[[k]]=paste(listsql[[k]],"'",vect[1000000],"'",")", sep="")
        } else {
        for(j in 2:(length(vect)-k*1000000)){
            #progress(j,999)
            cat(paste((j+(k-1)*100000),"\n"))
            listsql[[k]]=paste("(","'",vect[j+(k-1)*1000000],"'",",", sep="")
            }
        listsql[[k]]=paste(listsql[[k]],"'",vect[length(vect)],"'",")", sep="")
        }
    }    
  }         
return(listsql)
}     