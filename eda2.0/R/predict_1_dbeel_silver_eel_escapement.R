# Silver eel escapement estimation for each emu
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#Bcurrent, Bbest, B0 in t
#current escapement nb eel/ha  ou  kg/ha

#Conversion rate p
p<-0.05

if(secteur=="Crepe"){
	t<-100  #Valeur prise pour les pr�dictions du mod�le delta gamma
	
	M<-0.1386
	
	load(file=paste(datawd,"/dataEDAccm/biology_Crepe.RData",sep="")) #biology
	load(file=paste(datawd,"/dataEDAccm/fishing_Crepe.RData",sep="")) #fishing
	load(file=paste(datawd,"/dataEDAccm/halieutic_Crepe.RData",sep=""))  #halieutic
	#Poids moyen des Silver eel ??
	#Glass undifferencied, Silver : female & male
			Wbio<-biology$catchInKg*1000/biology$catchInNumber
	Wsilver<-mean(Wbio[biology$metier=="SILVER"],na.rm=T)*10^-3   #kg
	Wyellow<-mean(Wbio[biology$metier=="YELLOW"],na.rm=T)*10^-3   #kg
	Wglass<-mean(Wbio[biology$metier=="GLASS"],na.rm=T)*10^-3     #kg
	
	#Age moyen des yellow eel
	Lyellow<-mean(biology$age[biology$metier=="YELLOW"])
	#Age moyen de Silver eel
	Lsilver<-mean(biology$age[biology$metier=="SILVER"])
	#Lifespan ??
	Lifespan<-mean(biology$age[biology$metier=="SILVER"])

	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	#Yglass
	YglassAllYear<-tapply(halieutic$catch.kg[halieutic$metier=="GLASS"],halieutic$year[halieutic$metier=="GLASS"],sum) #kg
	Yglass<-YglassAllYear[t+1-Lifespan] #kg  (t+1 car les ann�es de halieutic$catch commence � 0)
	#Yyellow
	YyellowAllYear<-tapply(halieutic$catch.kg[halieutic$metier=="YELLOW"],halieutic$year[halieutic$metier=="YELLOW"],sum) #kg
	Yyellow<-YyellowAllYear[t+1-Lifespan+Lyellow] #kg
	#Ysilver
	YsilverAllYear<-tapply(halieutic$catch.kg[halieutic$metier=="SILVER"],halieutic$year[halieutic$metier=="SILVER"],sum) #kg
	Ysilver<-YsilverAllYear[t+1]  #kg
}


if(secteur=="Brittany"){
	M<-0.1386
	Lifespan<-12  #Tau T
	Lyellow<-4    #mean age of yellow eel removed by antrhopogenic mortalities
	Wglass<-0.33*10^-3 #kg
	Wyellow<-100*10^-3 #kg
	Wsilver<-800*10^-3 #kg
	t<-2010
	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	Yglass<-(15.3+2)*10^3 #tonnes*10^3-->kg
	Yyellow<-1.297*10^3  #kg
	Ysilver<-0        #kg
}

if(secteur=="Rhone"){
	M<-0.1386
	Lifespan<-4  #Tau T
	Lyellow<-2   #mean age of yellow eel removed by antrhopogenic mortalities
	Wglass<-0.33*10^-3 #kg
	Wyellow<-100*10^-3 #kg
	Wsilver<-800*10^-3 #kg
	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	Yglass<-0
	Yyellow<-0.128*10^6  #gramme   
	Ysilver<-0
}

if(secteur=="Western"){
	M<-0.1386
	Lifespan<-18  #Tau T
	Lyellow<-13    #mean age of yellow eel removed by antrhopogenic mortalities
	Wglass<-0.334*10^-3  #In Bevacqua_DemCam_DescriptionApplications see Alan email for report
	Wyellow<-178*10^-3 #kg
	Wsilver<-mean(c(217,300,647))*10^-3 #kg   217(burrishoole, 300 corrib fishery, 647 mask fishery
	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	Yglass<-0 #kg
	Yyellow<-35*10^3 #kg
	Ysilver<-28*10^3 #kg
	tauminuslambdayellow<-Lifespan-Lyellow
}

if(secteur=="Basque"){
	M<-0.1386
	Lifespan<-4    #Tau T
	Lyellow<-2     #mean age of yellow eel removed by antrhopogenic mortalities
	Wglass<-0.33*10^-3  #kg
	Wyellow<-17.77*10^-3 #kg
	Wsilver<-130.24*10^-3 #kg or 254.6*10^-3 kg (from Diputacion de Gipuzkoa)
	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	Yglass<-614 #kg in 2010
	Yyellow<-NA
	Ysilver<-NA
}


if(secteur=="Anglian"){
	M<-0.1386
	Lifespan<-18.8    #Tau T
	Lyellow<-10     #mean age of yellow eel removed by antrhopogenic mortalities
	Wglass<-0.3*10^-3  #kg
	Wyellow<-157.2*10^-3 #kg
	Wsilver<-382.8*10^-3 #kg
	YglassTemps<-t-Lifespan
	YellowTemps<-t-Lifespan+Lyellow
	Yglass<-0
	Yyellow<-13065 #kg
	Ysilver<-194 #kg
}


##Total number of Yellow eel per year
#secteur<-"Brittany"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YEB<-ModelYearCCMS
#secteur<-"Rhone"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YER<-ModelYearCCMS
#secteur<-"Western"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YEW<-ModelYearCCMS
#secteur<-"Basque"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YEBa<-ModelYearCCMS
#secteur<-"Anglian"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YEA<-ModelYearCCMS
#secteur<-"Crepe"
#load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
#YEC<-ModelYearCCMS



#............................#
#Current escapement Bcurrent
#ccm$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverarea/100
Ncurrent<-p*Wsilver/Wsilver*sum(ccm$nb_eel_per_segment)-Ysilver/Wsilver
Bcurrent<-(p*Wsilver*sum(ccm$nb_eel_per_segment)-Ysilver)*10^3  #gramme
Bcurrentkg<-(p*Wsilver*sum(ccm$nb_eel_per_segment)-Ysilver)  #kg
print(paste("Bcurrent in kg=",Bcurrentkg,sep=" "))
Ncurrent<-Bcurrentkg/Wsilver
print(paste("Ncurrent=",Ncurrent,sep=" "))

if(secteur=="Crepe"){
	load(file=paste(datawd,"/dataEDAccm/predictions/ModelYearCCMS",secteur,".RData",sep=""))
	#colonne 1 ModelYearCCMS = annee 1:99  (donc en fait c'est l'annee 51:149)
	#colonne 1 YsilverAllYear =annee 0:149
	NcurrentAllYear<-p*colSums(ModelYearCCMS[,names(ModelYearCCMS)%in%labels(1:100)])-YsilverAllYear[52:length(YsilverAllYear)]/Wsilver
	BcurrentAllYear<-(p*Wsilver*colSums(ModelYearCCMS[,names(ModelYearCCMS)%in%labels(1:100)])-YsilverAllYear[52:length(YsilverAllYear)])*10^3  #gramme
	BcurrentkgAllYear<-(p*Wsilver*colSums(ModelYearCCMS[,names(ModelYearCCMS)%in%labels(1:100)])-YsilverAllYear[52:length(YsilverAllYear)])  #Bcurrent in kg
}

x11()
plot(NcurrentAllYear,type="l",las=1,cex=0.8,xaxt="n",ylab="Ncurrent Total Number of silver eel",xlab="year",cex.axis=0.6)
points(NcurrentAllYear,type="p",las=1,cex=0.8,pch="+")
axis(1, at=1:length(NcurrentAllYear),labels = min(ers$annee):max(ers$annee),cex.lab=0.65)
x11()
plot(BcurrentkgAllYear,type="l",las=1,cex=0.8,xaxt="n",ylab="Bcurrent in kg",xlab="year",cex.axis=0.6)
points(BcurrentkgAllYear,type="p",las=1,cex=0.8,pch="+")
axis(1, at=1:length(BcurrentkgAllYear),labels = min(ers$annee):max(ers$annee),cex.lab=0.65)



#............................#
#Best achievable Bbest
	#BcO Bcurrent(t=2010,hi,j=0,Ysilver(t)=0)
if(secteur!="Crepe"){
	t<-2010
}
if(secteur=="Crepe"){
	t<-100
}
ccm0<-ccm
ccm0$cs_nbdams<-0
ccm0$cs_score<-0
ccm0$p_up_urban<-0
ccm0$p_up_agricultural<-0
ccm0$p_agricultural<-0
ccm0$p_urban<-0
ccm0$p_unimpact<-1
ccm0$p_up_no_impact<-1
ccm0$pred_d<-predict(md,fn_pred.set(mod=md,data=ccm0,annee=t)$pred.set,type='response')
ccm0$pred_pa<-predict(mpa,fn_pred.set(mod=mpa,data=ccm0,annee=t)$pred.set,type='response')
ccm0$pred_d_pa<-ccm0$pred_d*ccm0$pred_pa
ccm0$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverarea/100

Bcurrent0<-p*Wsilver*sum(ccm0$nb_eel_per_segment)
Bbest<-Bcurrent0+Yglass*Wsilver/Wglass*exp(-M*Lifespan)+Yyellow*Wsilver/Wyellow*exp(-M*(Lifespan-Lyellow))
Bbestkg<-Bbest
print(paste("Bbest in kg =",Bbestkg))


#............................#
#Pristine escapement Bo
#A finir
#Bo<-mean(BcO[,1:min(ers$annee)])
