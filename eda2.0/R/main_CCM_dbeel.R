# Fichier de chargement et de calcul de la couche CCM
# Etat = fonctionnel
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand, C�line Jouanin
###############################################################################

setwd("C:/Documents and Settings/cedric/Mes documents/Migrateur/programmes/workspace3.5/EDA")
source("EDACCM/init.r")
dbeel<-new("BaseEdaCCMriversegmentsdbeel",
		baseODBC="baseODBCdbeel",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western",
		joinschema="wrbd",
		jointable="wrbd_station_ccm_500",
		traveled_segments=vector(),
		dataprovider=as.character(NULL),
		viewoperation="dbeel.view_electrofishing") #  0 length ensures that all are loaded
dbeel<-loaddb(dbeel)
dbeel<-calculatedensities(dbeel)
ele<-dbeel@datadbeel
#write.table(ele,file=paste(datawd,"/dataEDAccm/dbeel_electrofishing.csv",sep=""),sep=";",row.names=FALSE)
#save(ele,file=paste(datawd,"/dataEDAccm/dbeel_electrofishing.Rdata",sep=""))
load(file=paste(datawd,"/dataEDAccm/electrofishing_dbeel.Rdata",sep=""))
