#####################################
# Chargement des couches SIG
#####################################
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
#library(rgdal)
#CE=readShapePoly("EDAcommun/data/bdcarthage/noeuds.shp")
#CE_simp=readShapeLines("EDAcommun/data/bdcarthage/COURS_EAU.SHP")
source("EDAv0/prog/fonctions_graphiques.r")
library(lattice)

trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1)))
# j'utilise rev pour inverser la s�quence
x11()
show.settings()
#names(trellis.par.get())
#YlOrBr <- c("#FFFFD4", "#FED98E", "#FE9929", "#D95F0E", "#993404")
#ramp=colorRampPalette(YlOrBr, space = "Lab")  # une fonction
### space="Lab" helps when colors don't form a natural sequence
#
#
#couleurs=ramp(100)
#trellis.par.set(set = FALSE, regions =
#            list(col =rev(couleurs) ))
#x11()
#show.settings()
##mathlab
ramp=colorRampPalette(c("#00007F", "blue", "#007FFF", "cyan",
                     "#7FFF7F", "yellow", "#FF7F00", "red", "#7F0000"))
couleurs=ramp(100)
trellis.par.set(set = FALSE, regions =
             list(col =couleurs ))
#x11()
#show.settings()
#display.brewer.all()
#ramp=colorRampPalette(brewer.pal(9, "Reds"))
#couleurs=rev(ramp(100))
#trellis.par.set(set = FALSE, regions =
#             list(col =couleurs ))
#x11()
#show.settings()
#CE=readShapePoly("horsco/Bassin_hydro_region.shp")
bassin=4
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),header=TRUE)
if (!exists("noeudsxy"))noeudsxy=read.table(file=paste("EDAcommun/data/extraction_parametres/noeuds_parcouruXY.txt",sep=""),header=TRUE)
barxy=merge(bar,noeudsxy, by="Id_BDCARTHAGE" )
spdf=CrSpPtsDF(xlabel="X",ylabel="Y",data=barxy)
#bbox(coordinates(spdf))
#bubble(spdf,"nb_barrages",main="nombre de barrages",maxsize = 2.5)
x11(14,10)
library(lattice)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1))) 
spplot(spdf,"cumul_denivele",main="cumul_denivele", pch=19,cex=0.1)




library(lattice)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1))) 

			  

CE=readShapeLines("EDAcommun/data/couche_shp/COURS_EAU123.shp")
#CE=readShapePoly("EDAcommun/data/couche_shp/FRANCE.SHP")
#CE_simp=readShapeLines("EDAcommun/data/bdcarthage/COURS_EAU.SHP")

###############
# barrages france
###############


bassin=4
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),header=TRUE)
if (!exists("noeudsxy"))noeudsxy=read.table(file=paste("EDAcommun/data/extraction_parametres/noeuds_parcouruXY.txt",sep=""),header=TRUE)
barxy=merge(bar,noeudsxy, by="Id_BDCARTHAGE" )
spdf=CrSpPtsDF(xlabel="X",ylabel="Y",data=barxy)
spdf@data=chnames(object=spdf@data,                  
        old_variable_name=c("nb_barrages","cumul_score","cumul_denivele","distance_mer.x"),          
        new_variable_name=c("nombre_de_barrages",
        "cumul_scores_franchissement",
        "cumul_denivel�s",
        "distances_mer"))
 #sinon correspondance partielle de nb en nb_barrages   
plot_SpPtsDF(         fond=CE,
                      xlim=NULL,
                      ylim=NULL,
                      donnee=spdf,  #un object de classe SpPtsDF
                      colonne="nombre_de_barrages",     # la colonne qu'on souhaite afficher
                      couleurs=c("green", "red"),
                      space="Lab",                   #c("rgb", "Lab")
                      probs=seq(0, 1, 0.1),
                      cex=0.4 ,
                      title="nombre de barrages",
                      text="source ONEMA"
                      )
for (i in c(1,2,3,5,6)){
bassin=i
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),header=TRUE)
barxy=merge(bar,noeudsxy, by="Id_BDCARTHAGE" )
spdf=CrSpPtsDF(xlabel="X",ylabel="Y",data=barxy)
addpointscut_SpPtsDF(donnee=spdf,  #un object de classe SpPtsDF
                  colonne="nb_barrages",                  
                  space="Lab",                   #c("rgb", "Lab")
                  probs=seq(0, 1, 0.1),
                  couleurs=c("green", "red"),
                  cex=0.4 )
}
bassin=4
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),header=TRUE)
if (!exists("noeudsxy"))noeudsxy=read.table(file=paste("EDAcommun/data/extraction_parametres/noeuds_parcouruXY.txt",sep=""),header=TRUE)
barxy=merge(bar,noeudsxy, by="Id_BDCARTHAGE" )
spdf=CrSpPtsDF(xlabel="X",ylabel="Y",data=barxy)
spdf@data=chnames(object=spdf@data,                  
        old_variable_name=c("nb_barrages","cumul_score","distance_mer.x"),          
        new_variable_name=c("nombre_de_barrages",
        "cumul_scores_franchissement",
         "distance_mer"))

sum(!is.na(spdf@data$"cumul_denivele"))        
plot_SpPtsDF(         fond=CE,
                      xlim = xlimloire,
                      ylim = ylimloire,
                      donnee=spdf,  #un object de classe SpPtsDF
                      colonne="cumul_denivele",     # la colonne qu'on souhaite afficher
                      couleurs=c("pink", "cadetblue2"),
                      space="Lab",                   #c("rgb", "Lab")
                      probs=seq(0, 1, 0.1),
                      cex=0.4 ,
                      title="denivel�s cumul�s des barrages",
                      text="source CEDRIC"
                      )
###############
# fin barrages france
###############


#a utiliser pour comparaison de donn�es
bbox(spdf)
spdf@data=chnames(object=spdf@data,                  
        old_variable_name=c("nb_barrages","cumul_score","cumul_denivele","distance_mer.x"),          
        new_variable_name=c("nombre_de_barrages",
        "cumul_scores_franchissement",
        "cumul_denivel�s",
        "distances_mer"))                       
lab1=round(mean(spdf@data$nombre_de_barrages,na.rm=TRUE))
lab2=round(mean(spdf@data$cumul_scores_franchissement,na.rm=TRUE))
lab3=round(mean(spdf@data$"cumul_denivel�s",na.rm=TRUE))
lab4=round(mean(spdf@data$distances_mer,na.rm=TRUE))
l1 = list("sp.text", c(170000,2200000), paste("Moy=",lab1),which = 1)
l2 = list("sp.text", c(150000,2300000), paste("Moy=",lab2),which = 2)
l3 = list("sp.text", c(150000,2300000), paste("Moy=",lab3),which = 3)
l4 = list("sp.text", c(150000,2300000), paste("Moy=",lab4),which = 4)
x11(20,15)
spplot(spdf,
        c("distances_mer",
        "distances_mer",
        "distances_mer",
        "distances_mer"),
        sp.layout=list(l1,l2,l3,l4))

