#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

# au pr�alable http://trac.eptb-vilaine.fr:8066/trac/wiki/CookBook%20pgsql2shp
# creation du shape pour ccm21.riversegment_france
# lancer dans dos :
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_France" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='France');"
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_Bretagne" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='Bretagne');"
source("EDACCM/init.r")
library(gam)
library(RColorBrewer)

#-----------------------------------#
# Reconstitution du jeu de donn�es
#-----------------------------------#
loadm<-function(file){
	path<-str_c(datawd,"/dataEDAccm/predictions/",file)
	load(path, envir = .GlobalEnv)
	
}
#load(file=paste(datawd,"/dataEDAccm/ccm.RData",sep=""))

loadm(file="ccmdata2.Rdata")
loadm("md198.RData")
loadm("mpa174.RData")
load(file=paste(datawd,"/dataEDAccm/ersok.RData",sep=""))
#-----------------------------------#
# Chargement des couches SIG
#-----------------------------------#
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
	install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
library(rgdal)
source("EDACCM/fonctions_graphiques.r")
library(lattice)
# trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
#						 cutoff.tails=0.1,
#						 alpha=1)))
#-----------------------------------#
# Graphique des pr�dictions sur les segments
#-----------------------------------#
# chargement de la couche de fond
# C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\uga2010" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 uga2010.uga

dsn=paste(shpwd,"uga2010.shp",sep="")
uga<-readOGR(dsn,"uga2010")
# C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_loire" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select wso1_id, the_geom from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='Loire');"

dsn=paste(shpwd,"riversegments_France.shp",sep="")
rb<-readOGR(dsn,"riversegments_France")
#dsn=paste(shpwd,"riversegments_Bretagne.shp",sep="")
#rb<-readOGR(dsn,"riversegments_Bretagne")
     # vecteur character des colonnes � int�ger
#  r�cup�ration des stations avec coordonn�es X et Y
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\stationsp2" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select st_x(the_geom) as X, st_y(the_geom) as Y, st_id,st_id, the_geom from bd_map.stationsp2;"

dsn=paste(shpwd,"stationsp2.shp",sep="")
st<-readOGR(dsn,"stationsp2")
# below I'm only joining with the model as there are less data
st1<-st[st@data$st_id%in%md198$data$st_id,]# redimensionnement de l'object spatial 
# pas facile � trouver celui l� m�me si au final il est intuitif il ne faut pas redimensionner st@data mais st...
# ajout des colonnes dans le spatial, je remplace toutes les colonnes car la requete ne va chercher que le wso1_id
st1<-mod_SpatialDataFrame(SpPDF=st1,  # un SpatialPolygonsDataFrame
		data= md198$data ,  #  un dataframe
		by.spdf="st_id",  # nom de la colonne contenant l'identifiant
		by.data="st_id",   # nom de la colone contenant l'indentifiant de data
		coldata=colnames(md198$data)[colnames(md198$data)!=c("st_id")])       # vecteur character des colonnes � int�ger
# str(st1@data)
# Cette fonction trace un graphique a partir d'un spatialpointdf
# et renvoit des points sur un fond de carte en cr�ant des couleurs
# progressives � partir d'un d�coupage en classe d'une variable continue
# elle utise aussi une colonne "nb" pour donner une taille de points diff�rents
x11(14,10)
mypalette<-rev(rainbow(6)) 
# carte pour la bretagne r�sidus
fun_graph_mod(xlabel="x", # par soucis de simplicit� je recr�e un spatial dataframe � partir des coord x et y
		ylabel="y", # on peut faire plus �l�gant mais c'est plus long...
		type="residus",  #residus, predites ou le nom de la variable � afficher, voir le switch ds la fonction
		mod=md198, # le mod�le test�
		colonne_commune="st_id", # colonne sur laquelle on veut calculer les nombres (ici station)
		data=st1@data, # donn�es � utiliser
		fond=c("uga","rb"), # cartes � mettre en fond
		couleurs=mypalette, 
		space="Lab",                   #c("rgb", "Lab")
		probs =seq(0,1,0.1),#seq(0, 1, 0.10) ou NULL # voir quantile
		#cutn=10,#avec cut on supprime les quantiles
		cex=0.2,
		title=iconv("R�sidus", "utf-8"),
		xlim =  bbox(rb)[1,], # valeur permettant de centrer la carte sur la bretagne
		ylim = bbox(rb)[2,], # id
		couleur_fond=c("grey95","lightcyan3"), # "paleturquoise3"# couleurs du fond
		surface=NULL,
		couleursurface=NULL,
		legendlab=NULL,
		legendval=NULL)                                 
# carte des valeurs pr�dites pour la bretagne
fun_graph_mod(xlabel="x",
		ylabel="y",
		type="predites",  #residus, predites ou le nom de la variable � afficher, voir le switch ds la fonction
		mod=md198,
		colonne_commune="st_id",
		data=st1@data,
		fond=c("uga","rb"),
		couleurs=mypalette,
		space="Lab",                   #c("rgb", "Lab")
		probs =seq(0,1,0.05),#seq(0, 1, 0.10) ou NULL # voir quantile
		#cutn=10,#avec cut on supprime les quantiles
		cex=0.2,
		title=iconv("Valeurs pr�dites", "utf-8"),
		xlim =  bbox(rb)[1,],
		ylim = bbox(rb)[2,],
		couleur_fond=c("grey95","lightcyan3"),
		surface=NULL,
		couleursurface=NULL,
		legendlab=NULL,
		legendval=NULL)  

# affichage d'une caract�ristique de p�che ex: efficacit�
fun_graph_mod(xlabel="x",
		ylabel="y",
		type="ot_efficacite",  #residus, predites ou le nom de la variable � afficher, voir le switch ds la fonction
		mod=md198,
		colonne_commune="st_id",
		data=st1@data,
		fond=c("uga","rb"),
		couleurs=mypalette,
		space="Lab",                   #c("rgb", "Lab")
		probs =seq(0,1,0.05),#seq(0, 1, 0.10) ou NULL # voir quantile
		#cutn=10,#avec cut on supprime les quantiles
		cex=0.2,
		title=iconv("Efficacit�s de p�che", "utf-8"),
		xlim =  bbox(rb)[1,],
		ylim = bbox(rb)[2,],
		couleur_fond=c("grey95","lightcyan3"),
		surface=NULL,
		couleursurface=NULL,
		legendlab=NULL,
		legendval=NULL) 


# prediction sur deux (presence abscence + presence)
st2<-st[st@data$st_id%in%mpa174$data$st_id,]
st2<-mod_SpatialDataFrame(SpPDF=st2,  # un SpatialPolygonsDataFrame
		data= mpa174$data ,  #  un dataframe
		by.spdf="st_id",  # nom de la colonne contenant l'identifiant
		by.data="st_id",   # nom de la colone contenant l'indentifiant de data
		coldata=colnames(mpa174$data)[colnames(mpa174$data)!=c("st_id")]) 



fun_graph_2mod(xlabel="x", # par soucis de simplicit� je recr�e un spatial dataframe � partir des coord x et y
		ylabel="y", # on peut faire plus �l�gant mais c'est plus long...
		type="residus",  #residus, predites ou le nom de la variable � afficher, voir le switch ds la fonction
		mod_d=md198, # le mod�le test� densit�
		mod_pa=mpa174,
		annee=2005,
		colonne_commune="st_id", # colonne sur laquelle on veut calculer les nombres (ici station)
		data=st2@data, # donn�es � utiliser
		fond=c("uga"), # cartes � mettre en fond # fond=("uga","rb")
		couleurs=mypalette, 
		space="Lab",                   #c("rgb", "Lab")
		probs =seq(0,1,0.1),#seq(0, 1, 0.10) ou NULL # voir quantile
		#cutn=10,#avec cut on supprime les quantiles
		cex=0.2,
		title=iconv("R�sidus mod�le de pr�sence abscence * densit�", "utf-8"),
		xlim =  bbox(rb)[1,], # valeur permettant de centrer la carte sur la bretagne
		ylim = bbox(rb)[2,], # id
		couleur_fond=c("grey95","lightcyan3"), # "paleturquoise3"# couleurs du fond
		surface=NULL,
		couleursurface=NULL,
		legendlab=NULL,
		legendval=NULL)