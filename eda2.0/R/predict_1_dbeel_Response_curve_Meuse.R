# Response curves for the delta-gamma model for BASQUE river basin
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################


p1<-funresponsecurve(var="annee",preprint=TRUE) # ,preprint=TRUE preprint to see the interim result
p2<-funresponsecurve(var="month",preprint=TRUE)#
p3<-funresponsecurve(var="distance_source",preprint=TRUE)#
p4<-funresponsecurve(var="distance_sea",preprint=TRUE)#
p5<-funresponsecurve("elev_mean",preprint=TRUE)#
p6<-funresponsecurve("cs_nbdams",preprint=TRUE)
p7<-funresponsecurve("p_urban",preprint=TRUE)#
p8<-funresponsecurve("p_agricultural",preprint=TRUE)#
p9<-funresponsecurve("p_up_urban",preprint=TRUE)
p10<-funresponsecurve("p_up_no_impact",preprint=TRUE)
graphics.off()
gamcheck(md)
gamcheck(mpa)
# ne lancer qu'� la fin puis relancer R.... Autrement les gam ne marchent plus

x11();print(p1)
x11();print(p2)
x11();print(p3)
x11();print(p4)
x11();print(p5)
x11();print(p6)
x11();print(p7)
x11();print(p8)
x11();print(p9)
x11();print(p10)




# the default below must correspond to c(model_var,add_model_var)
default_model_var<-apply(ccm[,model_var],2,median)
default_model_var["distance_sea"]<-0
default_model_var["elev_mean"]<-0
default_model_var["p_up_agricultural"]<-0
default_model_var["cs_nbdams"]<-0
default_model_var["cnbdams_dsea"]<-0
default_model_var["p_up_no_impact"]<-1
default_model_var["p_urban"]<-0
default_model_var["p_unimpact"]<-1
default_add_model_var<-c("annee"=2008,"month"=6)
datdef<-data.frame(nam=names(default_model_var),val=round(default_model_var,2))

#..........
# year
tapply(ers$annee,ers$annee,function(X) length(X))
don1<-funpredmodele(data=ccm,
		var="annee",
		model_var,
		add_model_var,
		niveautest=1984:2008,
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="year", # name for the year variable
		mpa,
		md
)

don1[,"year"]=as.factor(don1[,"year"])
don1[,"label"]<-NA
# for the label
don2<-don1[1,]
don2[1,1]<-quantile(don1[,1],0.80)
don2[1,2]<-1987
dat<-paste("predictions for :\n month=6 \n",paste(paste(datdef$nam,"=",datdef$val),collapse="\n"))
don2[1,"label"]<-dat
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(angle=90,hjust=1,col="grey10"),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=year))+ylab("densities model delta-gamma")
p<-p+geom_text(aes(y=mdxmpa,x=year,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))
theme_set(old_theme)

#..........
# month
unique(ers$month)

don1<-funpredmodele(data=ccm,
		var="month",
		model_var,
		add_model_var,
		niveautest=4:11,
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="month", # name for the variable chosen
		mpa,
		md
)
don1[,"month"]=as.factor(don1[,"month"])
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=0.5,vjust=1,col="grey10"),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=month))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n",paste(paste(datdef$nam,"=",datdef$val),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-50
don2[1,2]<-7
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=month,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1)) +scale_y_continuous(limits=c(0, 2)) 

#..........
# elev_mean
summary(ccm$elev_mean)

don1<-funpredmodele(data=ccm,
		var="elev_mean",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ccm$elev_mean),max(ccm$elev_mean),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="elev_mean", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=elev_mean))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="elev_mean"],"=",datdef$val[!datdef$nam=="elev_mean"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-0.5
don2[1,2]<-round(seq(min(ccm$elev_mean),max(ccm$elev_mean),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=elev_mean,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1)) 

#..........
# distance_sea
summary(ccm$distance_sea)

don1<-funpredmodele(data=ccm,
		var="distance_sea",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ccm$distance_sea),max(ccm$distance_sea),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="distance_sea", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=distance_sea))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="distance_sea"],"=",datdef$val[!datdef$nam=="distance_sea"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-0.3
don2[1,2]<-round(seq(min(ccm$distance_sea),max(ccm$distance_sea),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=distance_sea,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1)) 

#..........
# cs_nbdams
summary(ccm$cnbdams_dsea)

don1<-funpredmodele(data=ccm,
		var="cs_nbdams",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ccm$cs_nbdams),max(ccm$cs_nbdams),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="cs_nbdams", # name for the variable choosen
		mpa,
		md
)

x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=cs_nbdams))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="cs_nbdams"],"=",datdef$val[!datdef$nam=="cs_nbdams"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-0.5
don2[1,2]<-round(seq(min(ers$cs_nbdams),max(ers$cs_nbdams),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=cs_nbdams,label=label),size=4,col="grey10",hjust=0,data=don2)
subccm<-ccm[,colnames(don2)[-3]]
p + geom_point() +geom_line(aes(group=1))+geom_rug(data=ccm,aes(x=cs_nbdams),position='jitter')  

#..........
# cnbdams_dsea
summary(ccm$cnbdams_dsea)

don1<-funpredmodele(data=ccm,
		var="cnbdams_dsea",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$cs_score),max(ers$cs_score),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="cnbdams_dsea", # name for the variable choosen
		mpa,
		md
)

x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=cnbdams_dsea))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="cs_score"],"=",datdef$val[!datdef$nam=="cs_score"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-0.3
don2[1,2]<-round(seq(min(ers$cnbdams_dsea),max(ers$cnbdams_dsea),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=cnbdams_dsea,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))  

#..........
# p_urban
summary(ccm$p_urban)

don1<-funpredmodele(data=ccm,
		var="p_urban",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$p_urban),max(ers$p_urban),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="p_urban", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=p_urban))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="p_urban"],"=",datdef$val[!datdef$nam=="p_urban"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-45
don2[1,2]<-round(seq(min(ers$p_urban),max(ers$p_urban),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=p_urban,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))


#..........
# rain_mean
summary(ccm$rain_mean)

don1<-funpredmodele(data=ccm,
		var="rain_mean",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$rain_mean),max(ers$rain_mean),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="rain_mean", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=rain_mean))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="rain_mean"],"=",datdef$val[!datdef$nam=="rain_mean"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-45
don2[1,2]<-round(seq(min(ers$rain_mean),max(ers$rain_mean),length.out=25),2)[3]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=rain_mean,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))  +scale_y_continuous(limits=c(0, 60)) 



#..........
# relative_distance
summary(ers$relative_distance)

don1<-funpredmodele(data=ccm,
		var="relative_distance",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$relative_distance),max(ers$relative_distance),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="relative_distance", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=relative_distance))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="relative_distance"],"=",datdef$val[!datdef$nam=="relative_distance"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-45
don2[1,2]<-round(seq(min(ers$relative_distance),max(ers$relative_distance),length.out=25),2)[17]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=relative_distance,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))  +scale_y_continuous(limits=c(0, 60)) 



#..........
# p_up_agricultural
summary(ers$p_up_agricultural)

don1<-funpredmodele(data=ccm,
		var="p_up_agricultural",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$p_up_agricultural),max(ers$p_up_agricultural),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="p_up_agricultural", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=p_up_agricultural))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="p_up_agricultural"],"=",datdef$val[!datdef$nam=="p_up_agricultural"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-51
don2[1,2]<-round(seq(min(ers$p_up_agricultural),max(ers$p_up_agricultural),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=p_up_agricultural,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))  +scale_y_continuous(limits=c(0, 60)) 


#..........
# p_up_no_impact
summary(ers$p_up_no_impact)

don1<-funpredmodele(data=ccm,
		var="p_up_no_impact",
		model_var,
		add_model_var,
		niveautest=round(seq(min(ers$p_up_no_impact),max(ers$p_up_no_impact),length.out=25),2),
		default_model_var,
		default_add_model_var,
		newvar1="mdxmpa", # new name for the combined prediction
		newvar2="p_up_no_impact", # name for the variable choosen
		mpa,
		md
)
x11();
old_theme <- theme_update(
		axis.text.x = theme_text(hjust=1,col="grey10",angle=90),
		axis.text.y = theme_text(col="grey10",hjust=1)
)
p<-ggplot(don1,aes(y=mdxmpa,x=p_up_no_impact))+ylab("densities model delta-gamma")
dat<-paste("predictions for :\n year=2008 \n month=6 \n",paste(paste(datdef$nam[!datdef$nam=="p_up_no_impact"],"=",datdef$val[!datdef$nam=="p_up_no_impact"]),collapse="\n"))
don2<-don1[1,]
don2[1,1]<-48
don2[1,2]<-round(seq(min(ers$p_up_no_impact),max(ers$p_up_no_impact),length.out=25),2)[15]
don2[1,"label"]<-dat
p<-p+geom_text(aes(y=mdxmpa,x=p_up_no_impact,label=label),size=4,col="grey10",hjust=0,data=don2)
p + geom_point() +geom_line(aes(group=1))  +scale_y_continuous(limits=c(0, 60)) 
