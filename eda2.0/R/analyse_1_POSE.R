# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

analyse_rename<-function(data){
	data$uga[data$uga=="Bretagne"]<-"Brittany"
	data$uga[data$uga=="RhoneMediterranee"]<-"Rhone"
	return(data)
}

ccm<-analyse_rename(data=ccm)
ers_full<-analyse_rename(data=ers_full)
