# Nom fichier :        Traitement bda
# Adaptation � base_agglo 03/03/2017
# ici on peut squeezer une partie du programme pour acceder aux Rdata directement
choix=tk_select.list(c("oui","non"),preselect="non",multiple=FALSE
		,title="veux tu faire tourner le traitement?")
if  (choix=="oui"){
	
##############################################
#Programme
##############################################

#version de la base BDMAP manuel pour l'instant
	version$principal = 01/01/2016 #"23/06/2014"#"03/08/2011"
	
	
#extraction des annees
	sql_anneestaxon=fn_sql_readsql(con ="EDAcommun/sql/sql_uniqueannee_taxonanguille_bda.sql")
	req<-new("RequeteODBC")
	req@baseODBC<-baseODBCOracle
	req@sql=sql_anneestaxon
	req<-connect(req)
	les_annees=req@query
	choixannee=tk_select.list(les_annees$annee,preselect=les_annees$annee,multiple=TRUE,
			title="annee de peche")
# choix des m�thodes de prospection ces m�thodes n�cessite la table csp.codier
	req<-new("RequeteODBC")
	req@baseODBC<-baseODBCOracle
	req@sql=fn_sql_readsql(con ="EDAcommun/sql/sql_methodeprospection.sql")
	req=connect(req)
	methodeprospection=req@query
	save(methodeprospection, version,file=paste(datawd,"/dataEDAcommun/methodeprospection.Rdata",sep=""))
#load(file=paste(datawd,"/dataEDAcommun/methodeprospection.Rdata",sep=""))
	choixprospection=tk_select.list(as.character(methodeprospection$cd_libl),
			preselect=as.character(methodeprospection$cd_libl),
			multiple=TRUE,
			title="methode de prospection")
	op_cd_methodeprospection=methodeprospection$op_cd_methodeprospection[methodeprospection$cd_libl %in% choixprospection]
# choix des moyens de prospection 
	req<-new("RequeteODBC")
	req@baseODBC<-baseODBCOracle
	req@sql=fn_sql_readsql(con ="EDAcommun/sql/sql_moyenprospection.sql")
	req=connect(req)
	t_moyenprospection=req@query
	save(t_moyenprospection, version, version,file=paste(datawd,"/dataEDAcommun/t_moyenprospection.Rdata",sep=""))   
#load(file=paste(datawd,"/dataEDAcommun/t_moyenprospection.Rdata",sep=""))   	
	moyenprospection=tk_select.list(as.character(t_moyenprospection$cd_libl),
			preselect=as.character(t_moyenprospection$cd_libl),
			multiple=TRUE,
			title="moyens de prospection")
	op_cd_moyenprospection=t_moyenprospection$op_cd_moyenprospection[t_moyenprospection$cd_libl %in% moyenprospection]
# choix des objectifs de p�che 
	req<-new("RequeteODBC")
	req@baseODBC<-baseODBCOracle
	req@silent=FALSE
	req@sql=fn_sql_readsql(con ="EDAcommun/sql/sql_objectifpeche.sql")
	req=connect(req)
	t_objectifpeche=req@query
	save(t_objectifpeche, version,file=paste(datawd,"/dataEDAcommun/t_objectifpeche.Rdata",sep="")) 
#load(file=paste(datawd,"/dataEDAcommun/t_objectifpeche.Rdata",sep=""))
	objectifpeche=tk_select.list(as.character(t_objectifpeche$cd_libl),
			preselect=as.character(t_objectifpeche$cd_libl),
			multiple=TRUE,
			title="objectif de la p�che")
	op_cd_objectifpeche= t_objectifpeche$op_cd_objectifpeche[t_objectifpeche$cd_libl  %in% objectifpeche]
	
# requete pour aller chercher les densit�s
	
# d�claration des clauses where
	#################
	where1=paste("where op_cd_moyenprospection in",  vector_to_listsql(op_cd_moyenprospection))
	where2=""#"and op_cd_objectifpeche is NULL" #paste("and op_cd_objectifpeche in",  vector_to_listsql(op_cd_objectifpeche))
	where3=paste("and op_cd_methodeprospection in",  vector_to_listsql(op_cd_methodeprospection))
	where4=paste("where annee in",vector_to_listsql(choixannee))
	where5= "and cd_libl='G'"
	where6="and ta_code='ANG'"
	######################
	req<-new("RequeteODBC")
	req@baseODBC<-baseODBCOracle
	sql_densite<-fn_sql_readsql(con ="EDAcommun/sql/sql_densite.sql")
	sql_densite<-paste("select * from (",sql_densite,where1,where2,where3,") as subrequete",where4)
	req@sql<-sql_densite
	req<-connect(req)
	t_densite=req@query
	rm(req)
	cat(paste("nombre de densit�s = ", nrow(t_densite), "\n"))
#tableau des densit�s au premier passage	
	sql_densite_premierpassage=fn_sql_readsql(con ="EDAcommun/sql/sql_densite_premierpassage.sql")
	sql_densite_premierpassage=paste("select * from (",sql_densite_premierpassage,where1,where2,where3,") as subrequete",where4)
	reqp<-new("RequeteODBC")
	reqp@baseODBC<-baseODBCOracle
	reqp@sql<-sql_densite_premierpassage
	reqp<-connect(reqp)
	t_densite_premierpassage=reqp@query
	cat(paste("nombre de densit�s_premierpassage = ", nrow(t_densite_premierpassage), "\n"))      
	rm(reqp)
	index=match(t_densite_premierpassage$op_numero,t_densite$op_numero) #  dans quel ordre apparait le premier dans le second
	t_densite$pa_effectif=t_densite_premierpassage[index,"pa_effectif"]
	op_num=as.character(t_densite$op_numero)   # vecteur des ope s�lectionn�es 
	save(t_densite, version,file=paste(datawd,"/dataEDAcommun/t_densite.Rdata",sep=""))
# load(file=paste(datawd,"EDAcommun/data/t_densite.Rdata",sep="")
#r�cup�ration des lots
# sur la bse nationale il y a trop d'op�rations pour pouvoir
#charger un vecteur, la solution est de faire une requ�te r�duite
#=> essai de sql_lots_1
	sql_lots=fn_sql_readsql(con ="EDAcommun/sql/sql_lots_1.sql",warn=FALSE)
	
	
	reql<-new("RequeteODBC")
	reql@baseODBC<-baseODBCOracle
	reql@sql<-paste(sql_lots,where1,where2,where3,where5,where6,") as subquery",where4)     
	lots=connect(reql)@query   
	lots$range=lots$lo_taillemax-lots$lo_taillemin
	cat(paste("nombre de lots = ", nrow(lots), "\n"))
	save(lots, version,file=paste(datawd,"/dataEDAcommun/lots.Rdata",sep=""))
	rm(reql)
#load(file=paste(datawd,"/dataEDAcommun/lots.Rdata")
# requ�te  du tableau des classes de taille
#Les lots G ont �t� rer�parti dans le tableau des classes de taille si cette r�partition
#a �t� jug�e possible, une info sur cette rer�partion est dans une table syst�me
#un peu plus loin nous resserrons les crit�res sur cette rer�partion
#Les lots G sont r�partis au pro rata des classes de taille (r�gle de 3)
#puis pour celles qui restent autour du mode
	
	sql_classtail0<-fn_sql_readsql(con ="EDAcommun/sql/sql_classetaille.sql")
	reqt<-new("RequeteODBC")
	reqt@baseODBC<-baseODBCOracle
	reqt@sql<-paste(sql_classtail0,where1,where2,where3,where6,") as subquery",where4)
	classtail<-connect(reqt)@query
	rm(reqt)
	cat(paste("nombre de classes de taille = ", nrow(classtail), "\n")) 
	# les anguilles les plus grandes
	sort(classtail$ct_taille)[(length(classtail$ct_taille)-500):length(classtail$ct_taille)]
	# reclassement de quelques grandes
	classtail$ct_taille[classtail$ct_taille>=1200]=round(classtail$ct_taille[classtail$ct_taille>=1200]/100)*10
	
	save(classtail, version,file=paste(datawd,"/dataEDAcommun/classtail.Rdata",sep=""))   
# load(file="EDAcommun/data/classtail.Rdata")    
# cr�ation des nouvelles colonnes dans t_densite
	t_densite$ot_effectif[is.na(t_densite$ot_effectif)]=0
	classtail$class=cut(classtail$ct_taille,c(0,150,300,450,600,750,900,1200),right=FALSE)
	sumclasstail=tapply(X=classtail$ct_effectif,INDEX=list(classtail$op_numero,classtail$class),function(X) sum(X,na.rm=TRUE))
	sumclasstail[is.na(sumclasstail)]<-0
	sumclasstail<-as.data.frame(sumclasstail)
	colnames(sumclasstail)<-c('150','150_300','300_450','450_600','600_750','750_900','900')
	sumclasstail$nbtotalclasstail=rowSums(sumclasstail)
	sumclasstail$op_numero<-rownames(sumclasstail)	
	t_densite_final<-merge(t_densite,sumclasstail,by="op_numero",all.x=TRUE)
	t_densite_final$test_oteff_ct<-t_densite_final$ot_effectif==t_densite_final$nbtotalclasstail 
#	as.character(t_densite_final[92,"op_numero"])#"10090000211"
#	sumclasstail["10090000211",]
#	sum(classtail[classtail$op_numero=="10090000211","ct_effectif"]) #19
#	t_densite[t_densite$op_numero=="10090000211",]
	source("EDAcommun/prg/peche/funclasstail.r")
	t_densite_final=funclasstail(t_densite_final,limite=0.5,rangelotg=150) 	
	save(t_densite_final, version,file=paste(datawd,"/dataEDAcommun/t_densite_final.Rdata",sep=""))
#load(paste(datawd,"/dataEDAcommun/t_densite_final.Rdata",sep=""))
	
	
	##A PARTIR DE CETTE LIGNE t_densite a �t� remplac� par t_densite_final
	##A VERIFIER si c'est OK
	
	## remplacement des NA par des z�ro
	t_densite_final[t_densite_final$ot_effectif==0,c("ot_effectifestim","pa_effectif",
					"150","150_300","300_450","450_600","600_750","750_900","900")]=0
	##calcul des densit�s (en divisant par la surface des stations 
#t_densite[,c("ot_effectif","ot_effectifESTIM","PA_EFFECTIF",
#             "150","150_300","300_450","450_600","600_750","750_900","900"]= 
#    t_densite[,c("ot_effectif","ot_effectifESTIM","PA_EFFECTIF",
#    "150","150_300","300_450","450_600","600_750","750_900","900")]/t_densite$OP_SURFACEECHANTILLON                    
# SAUVEGARDE INTERMEDIAIRE APRES LE LONG TRAITEMENT   
   # newpath<-file.path(paste(datawd,"/dataEDAcommun/t_densite_final_int",strftime(as.POSIXlt(Sys.time()),"%d-%B-%Y") ,".Rdata",sep=""))      
   # fileName<-savefile(path=newpath)
   # save(t_densite_final,file=fileName)
#chargement des moyens de prospection
	load(paste(datawd,"/dataEDAcommun/t_moyenprospection.Rdata",sep=""))
	t_moyenprospection$moyen_prospection=t_moyenprospection$cd_libl
	t_densite_final=merge(t_densite_final,t_moyenprospection,by="op_cd_moyenprospection",all.x=TRUE,all.y=FALSE)
	colnames(t_densite_final)[ncol(t_densite_final)]<-"moyenprospection"
#chargement des m�thodes de prospection
	load(paste(datawd,"/dataEDAcommun/methodeprospection.Rdata",sep=""))
	methodeprospection$methode_prospection=methodeprospection$cd_libl
	t_densite_final=merge(t_densite_final,methodeprospection,by="op_cd_methodeprospection",all.x=TRUE,all.y=FALSE)
	colnames(t_densite_final)[ncol(t_densite_final)]<-"methodeprospection"
#chargement des objectifs de p�che
	load(paste(datawd,"/dataEDAcommun/t_objectifpeche.Rdata",sep=""))
	t_densite_final=merge(t_densite_final,t_objectifpeche ,by.x="op_cd_objectifpeche",by.y="op_cd_objectifpeche",all.x=TRUE,all.y=FALSE)
	#colnames(t_densite_final)[ncol(t_densite_final)+1]<-"objectifpeche"
t_densite_final$objectifpeche=NULL
#pour s'assurer que le code CSP est bien en character
	stopifnot(class(t_densite_final$st_codecsp)=="character")
	
	#permet de remplacer les valeurs manquantes d'une colonne par la m�diane
	
funremplace_val_manq_autres_stations=function(data,nomcolonnearemplacer,nomcolonnestation){
		# calcul des m�dianes sur toutes ope des stations
		med=tapply(data[,nomcolonnearemplacer],data[,nomcolonnestation],median,na.rm=TRUE)
		med=med[!is.na(med)]
		# recherche de la correpondance entre les num�ros de station
		# des lignes sans valeur et les medianes
		index=match(as.numeric(as.character(data[,nomcolonnestation]))[is.na(data[,nomcolonnearemplacer])],as.numeric(names(med)))
		# quelles donn�es sont remplacables
		index2=!is.na(index)
		print(paste("nombre de valeurs remplac�es =", sum(!is.na(index))," sur",length(index)))
		data[,nomcolonnearemplacer][is.na(data[,nomcolonnearemplacer])][index2]<-med[index[index2]]
		return( data)
	}
	
#remplace les surfaces manquantes
t_densite_final<-funremplace_val_manq_autres_stations(t_densite_final,"op_surfaceechantillon", "st_codecsp")
# [1] "nombre de valeurs remplac�es = 473  sur 836"
#cr�e un champ effort pour relativiser le nb de poisson pris
# l'effort de p�che pour les p�ches compl�tes est la surface de la station
# p�ches completes (843)
t_densite_final[t_densite_final$op_cd_methodeprospection==843,"effort"]=t_densite_final[t_densite_final$op_cd_methodeprospection==843,"op_surfaceechantillon"]
# l'effort de p�che pour les p�ches partielles sur berges correspond � deux fois la longueur #0,5m de la berge + 1,5m d'efficacit�
t_densite_final[t_densite_final$op_cd_methodeprospection==845,"effort"]=t_densite_final[t_densite_final$op_cd_methodeprospection==845,"op_longueur"]*2 
# l'effort de p�che pour les p�ches stratifi�es par points correspond � la surface rapport�e sur la station # (Belliard et al., 2007)	
t_densite_final[t_densite_final$op_cd_methodeprospection==1611,"effort"]=t_densite_final[t_densite_final$op_cd_methodeprospection==1611 ,"op_surfaceechantillon"] # (Belliard et al., 2007)
#NB : par points (grand milieu) : surface du point = 12.5 m�	
	
	}	
choixsauv=c("oui","non")
source("EDAcommun/prg/init/sauve.r")
choixsauv=select.list(choixsauv,preselect="non",multiple=FALSE,
		title="veux tu sauvegarder le r�sultat de la requ�te")
if (choixsauv=="oui"){
# Changements de t_densite_final
	t_densite_final<-t_densite_final[,-match(c(
	"op_longprospdroit",
	"op_longprospgauche",
	"op_longsdt",
	"op_largprospdroit",
	"op_largprospgauche",
	"op_cs_largeurlitmineur",
	"op_nbrzones",
	"op_tempspeche",
	"op_cd_typeinventaire",
	"op_cd_typezone",
	"op_cs_profmoyen",
	"op_largsdt",
	"st_penteign",
	"st_sbv",
	"st_distancemer",
	"st_altitude",
	"st_distancesource",
	"ta_code"),colnames(t_densite_final))]
	t_densite_final[,">750"]<-rowSums(t_densite_final[,c("750_900","900")],na.rm=TRUE)
	t_densite_final<-t_densite_final[,-match(c("750_900","900"),colnames(t_densite_final))]
	t_densite_final<-t_densite_final[,-match(c("test_oteff_ct","test_lotG","cd_libl.x","cd_libl.y","cd_libl"),colnames(t_densite_final))]
	save(t_densite_final, file=str_c(datawd,"/dataEDAcommun/t_densite_final23-juin-2014.Rdata"))
} else {
	fichiercharge=paste(datawd,"/dataEDAcommun/t_densiteAdefinir.Rdata",sep="")
	fileName=loadfile(path=fichiercharge)
	load(file=fileName)
}
##J'AI CHANGE LE CHEMIN DANS fichiercharge (ci-dessus) (avant c'�tait :fichiercharge=EDAcommun/data/t_densiteAdefinir.Rdata
## C> OK, EN FAIT LE FICHIER DOIT DE CREER DANS LE REPERTOIRE ET S'ECRIRE t_densite_final01/02/2010 par exemple, une fois qu'on aura fait le travail 
## on pointera vers ce fichier

# nettoyage des objects en m�moire
rm(fn_sql_0,fn_sql_readsql,vector_to_listsql.r)
rm(sqlstation,station,t_densite_premierpassage,les_annees,choix_anne)
rm(choix)


##POUR LA SAUVEGARDE DES DONNEES EST-CE BIEN AUSSI t_densite_final que tu souhaites sauvegarder ? =>OUI
write.table(t_densite_final,file=paste(datawd,"/dataEDAcommun/densite.txt",sep=""),sep="\t",row.names=FALSE,col.names=TRUE)
