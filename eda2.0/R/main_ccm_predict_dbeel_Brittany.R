# Using the calss 
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################


#  Brittany - Rhone - Western
###############################################################################
# uga2010
# C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\uga2010" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 uga2010.uga
# stationsp2
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\stationsp2" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select st_x(the_geom) as X, st_y(the_geom) as Y, st_id,st_codecsp, the_geom from bd_map.stationsp2;"
library(sp)
source("EDACCM/init.r")
#cutn dans fonctions_graphiques
#si on prend pas cutn � -0.1 on a pas les valeurs 0 affich�es visiblement car cutn : ]0,..[

secteur<-select.list(c("Brittany","Rhone","Basque","Western","Anglian"),multiple=FALSE)

library(gam)
library(RColorBrewer)
# C:\"Program Files"\PostgreSQL\9.0\bin\pgsql2shp -f "F:\IAV\eda\oria\district" -h 192.168.1.104 -p 5432 -u postgres -P petromyzon*** -g the_geom  eda2 "select * from european_wise2008.rbd_f1v3 where mscd_rbd in ('ES016','ES015')"
# C:\"Program Files"\PostgreSQL\9.0\bin\pgsql2shp -f "E:\workspace\EDAdata\dataEDAccm\shape\stationbasque" -h 192.168.1.104 -p 5432 -u postgres -P petromyzon*** -g the_geom -r -k  eda2 oria.oria_ccm_500
model_mod<-new("BaseEdamodel",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="model_mod",
		prkey="mod_id",
		zonegeo=secteur,
		namemd=str_c(secteur,"md"),
		namempa=str_c(secteur,"mpa"),
		annee=2008,
		mapfond=paste("district_",secteur,sep=""),
		mapstation="stationbasque",
		prkeystation="st_id")
if (secteur=="Basque"){
	my_xlim=c(3290000,3300000)
	my_ylim=c(2280000,2390000)
} else {
	myxlim=bbox(model_mod@datasp)[1,]
	myylim=bbox(model_mod@datasp)[2,]
} 
model_mod<-loadrdata(model_mod)
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method
x11(14,10)

map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Residuals", "utf-8"),
		xlim=my_xlim,
		ylim=my_ylim)
# ci dessous essai de l'option group = "cte"
map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals for 2008", "utf-8"),
		group="cte",
		xlim=my_xlim,
		ylim=my_ylim)
map(	object=model_mod,
		type="predites",    #residus, predites
		couleurs=c("deepskyblue","red"),
		probs=NULL,
		cutn=c(0,0.1,0.5,1,5,max(model_mod@pred,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("EDA predictions for the full model", "utf-8"),
		xlim=my_xlim,
		ylim=my_ylim)
map(	object=model_mod,
		type="d",    #residus, predites
		couleurs=c("deepskyblue","red"),
		probs=NULL,
		cutn=c(0,0.1,0.5,1,5,max(model_mod@mpa$data$d,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("observed densities>0", "utf-8"),
		xlim=my_xlim,
		ylim=my_ylim)

map(	object=model_mod,
		type="totalnumber",    #residus, predites
		couleurs=c("deepskyblue","red"),
		probs=NULL,
		cutn=c(-0.1,0.1,0.5,1,5,max(model_mod@mpa$data$totalnumber,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Observed numbers per electrofishing", "utf-8"),
		xlim=my_xlim,
		ylim=my_ylim)
# size according to density 
#Observed densities per electrofishing
#jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Observed_densities_per_electrofishing_",secteur,".jpeg",sep=""), quality=100)
map(	object=model_mod,
		type="d",    #residus, predites
		couleurs=c("deepskyblue","red"),
		probs=NULL,
		cutn=c(-0.1,0.1,0.5,1,5,max(model_mod@mpa$data$d,na.rm=TRUE)),         # ignore les quantiles
		cex=1.2,
		title=iconv("Observed densities per electrofishing", "utf-8"),
		group="size",
		xlim=my_xlim,
		ylim=my_ylim) 
#dev.off()
#str(model_mod)



# Pour renvoyer les pr�dicitons par ann�e

model_mod<-new("BaseEdamodel",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="model_mod",
		prkey="mod_id",
		zonegeo="Basque",
		namemd=str_c(secteur,"md"),
		namempa=str_c(secteur,"mpa"),
		annee=numeric(),# pour renvoyer un vecteur de longueur z�ro on ne peut pas passer NULL dans la classe
		mapfond="district",
		mapstation="stationbasque",
		prkeystation="st_id")
model_mod<-loadrdata(model_mod)
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method

map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals calculated against prediction per year", "utf-8"),
		group="cte",
		xlim=my_xlim,
		ylim=my_ylim)
# All years decale for residuals
map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals calculated for all years, jitter arround the point", "utf-8"),
		group="decale", # this option will use a jitter to show results for all year, with 5 or 7 rows of data
		xlim=my_xlim,
		ylim=my_ylim) 
# All years decale with density
map(	object=model_mod,
		type="d",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(-0.1,0.1,0.5,1,5,max(model_mod@mpa$data$d,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean density observed for all years, jitter arround the point", "utf-8"),
		group="decale", # this option will use a jitter to show results for all year, with 5 or 7 rows of data
		xlim=my_xlim,
		ylim=my_ylim) 
###################################################################################
# # PREDICTIONS SPATIALES POUR  MPA seulement
##################################################################################
model_mod<-new("BaseEdamodel",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="model_mod",
		prkey="mod_id",
		zonegeo="Basque",
		namemd=character(), # on ne veut pas de md
		namempa=str_c(secteur,"mpa"),
		annee=numeric(),
		mapfond="district",
		mapstation="stationbasque",
		prkeystation="st_id")
model_mod<-loadrdata(model_mod)
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) #
x11(14,10)
jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Mean_residuals_per_stations_mpa_",secteur,".jpeg",sep=""), width = 480, height = 480,quality=100)
map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-1,-0.5,0.5,1,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals per station : presence absence model", "utf-8"),
		group="size",
		xlim=my_xlim,
		ylim=my_ylim)
dev.off()

#Residuals per year: presence absence model
#lancer les lignes 183 � 192 avant la suite
x11(14,10)
#jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Residuals_per_year_mpa_",secteur,".jpeg",sep=""), width = 480, height = 480,quality=100)
map(object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-1,-0.5,0.5,1,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.5,
		title=iconv("Residuals per year: presence-absence model", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim
) 
#dev.off()

# PREDICTED VALUES FOR BEFORE 1998
model_mod<-loadrdata(model_mod)
model_mod@mpa$data<-subset(model_mod@mpa$data,model_mod@mpa$data$annee<1998)
model_mod@md$data<-subset(model_mod@md$data,model_mod@md$data$annee<1998)
model_mod<-predict(model_mod)# prediction par annee
table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method

#jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Predicted_values_mpa_inf1998_",secteur,".jpeg",sep=""), width = 480, height = 480,quality=100)
map(object=model_mod,
		type="predites",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(0,0.6,1),         # ignore les quantiles
		cex=0.9,
		title=iconv("Predicted value: presence-absence model year<1998", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload"
) 
#dev.off()

#######################################
# PREDICTED VALUES FOR AFTER 1998
#######################################
model_mod<-loadrdata(model_mod)
model_mod@mpa$data<-subset(model_mod@mpa$data,model_mod@mpa$data$annee>=1998)
model_mod@md$data<-subset(model_mod@md$data,model_mod@md$data$annee>=1998)
model_mod<-predict(model_mod)# prediction par annee
table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method

#jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Predicted_values_mpa_sup1998_",secteur,".jpeg",sep=""), width = 480, height = 480,quality=100)
map(object=model_mod,
		type="predites",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(0,0.6,1),         # ignore les quantiles
		cex=0.9,
		title=iconv("Predicted value: presence-absence model year>=1998", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload"
) 


###################################################################################
# # PREDICTIONS SPATIALES POUR  MD seulement
# TTODO finir le debuggage
##################################################################################
model_mod<-new("BaseEdamodel",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="model_mod",
		prkey="mod_id",
		zonegeo="Basque",
		namemd=str_c(secteur,"md"),
		namempa=character(), # on ne veut pas de mpa,
		annee=numeric(),
		mapfond="district",
		mapstation="stationbasque",
		prkeystation="st_id")
model_mod<-loadrdata(model_mod)
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) #

#Mean residuals per station: density model
jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Mean_residuals_per_station_md_",secteur,".jpeg",sep=""), width = 480, height = 480,quality=100)
map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-1,-0.5,0.5,1,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals per station: density model", "utf-8"),
		group="size",
		xlim=my_xlim,
		ylim=my_ylim)
map(object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-1,-0.5,0.5,1,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.6,
		title=iconv("Residuals per year: density model", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim
) 
# RESIDUALS FOR BEFORE 1998
model_mod<-loadrdata(model_mod)
index<-model_mod@md$data$annee<2000
model_mod@md$data<-subset(model_mod@md$data,index)
model_mod<-predict(model_mod)# prediction par annee
table (model_mod@md$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method
model_mod@redid<-model_mod@resid[index]
map(object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-1,-0.5,0.5,1,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.9,
		title=iconv("Residuals year<1998", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload"
) 
#######################################
# PREDICTED VALUES FOR AFTER 1998
#######################################
model_mod<-loadrdata(model_mod)
model_mod@mpa$data<-subset(model_mod@mpa$data,model_mod@mpa$data$annee>=1998)
model_mod@md$data<-subset(model_mod@md$data,model_mod@md$data$annee>=1998)
model_mod<-predict(model_mod)# prediction par annee
table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method

#Predicted value: presence-absence model
map(object=model_mod,
		type="predites",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(0,0.6,1),         # ignore les quantiles
		cex=0.9,
		title=iconv("Predicted value: presence-absence model year>=1998", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload"
) 
###################################################################################
# BELOW WE TRY A SUBSET OF YEARS MANUALLY, OPTION load_opt="noload" IS NECESSARY
##################################################################################
model_mod<-loadrdata(model_mod)

table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))# 26
model_mod@mpa$data<-subset(model_mod@mpa$data,model_mod@mpa$data$annee<1995)
model_mod@md$data<-subset(model_mod@md$data,model_mod@md$data$annee<1995)
table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method
map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals calculated against prediction per year,year 1981->1994", "utf-8"),
		group="cte",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload") # default is load but there we do not want to load again to use the reduced dataset

map(object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.5,
		title=iconv("Residuals per year", "utf-8"),
		group="decale",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload") # default is load but there we do not want to load again to use the reduced dataset
model_mod<-loadrdata(model_mod)
model_mod@mpa$data<-subset(model_mod@mpa$data,model_mod@mpa$data$annee>=1995)
model_mod@md$data<-subset(model_mod@md$data,model_mod@md$data$annee>=1995)
model_mod<-predict(model_mod)# prediction par annee
table (model_mod@mpa$data$annee)
length(table (model_mod@mpa$data$annee))#11 now
model_mod<-loadshp(model_mod)
# debug object<-model_mod ;object<-model_mod
model_mod<-predict(model_mod) # not necessary to run is called by map and resid
model_mod<-resid(model_mod) # runs also the previous method

map(	object=model_mod,
		type="residus",    #residus, predites
		couleurs=c("deepskyblue","forestgreen","azure4","deeppink","red"),
		probs=NULL,
		cutn=c(min(model_mod@resid,na.rm=TRUE),-5,-0.5,0.5,5,max(model_mod@resid,na.rm=TRUE)),         # ignore les quantiles
		cex=0.8,
		title=iconv("Mean residuals calculated against prediction per year,year >=1995", "utf-8"),
		group="cte",
		xlim=my_xlim,
		ylim=my_ylim,
		load_opt="noload") # default is load but there we do not want to load again to use the reduced dataset



#Distance de Cook
x11();plot(cooks.distance(model_mod@md),type="h",ylab="Distance de Cook")
x11();plot(cooks.distance(model_mod@mpa),type="h",ylab="Distance de Cook")


