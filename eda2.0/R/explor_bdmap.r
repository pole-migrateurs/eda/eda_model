# Nom fichier :        explor_BDmap
# Projet :             eda/EDAcommun/prg/peche    
# Organisme :          IAV
# Auteur :             C�dric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :   06/02/2007 10:58:37
# Compatibilite :      R 2.4.0
# Etat :               OK
# Description          fonction pour r�cup�rer les tables et diff�rents
#                       scripts d'exploration des tables
#**********************************************************************


# la fonction essaye d'�tablir une connection
# revoit une erreur en l'absence de connection
# puis demande les tables et renvoit une erreur en cas de pb de requete


fn_sql_liste_tables<-function(baseODBC, uid,pwd){
     
        e=expression(channel <-odbcConnect(baseODBC,
                                        uid = uid,
                                        pwd = pwd,
                                        case = "tolower",
                                        believeNRows = FALSE))
        print(paste("Try to connect ",baseODBC))
        # renvoit du resultat d'un try catch expression dans
        #l'ojet Connexion courante, soit un vecteur caract�re
        connexionCourante<-tryCatch(eval(e),
                        error=paste("unable to connect ",baseODBC))
        if ((class(connexionCourante)=="RODBC")[1]) print("Connection successfull") else
        print(connexionCourante)
        print(paste("Try query"))
        e=expression(query<-sqlTables(channel,errors=TRUE))
        resultatRequete<-tryCatch(eval(e),error = function(e) e,
                finally=odbcClose(channel))
        # teste le resultat, si il y a erreur
        #classe avec plusieurs variables erreur , condition..
        # d'ou le [1]
        #sinon classe data.frame
        if ((class(resultatRequete)=="data.frame")[1]) print("query ok") else
        print(resultatRequete)
        #print(resultatRequete)
        return("listetable"=query)
        }
        
# la fonction essaye d'�tablir une connection
# revoit une erreur en l'absence de connection
# puis d renvoit le type de donn�es des tables et une erreur en cas de pb de requete
       

# exploration de la base
# pour r�cup�rer la liste des tables
#source("EDAcommun/prg/peche/fn_sql_liste_tables.R")
listetable=fn_sql_liste_tables(baseODBC=baseODBC,uid=oracleid,pwd=oraclepwd)
listetable=listetable[listetable$TABLE_SCHEM=="BDMAP",]

# code g�n�rique
sql="SELECT EH_ID, EH_CODEGENERIQUE, EH_NOMUSUELCSP,EH_NOMENTITE FROM  BDMAP.ENTITEHYDRO"
source("EDAcommun/prg/peche/fn_sql_0.R")
codegenerique=fn_sql_0(
      sql,
      baseODBC=baseODBC,
      uid=oracleid,
      pwd=oraclepwd)[[1]]
edit(codegenerique)
save(codegenerique,file="EDAcommun/data/codegenerique.Rdata") 
# Agence => doit correspondre aux deux premi�ers lettres de la station
sql="SELECT * FROM  BDMAP.AGENCEEAU"
agence=fn_sql_0(
      sql,
      baseODBC="mapopale1",
      uid=oracleid,
      pwd=oraclepwd)[[1]]
agence
save(agence,file="EDAcommun/data/agence.Rdata")
# d�partements
sql="SELECT DP_ID ,DP_CODE, DP_NOM FROM  BDMAP.DEPARTEMENT"
dpt=fn_sql_0(
      sql,
      baseODBC="mapopale1",
      uid=oracleid,
      pwd=oraclepwd)[[1]]
#edit(dpt)
save(dpt,file="EDAcommun/data/dpt.Rdata")
#station
#sql=" SELECT * FROM  BDMAP.STATION WHERE ROWNUM < 100"
sql=" SELECT * FROM  BDMAP.STATION "
station=fn_sql_0(
      sql,
      baseODBC="mapopale1",
      uid=oracleid,
      pwd=oraclepwd)[[1]]
dim(station)      
write.csv(station, quote=FALSE,file = "EDAcommun/data/stationsBDMAP.csv")


# codier
sql=" SELECT * FROM  CSP.CODIER "
codier=fn_sql_0(
      sql,
      baseODBC="mapopale1",
      uid=oracleid,
      pwd=oraclepwd)[[1]]

# correspondance code du codier
      
sql=" select unique cd_libl, cd_code, op_cd_methodeprospection from operation inner join csp.codier on cd_id=op_cd_methodeprospection order by cd_code; "
codier2=fn_sql_0(
      sql,
      baseODBC="mapopale1",
      uid=oracleid,
      pwd=oraclepwd)[[1]]