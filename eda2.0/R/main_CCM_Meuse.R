# file Main Ireland.R
# main lauching file for EDACCM in the Meuse
# Author: cedric & Laura
# Author : 
###############################################################################

# Otherwise it won't work.
source("EDACCM/init.r")
#require(stacomiR)
#assign("showmerequest",1,envir_stacomi) 
#########################"
# chargement des donn�es###loading data
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Meuse")
ccm<-loaddb(ccm)# 888 lines
summary(ccm)



#########################"
# calculation of distance to the sea
############################
ccm1<-distance_sea_fromnodes(ccm)
# C14 In my case I don't want to drop the content of my column 
import_column(ccm1,newcolumn="cum_len_sea",newcolumntype="numeric",display=TRUE)  # fait
#########################"
# Calculation dams
############################
roeccm<-new("BaseEdaCCMriversegmentsROE",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse",
		joinschema="dbeel",
		jointable="obstacles_ccm_500",
		traveled_segments=vector())
roeccm<-loaddb(roeccm)
roeccm<-cumulated_dam_impact(roeccm)
col=colnames(roeccm@dataroe)[ colnames(roeccm@dataroe)!="wso1_id"]
stopifnot(all(roeccm@data[,"wso1_id"]==roeccm@dataroe[,"wso1_id"]))
roeccm@data[,col]<-roeccm@dataroe[,col]
for (i in c(3,5)){
	import_column(roeccm,col[i],newcolumntype="numeric",display=TRUE,askbeforeforefill=FALSE) # fait
} 

## OK this one is done ##
#############################
# Calculation of % clc
############################
##had to change the gid reference to wso1_id and western to Meuse
clc<-new("BaseEdaCCMriversegmentsclc",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse",
		joinschema="clc",
		jointable="surf_area_meuse_final",
		traveled_segments=vector())
clc<-loaddb(clc,silent=FALSE)
#data from the clc database loaded: 3022 lines
save(clc,file="C:/Users/cedric.briand/Desktop/clc.Rdata")
upstream_area(clc,silent=TRUE,from=1,to=3022)
#upstream_area(clc,silent=TRUE,from=10001,to=20000)
#upstream_area(clc,silent=TRUE,from=20001,to=30000)

#not sure which of these worked - whetehr the number refers to the row number of the wso1id number. 
# this is the number of lines you should have less than 10000
###done to here 20th April with Celine

object<-clc
###############################################################################
# R�int�gration des donn�es clc au niveau du primary catchment
##################################################################################
verbose<-FALSE
clc<-new("BaseEdaCCMriversegmentsclc",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse",
		joinschema="clc",
		jointable="surf_area_meuse_final",
		traveled_segments=vector())
clc<-loaddb(clc,silent=FALSE)

summary(clc)

##data from the clc database loaded: 3022 lines
##data from the riversegments loaded: 3022 lines

clc<-pass_joined_column(clc,newcolnames=c("catchment_area",
				"artificial_surfaces_11_13",
				"artificial_vegetated_14",
				"arable_land_21",
				"permanent_crops_22",
				"pastures_23",
				"heterogeneous_agricultural_24",
				"forest_31",
				"natural_32_33",
				"wetlands_4",
				"inland_waterbodies_51",
				"marine_water_52"))
the_col=c("catchment_area",
		"artificial_surfaces_11_13",
		"artificial_vegetated_14",
		"arable_land_21",
		"permanent_crops_22",
		"pastures_23",
		"heterogeneous_agricultural_24",
		"forest_31",
		"natural_32_33",
		"wetlands_4",
		"inland_waterbodies_51",
		"marine_water_52")
import_columns(clc,the_col,display=TRUE) # fait

###think i have done to here 9/may

#########################"
# Calcul des distances � la source
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse")
ccm<-loaddb(ccm)
#distance_source(ccm,silent=TRUE,from=1,to=nrow(data))



nrow(ccm)

distance_source(ccm,silent=TRUE,from=1)
distance_source(ccm,silent=TRUE,from=56250) # fait
#have run to here 9/5/2012
#looks like there is data in the distance to source column anywhere shreve is >1
#########################"
# chargement des donn�es
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse")
ccm<-loaddb(ccm)
ccm@data$c_area<-ccm@data$catchment_area
ccm1<-stream_order(ccm,silent=FALSE) # calculates shreeve stream order
#Valider puis valider
import_column(ccm1,newcolumn="shree",newcolumntype="numeric",display=FALSE) # fait
#this looks like Ok - there is a shree number in the column shree
####
#### here need to run first this to get area from the catchment table
#update ccm21.riversegments r set c_area=sub.c_area from
#(select r.wso1_id, area/1000000 as c_area from ccm21.riversegments r join ccm21.catchments c on c.wso1_id=r.wso1_id where r.wso1_id in (select wso1_id from europe.wso1 where area ='Ireland')) sub
#where sub.wso1_id=r.wso1_id
####
####
# object<-ccm
ccm2<-bassin_area(ccm,silent=FALSE)
#v�rification : ccm2@data[ccm2@data$wso_id=="304532",c("wso_id","wso1_id","catchment_area","up_catchment_area","up_area")]
import_column(ccm2,newcolumn="up_area",newcolumntype="numeric",display=FALSE)
#think i have done to here, but not all the river segments have an up-area?

#####it seems like something has gone wrong here #####
#A voir pour l'import des donn�es UGA
#import_column(ccm,newcolumn="uga",newcolumntype="numeric",display=FALSE)

#Ajout d'autre variables
#Calcul du rang de Sheidegger -- ALTER TABLE ccm21.riversegments ADD COLUMN scheid numeric
ccm@data$scheid<-ccm@data$shree*2
#Calcul des distances relatives -- ALTER TABLE ccm21.riversegments ADD COLUMN distance_relative numeric
ccm@data$distance_relative<-ccm@data$cum_len_sea/(ccm@data$cum_len_sea+ccm@data$distance_source)  
import_column(ccm,newcolumn="scheid",newcolumntype="numeric",display=FALSE)
import_column(ccm,newcolumn="distance_relative",newcolumntype="numeric",display=FALSE)

ccm<-loaddb_all(ccm)

###solved here cedric 10/05/2012 ###
##have done to here (9/may/2012) but i think there are some problems - not all wso_id1 
###entries have an up_area


#############################################
######################################
# Final data building
#####################################
##############################################
#-----------------------------------------
# Loading data for the riversegments
#------------------------------------------
source("EDACCM/init.r")
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse")

# the following query will take at least 3 minutes for france... Be patient...
# the geometries of the riversegments are not imported in R.
ccm<-loaddb_all(ccm) 
nrow(ccm@data) # 4238
ccm@data<-ccm@data[duplicated(ccm@data$wso1_id)==FALSE,]
nrow(ccm@data)

#------------------------------------------------------------------
#Sauvegarde de la CCM: 
#------------------------------------------------------------------

ccmMeuseSize<-select.list(c("Meuse_Fr_Be","Meuse_entiere"))

# Si on a des donn�es de p�ches de la hollande et de l'Allemagne, prendre:
if (ccmMeuseSize=="Meuse_entiere"){
	save(ccm,file=str_c(datawd,"/dataEDAccm/ccm_Meuse.RData"))
	load(file=str_c(datawd,"/dataEDAccm/ccm_Meuse.RData"))
}
#Si on a que les donn�es de belgique et de France:
if (ccmMeuseSize=="Meuse_Fr_Be"){
	if(getUsername() == 'user')
	{
		options(sqldf.RPostgreSQL.user = "postgres", 
				sqldf.RPostgreSQL.password = passworddistant,
				sqldf.RPostgreSQL.dbname = "eda2",
				sqldf.RPostgreSQL.host = "109.2.236.82", # "109.2.236.82" ou "1.100.1.6"
				sqldf.RPostgreSQL.port = 5432)
		
	}
	secteur<-"Meuse"
	source("EDACCM/analyse_22_data_extraction_clc.R")
	join_emu_wso1<-sqldf("select * from uga2010.join_emu_wso1")
	ccm@data<-analyse_2_emu(ccm@data,join_emu_wso1=join_emu_wso1)
	ccm@data<-subset(ccm@data,ccm@data$emu_cty_id=="FR"|ccm@data$emu_cty_id=="BE"|ccm@data$emu_cty_id=="LU")# 2512 lignes
	save(ccm,file=str_c(datawd,"/dataEDAccm/ccm_Meuse.RData"))
	#load(file=str_c(datawd,"/dataEDAccm/ccm_Meuse.RData"))
}
nrow(ccm@data) # 3022 ou 2512

#-----------------------------------------
# loading electrofishing results
#-----------------------------------------------
dbeel<-new("BaseEdaCCMriversegmentsdbeel",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="wso1_id",
		zonegeo="Meuse",
		joinschema="dbeel",
		jointable="observation_places_ccm_500",
		traveled_segments=vector(),
		dataprovider=character(length = 0), # permits selection of Belgium data 
		namesourcedatadbeel="dbeel.observation_places", 
		namestid="op_gislocation", # name of the primary key in the original station table (i.e. the daughter table namesourcedatadbeel)
		viewoperation="dbeel.view_electrofishing")
# Attention de faire retourner dbeel.observations_places_ccm_500
dbeel<-loaddb(dbeel)
# data from the dbeel database loaded: 59280 lines
# data from the dbeel database daughter table loaded: 90060 lines
# data from the joinschema.jointable loaded: 72524 lines

##str(dbeel)
# 889 projected stations at 500 m from a ccm segment
# 914 stations
#inserting data for riversegments manually
dbeel@data<-ccm@data
# nrow(dbeel@data) #3022
#colnames(dbeel@datadbeel) y a PAS de wso1_id et en plus y a 0 lignes
#colnames(dbeel@data) y a un wso1_id
#colnames(dbeel@datajoindbeel) y a un wso1_id
#unique(dbeel@datadbeel$ob_period)
dbeel<-calculatedensities(dbeel) #fait

# Pour joindre les deux tables il faut r�cup�rer les wso1_id de dbeel.observation_places_ccm_500
# join<-sqldf("select wso1_id, op_id from dbeel.observation_places_ccm_500",dbname="eda2")
# summary(join)
# colnames(join)
# dbeel@datajoindbeel<-merge(dbeel@datadbeel,join,by="op_id") 
dbeel@datajoindbeel<-merge(dbeel@datajoindbeel,dbeel@data,by="wso1_id")
#nrow(dbeel@datajoindbeel)

# select * from dbeel@datadbeel join dbeel.observation_places_ccm_500 on op_id=op_id
# min(dbeel@datadbeel$ob_starting_date,na.rm=TRUE) # "1991-07-08"
# in practise we join ccm<gid>jointable<st_id>sourcedatadbeel<op_id>view_electrofishing

#colnames(dbeel@datajoindbeel)[colnames(dbeel@datajoindbeel)=="st_id"]<-"op_id" # observation places
dbeel@datadbeel<-merge(dbeel@datajoindbeel,dbeel@datadbeel,by="op_id")
stopifnot(sum(duplicated(dbeel@datadbeel$op_id))==0)
dbeel@datadbeel<-dbeel@datadbeel[duplicated(dbeel@datadbeel$op_id)==FALSE,]
ers_full<-dbeel@datadbeel
duplicated(ers_full$ob_id)
# nrow(ers_full) #  2752 lignes

############ Deversements ope_id############:
#"1c086622-288a-4f9a-999e-0e550c268e6b"
#"de88ce38-cf80-4520-b187-d41b58161c0a"
#"fcddee22-8945-423f-9342-1e3f22b8fa84"
### =='328521'=='328497'=='328314'
# Mais ne sont pas dans ers car ce ne sont pas des p�ches compl�tes donc inutiles de les enlever


#####################################################
## Calcul des densit� quand il n'y a qu'un passage ##
#####################################################
# On utilise le rapport Nb Y.eel p1/Nb methode Carl and Strub --> pourcentage
# On l'affecte aux autes 
head(ers_full)
# la fonction met des z�ro si na a tous les passages.
ers_full[ers_full$ef_nbpas==1&!is.na(ers_full$ef_nbpas),"NCS"]<-NA #
ers_full[is.na(ers_full$ef_nbpas),"NCS"]<-NA # des NA quand on ne connait pas les passages (des 0 dans la fonction au dessus)
Navant<-sum(!is.na(ers_full$NCS)) # 571 - vercteur du nombre de NCS pas indiqu�
(mean((ers_full$nbp1/ers_full$NCS),na.rm=TRUE)) # 0.5538304 Moyennes du rapport Nb Y.eel p1/Nb methode Carl and Strub de toutes les p�ches
hist((ers_full$nbp1/ers_full$NCS),na.rm=TRUE) #
# Affectation de la moyenne:
index<-is.na(ers_full$NCS)&!is.na(ers_full$nbp1)
ers_full$NCS[index]<-(ers_full$nbp1/(mean((ers_full$nbp1/ers_full$NCS),na.rm=TRUE)))[index]
Napres<-sum(!is.na(ers_full$NCS)) #  2552 - vercteur du nombre de NCS pas indiqu�
# donc + 141 NCs indiqu�s.
### Transformation en densit�:
ers_full$densCS<-ers_full$NCS/ers_full$ef_wetted_area
Napres<-sum(!is.na(ers_full$densCS)) # 2491 car y a des surfaces mouill�es non connu
#nrow(ers_full)



#ers_full<-chnames(ers_full,"op_id","st_id")
save(ers_full,file=paste(datawd,"/dataEDAccm/ers_full_Meuse.RData",sep=""))
load(file=paste(datawd,"/dataEDAccm/ers_full_Meuse.RData",sep=""))
#min(ers_full$ob_starting_date)
#to check everything loaded properly
#str(ccm@data)
#############################################
######################################
# Quickmap for check
#####################################
##############################################
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAccm/shape/mapstationmeuse" -h 1.100.1.6 -p 5432 -u postgres -g the_geom -r -k eda2 "select id,st_transform(the_geom,4326) as the_geom,op_id from dbeel.observation_places_ccm_500 where  op_gis_layername='BDMAP' or op_gis_layername='STATIONS';"
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAccm/shape/mapobstaclemeuse" -h 1.100.1.6 -p 5432 -u postgres -g the_geom -r -k eda2 "select id,st_transform(the_geom,4326) as the_geom,op_id from dbeel.observation_places_ccm_500 where  op_gis_layername='OBSTACLES' or op_gis_layername='roe';"
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAccm/shape/mapemumeuse" -h 1.100.1.6 -p 5432 -u postgres -g geom -r -k wgeel "select gid,geom,emu_coun_abrev,emu_name,emu_name_short from carto.t_emuagreg_ema where emu_coun_abrev='BE' or emu_coun_abrev='LU' or emu_coun_abrev='NL' or emu_name='Meuse'  or emu_name='Maas' or emu_name_short='FR_Arto' ;"
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAccm/shape/Meu" -h 1.100.1.6 -p 5432 -u postgres -g the_geom -r -k eda2 "select wso1_id,ST_transform(ST_SimplifyPreserveTopology(the_geom,50),4326) as the_geom from ccm21.riversegments where wso1_id in (select wso1_id from europe.wso1 where area='Meuse');"
# This command is used to generate a shape follow instructions
# it will use the data 
#exportshape(ccm,host="1.100.1.6",area="Meuse")
shpwd<-"C:/Users/user/workspace/EDAdata/dataEDAccm/shape/"		
# the following is developped in BaseEdamodel.R but I need models to make it run.
# So I repeat everything manually there (no class method)
library(sp)
dsn=str_c("C:/Users/user/workspace/EDAdata/dataEDAccm/shape/","mapemumeuse",".shp")
fond<-readOGR(dsn,"mapemumeuse")
#print(CRSargs(CRS("+init=epsg:4326")))
fond<-spTransform(fond,CRS("+init=epsg:4326"))
#assign(x="fond",value=fond, envir=.GlobalEnv)
#Elvira replace host="localhost" or use simply exportshape(ccm,area="Ireland")
dsn=str_c(shpwd,"Meu",".shp")
mapccm<-readOGR(dsn,"Meu")
dsn=str_c(shpwd,"mapstationmeuse",".shp")
mapstation<-readOGR(dsn,"mapstationmeuse")

#require(dplyr)
#ers_full_fr<-dplyr::select(ers_full,name,ob_dp_name,densCS,ef_fishingmethod,ob_id,op_id,wso1_id)%.%
#		filter(ob_dp_name =="Laurent Beaulaton")%.%
#		group_by(op_id,name)%.%
#		summarize(n = n())%.%
#		arrange(op_id)				
#tolower(ers_full_fr[,1])
#		
#
#mapstationtest<-select(mapstation@data,op_id)%.%arrange(op_id)%.%group_by(op_id)%.%
#		summarize(n = n())
#mapstationtest[1,1]

nrow(mapstation@data) #12317
mapstation@data[,"x"]<-coordinates(mapstation)[,1]
mapstation@data[,"y"]<-coordinates(mapstation)[,2]

#mapstation@data$op_id%in%tolower(ers_full$op_id)
# je ne prends pas toutes les stations....
mapstation<-mapstation[mapstation@data$op_id%in%tolower(ers_full$op_id),]
#891
x11()
plot(fond,col="grey80")
plot(mapccm,col="blue",add=TRUE)
title(iconv("Meuse", "utf-8"))
points(mapstation,
		col="red",
		bg="red",
		pch=21,
		cex=0.5
)
dens<-ers_full[,"densCS"]
# mean density per station (all years)
dens=data.frame(dens=tapply(dens,ers_full[,"op_id"],mean,na.rm=TRUE),op_id=names(
				tapply(dens,ers_full[,"op_id"],mean,na.rm=TRUE)))
# summary(ers_full)
mapstation@data<-merge(mapstation@data,dens,by="op_id",all.x=FALSE,all.y=TRUE)
nb=data.frame(nb=tapply(ers_full[,"op_id"],ers_full[,"op_id"],length),
		op_id=names(tapply(ers_full[,"op_id"],ers_full[,"op_id"],length)))
mapstation@data<-merge(mapstation@data,nb,by="op_id",all.x=FALSE,all.y=TRUE)
bb<-bbox(mapstation)
elargit<-function(bb,coeff1,coeff2){
	if (bb[1,1]<0){
		bb[1,1]<-bb[1,1]+bb[1,1]*coeff1
	} else{
		bb[1,1]<-bb[1,1]-bb[1,1]*coeff1
	}
	if (bb[1,2]<0){
		bb[1,2]<-bb[1,2]-bb[1,2]*coeff1
	} else{
		bb[1,2]<-bb[1,2]+bb[1,2]*coeff1
	}
	if (bb[2,1]<0){
		bb[2,1]<-bb[2,1]+bb[2,1]*coeff2
	} else{
		bb[2,1]<-bb[2,1]-bb[2,1]*coeff2
	}
	if (bb[2,2]<0){
		bb[2,2]<-bb[2,2]-bb[2,2]*coeff2
	} else{
		bb[2,2]<-bb[2,2]+bb[2,2]*coeff2
	}
	return(bb)
}
bb<-elargit(bb,0.001,0.005)
x11()
plot(fond,col="grey80",xlim=bb[1,],
		ylim=bb[2,])
title(iconv("Observed densities per electrofishing eel.m-2", "utf-8"))
mypalette= colorRampPalette(c("darkblue","orange","azure4","deeppink","red"))
niveaux =cut(mapstation[["dens"]],c(-0.01,0,0.01,0.05,max(mapstation[["dens"]],na.rm=TRUE)))
couleur=mypalette(length(levels(as.factor(niveaux))))[match(niveaux,levels(as.factor(niveaux)))]
cex<-as.numeric(cut(mapstation[["nb"]],c(0,1,5,10,50,100)))/3
cex.name<-levels(cut(mapstation[["nb"]],c(0,1,5,10,50,100)))
points(mapstation,
		mapstation[["mean_dens"]],
		col=couleur,
		bg=couleur,
		pch=21,
		cex=cex)
legend(    x= "bottomleft",
		title="Classes",
		legend= levels(niveaux)    ,
		pch=19,
		xjust=0,
		pt.cex=1.5,
		col=c(mypalette(length(levels(as.factor(niveaux))))),
		horiz=FALSE,
		bty="n" )
legend( x= "bottomright",
		title="Nb",
		legend= cex.name,
		pch=19,
		col=c("grey"),
		pt.cex=unique(cex),
		horiz=FALSE,
		bty="n" )
###############################""
# Sorties graphiques pour la CCM
# voir /EDAdata/dataEDAccm/graphiques
##################################

map(ccm,bv="Meuse",
		var="distance_relative",
		zoom=6,
		loct=c(0.05,0.9),#loct coefficients de positionnement de la l�gende des percentiles
		loc=c(0.2,0.2), 
		mycut=NULL,
		palette="YlOrRd")  
require (RColorBrewer)
display.brewer.all()
map(ccm,bv="Meuse",
		var="cs_nbdams",
		zoom=6,
		loct=c(0.05,0.9),#loct coefficients de positionnement de la l�gende des percentiles
		loc=c(0.2,0.2), 
		mycut=c(0,5,10,30,50,100,200,220),
		palette="RdPu")

