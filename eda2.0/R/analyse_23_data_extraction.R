# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################



#Cr�ation des variables UGA2(1 si Adour 0 sinon), UGA2 

analyse_2_uga<-function(data){
UGA1<-rep(0,dim(data)[1])
UGA1[data$uga=="Adour"]<-1
data$UGA1<-UGA1
UGA2<-rep(0,dim(data)[1])
UGA2[data$uga=="ArtoisPicardie"]<-1
data$UGA2<-UGA2
UGA3<-rep(0,dim(data)[1])
UGA3[data$uga=="Bretagne"]<-1
data$UGA3<-UGA3
UGA4<-rep(0,dim(data)[1])
UGA4[data$uga=="Corse"]<-1
data$UGA4<-UGA4
UGA5<-rep(0,dim(data)[1])
UGA5[ers$uga=="Garonne"]<-1
data$UGA5<-UGA5
UGA6<-rep(0,dim(data)[1])
UGA6[data$uga=="Loire"]<-1
data$UGA6<-UGA6
UGA7<-rep(0,dim(data)[1])
UGA7[data$uga=="Meuse"]<-1
data$UGA7<-UGA7
UGA8<-rep(0,dim(data)[1])
UGA8[data$uga=="Rhin"]<-1
data$UGA8<-UGA8
UGA9<-rep(0,dim(data)[1])
UGA9[data$uga=="RhoneMediterranee"]<-1
data$UGA9<-UGA9
UGA10<-rep(0,dim(data)[1])
UGA10[data$uga=="SeineNormandie"]<-1
data$UGA10<-UGA10
#Regroupement de l'uga Rhin et Meuse
UGA11<-rep(0,dim(data)[1])
UGA11[data$uga%in%c("Rhin","Meuse")]<-1
data$UGA11<-UGA11
#Regroupement de l'uga Artois-Picardie, Rhin et Meuse
UGA12<-rep(0,dim(data)[1])
UGA12[data$uga%in%c("Rhin","Meuse","ArtoisPicardie")]<-1
data$UGA12<-UGA12
return(data)
}




#Cr�ation de la variable fUGA avec les UGA Rhin et Meuse regroup�s
analyse_2_fUGA<-function(data){
	data$fUGA<-data$uga
    data$fUGA[data$uga%in%c("Rhin","Meuse")]<-"RhinMeuse"
return(data)
}


