# Nom fichier :        RequeteODBC.R 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

#' @title RequeteODBC class 
#' @note Inherits from ConnexionODBC
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @slot baseODBC="vector" (inherited from ConnexionODBC)
#' @slot silent="logical" (inherited from ConnexionODBC)
#' @slot etat="character" (inherited from ConnexionODBC)
#' @slot connexion="ANY" (inherited from ConnexionODBC)
#' @slot sql="character"
#' @slot query="data.frame"
#' @slot open=logical is the connexion left open after the request ?
#' @example object=new("RequeteODBC")
setClass(Class="RequeteODBC",
		representation= representation(sql="character",query="data.frame",open="logical"),
		prototype = list(silent=TRUE,open=FALSE),
		contains="ConnexionODBC")

#' connect method loads a request to the database and returns either an error or a data.frame
#' @note assign("showmerequest",1,envir=envir_stacomi) permet d'afficher toutes les requetes passant par la classe connect
#' @returnType S4 object
#' @return An object of class RequeteODBC
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @example 
#' object=new("RequeteODBC")
#' object@open=TRUE
#' object@baseODBC=baseODBC
#' object@sql= "select * from t_lot_lot limit 100"
#' object<-connect(object)
#' odbcClose(object@connexion)
#' odbcCloseAll()
setMethod("connect",signature=signature("RequeteODBC"),definition=function(object) {     
			 # la connexion peut deja etre ouverte, on veut eviter de passer par la !
			if (is.null(object@connexion)){ 
					if (length(object@baseODBC)!=3)  {
						if (exists("baseODBC",envir=.GlobalEnv)) {
							object@baseODBC<-get("baseODBC",envir=.GlobalEnv)  
						} else {
							funout(get("msg",envir=envir_stacomi)$RequeteODBC.1,arret=TRUE)
						}
					}
					# ouverture de la connection ODBC
					e=expression(channel <-odbcConnect(object@baseODBC[1],
									uid = object@baseODBC[2],
									pwd = object@baseODBC[3],
									case = "tolower",
									believeNRows = FALSE))
					if (!object@silent) funout(paste(get("msg",envir=envir_stacomi)$RequeteODBC.2,object@baseODBC[1],"\n"))
					# renvoit du resultat d'un try catch expression dans
					#l'object Connexion courante, soit un vecteur caractere
					object@connexion<-tryCatch(eval(e), error=paste("connexion impossible ",object@baseODBC)) 
					# un object S3 RODBC
					if (class(object@connexion)=="RODBC") {
						if (!object@silent)funout(get("msg",envir=envir_stacomi)$RequeteODBC.4)
						object@etat=get("msg",envir=envir_stacomi)$RequeteODBC.4# success
					} else {
						object@etat<-object@connexion # report de l'erreur
						object@connexion<-NULL
						funout("connexion ODBC impossible",arret=TRUE)
					}
					# Envoi de la requete  
				} 
			if (!object@silent) funout(get("msg",envir=envir_stacomi)$RequeteODBC.5) # essai de la requete
			if (exists("showmerequest",envir=envir_stacomi)) print(object@sql)
			e=expression(query<-sqlQuery(object@connexion,object@sql,errors=TRUE))
			if (object@open) {
				# si on veut laisser la connexion ouverte on ne passe pas de clause finally
				resultatRequete<-tryCatch(eval(e),error = function(e) e)
			} else {
				# sinon la connexion est ferm�e � la fin de la requ�te
				resultatRequete<-tryCatch(eval(e),error = function(e) e,finally=odbcClose(object@connexion))
			}
			if ((class(resultatRequete)=="data.frame")[1]) {
				if (!object@silent) funout(get("msg",envir=envir_stacomi)$RequeteODBC.6)
				object@query=killfactor(query)     # au lieu de query 11/08/2009 11:55:20
				object@etat=get("msg",envir=envir_stacomi)$RequeteODBC.6
			} else {
				if (!object@silent) print(resultatRequete)
				object@etat=as.character(resultatRequete)
				print(object@etat)
			}
			return(object)
		})