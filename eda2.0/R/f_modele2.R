# 
# 
# Author: Celine.Jouanin
###############################################################################

######################################################
# permet d'�crire tous les mod�les possibles avec au plus une variables 
# pour chaque ligne de la liste des variables explicatives fournies
#
# exemple :
# f_modele2(list("A","B","A:B"),"y")
# produira
# y~A ; y~B ; y~A:B ; y~A + B ; y~A + A:B ; y~B + A:B ; y~A + B + A:B; y~NULL
#
# f_modele2(list(c("A","B"), "C"), "y")
# produira
# y~A ; y~C ; y~A+C  ; y~B ; y~B+C ; y~NULL
#######################################################
f_modele2=function(effet,y="y") {
	n=combos(length(effet))$binary
	
	for (i in 1:length(effet)) {
		prov=n[n[,i]==1,]
		
		if (length(effet[[i]])>1) {
			for (j in 2:length(effet[[i]])) {
				prov[,i]=j
				n=rbind(n,prov)
			}
		}
	}
	nc=ncol(n)
	formule=list()
	for (i in 1:nrow(n)){
		formule[[i]]="";
		for (j in 1:nc) {
			
			if (n[i,j]> 0) {
				if (formule[[i]]=="") {
					formule[[i]]=  effet[[j]][n[i,j]]
				} else {
					formule[[i]]=paste(formule[[i]],effet[[j]][n[i,j]], sep=" + ")
				}
			}
		}
		formule[[i]] = paste(y,formule[[i]],sep=" ~ ")
		
	}
	formule[[length(formule)+1]] = paste(y,'NULL',sep=" ~ ")
	
	return(formule)
}

