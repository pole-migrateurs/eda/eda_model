# D�termination du jeu de calibration et de validation
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#Sample by UGA 1/3-2/3
# G�n�rateur d'un nombre al�atoire (Alea) pour la France
ers$Alea<-runif(dim(ers)[1])

#Reordonne ers suivant l'alea d�croissant
ers<-ers[order(ers[,"Alea"],decreasing=T),]
ers$Val<-rep(0,dim(ers)[1])
ers$Val[1:round(2*dim(ers)[1]/3)]<-1


#Pour tous les UGA  OK !!!
ers$Number<-1:dim(ers)[1]
ech<-tapply(ers$Number,ers$fUGA,function(x) sample(x,round(2*length(x)/3),replace=FALSE)) 
ech1<-data.frame(unlist(ech))
names(ech1)<-"Number"
ers$Val<-rep(0,dim(ers)[1])
ers$Val[ers$Number%in%ech1$Number]<-1
table(ers$fUGA,ers$Val)

ers$Val[ers$Number%in%ersB$Number]<-1

#Par UGA et classe de distance � la mer  #Diff�re pour chaque UGA en fait !
ers$cl_distance_sea<-rep(0,dim(ers)[1])
ers$cl_distance_sea[ers$distance_sea<=70]<-1
ers$cl_distance_sea[ers$distance_sea>70]<-2
ers$cl_distance_sea[ers$distance_sea>187]<-3
ers$cl_distance_sea[ers$distance_sea>343]<-4

ech<-tapply(ers$Number,ers$fUGA|ers$cl_distance_sea,function(x) sample(x,round(2*length(x)/3),replace=FALSE)) 
ech1<-data.frame(unlist(ech))
names(ech1)<-"Number"
ers$Val<-rep(0,dim(ers)[1])
ers$Val[ers$Number%in%ech1$Number]<-1
table(ers$fUGA,ers$Val)





u<-split(ers,ers$uga)
M<-matrix(NA,c(10,dim(ers)[1]+1))
for(i in 1:length(u)){
	M[i,]<-u[i]
	M[i,"Val"]<-rep(0,(u)[1])
	u[1:round(2*length(u[i][[1]])/3),"Val"]<-1
}

ersini<-ers
ers<-ersini
ersi<-ers
#G�n�rateur d'un nombre al�atoire (Alea) par UGA
ersi<-ersi[order(ersi[,"Alea"],decreasing=T),]
ugaList<-levels(as.factor(ers$fUGA))
for(i in 1:length(ugaList)){
ersi[1:round(2*dim(ersi[ersi$uga%in%ugaList[i],])[1]/3),"Val"]<-1
}
ugaList<-levels(as.factor(ers$fUGA))
for(i in 1:length(ugaList)){
ersi[ersi$uga%in%ugaList[i],]<-ersi[order(ersi[ersi$uga%in%ugaList[i],"Alea"],decreasing=T),]

ersi$Val<-rep(0,dim(ersi[ersi$uga%in%ugaList[i],])[1])
ersi$Val[1:round(2*dim(ersi[ersi$uga%in%ugaList[i],])[1]/3)]<-1
}

round(2*tapply(ers$uga,ers$uga,length)/3)[1]
ers[ers$ugaList[i],Val]<-1
ers$Val[1:round(2*tapply(ers$uga,ers$uga,length)/3)[1]]<-1

ugaList<-levels(as.factor(ers$uga))
for(i in 1:length(ugaList)){
ers$Alea[ers$uga%in%ugaList[i]]<-round(runif(1),2) #nb al�atoire arrondi � 2 d�cimales
}


ers$Nrow<-1:dim(ers)[1]

sample(ers$Nrow[ers$uga%in%ugaList[i]],round(length(ers$Nrow[ers$uga%in%ugaList[i]])/3))



for(i in 1:length(ugaList)){
	x11()
	plot(ccm$elev_mean[ccm$uga%in%ugaList[i]]
order(MDate1[,"Alea",p],decreasing=T)


valid_id <- sample(1:dim(ers)[1], round(dim(ers)[1]/3))
ers$val<-0
ers[valid_id,"val"]<-1

ers_calibration<- ers[!is.element(1:dim(ers)[1], valid_id),]
str(ers_calibration); dim(ers_calibration)
cal_id<- vector("list",10)
set.seed(2540)
for(i in 1:10) {
	calid[[i]]<- sample(1:dim(ers_calibration)[1], 13)
	ers_calibration<-
			ers_calibration[!is.element(1:dim(ers_calibration)[1], cal.id[1:i]),]
}
sapply(cal_id,length)
ers_calibration<- ers[!is.element(1:dim(ers)[1], valid_id),] 
#create again the calibration data set
dim(ers_calibration)
ers_cal<- vector("list",10)
for(i in 1:10) ers_cal[[i]] <- ers_calibration[cal_id[[i]],]
plot(ers_cal[[1]])
for(i in 2:10) points(ers_cal[[i]], pch=2:10, col=2:10)
points(ers_val, pch=20, col="blue", cex=1.2)
rm(valid_id, cal_id)
