# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################


#Cr�ation d'une colonne dans ccm relative � la table ers_full et ers
ccm$eff<-ccm$wso1_id%in%ers_full$wso1_id  #ensemble des donn�es de p�ches
ccm$efc<-ccm$wso1_id%in%ers$wso1_id  #ensemble des donn�es p�ches compl�tes uniquement
ccm$es<-0  #CCM global
ccm$es[ccm$efc==TRUE]<-1  #ers
ccm$es[ccm$eff==TRUE&ccm$efc==FALSE]<-2
#es=1 : ERS, es>=1 : ERS_full
#ccm[577,] ccm[578,] ...
#Colonnes � s�lectionner dans la ccm :
#names(ccm) 7,8,11,13:21,25:29,31:58
for(i in c(7,8,13:16,18:20,25:29,32:55,57:58)){
	x11()
	par(mar=c(2.2,3.8,1.4,1.2))
	boxplot(ccm[,i],ccm[ccm$es>=1,i],ccm[ccm$es==1,i],ylab=names(ccm)[i],axis=F)
	axis(1,1:3,c("ccm","ers_full","ers"))
	lines(c(0.6,1.4),c(mean(ccm[,i],na.rm=T),mean(ccm[,i],na.rm=T)),col="red")
	lines(c(1.6,2.4),c(mean(ccm[ccm$es>=1,i],na.rm=T),mean(ccm[ccm$es>=1,i],na.rm=T)),col="red")
	lines(c(2.6,3.4),c(mean(ccm[ccm$es==1,i],na.rm=T),mean(ccm[ccm$es==1,i],na.rm=T)),col="red")
}

#Test statistique de la Diff�rence entre CCM et ERS (Test de Mann-Whitney)
for(i in c(7,8,13:16,18:20,25:29,32:55,57:58)){
	print(names(ccm)[i])
	print(wilcox.test(ccm[,i],ccm[ccm$es==1,i]))
}
#Pas de diff�rence significative pour :up_marwat_52, up_art_14, marwat_52, inwat_51
	
#Test statistique de la Diff�rence entre CCM et ERS_full (Test de Mann-Whitney)
for(i in c(7,8,13:16,18:20,25:29,32:55,57:58)){
	print(names(ccm)[i])
	print(wilcox.test(ccm[,i],ccm[ccm$es>=1,i]))
}
#Pas de diff�rence significative pour :up_marwat_52, marwat_52

#En fonction du rang de strahler
for(j in 1:8){
for(i in c(7,8,13:16,18:20,25:29,32:55,57:58)){
	x11()
	par(mar=c(2.2,3.8,1.1,1.2))
	boxplot(ccm[ccm$strahler==j,i],ccm[ccm$es>=1&ccm$strahler==j,i],ccm[ccm$es==1&ccm$strahler==j,i],ylab=names(ccm)[i],axis=F,main=paste("strahler=",j))
	axis(1,1:3,c("ccm","ers_full","ers"))
	lines(c(0.6,1.4),c(mean(ccm[ccm$strahler==j,i]),mean(ccm[ccm$strahler==j,i])),col="red")
	lines(c(1.6,2.4),c(mean(ccm[ccm$es>=1&ccm$strahler==j,i]),mean(ccm[ccm$es>=1&ccm$strahler==j,i])),col="red")
	lines(c(2.6,3.4),c(mean(ccm[ccm$es==1&ccm$strahler==j,i]),mean(ccm[ccm$es==1&ccm$strahler==j,i])),col="red")
	}
}

#Strahler
x11()
par(mfrow=c(1,3))
barplot(tapply(ccm$strahler,ccm$strahler,sum)/length(ccm$strahler),main="ccm")
barplot(tapply(ccm[ccm$es>=1,"strahler"],ccm[ccm$es>=1,"strahler"],sum)/length(ccm[ccm$es>=1,"strahler"]),main="ers_full")
barplot(tapply(ccm[ccm$es==1,"strahler"],ccm[ccm$es==1,"strahler"],sum)/length(ccm[ccm$es==1,"strahler"]),main="ers")
  #--> privil�gie les ordres de strahler 2-5 plutot que 1

#UGA
x11()
par(mfrow=c(1,3))
barplot(tapply(ccm$uga,ccm$uga,sum)/length(ccm$uga),main="ccm")
barplot(tapply(ccm[ccm$es>=1,"uga"],ccm[ccm$es>=1,"uga"],sum)/length(ccm[ccm$es>=1,"uga"]),main="ers_full")
barplot(tapply(ccm[ccm$es==1,"uga"],ccm[ccm$es==1,"uga"],sum)/length(ccm[ccm$es==1,"uga"]),main="ers")
#probl�mes avec UGA=NA  wso_id=442529, 291495, 342310
#summary(as.factor(ccm[is.na(ccm$uga),"wso_id"]))

#Distribution des trois jeux de donn�es - density plot
#Probl�me : cum_len_sea avec NA dans CCM et ers_full
#Variables contenant des NA : cum_len_sea14, c_height 16,c_score17
for(i in c(7,8,13,15,17:20,25:29,32:55,57:58)){
	x11()
	plot(density(ccm[,i]), 
			xlim=range(c(ccm[,i],ccm[ccm$es>=1,i],ccm[ccm$es==1,i])),
			ylim=range(c(density(ccm[,i])$y,density(ccm[ccm$es>=1,i])$y,density(ccm[ccm$es==1,i])$y)))
	lines(density(ccm[ccm$es>=1,i]), col=2)
	lines(density(ccm[ccm$es==1,i]), col=3)
	rug(ccm[,i], col=1, ticksize=0.01, line=2.5)
	rug(ccm[ccm$es>=1,i], col=2, ticksize=0.01, line=3.0)
	rug(ccm[ccm$es==1,i], col=3, ticksize=0.01, line=3.5)
}

#Description des variables par classes
#for(i in c(13:21,25:29,32:55,57:58)){
#	print(assign(paste("ccm$cl",names(ccm)[i],sep=""),cut(ccm[,i],12)))
#}


ccm$CLalt_gradie<-cut(ccm$alt_gradie,12)
ccm$CLdistance_sea<-cut(ccm$distance_sea,12)
ccm$CLdistance_source<-cut(ccm$distance_source,12)
ccm$CLc_height<-cut(ccm$c_height,12)
ccm$CLnbdams<-cut(ccm$nbdams,12)
ccm$CLcs_height<-cut(ccm$cs_height,12)
ccm$CLcs_nbdams<-cut(ccm$cs_nbdams,12)
ccm$CLarea_km2<-cut(ccm$area_km2,12)
ccm$CLelev_mean<-cut(ccm$elev_mean,12)
ccm$CLslope_mean<-cut(ccm$slope_mean,12)
ccm$CLrain_mean<-cut(ccm$rain_mean,12)
ccm$CLtemp_mean<-cut(ccm$temp_mean,12)
ccm$CLcatchment_area<-cut(ccm$catchment_area,12)
ccm$CLup_catchment_area<-cut(ccm$up_catchment_area,12)
ccm$CLrelative_distance<-cut(ccm$relative_distance,12)

for(i in 62:76){
all <- c(table(ccm[,i])/dim(ccm)[1],
		table(ccm[ccm$es>=1,i])/dim(ccm[ccm$es>=1,])[1],
		table(ccm[ccm$es==1,i])/dim(ccm[ccm$es==1,])[1])
x11()
barplot(table(ccm[,i])/dim(ccm)[1],ylim=c(0,max(all)),las=3,ylab=names(ccm)[i])
barplot(table(ccm[ccm$es>=1,i])/dim(ccm[ccm$es>=1,])[1],ylim=c(0,max(all)),las=3,col="red",add=TRUE)
barplot(table(ccm[ccm$es==1,i])/dim(ccm[ccm$es==1,])[1],ylim=c(0,max(all)),las=3,col="blue",add=TRUE)
legend("topright",c("ccm","ers","ers_full"),col=c("grey","red","blue"),bty="n",box.lty=1,fill=c("grey","red","blue"))
}
#colnames(ccm)
#CCM et ERS
for(i in 62:76){
	par(mar=c(3,3.8,1.4,1.2))
	all <- c(table(ccm[,i])/dim(ccm)[1],
			table(ccm[ccm$es==1,i])/dim(ccm[ccm$es==1,])[1])
	x11()
	barplot(table(ccm[,i])/dim(ccm)[1],ylim=c(0,max(all)),las=3,ylab=substring(names(ccm)[i],3))
	barplot(table(ccm[ccm$es==1,i])/dim(ccm[ccm$es==1,])[1],ylim=c(0,max(all)),las=3,col="blue",add=TRUE)
	legend("topright",c("ccm","ers"),col=c("grey","blue"),bty="n",box.lty=1,fill=c("grey","blue"))
}



#Histogramme - Stacking boxplot - A FINIR
for(i in c(7,8,13:16,18:20,25:29,32:55,57:58)){
	all <- c(ccm[,i],ccm[ccm$es>=1,i],ccm[ccm$es==1,i]);
	hist(ccm[,i], breaks=seq(min(all), max(all)+300, by=300),
		xlim = c(min(all), max(all+0.01)),
		main = "hist", xlab = "distribution", ylab= "", col=1);
	box();
	par(new=T);
	hist(ccm[ccm$es>=1,i], breaks=seq(min(all), max(all)+300, by=300),
		xlim = c(min(all), max(all+0.01)),
		main = "", xlab = "", ylab= "", col=2);
	box();
	par(new=T);
	hist(ccm[ccm$es==1,i], breaks=seq(min(all), max(all)+300, by=300),
		xlim = c(min(all), max(all+0.01)),
		main = "", xlab = "", ylab= "", col=3);
	box();
}




#D�termination de classes : bin.var(ccm$elev_mean, bins=10, method='intervals',labels=NULL)

# calcul des pentes moyennes
#ccm$work<-(ccm$elev_mean*ccm$distance_sea)^(1/2) #Probl�me dans le calcul, je comprend pas

boxplot()
hist(ccm$elev_mean, col = "yellow",
		border = "red",
		main= paste("Elev_mean", nrow(ccm),
				"river segments" ),
		xlab = "Elev_mean", proba = TRUE,
		ylim=c(0,max(densccm$y)))
lines(density(ccm$elev_mean), lwd = 3,
		col = "darkblue" )
lines(density(ers_full$elev_mean), lwd = 3,
		col = "green" )
lines(density(ers$elev_mean), lwd = 3,
		col = "violet" )



