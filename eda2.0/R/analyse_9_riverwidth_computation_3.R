# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#///////////////////////////////////////////////////////////////#
#                        Data Selection                         #
#  shreve>1, annee>1987, methode complete, avril-octobre        #
#...............................................................#

ers0<-subset(ers_full,!is.na(ers_full$op_cs_largeurlameeau)&
					ers_full$op_cs_largeurlameeau!=0&
					ers_full$op_cs_largeurlameeau!=999&
					!is.na(ers_full$up_area)&
					ers_full$month<11&
					ers_full$month>3&
					ers_full$op_cd_methodeprospection==843&
					ers_full$annee>1987&
					!is.na(ers_full$uga))
	
# Suppression des stations :
ers0<-ers0[!ers0$st_id%in%c("0547C025","06840051","04430072","06250162","0532B006"),]
	
#...............................................................#
#                Variable Description                           #
#...............................................................#
# up_area
x11()
boxplot(ccm$up_area,ers0$up_area,las=2)
axis(1,1:2,c("ccm","ers0"))

#op_cs_largeurlameeau ~ UGA  --ERS0
x11()
par(mar=c(8.5,2.5,1,1))
boxplot(ers0$op_cs_largeurlameeau~ers0$uga,las=2)

#up_area ~ UGA --CCM
x11()
par(mar=c(8.5,3.5,1,1))
boxplot(ccm$up_area~ccm$uga,las=2)

#up_area ~ UGA --ERS0
x11()
par(mar=c(8.5,3.5,1,1))
boxplot(ers0$up_area~ers0$uga,las=2)

#up_area ~ strahler
x11()
par(mar=c(2.5,3.5,1,1))
boxplot(ccm$up_area~ccm$strahler,las=1,log="y")
#up_area ~ strahler
x11()
par(mar=c(2.5,3.5,1,1))
boxplot(ers0$up_area~ers0$strahler,las=1,log="y")


#Probl�me de relev� ou changement de station (pas la m�me largeur de lame eau pour la station 04490500
# probl�me r�gl�
ers0[ers0$wso1_id=="355628",c("annee","month","wso1_id","up_area","op_cs_largeurlameeau","uga","st_id","strahler")]

#visiblement le protocole n'est pas bien standardis� ou il y a eu des probl�mes de suivi
#--> Pourquoi ne pas prendre en compte que les stations appartenant au RHP pour le mod�le?

#Etude du coefficient de variation de la largeur de la lame d'eau
x11()
hist(ers0$CVlameeau,100)
x11()
hist(ers0$CVlameeau[ers0$CVlameeau<0.3],100)

#Etude du coefficient de variation de la largeur du lit mineur
x11()
hist(ers0$CVlitmin,100)

#COnserve que les stations o� CVlameeau <=20%
ers0<-ers0[ers0$CVlameeau<=0.15,]



#...............................................................#
#  Pr�paration d'un fichier avec les m�dianes par station
#...............................................................#
ers0_station=data.frame(st_id=unique(ers0$st_id), 
		st_cs_largeurlameeau=NA , st_up_area =NA, st_uga=ers0[1,'uga'], shreve=ers0[1,"shree"])


ers_station<-ers0_station ### A VOIR !!!
for (i in 1:nrow(ers_station)) {
	sel=ers0$st_id==ers_station$st_id[i]
	
	ers0_station[i,'st_cs_largeurlameeau']= median(ers0[sel,'op_cs_largeurlameeau'])
	ers0_station[i,'st_up_area']= median(ers0[sel,'up_area'])
	ers0_station[i,'st_uga']= unique(ers0[sel,'uga'])
	ers0_station[i,'shreve']=unique(ers0[sel,'shree'])
	ers0_station[i,'CVlameeau']=unique(ers0[sel,'CVlameeau'])
}
save(ers0_station,file=paste(datawd,"/dataEDAccm/ers0_station.RData",sep=""))
load(file=paste(datawd,"/dataEDAccm/ers0_station.RData",sep=""))

dim(ers0_station) # 5113    6

ers0_station1<-ers0_station[ers0_station$shreve>2&!(is.na(ers0_station$shreve)),]
dim(ers0_station1)
ers0_station2<-ers0_station[ers0_station1$CVlameeau<5&!(is.na(ers0_station1$CVlameeau)),]
dim(ers0_station2)


# Modele 1 : width~catchment_area
lm1<-lm(ers0_station2$st_cs_largeurlameeau~ers0_station2$st_up_area)

# Modele 2 : log(width)~log(catchment_area)
lm2<-lm(log(ers0_station2$st_cs_largeurlameeau)~log(ers0_station2$st_up_area))

# Modele 3 : log(width)~log(catchment_area)+ uga
lm3<-lm(log(ers0_station2$st_cs_largeurlameeau)~log(ers0_station2$st_up_area)+ers0_station2$st_uga)

# Modele 4 : log(width)~log(catchment_area)+ log(shreve index)
lm4<-lm(log(ers0_station2$st_cs_largeurlameeau)~log(ers0_station2$st_up_area)+log(ers0_station2$shreve))

# Modele 5 : log(width)~log(catchment_area)+ log(shreve index) + uga 
lm5<-lm(log(ers0_station2$st_cs_largeurlameeau)~log(ers0_station2$st_up_area)+log(ers0_station2$shreve)+ers0_station2$st_uga)

AIC(lm1,lm2,lm3,lm4,lm5)
summary(lm5)
#The model explain (only) 41% of the variation
#autocorrelation spatiale ?
		
x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station2)
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station2,cex=0.4, ylim=c(0,20),xlim=c(0,200))
x11()
plot(log(st_cs_largeurlameeau)~log(st_up_area),data=ers0_station2,cex=0.4)
abline(lm(log(ers0_station2$st_cs_largeurlameeau)~log(ers0_station2$st_up_area)))

x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station)
abline(lm(ers0_station$st_cs_largeurlameeau~ers0_station$st_up_area))

mod<-lm4
coef(mod)
fitted(mod)
residuals(mod)
predict(mod,interval="confidence")

#drop1 command compares the full model with a model in which the interaction is dropped
#if we have an interaction in the model
drop1(mod)

op<-par(mfrow=c(2,2),mar=c(5,4,1,2))
plot(mod,add.smooth=FALSE,which=1)
hist(resid(mod),xlab="residuals",main="")
plot(log(ers0_station2$st_up_area),resid(mod),xlab="log(up_area)",ylab="Residuals")
par(op)

op<-par(mfrow=c(2,2))
plot(mod,add.smooth=FALSE)
par(op)
#panel D identifies potential and influential observations. 
#Leverage measures whether any observation has extreme values of the explanatory variables


x11()
plot(residuals(mod)~fitted(mod))

x11()
xyplot(log(st_cs_largeurlameeau)~log(st_up_area), groups=st_uga,
		data=ers0_station2, type='p', 
		auto.key = list(x = 0.05, y = 0.95))
x11()
plot(log(st_cs_largeurlameeau)~log(st_up_area),data=ers0_station)
abline(lm(log(ers0_station$st_cs_largeurlameeau)~log(ers0_station$st_up_area)))

x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station[ers0_station$st_cs_largeurlameeau>0,])
abline(lm(ers0_station$st_cs_largeurlameeau~ers0_station$st_up_area))

# Mod�le avec intercept differents
lmPL4=lm(log(st_cs_largeurlameeau)~ st_uga + log(st_up_area), 
		data=ers0_station, subset=  st_cs_largeurlameeau>0)
summary(lmPL4)
AIC(lmPL4)
#plot(lmPL4) 

## lmPL4 est mieux mais sans nettoyage de la base je prendrais lmPL3 comme premier application

## modele avec racine carr�e de la surface
lmPL5=lm(st_cs_largeurlameeau~  sqrt(st_up_area)-1  , 
		data=ers0_station, subset=  st_cs_largeurlameeau>0)
summary(lmPL5)
AIC(lmPL5)
summary(lm(ers0_station$st_cs_largeurlameeau~lmPL5$fitted))

# Mod�le avec intercepts differents par UGA
lmPL3=lm(log(st_cs_largeurlameeau)~  log(st_up_area), 
		data=ers0_station, subset=  st_cs_largeurlameeau>0)
summary(lmPL3)
AIC(lmPL3)
summary(lm(ers0_station$st_cs_largeurlameeau~exp(lmPL3$fitted)))
#plot(lmPL3)
#ypredit<-lmPL3$coefficients[[2]]*log(st_up_catchment_area)+lmPL3$coefficients[[1]]


newData3= data.frame(
		st_up_area =seq(min(ers0_station$st_up_area), 
				max(ers0_station$st_up_area), length.out=100))
newData3$st_cs_largeurlameeau_lmPL3= exp(predict(lmPL3,newdata=newData3, type=('response')))
newData3$st_cs_largeurlameeau_theo=  predict(lmPL3,newdata=newData3, type=('response'))

x11()
xyplot(log(st_cs_largeurlameeau)~log(st_up_area), groups=st_uga,
		data=ers0_station, type='p', 
		auto.key = list(x = 0.05, y = 0.95))
x11()
plot(log(st_cs_largeurlameeau)~log(st_up_area),data=ers0_station)
abline(lm(log(ers0_station$st_cs_largeurlameeau)~log(ers0_station$st_up_area)))

x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station)
abline(lm(ers0_station$st_cs_largeurlameeau~ers0_station$st_up_area))

mod1<-lm(ers0_station$st_cs_largeurlameeau~ers0_station$st_up_area)
coef(mod1)
fitted(mod1)
residuals(mod1)
predict(mod1,interval="confidence")

x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station)
abline(mod1)
pred.frame<-data.frame(st_up_area =ers0_station$st_up_area)
pc<-predict(mod1,interval="confidence",newdata=pred.frame)
pp<-predict(mod1,interval="prediction",newdata=pred.frame)
matlines(pred.frame,pc[,2:3],lty=c(2,2),col="blue")
matlines(pred.frame,pp[,2:3],lty=c(3,3),col="red")
legend(locator(1),c("confiance","prediction"),lty=c(2,3)
		, col=c("blue","red"),bty="n")

x11()
layout(matrix(1:4,2,2))
plot(mod1)

mod2<-lm(log(ers0_station$st_cs_largeurlameeau)~log(ers0_station$st_up_area))
x11()
plot(log(st_cs_largeurlameeau)~log(st_up_area),data=ers0_station)
abline(mod2)
pred.frame<-data.frame(st_up_area =log(ers0_station$st_up_area))
pc<-predict(mod2,interval="confidence",newdata=pred.frame)
pp<-predict(mod2,interval="prediction",newdata=pred.frame)
matlines(pred.frame,pc[,2:3],lty=c(2,2),col="blue")
matlines(pred.frame,pp[,2:3],lty=c(3,3),col="red")
legend(locator(1),c("confiance","prediction"),lty=c(2,3)
		, col=c("blue","red"),bty="n")
x11()
layout(matrix(1:4,2,2))
plot(mod2)

ers0_station1<-ers0_station[ers0_station$st_up_area<=800,]
mod3<-lm(ers0_station1$st_cs_largeurlameeau~ers0_station1$st_up_area)
x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station1,cex=0.4,ylim=c(0,40))
abline(mod3)
pred.frame<-data.frame(st_up_area =ers0_station1$st_up_area)
pc<-predict(mod3,interval="confidence",newdata=pred.frame)
pp<-predict(mod3,interval="prediction",newdata=pred.frame)
matlines(pred.frame,pc[,2:3],lty=c(2,2),col="blue")
matlines(pred.frame,pp[,2:3],lty=c(3,3),col="red")
legend(locator(1),c("confiance","prediction"),lty=c(2,3)
		, col=c("blue","red"),bty="n")

mod4<-lm(log(ers0_station1$st_cs_largeurlameeau)~ers0_station1$st_up_area)
x11()
plot(log(st_cs_largeurlameeau)~st_up_area,data=ers0_station1)
abline(mod4)
pred.frame<-data.frame(st_up_area =ers0_station1$st_up_area)
pc<-predict(mod4,interval="confidence",newdata=pred.frame)
pp<-predict(mod4,interval="prediction",newdata=pred.frame)
matlines(pred.frame,pc[,2:3],lty=c(2,2),col="blue")
matlines(pred.frame,pp[,2:3],lty=c(3,3),col="red")
legend(locator(1),c("confiance","prediction"),lty=c(2,3)
		, col=c("blue","red"),bty="n")

library(nlme)
#ers0_station$lst_up_area=log(ers0_station$st_up_area*10000)
nls1<-nls(st_cs_largeurlameeau~alpha*st_up_area^beta, data=ers0_station,start=list(alpha=1.37,beta=0.32))
pred.frame<-ers0_station[1:500,]
pred.frame[,]<-NA
pred.frame$st_up_area=seq(from=0,to=800,length.out=500)
pred.frame$pred<-predict(object=nls1,newdata=pred.frame,interval="prediction")
summary(nls1)
x11()
plot(st_cs_largeurlameeau~st_up_area,data=ers0_station,log="y",xlim=c(0,800))
points(pred~st_up_area,data=pred.frame,col="red",type="l")



nls2<-nls(st_cs_largeurlameeau~gamma*shreve^lambda, data=ers0_station,start=list(gamma=3.33,lambda=0.28))
summary(nls2)
pred.frame<-ers0_station[1:500,]
pred.frame[,]<-NA
pred.frame$shreve=seq(from=0,to=200,length.out=500)
pred.frame$pred<-predict(object=nls2,newdata=pred.frame,interval="prediction")

x11()
plot(st_cs_largeurlameeau~shreve,data=ers0_station,log="xy",cex=0.5,pch=16)
points(pred~shreve,data=pred.frame,col="green",type="l")


nls3<-nls(st_cs_largeurlameeau~alpha*st_up_area^beta +gamma*shreve^lambda, data=ers0_station,start=list(alpha=1.37,beta=0.32,gamma=3.33,lambda=0.28))
summary(nls3)
pred.nls3<-predict(nls,type=response)

b<-coef(nls3)
nls4<-nls(st_cs_largeurlameeau~alpha[st_uga]*st_up_area^beta[st_uga] +gamma[st_uga]*shreve^lambda[st_uga], data=ers0_station,start=list(alpha=rep(b[1],10),beta=rep(b[2],10),gamma=rep(b[3],10),lambda=rep(b[4],10)))

ers0_stationu<-ers0_station[ers0_station$st_uga=="Corse",]
nls5<-nls(st_cs_largeurlameeau~alpha*st_up_area^beta +gamma*shreve^lambda, data=ers0_stationu,start=list(alpha=1.37,beta=0.32,gamma=3.33,lambda=0.28))

nls31<-nls(st_cs_largeurlameeau~alpha*st_up_area*I[beta] +gamma*shreve*I[lambda], data=ers0_station,start=list(alpha=1.37,beta=0.32,gamma=3.33,lambda=0.28))
summary(nls31)

