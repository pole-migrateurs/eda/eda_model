# Nom fichier :        libraries.R
# Projet :             EDA/EDAcommun/prg/init
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             Cedric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :    20/02/2008 19:48:45
# Description :         chargement initial des librairies n�cessaires au projet
# Data needed
# Packages needed :
# remarques
#
#**********************************************************************

##########

load_library("utils")
#load_library("BMA")
load_library("R2HTML")
load_library("VGAM")
load_library("car")
load_library(c("tcltk2","tcltk","Hmisc","MASS","lattice","svMisc")) #depre
load_library("stringr")
load_library("tcltk")
#require("tcltk2")
load_library("RODBC")
#load_library("gWidgets")
#load_library("gWidgetsRGtk2")
#load_library("lattice")
load_library("MASS")
load_library("pool")
load_library("DBI")
load_library("RPostgreSQL")
load_library("sqldf")
load_library("sf")
load_library("leaflet")
load_library(c("sp", "maptools","rgdal"))
load_library("lattice")
load_library("ggmap")
load_library("tidyverse")
load_library("stacomirtools")
