#River width for Meuse 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

##############################################
# LARGEURS DE COURS D'EAU
##############################################

# see analyse_92_river_width_computation_emu_model.R line 31 for basque country
# loading prediction model for france  (width km)
load(file=str_c(datawd,"/dataEDAccm/predictions/riverwidthmodel",".Rdata"))
# loads lm_rw linear model for river width and ers_station
ccm1<-ccm
ccm1$uga<-"Meuse"
ccm1<-ccm1[,c("up_area","uga","shree")]  #up_area km�
ccm1$uga<-as.factor(ccm1$uga)
levels(ccm1$uga)<-levels(ers_station$uga)[7]
ccm$riverwidth<-exp(predict(lm_rw,newdata=ccm1,type="response"))  
ccm$riverarea<-(ccm$riverwidth*ccm$shape_leng)
# riverwidth (m)
ccm$riverareakm<-(ccm$riverwidth*ccm$shape_leng)/10^6   #(riverwidth (m) * shape_leng (m))/10^6 -->km�
#Nb of eel per segment for 100m� ??  
ccm$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverarea/100 #-->100m�
#ccm$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverareakm/(100/10^6) #-->100m�


### Sortie de r�sultats:
#install.packages('dplyr')
require('dplyr')

## Yellow eel:
ccm$emu_coun_abrev<-as.factor(ccm$emu_coun_abrev)
#
#head(ers)		
#belg<-subset(ers,ers$op_gis_layername=="STATIONS")
#fran<-subset(ers,ers$op_gis_layername=="BDMAP")
#belg$totalnumber
#fran$totalnumber 
#fran$nbp1
#belg$nbp1
#subset(fran,fran$totalnumber=='43')
#fran$d 
#belg$d
#ccm$nb_eel_per_segment = str_replace(ccm$nb_eel_per_segment,"Inf",0)
#ccm$nb_eel_per_segment<-as.numeric(ccm$nb_eel_per_segment)

print("total number of yellow eel")
print(sum(ccm$nb_eel_per_segment,na.rm=T)) # 96114.96 /14761.39 /131853.5
print(select(ccm,nb_eel_per_segment,emu_name_short)%.% group_by(emu_name_short)%.%summarize(Nb_yellow_eel=sum(nb_eel_per_segment,na.rm=T)))

##Total number of silver eel= total number of yellow eel * 0.05
print("total number of silver eel")
print(sum(ccm$nb_eel_per_segment,na.rm=T)*0.05) # 738.0693
print(select(ccm,nb_eel_per_segment,emu_coun_abrev)%.% group_by(emu_coun_abrev)%.%summarize(Nb_yellow_eel=sum(nb_eel_per_segment,na.rm=T)*0.05))
print("total water surface (km�)")


## Yellow eel by surface
print("number of yellow eel per 100 m�")
print(100*sum(ccm$nb_eel_per_segment,na.rm=T)/sum(ccm$riverarea,na.rm=T))  # 0.17276 anguille jaune / 100 m� densite moyenne predite

print(select(ccm,nb_eel_per_segment,emu_coun_abrev,na.rm=T)%.% group_by(emu_coun_abrev)%.%summarize(Nb_yellow_eel=100*sum(nb_eel_per_segment,na.rm=T)/sum(ccm$riverarea)))
## NL > DE > BE > FR > NA > LU --> logique
print(select(ccm,riverarea,emu_coun_abrev)%.% group_by(emu_coun_abrev)%.%summarize(surface=sum(ccm$riverarea)))
## BE > FR > NL > DE > LU 

## Silver eel (*0.05) by surface:
#print(sum(ccm$riverarea)/10^6) # water surface area in km2
print(sum(ccm$riverareakm,na.rm=T)) #d�j� en 55.63351 km�
print("number of siver eel per 100 m�")
print(100*sum(ccm$nb_eel_per_segment,na.rm=T)*0.05/sum(ccm$riverarea,na.rm=T)) # 0.008638225 anguille argent�e pour 100 m�
print(select(ccm,nb_eel_per_segment,emu_coun_abrev,na.rm=T)%.% group_by(emu_coun_abrev)%.%summarize(Nb_yellow_eel=100*sum(nb_eel_per_segment,na.rm=T)*0.05/sum(ccm$riverarea)))
## NL > DE > BE > FR > LU --> logique

#Trois wso1_id o� pas de up_area : 342882, 350998, 347851
#Plusieurs segments sans pr�diction (NA, ou NaN)


## Pr�diction par sous-bassins Cf. Wso1_4 dans catchment c.a.d par sous bassin 
## Selection dans postgresql apr�s avoir rentr� les r�sultats du mod�le









