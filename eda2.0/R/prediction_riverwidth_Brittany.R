#River width for Brittany EMU based on Brittany river width model
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

##############################################
# LARGEURS DE COURS D'EAU
##############################################

# see analyse_92_river_width_computation_emu_model.R line 31 for basque country
# loading prediction model for france  (width km)
load(file=str_c(datawd,"/dataEDAccm/predictions/riverwidthmodel",".Rdata"))
# loads lm_rw linear model for river width and ers_station
ccm1<-ccm
ccm1$uga<-"Bretagne"
ccm1<-ccm1[,c("up_area","uga","shree")]  #up_area km�
ccm1$uga<-as.factor(ccm1$uga)
levels(ccm1$uga)<-levels(ers_station$uga)
ccm$riverwidth<-exp(predict(lm_rw,newdata=ccm1,type="response"))  # riverwidth (m)
ccm$riverarea<-ccm$riverwidth*ccm$shape_leng #-->m�
ccm$riverareakm<-(ccm$riverwidth*ccm$shape_leng)/10^6   #(riverwidth (m) * shape_leng (m))/10^6 -->km�
#Nb of eel per segment for 100m� ??  
ccm$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverarea/100 #-->100m�
#ccm$nb_eel_per_segment<-ccm$pred_d*ccm$pred_mpa*ccm$riverareakm/(100/10^6) #-->100m�
print("total number of yellow eel")
print(sum(ccm$nb_eel_per_segment,na.rm=T)) # 15629244

#Total number of silver eel= total number of yellow eel * 
print("total number of silver eel")
print(sum(ccm$nb_eel_per_segment,na.rm=T)*0.05) # 781462.2
print("total water surface (km�)")
#print(sum(ccm$riverarea)/10^6) # water surface area in km2
print(sum(ccm$riverareakm,na.rm=T)) #d�j� en km�
print("number of siver eel per 100 m�")
print(100*sum(ccm$nb_eel_per_segment,na.rm=T)*0.05/sum(ccm$riverarea,na.rm=T)) # 2.04 anguille argent�e pour 100 m�
print("number of yellow eel per 100 m�")
print(100*sum(ccm$nb_eel_per_segment,na.rm=T)/sum(ccm$riverarea,na.rm=T))  # 40.8 anguille jaune / 100 m� densite moyenne predite

#Trois wso1_id o� pas de up_area : 342882, 350998, 347851
#Plusieurs segments sans pr�diction (NA, ou NaN)
