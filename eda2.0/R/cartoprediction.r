#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

# au pr�alable http://trac.eptb-vilaine.fr:8066/trac/wiki/CookBook%20pgsql2shp
# creation du shape pour ccm21.riversegment_france
# lancer dans dos :
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_France" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='France');"
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_Bretagne" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='Bretagne');"
source("EDACCM/init.r")
library(gam)
library(RColorBrewer)

#-----------------------------------#
# Reconstitution du jeu de donn�es
#-----------------------------------#
loadm<-function(file){
	path<-str_c(datawd,"/dataEDAccm/predictions/",file)
	load(path, envir = .GlobalEnv)
	
}
#load(file=paste(datawd,"/dataEDAccm/ccm.RData",sep=""))
loadm(file="ccmdata2.Rdata")
# chargement d'une des pr�dictions de mod�le 
loadm(file="ModelFranceUGA2.Rdata")  # ModelFranceUGAOK 
colnames(ModelFranceUGAOK)<-str_c("y",c(1984:2008))
 #str(ModelFranceUGAOK)  
 nrow(ccm) # 170744
 nrow(ModelFranceUGAOK)  # 170744
 ModelFrCCM<-cbind(ccm,ModelFranceUGAOK )  
#-----------------------------------#
# Chargement des couches SIG
#-----------------------------------#
 necessary = c('sp', 'maptools','rgdal')
 if(!all(necessary %in% installed.packages()[, 'Package']))
	 install.packages(c('sp', 'maptools','rgdal'), dep = T)
 library(sp)
 library(maptools)
 library(rgdal)
 source("EDACCM/fonctions_graphiques.r")
 library(lattice)
#-----------------------------------#
# Graphique des pr�dictions sur les segments
#-----------------------------------#
library(lattice)
dsn=paste(shpwd,"riversegments_Bretagne.shp",sep="")
rb<-readOGR(dsn,"riversegments_Bretagne")
#rb<-readShapeLines(dsn)
# ajout des colonnes dans le spatial, je remplace toutes les colonnes car la requete ne va chercher que le wso1_id
rb1<-mod_SpatialDataFrame(SpPDF=rb,  # un SpatialPolygonsDataFrame
		data= ModelFrCCM ,  #  un dataframe
		by.spdf="wso1_id",  # nom de la colonne contenant l'identifiant
		by.data="wso1_id",   # nom de la colone contenant l'indentifiant de data
		coldata=colnames(ModelFrCCM)[3:ncol(ModelFrCCM)])       # vecteur character des colonnes � int�ger
 # str(rb1@data)
#-----------------------------------#
# Graphique des pr�dictions sur les riversegments
#-----------------------------------#
#show.settings()
mypalette<-rev(brewer.pal(7,"Spectral"))
#trellis.par.get("regions") 
#mypalette<-rev(heat.colors(16))
x11(20,15)
trellis.par.set("regions" =list(col =mypalette)) # il faut associer les �l�ments lattice au fichier x11() ouvert
# construction d'une cl� (see ?xyplot)
#plot(rb1)
#rb1@data=complete.cases(rb1@data)
# colnames(rb1@data)
#spplot(rb1,c("y2008"))
spplot(rb1,
		c("y2008"),
		cuts=6,
		key=list(rectangles = Rows(trellis.par.get("regions"),
				c(1:6)), 
				text = list(lab = levels(cut(c(rb1@data$y2008),6)[!is.na(cut(c(rb1@data$y2008),6))])),
				title = "Densities/100 m2",
				cex.title=0.9),
		col.regions=trellis.par.get("regions")$col,
		key.space="right",                                                            
		cex=0.3)
# je ne comprends pas pourquoi celui l� ci dessous ne marche pas mais pas forc�ment utile, les sppoints r�agissent
# diff�remment des autes....
spplot(rb1,
		c("y2008","y1984"),
		cuts=6, # a list only for spatial point data.frame
		key=list(rectangles = Rows(trellis.par.get("regions"),
							c(1:6)), 
					text = list(lab = levels(cut(c(rb1@data$y2008,rb1@data$y1984),6)[!is.na(cut(c(rb1@data$y2008,rb1@data$y1984),6))])),
					title = "Densities/100 m2",
						cex.title=0.9),
				col.regions=trellis.par.get("regions"),
				key.space="right",                                                            
				cex=0.3)		



#-----------------------------------#
# Graphique des r�sulats du mod�le et des r�sidus
#-----------------------------------#

