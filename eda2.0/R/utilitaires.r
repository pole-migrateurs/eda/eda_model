# Nom fichier :        utilitaires
# Projet :             edacommun/init
# Organisme :          IAV
# Auteur :             C�dric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :   30/07/2007 10:58:37
# Compatibilite :      R 2.6.0
# Etat :               devt en cours
# Description     
    
# chnames : fonction remlace les noms des variables dans un data.frame
# tabcroisdynnb tabcroisdyn : tableaux crois�s dynamiques
# rightjoin, innerjoin  : fonctions de jointure sur cl�s primaires
# ind : fonction pour renvoyer les index dans b  des valeurs du vecteur a   
# indrepeated :  # retourne l'index des valeurs r�p�t�es d'un vecteur
# induk : renvoit l'index des valeurs apparaissant une seule fois
# progress  : progression dans les boucles (sans le bug)
# killfactor  : a utiliser �ventuellement apr�s un import ODBC

#' This function replaces the variable names in a data.frame
#' @param object a data frame
#' @param old_variable_name 
#' @param new_variable_name 
#' @returnType data.frame
#' @return object
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @export
chnames=function(object,
		old_variable_name,
		new_variable_name){
	if (length(old_variable_name)!=length(new_variable_name)) stop("les variables de remplacement doivent avoir le meme nombre que les variables de depart")
	if (!all(!is.na(match(old_variable_name,colnames(object))))) {
		stop(paste("les noms",paste(is.na(match(old_variable_name,colnames(object))),collapse="/"),"ne correspondent pas aux variables du tableau"))
	}
	colnames(object)[match(old_variable_name,colnames(object))]<- new_variable_name
	return(object)
}

# fonction  id tableau crois� dynamique
# permet de replacer des lignes correspondant � des champs r�p�t�s dans tab1,
# dans un tableau type tableau crois� dynamique (champ unique) tab2
# en faisant sum ou mean
tabcroisdyn=function(tab1,  # data.frame
                  tab2,     #data.frame
                  id_tab1,  #texte
                  val_tab1, #texte
                  id_tab2,  #texte
                  val_tab2, #texte
                  oper=sum, # ou mean ou NA pour nb
                  na.rm=TRUE) 
                  {
  valeur.niveaux=tapply(X=tab1[,val_tab1],INDEX=tab1[,id_tab1],FUN=oper,na.rm=na.rm)
  index=names(valeur.niveaux)%in% tab2[,id_tab2]
  index2=match(names(valeur.niveaux),tab2[,id_tab2])[index]
  tab2[index2,val_tab2]=valeur.niveaux[index] 
  return(tab2)
}   
# fonction  id tableau crois� dynamique
# permet de replacer des lignes correspondant � des champs r�p�t�s dans tab1,
# dans un tableau type tableau crois� dynamique (champ unique) tab2
# en faisant nb
tabcroisdynnb=function(tab1,  # data.frame
                  tab2,     #data.frame
                  id_tab1,  #texte
                  val_tab1, #texte
                  id_tab2,  #texte
                  val_tab2 #texte
                  ) 
                  {
  tab1[,"nb"]=rep(1,length=nrow(tab1))
  nb.niveaux=tapply(X=tab1[,"nb"],INDEX=tab1[,id_tab1],FUN=sum,na.rm=FALSE)
  index=names(nb.niveaux)%in% tab2[,id_tab2]
  index2=match(names(nb.niveaux),tab2[,id_tab2])[index]
  tab2[index2,val_tab2]=nb.niveaux[index]
  return(tab2)
}
# fonction pour renvoyer la position de a dans b
# match returns a vector of the positions of (first) matches of its first argument in its second.
# match(a,b)



#fonction pour renvoyer les index dans b  des valeurs du vecteur a
# b peut apparaitre plusieurs fois dans a
#attention le vecteur de r�sultat est dans le d�sordre
ind=function(a,b){
    index=(1:length(b))[b%in%a]
    return(index)
}

# retourne l'index des valeurs r�p�t�es d'un vecteur
indrepeated=function(a){
    sol=match(unique(a),a)     #index des valeurs uniques
    rep=a[-sol]  # valeurs r�p�t�es
    return(ind(rep,a))   # index des valeurs r�p�t�es
}
# renvoit l'index des valeurs apparaissant une seule fois
induk=function(a){
    sol=match(unique(a),a)     #inmdex des valeurs uniques
    return(sol)   # index des valeurs r�p�t�es
}

# value=25458
# maxvalue=100000
# version corrig�e de progress (qui n'efface plus pour des nombres sup�rieurs � 10000)
progress=function (value, max.value = NULL) 
{
    if (!is.numeric(value)) 
        stop("`value' must be numeric!")
    if (is.null(max.value)) {
        max.value <- 100
        percent <- TRUE
    }
    else percent <- FALSE
    if (!is.numeric(max.value)) 
        stop("`max.value' must be numeric or NULL!")
    erase.only <- (value > max.value)
    max.value <- as.character(round(max.value))
    l <- nchar(formatC(max.value, format="d")) # modif c�dric qui n'aime que les integer
    
    value <- formatC(round(value),format="d", width = l)
    if (percent) {
        backspaces <- paste(rep("\b", l + 18), collapse = "")
        if (erase.only) 
            message <- ""
        else message <- paste("Progression: ", value, "%  ", sep = "")
        cat(backspaces, message, sep = "")
    }
    else {
        backspaces <- paste(rep("\b", 2 * l + 20), collapse = "")
        if (erase.only) 
            message <- ""
        else message <- paste("Progression: ", value, " sur ", max.value, 
            "  ", sep = "")
        cat(backspaces, message, sep = "")
    }
    if (.Platform$OS.type == "windows") 
        flush.console()
    invisible(NULL)
}


# pour killer les facteurs
killfactor=function(df){
    for (i in 1:ncol(df))
    {
    if(is.factor(df[,i])) df[,i]=as.character(df[,i])
    }
 return(df)
}

# Description :         ajoute deux data frame l'un derri�re l'eau
ajout_data=function(data1,data2)
{
  data=data1
  data[(dim(data1)[1]+1):(dim(data1)[1]+dim(data2)[1]),]=data2
  return(data)
}


#test de chaque effet d'un mod�le (VGAM)
test_variable=function(modele,y,donnees,famille,...)
{
  liste_variable=attr(terms(modele),"term.labels")
  reponse=data.frame(variable=c("complet",liste_variable), vraisemblance=NA, df=NA,p=NA)
  reponse[1,"vraisemblance"]=logLik(modele)
  reponse[1,"df"]=df.residual(modele)
  for(i in 1:length(liste_variable))
  {
    modele_variable=vgam(as.formula(paste(y,"~",paste(liste_variable[-i],collapse="+"),sep="")),data=donnees,family=famille,...)
    reponse[i+1,"vraisemblance"]=logLik(modele_variable)
    reponse[i+1,"df"]=df.residual(modele_variable)
    reponse[i+1,"p"]=1 - pchisq(reponse[1,"vraisemblance"] - reponse[i+1,"vraisemblance"],df=reponse[i+1,"df"] - reponse[1,"df"])
  }
  reponse$AIC=-2*reponse$vraisemblance+2*reponse$df
  return(reponse)
}


#test de chaque effet d'un mod�le (GAM)
test_variable_gam=function(modele,y,donnees,famille,...)
{
  liste_variable=attr(terms(modele),"term.labels")
  reponse=data.frame(variable=c("complet",liste_variable), vraisemblance=NA, df=NA,p=NA,AIC=NA)
  reponse[1,"vraisemblance"]=logLik(modele)
  reponse[1,"df"]=df.residual(modele)
  reponse[1,"AIC"]=AIC(modele)
  for(i in 1:length(liste_variable))
  {
    modele_variable=gam(as.formula(paste(y,"~",paste(liste_variable[-i],collapse="+"),sep="")),data=donnees,family=famille,...)
    reponse[i+1,"vraisemblance"]=logLik(modele_variable)
    reponse[i+1,"df"]=df.residual(modele_variable)
    reponse[i+1,"p"]=ifelse(is.na(anova(modele,modele_variable,test="F")$"Pr(>F)"[2]),NA,anova(modele,modele_variable,test="F")$"Pr(>F)"[2])
    reponse[i+1,"AIC"]=AIC(modele_variable)
  }
  return(reponse)
}

#pour extraire les pr�dictions d'un param
# pour les variables num�riques, on peut soit faire du quantile option var_num_complete=FALSE ou avoir toutes les valeurs num�riques : var_num_complete=TRUE
prediction=function(modele,jeu_init,variable, var_num_complete=FALSE)
{
  nom_variable=names(jeu_init)
  variable_modele=logical(length(nom_variable))
  for(i in 1:length(nom_variable))
  {
    variable_modele[i]=(length(grep(nom_variable[i],attr(terms(modele),"term.labels")))>0)
  }

  variable_a_examiner=switch(mode(jeu_init[,variable]), logical = c(TRUE,FALSE), numeric = if(var_num_complete) sort(unique(jeu_init[,variable])) else quantile(jeu_init[,variable],prob=seq(0,1,0.05)), character =unique(jeu_init[,variable]))
  
  jeu_prediction=jeu_init[1:length(variable_a_examiner),nom_variable[variable_modele]]

  for(i in 1:length(nom_variable))
  {
    if(variable_modele[i])
    {
      jeu_prediction[,nom_variable[i]]=switch(mode(jeu_init[,i]), logical = FALSE, numeric = median(jeu_init[,i]), character = names(which.max(table(jeu_init[,i]))))
      if(nom_variable[i]=="surface_station") jeu_prediction[,nom_variable[i]]=100
    }
  }

  jeu_prediction[,variable]=variable_a_examiner
  
  jeu_prediction$p = predict(modele,jeu_prediction,type="response")
  
  return(jeu_prediction[,c(variable,"p")])
  
}

concat=function(vec){
if (class(vec)!="character") STOP("ce fn ne marche qu'avec des caracteres" )
concat=vec[1]
for (i in 2:length(vec)){
concat=paste(concat,vec[i])
}
return(concat)
}


ex<-function(d=NULL){
if (is.null(d)){
xl=tk_select.list(ls(envir=globalenv()), preselect = NULL, multiple = FALSE, title = "choisir l'object")
write.table(get(xl),"clipboard",sep="\t",col.names=NA)
} else {
write.table(d,"clipboard",sep="\t",col.names=NA)
}
}

load_library=function(necessary) {
	if(!all(necessary %in% installed.packages()[, 'Package']))
		install.packages(necessary[!necessary %in% installed.packages()[, 'Package']], dep = T)
	for(i in 1:length(necessary))
		library(necessary[i], character.only = TRUE)
}


corvif <- function(dataz) {
	dataz <- as.data.frame(dataz)
	#correlation part
	#cat("Correlations of the variables\n\n")
	#tmp_cor <- cor(dataz,use="complete.obs")
	#print(tmp_cor)
	
	#vif part
	form    <- formula(paste("fooy ~ ",paste(strsplit(names(dataz)," "),collapse=" + ")))
	dataz   <- data.frame(fooy=1,dataz)
	lm_mod  <- lm(form,dataz)
	
	cat("\n\nVariance inflation factors\n\n")
	print(myvif(lm_mod))
}


#Support function for corvif. Will not be called by the user
myvif <- function(mod) {
	v <- vcov(mod)
	assign <- attributes(model.matrix(mod))$assign
	if (names(coefficients(mod)[1]) == "(Intercept)") {
		v <- v[-1, -1]
		assign <- assign[-1]
	} else warning("No intercept: vifs may not be sensible.")
	terms <- labels(terms(mod))
	n.terms <- length(terms)
	if (n.terms < 2) stop("The model contains fewer than 2 terms")
	if (length(assign) > dim(v)[1] ) {
		diag(tmp_cor)<-0
		if (any(tmp_cor==1.0)){
			return("Sample size is too small, 100% collinearity is present")
		} else {
			return("Sample size is too small")
		}
	}
	R <- cov2cor(v)
	detR <- det(R)
	result <- matrix(0, n.terms, 3)
	rownames(result) <- terms
	colnames(result) <- c("GVIF", "Df", "GVIF^(1/2Df)")
	for (term in 1:n.terms) {
		subs <- which(assign == term)
		result[term, 1] <- det(as.matrix(R[subs, subs])) * det(as.matrix(R[-subs, -subs])) / detR
		result[term, 2] <- length(subs)
	}
	if (all(result[, 2] == 1)) {
		result <- data.frame(GVIF=result[, 1])
	} else {
		result[, 3] <- result[, 1]^(1/(2 * result[, 2]))
	}
	invisible(result)
}

# funciton to clean sql code when loaded externally
fnclean <- function(sql){
  sql <- gsub("\r", "", sql)
  sql <- gsub("\n", " ", sql)
  sql <- gsub("\t", "", sql)
  return(sql)
}