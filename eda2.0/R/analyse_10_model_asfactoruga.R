# 
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################


gam1<-gam(d~annee:as.factor(uga)+s(elev_mean),data=ers[ers$totalnumber>0,],family=Gamma(link=log));summary(gam1)

md210uga<-gam(d~annee:as.factor(uga)+s(elev_mean)+s(distance_sea)+cs_nbdams:as.factor(uga)+s(p_up_no_impact)+s(p_up_urban),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md211uga<-gam(d~annee:uga+s(elev_mean)+s(distance_sea)+cs_nbdams:uga+s(p_up_no_impact)+s(p_up_urban),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md212uga<-gam(d~s(annee:uga)+s(elev_mean)+s(distance_sea)+cs_nbdams:uga+s(p_up_no_impact)+s(p_up_urban),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md213uga<-gam(d~s(annee:UGA1)+s(elev_mean)+s(distance_sea)+s(cs_nbdams:UGA1)+s(p_up_no_impact)+s(p_up_urban),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md212uga<-gam(d~annee:uga+s(elev_mean)+s(distance_sea)+cs_nbdams:uga+s(p_up_no_impact)+s(p_up_urban),data=ers[ers$totalnumber>0,],family=Gamma(link=log))

md212uga<-gam(d~s(annee,by=UGA1,bs="cr")+
				s(annee,by=UGA2,bs="cr")+
				s(annee,by=UGA3,bs="cr")+
				s(annee,by=UGA4,bs="cr")+
				s(annee,by=UGA5,bs="cr")+
				s(annee,by=UGA6,bs="cr")+
				s(annee,by=UGA11,bs="cr")+
				s(annee,by=UGA9,bs="cr")+
				s(annee,by=UGA10,bs="cr")+
				s(elev_mean)+s(distance_sea)+s(p_up_no_impact)+s(p_up_urban)+
				s(cs_nbdams,by=UGA1,bs="cr")+
				s(cs_nbdams,by=UGA1,bs="cr")+
				s(cs_nbdams,by=UGA3,bs="cr")+
				s(cs_nbdams,by=UGA4,bs="cr")+
				s(cs_nbdams,by=UGA5,bs="cr")+
				s(cs_nbdams,by=UGA6,bs="cr")+
				s(cs_nbdams,by=UGA11,bs="cr")+
				s(cs_nbdams,by=UGA9,bs="cr")+
				s(cs_nbdams,by=UGA10,bs="cr")
				,data=ers[ers$totalnumber>0,],family=Gamma(link=log))

md212uga<-gam(d~s(annee,by=UGA1,bs="cr")+
				s(annee,by=UGA2,bs="cr")+
				s(annee,by=UGA3,bs="cr")+
				s(annee,by=UGA4,bs="cr")+
				s(annee,by=UGA5,bs="cr")+
				s(annee,by=UGA6,bs="cr")+
				s(annee,by=UGA11,bs="cr")+
				s(annee,by=UGA9,bs="cr")+
				s(annee,by=UGA10,bs="cr")+
				s(elev_mean)+s(distance_sea)+s(p_up_no_impact)+s(p_up_urban)+
				s(cs_nbdams),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
summary(md212uga)

md212uga<-gam(d~s(annee,by=as.factor(uga),bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr")+
				s(cs_nbdams,bs="cr")
		,data=ers[ers$totalnumber>0,],family=Gamma(link=log))

plot(md212uga,se=T,select=1,scale=0)
x11();par(mfrow=c(2,3));plot(md212uga,se=T,pages=1)
# 

md212uga<-gam(d~s(annee,by=as.factor(uga),bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr")+
				s(cs_nbdams,by=as.factor(uga),bs="cr")
		,data=ers[ers$totalnumber>0,],family=Gamma(link=log))

md212uga<-gam(d~s(annee,by=as.factor(fUGA),bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr")+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr")
		,data=ers[ers$totalnumber>0,],family=Gamma(link=log))
summary(md212uga)

detach("package:gam")
library(mgcv)
mpa219uga<-gam(totalnumber!=0~s(annee,by=as.factor(fUGA),bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+s(distance_source,bs="cr")+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr")+
				s(p_up_agricultural,bs="cr")+s(p_up_urban,bs="cr"),data=ers,family=binomial())
mpa219uga

md210uga<-gam(d~s(annee,by=as.factor(fUGA),bs="cr")+
			 s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+
			 s(cs_nbdams,by=as.factor(fUGA),bs="cr")+
			 s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr"),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md210uga

md210sansuga<-gam(d~s(annee,bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+
				s(cs_nbdams,bs="cr")+
				s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr"),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md210ugaannee<-gam(d~s(annee,by=as.factor(fUGA),bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+
				s(cs_nbdams,bs="cr")+
				s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr"),data=ers[ers$totalnumber>0,],family=Gamma(link=log))
md210ugadams<-gam(d~s(annee,bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr")+
				s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr"),data=ers[ers$totalnumber>0,],family=Gamma(link=log))

mpa219sansuga<-gam(totalnumber!=0~s(annee,bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+s(distance_source,bs="cr")+
				s(cs_nbdams,bs="cr")+
				s(p_up_agricultural,bs="cr")+s(p_up_urban,bs="cr"),data=ers,family=binomial())

#M�thode REML ou GCV - meilleur mod�le avec GCV
md210sansuga2<-gam(d~s(annee,bs="cr",k=5)+
				s(elev_mean,bs="cr",k=5)+s(distance_sea,bs="cr",k=5)+
				s(cs_nbdams,bs="cr",k=5)+
				s(p_up_no_impact,bs="cr",k=5)+s(p_up_urban,bs="cr",k=5),data=ers[ers$totalnumber>0,],family=Gamma(link=log),method="REML")
#En limitant le nombre de ddl
md210ugak5<-gam(d~s(annee,by=as.factor(fUGA),bs="cr",k=5)+
				s(elev_mean,bs="cr",k=5)+s(distance_sea,bs="cr",k=5)+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr",k=5)+
				s(p_up_no_impact,bs="cr",k=5)+s(p_up_urban,bs="cr",k=5),data=ers[ers$totalnumber>0,],family=Gamma(link=log),method="REML")
md210ugak3<-gam(d~s(annee,by=as.factor(fUGA),bs="cr",k=3)+
				s(elev_mean,bs="cr",k=3)+s(distance_sea,bs="cr",k=3)+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr",k=3)+
				s(p_up_no_impact,bs="cr",k=3)+s(p_up_urban,bs="cr",k=3),data=ers[ers$totalnumber>0,],family=Gamma(link=log),method="REML")
md210ugak4<-gam(d~s(annee,by=as.factor(fUGA),bs="cr",k=4)+
				s(elev_mean,bs="cr",k=4)+s(distance_sea,bs="cr",k=4)+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr",k=4)+
				s(p_up_no_impact,bs="cr",k=4)+s(p_up_urban,bs="cr",k=4),data=ers[ers$totalnumber>0,],family=Gamma(link=log),method="REML")

md210ugak45<-gam(d~s(annee,by=as.factor(fUGA),bs="cr",k=4)+
				s(elev_mean,bs="cr",k=3)+s(distance_sea,bs="cr",k=4)+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr",k=4)+
				s(p_up_no_impact,bs="cr",k=5)+s(p_up_urban,bs="cr",k=5),data=ers[ers$totalnumber>0,],family=Gamma(link=log),method="REML")
mpa219ugak3<-gam(totalnumber!=0~s(annee,by=as.factor(fUGA),bs="cr",k=3)+
				s(elev_mean,bs="cr",k=3)+s(distance_sea,bs="cr",k=3)+s(distance_source,bs="cr",k=3)+
				s(cs_nbdams,by=as.factor(fUGA),bs="cr",k=3)+
				s(p_up_agricultural,bs="cr",k=3)+s(p_up_urban,bs="cr",k=3),data=ers,family=binomial())


x11();par(mfrow=c(2,3));plot(md210sansuga2,se=T)
x11();par(mfrow=c(2,3));plot(md210sansuga,se=T)
AIC(md210sansuga)
AIC(md210sansuga2)
summary(md210sansuga)
summary(md210sansuga2)

md210sansuga3<-gam(d~s(annee,bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")+
				s(cs_nbdams,bs="cr")+
				s(p_up_no_impact,bs="cr")+s(p_up_urban,bs="cr"),data=ers[ers$totalnumber>0,],family=Gamma(link=log))




#Comparaison mod�le avec ou sans interraction
#Pr�sence-abscence
anova(mpa219sansuga,mpa219uga,test="F")
#Densit�
anova(md210sansuga,md210uga,test="F")
anova(md210sansuga,md210ugadams,test="F")
anova(md210sansuga,md210ugaannee,test="F")
anova(md210ugaannee,md210uga,test="F")
anova(md210ugadams,md210uga,test="F")

acf(residuals(md210uga))
acf(residuals(mpa219uga))
gam.check(md210uga)
gam.check(mpa219uga)


model<-mpa219uga
model<-md210uga
model<-md210ugak45
for(i in seq(1,length(model$smooth),by=6)){
x11();par(mfrow=c(2,3))
plot(model,se=T,select=i,scale=0)
plot(model,se=T,select=i+1,scale=0)
plot(model,se=T,select=i+2,scale=0)
plot(model,se=T,select=i+3,scale=0)
plot(model,se=T,select=i+4,scale=0)
plot(model,se=T,select=i+5,scale=0)
}

#Model Validation





library(lattice)
xyplot(ers$totalnumber~ers$annee|ers$uga,xlab="Year",ylab="Effectif",
		layout=c(3,4),scales=list(alternating=T,x=list(relation = "same"),
				y = list(relation = "free")),
		panel=function(x,y){
			panel.xyplot(x,y,col=1)
			panel.grid(h=-1,v=2)
			panel.loess(x,y,col=1,span=0.5)
			I2<-is.na(y)
			panel.text(x[I2],min(y,na.rm=T),'|',cex=0.5)  })



md212uga<-gam(d~s(annee,bs="cr")+
				s(elev_mean,bs="cr")+s(distance_sea,bs="cr")
		,data=ers[ers$totalnumber>0,],family=Gamma(link=log))

