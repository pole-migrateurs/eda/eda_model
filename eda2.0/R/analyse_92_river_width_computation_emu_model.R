# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

ersRW<-data.frame(ers_full0,row.names=1:dim(ers_full0)[1])
# Modele 1 : width~catchment_area
lm1<-lm(op_cs_largeurlameeau~up_area,data=ersRW)

# Modele 2 : log(width)~log(catchment_area)
lm2<-lm(log(op_cs_largeurlameeau)~log(up_area),data=ersRW)

# Modele 3 : log(width)~log(catchment_area)+ uga
lm3<-lm(log(op_cs_largeurlameeau)~log(up_area)+uga,data=ersRW)

# Modele 4 : log(width)~log(catchment_area)+ log(shreve index)
lm4<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree),data=ersRW)
# Modele 41 : log(width)~log(catchment_area)+ log(strahler index)
lm41<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler),data=ersRW)

# Modele 5 : log(width)~log(catchment_area)+ log(shreve index) + uga 
lm5<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree)+uga,data=ersRW)
# Modele 51 : log(width)~log(catchment_area)+ log(strahler index) + uga 
lm51<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler)+uga,data=ersRW)

# Modele 6 : log(width)~log(catchment_area)*uga+ log(shreve index)*uga 
lm6<-lm(log(st_cs_largeurlameeau)~log(st_up_area):st_uga+log(shreve):st_uga,data=ers0_station)

###############################################################
# BEST MODEL  avec la largeur en km !!
#ers0_station.RData provient de analyse_9_riverwidth_computation.R
load(file=paste(datawd,"/dataEDAccm/ers0_station.RData",sep=""))
ers_station<-chnames(ers0_station,old_variable_name=c("st_up_area","st_uga","shreve"),new_variable_name=c("up_area","uga","shree"))
ers_station$uga=as.factor(ers_station$uga)
ers_station$st_cs_largeurlameeau<-ers_station$st_cs_largeurlameeau  #m  (op_cs_largeurlameeau est en m !!)
lm_rw<-lm(log(st_cs_largeurlameeau)~log(up_area):uga+log(shree):uga,data=ers_station)
save(lm_rw,ers_station,file=str_c(datawd,"/dataEDAccm/predictions/riverwidthmodel",".Rdata"))
# Modele 61 : log(width)~log(catchment_area)*uga+ log(strahler index)*uga 
lm61<-lm(log(op_cs_largeurlameeau)~log(up_area):uga+log(strahler):uga,data=ersRW)

# Modele 7 : log(width)~log(catchment_area)*uga+ log(shreve index)*uga 
lm7<-lm(log(op_cs_largeurlameeau)~log(up_area)*uga+log(shree)*uga,data=ersRW)
# Modele 71 : log(width)~log(catchment_area)*uga+ log(strahler index)*uga 
lm71<-lm(log(op_cs_largeurlameeau)~log(up_area)*uga+log(strahler)*uga,data=ersRW)

# Modele 8 : log(width)~log(catchment_area)*uga*month+ log(shreve index)*uga
lm8<-lm(log(op_cs_largeurlameeau)~log(up_area):uga:month+log(shree):uga,data=ersRW)
# Modele 81 : log(width)~log(catchment_area)*uga*month+ log(strahler index)*uga
lm81<-lm(log(op_cs_largeurlameeau)~log(up_area):uga:month+log(strahler):uga,data=ersRW)

# Modele 9 : log(width)~log(catchment_area)+ log(shreve index)+offset(month) 
lm9<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree),offset=month,data=ersRW)
# Modele 91 : log(width)~log(catchment_area)+ log(strahler index)+offset(month) 
lm91<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler),offset=month,data=ersRW)

# Modele 10 : log(width)~log(catchment_area)*uga*month+ log(shreve index)+month 
lm10<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree)+month,data=ersRW)
# Modele 101 : log(width)~log(catchment_area)*uga*month+ log(shreve index)+month 
lm101<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler)+month,data=ersRW)

# Modele 11 : log(width)~log(catchment_area)
lm11<-lm(op_cs_largeurlameeau~shree,data=ersRW)
# Modele 12 : log(width)~log(catchment_area)
lm12<-lm(log(op_cs_largeurlameeau)~log(shree),data=ersRW)
# Modele 111 : log(width)~log(catchment_area)
lm111<-lm(op_cs_largeurlameeau~strahler,data=ersRW)
# Modele 121 : log(width)~log(catchment_area)
lm121<-lm(log(op_cs_largeurlameeau)~log(strahler),data=ersRW)

#Pour le mod�le lm6 utilis�
save(lm6,file=str_c(datawd,"/dataEDAccm/predictions/lm6.Rdata"))
AIC(lm1,lm2,lm3,lm4,lm41,lm5,lm51,lm6,lm61,lm7,lm71,lm8,lm81,lm9,lm91,lm10,lm101,lm11,lm111,lm12,lm121)


# Modele 6 : log(width)~log(catchment_area)*uga+ log(shreve index)*uga 
lm6<-lm(log(op_cs_largeurlameeau)~log(up_area):uga+log(shree):uga,data=ersRW)
model<-lm6
par(mfrow=c(2,2));plot(model)
x11()
plot(model)

#D�tection des points influents
inflm.SR<-influence.measures(model)
length(which(apply(inflm.SR$is.inf, 1, any)))

r1<-rstandard(model)
r2<-rstudent(model)
rmodel<-data.frame(ersRW$st_id,r1,r2)
rmodel
unique(rmodel$ersRW.st_id[rmodel$r2>2])

#Influence
ginf<-influence(model)
library(lmtest)
library(faraway)
halfnorm(lm.influence(model)$hat,lab=ersRW$st_id)
cook <- cooks.distance(model)
halfnorm(cook,3,labs=ersRW$st_id,ylab="Cook's distances")
plot(ginf$coef[,2])
identify(1:length(ginf$coef[,2]),ginf$coef[,2],ersRW$st_id)


#    TTODO cr�ation d'une colonne pour r�cup�rer les codecsp influents
#et visualiser sur un graphique avec une couleur diff�rente


#Mod�le pour chaque UGA
ersRW<-split(ersRW,ersRW$uga)
for(i in 1:length(levels(as.factor(ers_full$uga)))){
# Modele 1 : width~catchment_area
lm1<-lm(op_cs_largeurlameeau~up_area,data=ersRW[[i]])

# Modele 2 : log(width)~log(catchment_area)
lm2<-lm(log(op_cs_largeurlameeau)~log(up_area),data=ersRW[[i]])

# Modele 4 : log(width)~log(catchment_area)+ log(shreve index)
lm4<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree),data=ersRW[[i]])
# Modele 41 : log(width)~log(catchment_area)+ log(strahler index)
lm41<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler),data=ersRW[[i]])

# Modele 9 : log(width)~log(catchment_area)+ log(shreve index)+offset(month) 
lm9<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree),offset=month,data=ersRW[[i]])
# Modele 91 : log(width)~log(catchment_area)+ log(strahler index)+offset(month) 
lm91<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler),offset=month,data=ersRW[[i]])

# Modele 10 : log(width)~log(catchment_area)*uga*month+ log(shreve index)+month 
lm10<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree)+month,data=ersRW[[i]])
# Modele 101 : log(width)~log(catchment_area)*uga*month+ log(shreve index)+month 
lm101<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(strahler)+month,data=ersRW[[i]])

# Modele 11 : log(width)~log(catchment_area)
lm11<-lm(op_cs_largeurlameeau~shree,data=ersRW[[i]])
# Modele 12 : log(width)~log(catchment_area)
lm12<-lm(log(op_cs_largeurlameeau)~log(shree),data=ersRW[[i]])
# Modele 111 : log(width)~log(catchment_area)
lm111<-lm(op_cs_largeurlameeau~strahler,data=ersRW[[i]])
# Modele 121 : log(width)~log(catchment_area)
lm121<-lm(log(op_cs_largeurlameeau)~log(strahler),data=ersRW[[i]])

print(levels(as.factor(ersRW[[i]]$uga)))
print(AIC(lm1,lm2,lm4,lm41,lm9,lm91,lm10,lm101,lm11,lm111,lm12,lm121))
}

ersRW1<-data.frame(ers_full0,row.names=1:dim(ers_full0)[1])

i<-10
lm4<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree),data=ersRW[[i]])
lm10<-lm(log(op_cs_largeurlameeau)~log(up_area)+log(shree)+month,data=ersRW[[i]])
plot(lm4)
plot(lm10)

model<-lm4
model<-lm10
#D�tection des points influents
inflm.SR<-influence.measures(model)
length(which(apply(inflm.SR$is.inf, 1, any)))

r1<-rstandard(model)
r2<-rstudent(model)
rmodel<-data.frame(ersRW[[i]]$st_id,r1,r2)
rmodel
unique(rmodel$"ersRW..i...st_id"[rmodel$r2>2])

#Influence
ginf<-influence(model)
library(lmtest)
library(faraway)
halfnorm(lm.influence(model)$hat,lab=ersRW$st_id)
cook <- cooks.distance(model)
halfnorm(cook,3,labs=ersRW$st_id,ylab="Cook's distances")
plot(ginf$coef[,2])
identify(1:length(ginf$coef[,2]),ginf$coef[,2],ersRW[[i]]$st_id)

ersRW1[c(3626,4766,5494,4862,5335),c("st_id","gid","uga","annee","op_cs_largeurlameeau","up_area")]
