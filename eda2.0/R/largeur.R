plot(ers_full$distance_source_station,ers_full$largeur_station)
plot(ers_full$pente,ers_full$largeur_station)
plot(ers_full$altitude,ers_full$largeur_station)
plot(ers_full$distance_source_station+ers_full$distance_mer,ers_full$largeur_station)
ers_full$longueur_riv=ers_full$distance_source_station+ers_full$distance_mer
ers_full$dist_rel=ers_full$distance_mer/ers_full$longueur_riv
plot(ers_full$dist_rel,ers_full$largeur_station)

require(gam)
modele=gam(largeur_station~s(distance_source_station),data=ers_full)