# 
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################
# Pour tester la validit� des mod�les candidats, des approches compl�mentaires � l'AIC sont :
# - l'analyse des r�sidus (se basant sur des m�thodes d'analyse graphique) 
# pour v�rifier si les r�sidus ne pr�sentent aucune structure particuli�re
# - L�analyse�de�la�variance�qui�consiste��expliquer�la�variance�totale�
# en�fonction�de�la�variance�due�aux�facteurs�(la�variance�expliqu�e�par�le�mod�le),�
# de�la�variance�due��l�interaction�entre�les�facteurs�et�de�la�variance�r�siduelle�al�atoire�(la�variance�non�expliqu�e�par�le�mod�le)
#,�et�puis�de�comparer�ces�variances.
# -Les�tests�d�hypoth�ses,�essentiellement�ceux�qui�se�basent�sur�la�statistique�de�Wald,
#�permettant�ainsi�de�tester�la�significativit�des�param�tres.


mod1_densite<-md210
mod1_densite<-mpa219

#Fonction pour l'analyse du mod�le
gamcheck<-function(mod1_densite){
	x11();par(mfrow=c(2,5));plot.gam(mod1_densite,se=T,ask=FALSE)
	x11();par(mfrow=c(3,3));plot(mod1_densite,residuals=T)
	#x11();test_distribution(mod1_densite,graph=1)
	#x11();test_distribution(mod1_densite,graph=2)
	
	#QQplot or histogram of the residuals for normality
	x11();hist(residuals(mod1_densite))
	#Residuals versus fitted values to verify homogeneity
	x11();plot(fitted(mod1_densite),residuals(mod1_densite))

	x11();plot(mod1_densite$y,residuals(mod1_densite))
	x11();plot(mod1_densite$data$d,mod1_densite$data$d-fitted(mod1_densite))
	#Spearman correlation between fitted and observed values
	cor.test(mod1_densite$data$d,fitted(mod1_densite),method="spearman",exact=TRUE)
	
	cor.test(mod1_densite$data$d,mod1_densite$data$d-fitted(mod1_densite),method="spearman",exact=TRUE)
	
	mdlm<-lm(mod1_densite$data$d~fitted(mod1_densite))
	plot(mod1_densite$data$d~fitted(mod1_densite))
	lines(mdlm)
	
	x11();plot(mod1_densite$data$d,fitted(mod1_densite))
	x11();plot(mod1_densite$data$totalnumber,fitted(mod1_densite))
	
	#AIC
	aic=AIC(mod1_densite)
	aic
	#Pourcentage de d�viance
	df=mod1_densite$df.residual
	#D�vianceExpliqu�e=1-D�vianceR�siduelle/D�vianceNulle
	#Le degr� de concordance entre les observations et les valeurs ajust�es par le mod�le (Pearce & Ferrier 2000) est mesur�
	#Plus un mod�le explique convenablement les variations de la r�ponse et plus la proportion de d�viance expliqu�e est proche de 1.
	pdev=1-(deviance(mod1_densite)/mod1_densite$null.deviance)
	print(c("pdev",pdev))
}



#Residuals versus each explanatory variable to check independence
plot(residuals(md210)~as.factor(ers[ers$totalnumber >0,"annee"]))
plot(residuals(md210)~ers[ers$totalnumber >0,"elev_mean"])
plot(residuals(md210)~ers[ers$totalnumber >0,"distance_sea"])

library(gam)
md210lo<-gam(d~lo(annee,span=0.5)+lo(elev_mean,span=0.5)+lo(distance_sea,span=0.5)+lo(cs_nbdams,span=0.5)+lo(p_up_no_impact,span=0.5)+lo(p_up_urban,span=0.5),data=ers[ers$totalnumber>0,])

#Pour chaque UGA - Mod�le global France  -- pas test de v�rification (possible d�tecter valeur �trange)
ugaList<-levels(as.factor(ccm$uga))
gamcheckuga<-function(mod1_densite){
for(i in 1:length(ugaList)){
	x11();plot(mod1_densite$data$totalnumber[mod1_densite$data$uga%in%ugaList[i]],fitted(mod1_densite[mod1_densite$data$uga%in%ugaList[i]]))
}
