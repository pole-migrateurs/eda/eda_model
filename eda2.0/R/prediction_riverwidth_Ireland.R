# River width for Western EMU based on a raster layer
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################
# initial model by elvira
#Log10(Wet width + 1) = 0.22734+ 0.20045 (log10 catchment area) + 0.25939 (log 10 Shreve index)
#ccm$riverwidth<-((10^0.22734)*((ccm$up_area*100)^0.20045)*(ccm$shree^0.25939))-1  #m
#ccm$riverwidth[ccm$shree==1]<-0.8  #m


#####################
# Data load and transform
######################
if(getUsername() == 'cedric.briand')
{
	options(sqldf.RPostgreSQL.user = "postgres", 
			sqldf.RPostgreSQL.password = passworddistant,
			sqldf.RPostgreSQL.dbname = "eda2",
			sqldf.RPostgreSQL.host = "w3.eptb-vilaine.fr",#, w3.eptb-vilaine.fr # 1.100.1.6
			sqldf.RPostgreSQL.port = 5432)
	
}
# check your username
if(getUsername() == 'edeeyto')
{
	options(sqldf.RPostgreSQL.user = "postgres", 
			sqldf.RPostgreSQL.password = "Desktop12",
			sqldf.RPostgreSQL.dbname = "eda2",
			sqldf.RPostgreSQL.host = "localhost", 
			sqldf.RPostgreSQL.port = 5432)

}


join_emu_wso<-sqldf("select * from ireland.join_emu_wso")
# two problem
# first the layer joining wso and emu is missing 3 wso_id which correspond
# to 11 lines in the ccm dataframe.
# we use a subset from the ccm where these lines are missing (as there wont be any emu_name_short for those lines)
#second the join_emu_wso has some repeated lines I don(t know why) . So we remove them before the joining.
rep<-indrepeated(join_emu_wso$wso_id) # 4 lines
join_emu_wso<-join_emu_wso[-rep,]
indexwithemu<-(ccm$wso_id%in%join_emu_wso$wso_id)
sum(!indexwithemu)

join_emu_wso<-join_emu_wso
require(Hmisc)
ccm$riverwidth<-ccm$western_width

#bas<-ccm[which(ccm$nextdownid==-9999),c("wso_id","up_area")]
#bas<-chnames(bas,"up_area","basin_area")
# tried wccm0$p_mod_calc_surf didn't improve the model
nrow(ccm)

wccm0<-ccm[indexwithemu,c("riverwidth","shree","strahler","distance_source","up_area","p_non_calc_surf","p_very_calc_surf","wso_id","rain_mean")]
#wccm0<-merge(wccm0,bas,by.x="wso_id",by.y="wso_id",all.x=TRUE,all.y=FALSE)
nrow(wccm0)
wccm0$lshree<-log(wccm0$shree)
wccm0$lr<-log(wccm0$riverwidth)
wccm0$lup<-log(wccm0$up_area)
#wccm0$lbasin_area<-log(wccm0$basin_area)
wccm0$wso_id<-as.factor(wccm0$wso_id)
wccm0$strahler<-as.factor(wccm0$strahler)
wccm0$basin<-str_c("B",as.numeric(as.character(wccm0$wso_id)))
wccm0$basin[wccm0$basin_area<1000]<-"S"
wccm0$rain_mean[wccm0$rain_mean==0]<-mean(wccm0$rain_mean) # 1 value
wccm0<-merge(wccm0,join_emu_wso[,c(1,3,4,5)])
##Now only the subdataset
wccm0$emu_name_short[wccm0$emu_name_short=="GB_Neag"]<-"IE_East"
wccm0$emu_name_short<-as.factor(wccm0$emu_name_short)
wccm<-wccm0[complete.cases(wccm0),] # 267 values
str(wccm)

#######################
# DATA DESCRIPTION
##################

describe(wccm0)
x11()
op<-par(mfrow=c(3,2),mar=c(3,3,3,1))
dotchart(wccm0$lshree,main="lshree",group=wccm0$strahler)
dotchart(wccm0$distance_source,main="distance_source",group=wccm0$strahler)
dotchart(wccm0$lup,main="lup",group=wccm0$strahler)
dotchart(wccm0$lr,main="log(river width)",group=wccm0$strahler)
dotchart(wccm0$p_non_calc_surf,main="p_non_calc_surf",group=wccm0$strahler)
dotchart(wccm0$p_non_calc_surf,main="p_very_calc_surf",group=wccm0$strahler)
dotchart(wccm0$rain_mean,main="rain_mean",group=wccm0$strahler)

describe(wccm)
x11()
op<-par(mfrow=c(3,2),mar=c(3,3,3,1))
dotchart(wccm$lshree,main="lshree",group=wccm$strahler)
dotchart(wccm$distance_source,main="distance_source",group=wccm$strahler)
dotchart(wccm$lup,main="lup",group=wccm$strahler)
dotchart(wccm$lr,main="log(river width)",group=wccm$strahler)
dotchart(wccm$p_non_calc_surf,main="p_non_calc_surf",group=wccm$strahler)
dotchart(wccm$p_non_calc_surf,main="p_very_calc_surf",group=wccm$strahler)
dotchart(wccm$basin_area,main="basin_area",group=wccm$strahler)
dotchart(wccm$rain_mean,main="rain_mean",group=wccm$strahler)
x11()
#hist(wccm$riverwidth);boxplot(ccm$riverwidth); densityplot(ccm$riverwidth);hist(wccm$shree);hist(wccm$lshree);hist(sqrt(wccm$up_area))
library(corrgram)
corrgram(wccm[,],lower.panel=panel.shade, upper.panel=panel.pie,order=TRUE)
corrgram(wccm[,c("lr","lshree","strahler","distance_source","lup")],lower.panel=panel.shade, upper.panel=panel.pie,order=TRUE)
library(ade4)
acpcor<-dudi.pca(wccm[,c("lr","lshree","distance_source","lup","p_non_calc_surf","p_very_calc_surf")], 
		center=TRUE, scannf =FALSE, scale=TRUE,nf=2) ## 2 axes 
s.corcircle(acpcor$co,xax=1,yax=2)
# lup, distance_source and lshree are very very correlated

x11()
op<-par(mfrow=c(2,2),mar=c(3,3,3,1))
dotchart(wccm$lshree,main="lshree",group=wccm$strahler)
dotchart(wccm$distance_source,main="distance_source",group=wccm$strahler)
dotchart(wccm$lup,main="lup",group=wccm$strahler)
dotchart(wccm$lr,main="log(river width)",group=wccm$strahler)
############################
#LM AS A FIRST TRY (you really don't see all I've tried, objective increase AIC##decrease AIC?
############################
# To summarise, wso_id (basin) is significant but then you cannot use it to predict
# So I have tried to insert the size of the basin as a whole to improve the model
lm0<-lm(lr~lshree+lup+distance_source+p_non_calc_surf+p_very_calc_surf+strahler+lbasin_area+rain_mean,data=wccm)
drop1(lm0,test="Chisq")
stepAIC(lm(lr~lshree+lup+distance_source+p_non_calc_surf+p_very_calc_surf+strahler+basin+rain_mean,data=wccm),trace=TRUE)
summary(lm1<-lm(lr~lshree+p_very_calc_surf+lup+distance_source+basin,data=wccm)) #Adjusted R-squared:  0.67 
anova(lm1,test="F") #lup and distance_source are not kept
AIC(lm1) # 419 with wso_id, 594 with basin_area, 517 with basin
E<-resid(lm1)
plot(lm1) # no trend in the residuals...


############################
#GAM MODEL 
############################


require(mgcv)
summary(g1<-gam(lr~s(distance_source)+s(lup)+s(lshree)+wso_id+s(p_very_calc_surf),data=wccm)) # best model 77.5 deviance explained but pb with extrapolation
# shree is correlated with up , drop it ?
AIC(g1)# 428
plot(g1)
summary(g10<-gam(lr~s(distance_source)+s(lup)+wso_id+p_very_calc_surf,data=wccm)) # best model 75.6 deviance explained but pb with extrapolation
# shree is correlated with up , drop it ?
AIC(g10)# 442

summary(g2<-gam(lr~s(distance_source)+s(lup)+basin+s(p_very_calc_surf),data=wccm)) # best model 72.6 deviance explained but pb with extrapolation
AIC(g2)# 458
plot(g2)

#best model without "basin"  
summary(g3<-gam(riverwidth~s(distance_source)+s(lup)+rain_mean+s(p_very_calc_surf),data=wccm),family=Gamma(link=log)) # 65% deviance
AIC(g1,g2,g3) # there is something weird to do with the basins
x11()
op<-par(mfrow=c(1,3))
plot(g3)
str(wccm)

names(wccm)
#[1] "wso_id"           "riverwidth"       "shree"            
#"strahler"         "distance_source"  "up_area"       
#"p_non_calc_surf" 
#[8] "p_very_calc_surf" "rain_mean"        "basin_area"    
#"lshree"   "lr"    "lup"  "lbasin_area"     
#[15] "basin"  "emu_name_short"   "emu_name"  "emu_coun_abrev"  
summary(g3<-gam(lr~s(distance_source)+s(lup)
+rain_mean+s(p_very_calc_surf),data=wccm))#65%
AIC(g3)#538

summary(g3.1<-gam(lr~s(lshree)+s(lup)
+rain_mean+s(p_very_calc_surf),data=wccm))#64.8
AIC(g3.1)#544
hist(wccm$shree)
hist(wccm$lshree)


summary(g3.2<-gam(lr~s(shree)+s(lup)
+rain_mean,data=wccm))#55.9% - need  to have the geology in it
AIC(g3.2)#610

summary(g3.3<-gam(lr~s(shree)+s(lup)
+s(p_very_calc_surf),data=wccm))#62.8%
AIC(g3.3)#555##ran also adds something

summary(g3.4<-gam(lr~s(shree)+s(distance_source)
+s(p_very_calc_surf)+rain_mean,data=wccm))#64.2
AIC(g3.4)#543

summary(g3.5<-gam(lr~s(shree)+s(lup)
+s(p_non_calc_surf)+rain_mean,data=wccm))#58.9
AIC(g3.5)#587 verycalc is better then non-calc

AIC(g3, g3.1)
gam.check(g3)
gam.check(g3.1)
plot(g3)
plot(g3.1)

#using log link instead of the log transformed variables

summary(g3<-gam(riverwidth~s(distance_source)
+s(lup)+rain_mean+s(p_very_calc_surf)
,data=wccm),family=Gamma(link=log))# 65% deviance seems like rain and lup arent sig?
anova(g3)
AIC(g3)#2884
gam.check(g3)
plot(g3)

summary(g4<-gam(riverwidth~s(distance_source) +
emu_name_short +s(lup)+rain_mean+s(p_very_calc_surf)
				,data=wccm),family=Gamma(link=log)) #70.4%
anova(g4)
AIC(g4)#2838
gam.check(g4)
plot(g4)

#take out lup
summary(g5<-gam(riverwidth~s(distance_source) +
emu_name_short +rain_mean+s(p_very_calc_surf)
,data=wccm),family=Gamma(link=log)) 

anova(g5)
AIC(g5)#2838
gam.check(g5)
plot(g5)

#take out rain
summary(g6<-gam(riverwidth~s(distance_source) +
emu_name_short +s(p_very_calc_surf)
,data=wccm),family=Gamma(link=log))# 69.2%

anova(g6)
AIC(g6)#2838
gam.check(g6)
plot(g6)

#take out emu
summary(g7<-gam(riverwidth~s(distance_source) +
 +s(p_very_calc_surf)
,data=wccm),family=Gamma(link=log))# 63.9

anova(g7)
AIC(g7)#2884
gam.check(g7)
plot(g7)
AIC(g7, g3)

require(mgcv)
summary(gm0<-gamm(lr~s(distance_source) +
						emu_name_short +s(p_very_calc_surf)
				,data=wccm)) #70.4%
AIC(gm0$lme)#455.71
##############################################
#THIS IS THE GOOD MODEL (below) !
#####################################
gm1<-gamm(lr~s(distance_source) +
						emu_name_short +s(p_very_calc_surf)
				,data=wccm,
				weights=varIdent(form=~1|emu_name_short)) #70.4%
AIC(gm1$lme)#432.7
E<-resid(gm1$lme,type="normalized")
summary(gm1$gam)
AIC(gm1)#2257
gam.check(gm1$gam)
plot(g4)

########################
# Model validation
########################
E<-resid(g1)
E<-resid(g2)
E<-resid(g3)
E<-resid(g3.1)
E<-resid(g4)
E<-resid(g6)
E<-resid(g7)
x11()
op<-par(mfrow=c(2,3))
boxplot(E~wccm$emu_name_short )
plot(wccm$distance_source,E)
plot(wccm$rain_mean,E)
plot(wccm$shree,E)
plot(wccm$lup,E)
x11()
op<-par(mfrow=c(1,3))
boxplot(E~wccm$emu_name_short)
##previous analysis - hydrometric ares of the shannon come out funny -  drainage etc.we concluded that it was probab;y something to
##do with where there ha been rainage by OPW
##had to leave out hydrometric area anywya, becuase of prediction prolems

# ignoring the basins comes at a cost.
plot(wccm$p_very_calc_surf,E)
plot(wccm$lup,E)

##if you leave in emu, you get higher deviance explained, lower AIC, but dodgier residulas.
#if you leave out emu, you get lower deviance explained, higher AIC, but residuals look ok
#i vote to go with model g7. very smimialr to g3, but simpler. 
#we have no measuremtns from neagh bann, so we cant use emu either
########################
# Extrapolation
########################

#need to get the prediction of g7 and add them into wccm0
#how to fill in the predicted data into wccm0$lr??
#or do we just save it seperately?
#unique(wccm$emu_name_short)[order(unique(wccm$emu_name_short))]
#unique(wccm0$emu_name_short)[order(unique(wccm0$emu_name_short))]
#wccm0$emu_name_short0<-wccm0$emu_name_short
#wccm0$emu_name_short[wccm0$emu_name_short=="GB_Neag"]<-"IE_East"
#wccm0$emu_name_short<-as.character(wccm0$emu_name_short)
predlr <- exp(predict(gm1$gam,newdata = wccm0))

str(wccm0)
str(predlr)
head(predlr)
hist(predlr, breaks=50)
head (wccm0)
str(ccm)
head(ccm)



ccm$riverwidth[indexwithemu]<-predlr
ccm$riverarea<-ccm$riverwidth*ccm$shape_leng #m*m  -->m�
ccm$riverareakm<-ccm$riverarea/10^6 #m*m/10^6  -->km�
save(ccm,file=paste(datawd,"/dataEDAccm/ccm",secteur,".RData",sep=""))


