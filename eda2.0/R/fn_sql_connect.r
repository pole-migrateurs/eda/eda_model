# Nom fichier :        fn_sql_0
# Projet :             controle migrateur / traitement
# Organisme :          IAV
# Auteur :             C�dric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :   06/02/2007 10:58:37
# Compatibilite :      R 2.4.0
# Etat :               OK
# Description          fonction g�n�rique de connection � la base sans date
#**********************************************************************


# la fonction essaye d'�tablir une connection
# revoit une erreur en l'absence de connection
# puis teste la requ�te et renvoit une erreur en cas de pb de requete
# pour test   baseODBC="BD_CONTMIG.centraldata"

fn_sql_connect<-function(baseODBC, silent=TRUE){
     
        e=expression(channel <-odbcConnect(baseODBC,
                                        uid = "postgres",
                                        pwd = passwordlocal,
                                        case = "tolower",
                                        believeNRows = FALSE))
        if (!silent) print(paste("Try to connect ",baseODBC))
        # renvoit du resultat d'un try catch expression dans
        #l'ojet Connexion courante, soit un vecteur caract�re
        connexionCourante<-tryCatch(eval(e),
                        error=paste("unable to connect ",baseODBC))
        
          if (class(connexionCourante)=="RODBC") {if (!silent)print("Connection successfull")} else print(connexionCourante)
         return(connexionCourante)
        }



