# Fichier d'analyse permettant d'extraire les donn�es (sans outliers) pour
# le calcul de river width
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#Remplacement du wso1_id lorsque les stations sont mal projet�es
#see in the trac

ers0<-subset(ers_full,!is.na(ers_full$op_cs_largeurlameeau)&
				ers_full$op_cs_largeurlameeau!=999&
				!is.na(ers_full$up_area)&
				ers_full$month<11&
				ers_full$month>3&
				ers_full$op_cd_methodeprospection==843&
				ers_full$annee>1987&
				!is.na(ers_full$uga)&
				ers_full$op_cs_largeurlameeau>0)

#op_cs_largeurlameeau>0 integrated (no more problems with round data in bdmap) 

# Suppression des stations :
#ers0<-ers0[!ers0$st_id%in%c("0547C025","06840051","04430072","06250162","0532B006"),]

#Supression des stations mal projet�s (suite aux analyses ci-dessous, reste encore des pb non r�solus)
#ers0<-ers0[!ers0$st_id%in%c("02540157","02570032","02680142","02550134","02550022","02550134","05630038",
#				"05310014","062B0036","01800015","01800023","01800120","03580002",
#				"03760112","06210010","03080009","03080085","03080190","03910039","05330018"),]
#Ces suppressions sont prises en compte dans analyse_2_data_extraction

#Suppression des outliers pour le calcul de river width
ers0<-ers0[!ers0$st_id%in%c("02680142","03500175","02570215",
				"04030096","03890167","04430070","05470099","062B0009",
				"04290092","0540A002","02080024","0546C012","05470064","05240101",
				"01800121","01800120","05400121","05400122","05400138","05405010",
				"03020108","03890027","03890155"),]
#Suppression de l'outlier pour la bretagne (mais seule station pour ce rang --> plus de station avec rang>4)
ers0<-ers0[!ers0$st_id=="04560120",]

#A voir si on les garde pour l'analyse
#Suppression des stations pour le calcul de river width
#ou il n'y a qu'une seule donn�e par uga suivant le rang de strahler
ers0<-ers0[!ers0$st_id%in%c("02540188","05470067","04636678","03770033","03770095"),]


#ERS data : ers0
table(ers0$uga,ers0$strahler)
table(ers0$st_id,ers0$month)

#ERS_full data
table(ers_full$uga,ers_full$strahler)
table(ers_full$st_id,ers_full$month)
#Selection des donn�es
ers_full0<-subset(ers_full,!is.na(ers_full$op_cs_largeurlameeau)&
				ers_full$op_cs_largeurlameeau!=999&
				!is.na(ers_full$up_area)&
				ers_full$month<11&
				ers_full$month>3&
				ers_full$annee>1987&
				!is.na(ers_full$uga)&
				ers_full$op_cs_largeurlameeau>0)

#Suppression des outliers pour les donn�es ers_full (idem que ers)
ers_full0<-ers_full0[!ers_full0$st_id%in%c("02680142","03500175","02570215",
				"04030096","03890167","04430070","05470099","062B0009",
				"04290092","0540A002","02080024","0546C012","05470064","05240101",
				"01800121","01800120","05400121","05400122","05400138","05405010",
				"03020108","03890027","03890155"),]
#Suppression de l'outlier pour la bretagne (mais seule station pour ce rang --> plus de station avec rang>5)
ers_full0<-ers_full0[!ers_full0$st_id=="04560145",]
#Comparaison avec la base CCM
table(ccm$uga,ccm$strahler)


#/////////////////////////////////////#
#Dataset use in these analysis
#you must change ers0 by ers_full if you want
ers_emu<-split(ers0,ers0$uga)
ers_emu<-split(ers_full0,ers_full0$uga)
#.......................................#

# (1) River width (op_cs_largeurlameeau) en fonction de strahler par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	boxplot(ers_emu[[i]][,"op_cs_largeurlameeau"]~ers_emu[[i]][,"strahler"],main=paste(names(ers_emu[i])),xlab="strahler",ylab="river width")
}

library(ggplot2)
ers0$strahl=as.factor(ers0$strahler) 
ers_full0$strahl=as.factor(ers_full0$strahler)
x11()
g<-ggplot(ers0) #or g<-ggplot(ers_full0)
g+geom_boxplot(aes(x=strahl,y=op_cs_largeurlameeau,fill=strahl))+
		facet_wrap(~uga)+
		scale_y_log()+
		ylab("log(wetted river width)")+
		xlab("strahler")+
		opts(title="wetted river width ~ strahler order")
x11()
g<-ggplot(ers0) #or g<-ggplot(ers_full0)
g+geom_boxplot(aes(x=strahl,y=op_cs_largeurlameeau,fill=strahl))+facet_wrap(~uga)+scale_y_continuous(limits=c(0,400))


# (2) Up_area en fonction du rang de strahler par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	boxplot(ers_emu[[i]][,"up_area"]~ers_emu[[i]][,"strahler"],main=paste(names(ers_emu[i])),xlab="strahler",ylab="up_area")
}

x11()
g<-ggplot(ers0) #or g<-ggplot(ers_full0)
g+geom_boxplot(aes(x=strahl,y=up_area,fill=strahl))+
		facet_wrap(~uga)+scale_y_log()+
		ylab("log(upstream_area)")+
		xlab("strahler")+
		opts(title="upstream area ~ strahler order")


# (3) River width en fonction de up_area par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	plot(ers_emu[[i]][,"op_cs_largeurlameeau"]~ers_emu[[i]][,"up_area"],main=paste(names(ers_emu[i])),xlab="up_area",ylab="river width")
}

x11()
g<-ggplot(ers0) #or g<-ggplot(ers_full0)
g+geom_point(aes(x=up_area,y=op_cs_largeurlameeau,col=strahl))+
		facet_wrap(~uga)+
		scale_y_log()+
		scale_x_log()+
		ylab("log(wetted river width)")+
		xlab("log(up_area)")+
		geom_smooth(aes(x=up_area,y=op_cs_largeurlameeau), method="lm",fullrange=TRUE,formula=y ~ x)+
		opts(title="wetted river width ~ upstream area")

x11()
g<-ggplot(ers0) #or g<-ggplot(ers_full0)
g+geom_point(aes(x=up_area,y=op_cs_largeurlameeau,col=strahl))+
		facet_wrap(~uga)+
		scale_y_log()+
		scale_x_log()+
		ylab("log(wetted river width)")+
		xlab("log(up_area)")+
		opts(title="wetted river width ~ up_area")



# (4) River width (op_cs_largeurlameeau) en fonction de strahler par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	boxplot(ers_emu[[i]][,"op_cs_largeurlameeau"]~ers_emu[[i]][,"strahler"],main=paste(names(ers_emu[i])),xlab="strahler",ylab="river width")
	x11()
	boxplot(ers_emu[[i]][,"up_area"]~ers_emu[[i]][,"strahler"],main=paste(names(ers_emu[i])),xlab="strahler",ylab="up_area")
	x11()
	plot(ers_emu[[i]][,"op_cs_largeurlameeau"]~ers_emu[[i]][,"up_area"],main=paste(names(ers_emu[i])),xlab="up_area",ylab="river width")
}
# En fonction de shreve
x11()
g<-ggplot(ers0)
g+geom_point(aes(x=shree,y=op_cs_largeurlameeau),alpha=0.5)+
		facet_wrap(~uga)+
		scale_y_log()+
		scale_x_log()+
		ylab("log(wetted river width)")+
		xlab("shreve")+
		geom_smooth(aes(x=shree,y=op_cs_largeurlameeau), method="lm",fullrange=TRUE,formula=y ~ x)+
		opts(title="wetted river width ~ shreve order")

#EMU Seine Normandie (uga=10)
eSN<-ers_emu[[10]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eSN1<-subset(eSN,eSN[,"op_cs_largeurlameeau"]>40)
eSN1
eSN2<-subset(eSN,strahler==1&op_cs_largeurlameeau>15)
eSN2
eSN3<-subset(eSN,up_area>3000)
eSN3


#EMU RhoneMediterranee (uga=9)
eRM<-ers_emu[[9]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eRM1<-subset(eRM1,up_area>3000,)
eRM1
eRM2<-subset(eRM,strahler==5&up_area>2000)
eRM2
eRM3<-subset(eRM,strahler==4&op_cs_largeurlameeau>40)
eRM3
eRM4<-subset(eRM,strahler==2&op_cs_largeurlameeau>30)
eRM4
eRM5<-subset(eRM,strahler==1&op_cs_largeurlameeau>200)
eRM5


#EMU Rhin (uga=8)
eR<-ers_emu[[8]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eR1<-subset(eR,op_cs_largeurlameeau>12)
eR1
eR2<-subset(eR,up_area>400)
eR2
eR3<-subset(eR,strahler==8)
eR3
eR4<-subset(eR,op_cs_largeurlameeau>400&strahler==5)
eR4

boxplot(ers_emu[[8]][,"up_area"])

#EMU Meuse (uga=7)
eM<-ers_emu[[7]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eM1<-subset(eM,up_area>1000)
eM1
eM2<-subset(eM,strahler>3&op_cs_largeurlameeau<10)
eM2

#EMU Loire (uga=6)
eL<-ers_emu[[6]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eL1<-subset(eL,op_cs_largeurlameeau>40)
eL1
eL2<-subset(eL,strahler==2&op_cs_largeurlameeau>20)
eL2
eL3<-subset(eL,strahler==6)
eL3
eL4<-subset(eL,strahler==5&op_cs_largeurlameeau>200)
eL4

#EMU Garonne (uga=5)
eG<-ers_emu[[5]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eG1<-subset(eG,op_cs_largeurlameeau>25)
eG1
eG2<-subset(eG,strahler==6)
eG2
eG3<-subset(eG,strahler==5)
eG3
eG4<-subset(eG,strahler==1&op_cs_largeurlameeau>100)
eG4

#EMU Corse (uga=4)
eC<-ers_emu[[4]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eC1<-subset(eC,strahler>4)
eC1
eC2<-subset(eC,strahler==3&op_cs_largeurlameeau>15)
eC2

#EMU Bretagne (uga=3)
eB<-ers_emu[[3]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eB1<-subset(eB,up_area>1000)
eB1
eB2<-subset(eB,strahler>3)
eB2
eB3<-subset(eB,strahler==1&op_cs_largeurlameeau>10)
eB3

#EMU Artois Picardie (uga=2)
eAP<-ers_emu[[2]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eAP1<-subset(eAP,up_area>1000)
eAP1
eAP2<-subset(eAP,strahler==3&op_cs_largeurlameeau<3)
eAP2
eAP3<-subset(eAP,strahler==3&up_area>600)
eAP3
eAP4<-subset(eAP,op_cs_largeurlameeau>40)
eAP4

#EMU Adour (uga=1)
eA<-ers_emu[[1]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eA1<-subset(eA,up_area>2000)
eA1		
eA2<-subset(eA,op_cs_largeurlameeau>20)
eA2		
eA3<-subset(eA,op_cs_largeurlameeau>30&strahler==2)
eA3	
eA4<-subset(eA,up_area>6000)
eA4	

#En fonction de Shreve
#River width (op_cs_largeurlameeau) en fonction de strahler par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	boxplot(ers_emu[[i]][,"op_cs_largeurlameeau"]~ers_emu[[i]][,"shree"],main=paste(names(ers_emu[i])),xlab="shreve",ylab="river width")
}
#Up_area en fonction du rang de shreve par emu
for(i in 1:length(levels(as.factor(ers_full$uga)))){
	x11()
	boxplot(ers_emu[[i]][,"up_area"]~ers_emu[[i]][,"shree"],main=paste(names(ers_emu[i])),xlab="shree",ylab="up_area")
}

#Etude relation river width~up_area en fonction de chaque rang de strahler
ers_stra<-split(ers0,ers0$strahler)
for(i in 1:length(levels(as.factor(ers0$strahler)))){
	x11()
	plot(ers_stra[[i]][,"op_cs_largeurlameeau"]~ers_stra[[i]][,"up_area"],main=paste(names(ers_stra[i])),xlab="up_area",ylab="river width")
}

eS2<-ers_stra[[2]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eS21<-subset(eS2,op_cs_largeurlameeau>30)
eS21	
eS5<-ers_stra[[5]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eS51<-subset(eS5,up_area>4000)
eS51	
eS3<-ers_stra[[3]][,c("uga","annee","month","st_id","wso1_id","wso_id","op_cs_largeurlameeau","up_area","strahler")]
eS31<-subset(eS3,up_area>800)
eS31
