# TTODO: Processing max temperatures in summer and mean in winter
# test OK 20 mai 2010
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: Cédric Briand, Céline Jouanin
###############################################################################


source("EDACCM/init.r")

#**********************************************************#
#              Loading data for mean temperatures          #
#**********************************************************#
tmpbase=new("BaseEda",
		baseODBC="baseODBCccm",
		schema="temperature",
		table="tmp",
		prkey="gridid")
tmpbase<-loaddb(tmpbase)
#str(tmpbase@data)
library(ggplot2)
# selectMethod("loaddb","BaseEda") the method in BaseEdaCCM pointed to class BaseEda 
# casting the data in long format, one column variable contains the column name, values are in column value
# I discard the_geom column from the analysis


#**********************************************************#
#              Modifying dataframe structure               #
#**********************************************************#
# beware if updated with new years after 2002 the number of column (before the_geom column) might no longer be 316
tmp<-melt(data=tmpbase@data[,1:316],
		id.vars=c(1:4),
		measure.varsc=c(5:316),
		variable_name = "monthyear",
		na.rm = FALSE)


#**********************************************************#
#              Extracting month and year                   #
#**********************************************************#
tmp$monthyear=as.character(tmp$monthyear)
# the  monthyear is written a m10y2003
# strsplit returns a list with a vector of length 2 "m1" "2003"
# The two function below get either 1 or 2003
getmonth<-function(x){
	mi=unlist(strsplit(x,split="y"))[1]
	i=substr(mi,2,nchar(mi))
}
getyear<-function(x){
	unlist(strsplit(x,split="y"))[2]
}

#**********************************************************#
#            Getting the predicted values                  #
#**********************************************************#
#...........................................................
# Extracting the month and year from the monthyear variable
tmp$month=sapply(tmp$monthyear,getmonth)
tmp$year=sapply(tmp$monthyear,getyear)
#...........................................................
# Checking mean temperatures
meantemp=tapply(tmp$value,tmp$month,mean) # the coldest month are dec jan feb, the hottest are 6 7 8
names(meantemp)[c(1,5:12)]<-paste("0",names(meantemp)[c(1,5:12)],sep="")
meantemp<-meantemp[order(names(meantemp))]
plot(meantemp,xlab="mois",ylab=iconv("température moyenne","UTF8"),
		main=iconv("Evolution des températures moyennes suivant le mois","UTF8"),xaxt="n",las=1,ylim=c(0,20))
axis(1,c(1:12),at=1:12)
points(c(1,2,12),meantemp[c(1,2,12)],col="blue",pch=16)
points(c(6,7,8,9),meantemp[c(6,7,8,9)],col="red",pch=16)
tmp[tmp$month==12|tmp$month==1|tmp$month==2,"season"]<-"winter"
tmp[tmp$month==6|tmp$month==7|tmp$month==8|tmp$month==9,"season"]<-"summer"
tmp[is.na(tmp$season),"season"]<-"other"
summer=tapply(tmp$value[tmp$season=="summer"],tmp$gridid[tmp$season=="summer"],mean)
if (all(names(summer)==tmpbase@data$gridid)){
	tmpbase@data$summer<-summer 
} else stop("check correspondance before integrating the data")

#...........................................................
# means of monthly mean tempertatures...
winter=tapply(tmp$value[tmp$season=="winter"],tmp$gridid[tmp$season=="winter"],mean)
if (all(names(winter)==tmpbase@data$gridid)){
	tmpbase@data$winter<-winter
}else stop("check correspondance before integrating the data")


#**********************************************************#
# Calculating the summer and winter temperatures per year  #
#**********************************************************#

#...........................................................
# Creating a unic number for each gridid (begin at 1 and without breaks)
Ndata<-dim(tmp)[1]
Num<-rep(0,max(tmp$gridid))
for(i in 1:Ndata){
	Num[i]<-which(tmp$gridid[i]==tmp$gridid[!(duplicated(tmp$gridid))])
}
tmp$Num<-Num


#...........................................................
#Calculating winter temperatures per year
NNum<-length(levels(as.factor(tmp$Num)))
Nyear<-length(as.numeric(min(tmp$year)):as.numeric(max(tmp$year)))
tmpwinter<-array(NA,c(NNum,Nyear))
for(i in 1:NNum){
	for(j in (as.numeric(min(tmp$year))+1):as.numeric(max(tmp$year))){
		tmpwinter[i,j-as.numeric(min(tmp$year))+1]<-sum(tmp$value[tmp$month=="12"&tmp$year==(j-1)&tmp$Num==i],
														tmp$value[tmp$month=="1"&tmp$year==j&tmp$Num==i],
														tmp$value[tmp$month=="2"&tmp$year==j&tmp$Num==i])/3
	}
}
tmpwinter<-as.data.frame(tmpwinter)
#...........................................................
# Changing the variable name in tmpwinter
names(tmpwinter)<-paste("winter",as.numeric(min(tmp$year)):as.numeric(max(tmp$year)),sep="")
tmpwinter$Num<-1:dim(tmpwinter)[1]

tmpbase@data$Num<-tmp$Num[which(tmpbase@data$gridid%in%tmp$gridid)]

#...........................................................
#Integrating winter temperatures per year in tmpbase
#if (all(row.names(tmpwinter)==tmpbase@data$Num)){
#	tmpbase@data$tmpwinter<-tmpwinter 
#} else stop("check correspondance before integrating the data")
tmpbase@data<-merge(tmpbase@data,tmpwinter,by.x="Num")

#...........................................................
#Calculating summer temperatures per year
tmpsummer<-tapply(tmp$value[tmp$season=="summer"],list(tmp$gridid[tmp$season=="summer"],tmp$year[tmp$season=="summer"]),mean) 
tmpsummer<-as.data.frame(tmpsummer)

x11()
matplot(t(tmpsummer[,1:26]),type="l")
#...........................................................
# Changing the variable name in tmpsummer
dimnames(tmpsummer)[[2]]<-paste("summer",as.numeric(min(tmp$year)):as.numeric(max(tmp$year)),sep="")
tmpsummer$Num<-1:dim(tmpsummer)[1]

#...........................................................
#Integrating summer temperatures per year in tmpbase
tmpbase@data<-merge(tmpbase@data,tmpsummer,by="Num")

#...........................................................
#Calculating annual temperatures per year
tmpannual<-tapply(tmp$value,list(tmp$gridid,tmp$year),mean) 
tmpannual<-as.data.frame(tmpannual)

x11()
matplot(t(tmpannual[,1:26]),type="l")
#...........................................................
# Changing the variable name in tmpannual
dimnames(tmpannual)[[2]]<-paste("annual",as.numeric(min(tmp$year)):as.numeric(max(tmp$year)),sep="")
tmpannual$Num<-1:dim(tmpannual)[1]

#...........................................................
#Integrating annual temperatures per year in tmpbase
tmpbase@data<-merge(tmpbase@data,tmpannual,by="Num")

#**********************************************************#
#     Writing them back into the database temperature.tmp  #
#**********************************************************#
# The programming is well done so we can simply use the import_column method
# it will write in the proper schema and table in the database
# and will get them from the class attributes

import_column(tmpbase,newcolumn="summer",newcolumntype="numeric",display=TRUE)
import_column(tmpbase,newcolumn="winter",newcolumntype="numeric",display=TRUE)

#...........................................................
#Writing the winter and summer temperature per year columns into the database
#Winter temperatures
for(i in as.numeric(min(tmp$year)):as.numeric(max(tmp$year))){
import_column(tmpbase,newcolumn=paste("winter",i,sep=""),newcolumntype="numeric",display=TRUE)
}

#Summer temperatures
for(i in as.numeric(min(tmp$year)):as.numeric(max(tmp$year))){
	import_column(tmpbase,newcolumn=paste("summer",i,sep=""),newcolumntype="numeric",display=TRUE)
}

#Annual temperatures
for(i in as.numeric(min(tmp$year)):as.numeric(max(tmp$year))){
	import_column(tmpbase,newcolumn=paste("annual",i,sep=""),newcolumntype="numeric",display=TRUE)
}


# at this point you need to create the joined file

