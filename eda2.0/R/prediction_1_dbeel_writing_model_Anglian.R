# Writing models in resultmodel and model_mod into postgre
# ANGLIAN
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

##############################################
# ECRITURE DU MODELE ANGLIAN DANS LA BASE
##############################################
# help command with the data base
#delete from ccm21.resultmodel;
#delete from ccm21.model_mod;
#alter sequence  ccm21.model_mod_mod_id_seq restart with 1;

# ccm21.resultmodel table de r�sultats des mod�les
# ccm21.model_mod table de description des mod�les
model_mod<-new("BaseEda",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="model_mod",
		prkey="mod_id")


# ccm21.model_mod table de description des mod�les
resultmodel<-new("BaseEda",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="resultmodel",
		prkey="res_id")
# ECRITURE DU MODELE DE DENSITE
#model_mod<-loaddb(model_mod)
# Pour ins�rer les donn�es attention de rajouter "'"
form<-formula(md)
form<-paste("'",as.character(form)[2],as.character(form)[1],as.character(form)[3],"'")
model_mod@data<-data.frame("mod_name"="'Anglian density model'",
		"mod_date"="'27/07/2011'",
		"mod_description"="'best density model for Anglian country'",
		"mod_location"="'Anglian'",
		"mod_formula"=form)
rescolumns<-c("mod_name","mod_date","mod_description","mod_location","mod_formula")
insert_into(model_mod,rescolumns)


# v�rifier l'identifiant

rescolumns<-c("res_wso1_id" ,
		"res_riversegmentgid" ,		
		"res_value",
		"res_mod_id")
resultmodel@data<-cbind(ccm[,c("wso1_id","gid","pred_d")],"res_mod_id"=2)
colnames(resultmodel@data)<-rescolumns
insert_into(resultmodel,rescolumns)

# ECRITURE DU MODELE DE PRESENCE ABSENCE
#model_mod<-loaddb(model_mod)
# Pour ins�rer les donn�es attention de rajouter "'"
form<-formula(mpa)
form<-paste("'",as.character(form)[2],as.character(form)[1],as.character(form)[3],"'")
model_mod@data<-data.frame("mod_name"="'Anglian presence absence model'",
		"mod_date"="'27/07/2011'",
		"mod_description"="'Best presence absence model for Anglian country'",
		"mod_location"="'Anglian'",
		"mod_formula"=form
)
rescolumns<-c("mod_name","mod_date","mod_description","mod_location","mod_formula")
insert_into(model_mod,rescolumns)
# v�rifier l'identifiant dans la table ici =2

rescolumns<-c("res_wso1_id" ,
		"res_riversegmentgid" ,		
		"res_value",
		"res_mod_id")
resultmodel@data<-cbind(ccm[,c("wso1_id","gid","pred_pa")],"res_mod_id"=3)
colnames(resultmodel@data)<-rescolumns
insert_into(resultmodel,rescolumns)


# ECRITURE DU MODELE COMBINE
ccm$pred_d_pa<-ccm$pred_d*ccm$pred_pa
model_mod@data<-data.frame("mod_name"="'Anglian full model'",
		"mod_date"="'23/06/2011'",
		"mod_description"="'Best model for Anglian country'",
		"mod_location"="'Anglian'")
rescolumns<-c("mod_name","mod_date","mod_description","mod_location")
insert_into(model_mod,rescolumns)
# v�rifier l'identifiant dans la table ici =3

rescolumns<-c("res_wso1_id" ,
		"res_riversegmentgid" ,		
		"res_value",
		"res_mod_id")
resultmodel@data<-cbind(ccm[,c("wso1_id","gid","pred_d_pa")],"res_mod_id"=4)
colnames(resultmodel@data)<-rescolumns
insert_into(resultmodel,rescolumns)