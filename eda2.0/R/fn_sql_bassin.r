#table repr�sente le chemin vers la table
#fn_sql_bassin("tablebassin.csv")
#table="tablebassin.csv"
fn_sql_bassin<- function(table) {

        tablebassin<- read.table(
        table,header=TRUE,sep=";",
        quote="\"",dec=".",fill=TRUE,as.is=TRUE)

        bassin=tk_select.list(unique(tablebassin$nom_bassin),preselect=unique(tablebassin$nom_bassin),
        multiple=TRUE,title="choix du (des) bassins")
        
      match<-tablebassin$nom_bassin%in%bassin
      tablebassin=subset(tablebassin,match)
       riviere=tk_select.list(tablebassin$nom_riviere,
       preselect=tablebassin$nom_riviere,multiple=TRUE
                                        ,title="choix de la (des) rivi�res")
       match<-tablebassin$nom_riviere%in%riviere
       tablebassin=subset(tablebassin,match)
       hydro=tablebassin$code_hydro

      if (length(hydro)==1) {
                where=paste( " WHERE   BDMAP.ENTITEHYDRO.EH_CODEGENERIQUE LIKE '",
                hydro,
                "%'", sep="")
        } else {
                where=paste( " WHERE   BDMAP.ENTITEHYDRO.EH_CODEGENERIQUE LIKE '",
                hydro[1],
                "%'", sep="")
                
                for (j in 2:(length(hydro))){
                where=paste(where,
                " OR   BDMAP.ENTITEHYDRO.EH_CODEGENERIQUE LIKE '",
                hydro[j],
                "%'", sep="")
                }
        }
        
        
       # Requete sql
                sql = paste(
                "SELECT",
                "EH_CODEGENERIQUE",
                "FROM ",
                "BDMAP.ENTITEHYDRO",
                where,
                " ;",
                sep=" ")
return(sql)}
