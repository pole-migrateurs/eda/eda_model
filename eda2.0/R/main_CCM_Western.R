# Fichier de chargement et de calcul de la couche CCM
# Etat = fonctionnel
#
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand, C�line Jouanin
###############################################################################

#setwd("C:/Documents and Settings/cedric/Mes documents/Migrateur/programmes/workspace3.5/EDA")
source("EDACCM/init.r")
#########################"
# chargement des donn�es
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western")
ccm<-loaddb(ccm)# 958 lines
#########################"
# Calcul des distances � la mer
############################
ccm1<-distance_sea_fromnodes(ccm)
import_column(ccm1,newcolumn="cum_len_sea",newcolumntype="numeric",display=TRUE)
#########################"
# Calcul dams
############################
roeccm<-new("BaseEdaCCMriversegmentsROE",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="France",
		joinschema="geobs2010",
		jointable="roe_ccm_300",
		traveled_segments=vector())
roeccm<-loaddb(roeccm)
roeccm<-cumulated_dam_impact(roeccm)
col=colnames(roeccm@dataroe)[ colnames(roeccm@dataroe)!="gid"]
stopifnot(all(roeccm@data[,"gid"]==roeccm@dataroe[,"gid"]))
roeccm@data[,col]<-roeccm@dataroe[,col]
for (i in 1:length(col)){
	import_column(roeccm,col[i],newcolumntype="numeric",display=TRUE,askbeforeforefill=FALSE)
}
#############################
# Calcul des % clc
############################
clc<-new("BaseEdaCCMriversegmentsclc",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western",
		joinschema="clc",
		jointable="surf_area_ireland_final",
		traveled_segments=vector())
clc<-loaddb(clc,silent=FALSE)
upstream_area(clc,silent=TRUE,from=1,to=958)
upstream_area(clc,silent=TRUE,from=10001,to=20000)
upstream_area(clc,silent=TRUE,from=20001,to=30000)
upstream_area(clc,silent=TRUE,from=30001,to=40000)
upstream_area(clc,silent=TRUE,from=40001,to=50000)
upstream_area(clc,silent=TRUE,from=50001,to=58585)
###############################################################################
# R�int�gration des donn�es clc au niveau du primary catchment
##################################################################################
clc<-new("BaseEdaCCMriversegmentsclc",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western",
		joinschema="clc",
		jointable="surf_area_ireland_final",
		traveled_segments=vector())
clc<-loaddb(clc,silent=FALSE)
clc<-pass_joined_column(clc,newcolnames=c("catchment_area",
				"artificial_surfaces_11_13",
				"artificial_vegetated_14",
				"arable_land_21",
				"permanent_crops_22",
				"pastures_23",
				"heterogeneous_agricultural_24",
				"forest_31",
				"natural_32_33",
				"wetlands_4",
				"inland_waterbodies_51",
				"marine_water_52"))
the_col=c("catchment_area",
		"artificial_surfaces_11_13",
		"artificial_vegetated_14",
		"arable_land_21",
		"permanent_crops_22",
		"pastures_23",
		"heterogeneous_agricultural_24",
		"forest_31",
		"natural_32_33",
		"wetlands_4",
		"inland_waterbodies_51",
		"marine_water_52")
import_columns(clc,the_col,display=TRUE)


#########################"
# Calcul des distances � la source
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western")
ccm<-loaddb(ccm)
#distance_source(ccm,silent=TRUE,from=1,to=nrow(data))
distance_source(ccm,silent=TRUE,from=1)
distance_source(ccm,silent=TRUE,from=56250)
#########################"
# chargement des donn�es
############################
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western")
ccm<-loaddb(ccm)
ccm1<-stream_order(ccm,silent=FALSE) # calculates shreeve stream order
#Valider puis valider
import_column(ccm1,newcolumn="shree",newcolumntype="numeric",display=FALSE)

ccm2<-bassin_area(ccm,silent=FALSE)
#v�rification : ccm2@data[ccm2@data$wso_id=="304532",c("wso_id","wso1_id","catchment_area","up_catchment_area","up_area")]
import_column(ccm2,newcolumn="up_area",newcolumntype="numeric",display=FALSE)

#A voir pour l'import des donn�es UGA
#import_column(ccm,newcolumn="uga",newcolumntype="numeric",display=FALSE)

#Ajout d'autre variables
#Calcul du rang de Sheidegger -- ALTER TABLE ccm21.riversegments ADD COLUMN scheid numeric
ccm@data$scheid<-ccm@data$shree*2
#Calcul des distances relatives -- ALTER TABLE ccm21.riversegments ADD COLUMN distance_relative numeric
ccm@data$distance_relative<-ccm@data$cum_len_sea/(ccm@data$cum_len_sea+ccm@data$distance_source)  
import_column(ccm,newcolumn="scheid",newcolumntype="numeric",display=FALSE)
import_column(ccm,newcolumn="distance_relative",newcolumntype="numeric",display=FALSE)

ccm<-loaddb_all(ccm)
ccm@data$c_area<-ccm@data$area/1000000
import_column(ccm,newcolumn="c_area",newcolumntype="numeric",display=FALSE)


#############################################
######################################
# Final data building
#####################################
##############################################
#-----------------------------------------
# Loading data for the riversegments
#------------------------------------------
source("EDACCM/init.r")
ccm=new("BaseEdaCCMriversegments",
		baseODBC="baseODBCccm",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western")
# the following query will take at least 3 minutes for france... Be patient...
# the geometries of the riversegments are not imported in R.
ccm<-loaddb_all(ccm) # 949

#Sauvegarde des donn�es pour avoir l'ensemble des river segments de la CCM
save(ccm,file=paste(datawd,"/dataEDAccm/ccm_Western.RData",sep=""))
load(file=paste(datawd,"/dataEDAccm/ccm_Western.RData",sep=""))

#-----------------------------------------
# loading electrofishing results
#-----------------------------------------------
dbeel<-new("BaseEdaCCMriversegmentsdbeel",
		baseODBC="baseODBCdbeel",
		schema="ccm21",
		table="riversegments",
		prkey="gid",
		zonegeo="Western",
		joinschema="wrbd",
		jointable="wrbd_station_ccm_500",
		traveled_segments=vector(),
		dataprovider="Russell Poole & Elvira de Eito", # permits selection of IRELAND in the view
		namesourcedatadbeel="wrbd.stationdbeel", # table containing stations identifier not present in the dbeel.observation_places table
		namestid="st_id", # name of the primary key in the original station table (i.e. the daughter table namesourcedatadbeel)
		viewoperation="dbeel.view_electrofishing")
dbeel<-loaddb(dbeel)
# 88 electrofishing stations.
# inserting data for riversegments manually
dbeel@data<-ccm@data
dbeel<-calculatedensities(dbeel)
# in practise we join ccm<gid>jointable<st_id>sourcedatadbeel<op_id>view_electrofishing
dbeel@datajoindbeel<-merge(dbeel@datajoindbeel,dbeel@data,by="wso1_id")
dbeel@datajoindbeel<-merge(dbeel@datajoindbeel,dbeel@sourcedatadbeel,by="st_id")
dbeel@datadbeel<-merge(dbeel@datajoindbeel,dbeel@datadbeel,by="op_id")
names(dbeel@datadbeel)[names(dbeel@datadbeel)=="wso1_id.x"]<-"wso1_id"
dbeel@datadbeel<-dbeel@datadbeel[,-match("wso1_id.y",names(dbeel@datadbeel))]

ers_full<-dbeel@datadbeel
save(ers_full,file=paste(datawd,"/dataEDAccm/ers_full_Western.RData",sep=""))
load(file=paste(datawd,"/dataEDAccm/ers_full_Western.RData",sep=""))
