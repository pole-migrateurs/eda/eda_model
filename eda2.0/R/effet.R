# -------------------------------------
# Auteur : Beaulaton Laurent 16/02/2007
# modifi� par : Beaulaton Laurent 28/05/2007 10:45:31
# -------------------------------------



########################
# discours introductif #
########################


#cat("\nVous allez enter dans un nouvel univers...\n...attention aux yeux\nsign� Laurent BEAULATON\n")
#cat("\n\nversion am�lior�e\n\tavec la participation de M�lanie B�guer, Patrick Lambert et Fran�oise Daverat\n")

#######################################################
# premi�re fonction
# permet d'�crire tous les mod�les possibles avec les variables explicatives fournies
#
# exemple :
# modele(c("A","B","A:B"),"y")
# produira --> y~NULL y~A ; y~B ; y~A:B ; y~A + B ; y~A + A:B ; y~B + A:B ; y~A + B + A:B
#######################################################


f_modele=function(effet,y="y") {
	
	require(hier.part)
	
	n=combos(length(effet))$binary		# combos (du package hier.part) permet de d�finir toutes les combinaisons possibles pour x variables
	log=matrix(data=as.logical(n),nrow=nrow(n),ncol=ncol(n))	#transforme en une matrice de vrai/faux
	
	modele=list()		
	for (i in 1:nrow(log)) modele[[i]]=effet[log[i,]]	#�tabli la liste des combinaisons d'effet
	modele[[length(modele)+1]]="NULL"
	
	formule=list()
	for (i in 1:length(modele)) formule[[i]]=paste(y,paste(modele[[i]],collapse="+"),sep="~")
	# ajoute � la liste des combinaisons les y~ et les + pour obtenir des mod�les
	
	return(formule)
}

#######################################################
# premi�re fonction bis
# la m�me, mais permet de comparer des mod�les dont les effets2 ne doivent 
# pas �tre inclus dans les m�mes mod�les
#######################################################

f_modele_bis=function(effet1,effet2,y="y") {
	effet=c(effet1,effet2)
	
	require(hier.part)
	
	n1=matrix(NA,nrow=dim(combos(length(effet1))$binary)[1]+1,ncol=dim(combos(length(effet1))$binary)[2])
	
	n1bis=combos(length(effet1))$binary
	n2=matrix(0,nrow=length(effet2),ncol=length(effet2))
	diag(n2)=1
	
	n1[1:dim(n1bis)[1],]=n1bis
	
	n1[dim(n1)[1],]=rep(0,length(effet1))
	
	
	n=matrix(NA,nrow= dim(n1)[1]* dim(n2)[1],ncol=dim(n1)[2]+ dim(n2)[2])
	
	for (i in 1:dim(n1)[1]){
		for (j in 1:dim(n2)[1]){
			n[(i-1)*dim(n2)[1]+j,]=c(n1[i,],n2[j,])
		}
	}
	
	log=matrix(data=as.logical(n),nrow=nrow(n),ncol=ncol(n))
	
	modele=list()		
	for (i in 1:nrow(log)) modele[[i]]=effet[log[i,]]	#�tabli la liste des combinaisons d'effet
	modele[[length(modele)+1]]="NULL"
	
	formule=list()
	for (i in 1:length(modele)) formule[[i]]=paste(y,paste(modele[[i]],collapse="+"),sep="~")
	# ajoute � la liste des combinaisons les y~ et les + pour obtenir des mod�les
	
	return(formule)
}




#######################################################
# deuxi�me fonction (en fait deux fonctions)
# permet de faire tourner tous les GLM (ou GAM) sp�cifier dans mod�le et stocke notamment les AIC de ces mod�les
# une nouvelle classe d'object est cr��e (aic.glm) pour stocker ces informations
#
# exemple :
# glmaic(modele,data=donnees,family=Gamma(link="log")
# idem avec glmaic
#######################################################



glmaic=function(modele,sub=NULL,poids=NULL,...) {
	
	resultat=data.frame(modele=rep(NA,length(modele)),lien=NA,distribution=NA,
			aic=NA,ddl.null=NA,ddl.residu=NA,dat=NA,lik=NA,kappa=NA)
	cat("nombre de mod�le : ",length(modele),"\n")
	
	for (i in 1:length(modele)) {
		tmps.ini=Sys.time()         #top chrono
		glm1=glm(as.formula(modele[[i]]),subset=sub, weights=poids,...)#fait tourner le glm
		glm1.kappa =glm.kappa(glm1)							# calcul l'indice de Kappa
		tmps.fin=Sys.time()         #top chrono
		
		# stocke les informations du mod�le dans resultat		
		resultat[i,]=c(as.character(glm1$formula)[3],glm1$family$link,glm1$family$family,glm1$aic,glm1$df.null,glm1$df.residual,difftime(tmps.fin,tmps.ini,units="sec"),as.integer(logLik(glm1)), glm1.kappa)
		cat("le modele ",i," est fini\t\t|\tddl : ",glm1$df.null-glm1$df.residual,"\t|\tAIC : ",as.integer(glm1$aic),"\t|\ttemps mis : ",difftime(tmps.fin,tmps.ini,units="sec")," s\n")
		
		#sauvegarde les r�sultats en cas de crash
		save(resultat,file="C:/sauve.Rdata")
		rm(glm1)
	}
	
	class(resultat)="aic.glm"		#transforme resultat en object de classe aic.glm
	
	return(resultat)
}


gamaic=function(modele,sub=NULL,poids=NULL,fichier_sauvegarde=NULL,...) {
#  require(gam)
	
	resultat=data.frame(modele=rep(NA,length(modele)),lien=NA,distribution=NA,aic=NA,ddl.null=NA,ddl.residu=NA,dat=NA,lik=NA)#,kappa=NA)
	cat("nb models : ",length(modele),"\n")
	
	for (i in 1:length(modele)) {
		tmps.ini=Sys.time()         #top chrono
		#glm1<-gam(as.formula(modele[[i]]),subset=sub, weights=poids,...)#fait tourner le glm
		tryCatch(
				glm1<-gam(as.formula(modele[[i]]),subset=sub, weights=poids,...),
				error=function(e) {
					print(str_c("iter",i))
					print(e)
					glm1<<-NA
				}
		)
		#glm1=gam(as.formula(modele[[i]]),data=jeu_final,family=neg.bin(1),maxit=100)
#		glm1.kappa =glm.kappa(glm1)							# calcul l'indice de Kappa
		tmps.fin=Sys.time()         #top chrono
		if (is.na(glm1)){
			resultat[i,]=c(modele[[i]],NA,NA,NA,NA,NA,difftime(tmps.fin,tmps.ini,units="sec"),NA)#, glm1.kappa)
			cat("model ",i," not converged\t\t|\tddl : ","\t|\ttime taken : ",difftime(tmps.fin,tmps.ini,units="sec")," s\n")
		}else{
			# stocke les informations du mod�le dans resultat		
			resultat[i,]=c(as.character(glm1$formula)[3],glm1$family$link,glm1$family$family,glm1$aic,glm1$df.null,glm1$df.residual,difftime(tmps.fin,tmps.ini,units="sec"),as.integer(logLik(glm1)))#, glm1.kappa)
			cat("model ",i," finished\t\t|\tddl : ",glm1$df.null-glm1$df.residual, "\t|\tAIC : ",as.integer(glm1$aic),"\t|\ttime taken : ",difftime(tmps.fin,tmps.ini,units="sec")," s\n")
		}
			#sauvegarde les r�sultats en cas de crash
		if (!is.null(fichier_sauvegarde))	save(resultat,file=fichier_sauvegarde)
		rm(glm1)
	}
	
	class(resultat)=c("aic.glm","data.frame")		#transforme resultat en object de classe aic.glm
	
	return(resultat)
}


gamaicOK=function(modele,sub=NULL,poids=NULL,fichier_sauvergarde="sauve.RData",...) {
#  require(gam)
	
	resultat=data.frame(modele=rep(NA,length(modele)),lien=NA,distribution=NA,aic=NA,ddl.null=NA,ddl.residu=NA,dat=NA,lik=NA)#,kappa=NA)
	cat("nombre de mod�le : ",length(modele),"\n")
	
	for (i in 1:length(modele)) {
		tmps.ini=Sys.time()         #top chrono
		glm1=gam(as.formula(modele[[i]]),subset=sub, weights=poids,...)#fait tourner le glm
#			glm1=gam(as.formula(modele[[i]]),data=jeu_final,family=neg.bin(1),maxit=100)
#		glm1.kappa =glm.kappa(glm1)							# calcul l'indice de Kappa
		tmps.fin=Sys.time()         #top chrono
		
		# stocke les informations du mod�le dans resultat		
		if(gam(as.formula(modele[[i]]),subset=sub, weights=poids,...)$aic!="NA"){
			resultat[i,]=c(as.character(glm1$formula)[3],glm1$family$link,glm1$family$family,glm1$aic,glm1$df.null,glm1$df.residual,difftime(tmps.fin,tmps.ini,units="sec"),as.integer(logLik(glm1)))#, glm1.kappa)
			cat("le modele ",i," est fini\t\t|\tddl : ",glm1$df.null-glm1$df.residual, "\t|\tAIC : ",as.integer(glm1$aic),"\t|\ttemps mis : ",difftime(tmps.fin,tmps.ini,units="sec")," s\n")
			if(gam(as.formula(modele[[i]]),subset=sub, weights=poids,...)$aic=="NA"){
				print(modele[[i]])
				break		
			}
		}
		#sauvegarde les r�sultats en cas de crash
		save(resultat,file=fichier_sauvergarde)
		rm(glm1)
	}
	
	class(resultat)=c("aic.glm","data.frame")		#transforme resultat en object de classe aic.glm
	
	return(resultat)
}

########################################
# calcul de kappa

glm.kappa=function(glm.resultat,seuil=.5){
	pFit=glm.resultat$fitted.value	# r�cup�re les valeurs pr�dites
	pFit[pFit<seuil]=0		# proba <50% --> inactif
	pFit[pFit>=seuil]=1	#  proba >50% --> actif
	
	matriceConfusion <-table(glm.resultat$y, pFit)
	
	tsum <- sum(matriceConfusion)
	ttab <- matriceConfusion/tsum
	
	tm1 <- apply(ttab, 1, sum)
	tm2 <- apply(ttab, 2, sum)
	agreeP <- sum(diag(ttab))
	chanceP <- sum(tm1 * tm2)
	
	kappa <- (agreeP - chanceP)/(1 - chanceP)
	
	if (kappa==-Inf) kappa=0.0
	
	return(kappa)
}


#######################################################
# troisi�me fonction
# d�finit la fonction g�n�rique print pour les object de classe aic.glm
# permet d'afficher le meilleur mod�le et tous les mod�les ayant un delta(AIC) inf�rieur � delta.aic
#
# exemple : 
# glmaic ou print(glmaic) --> affiche le meilleur mod�le et ceux �tant � 0.0001 (valeur par d�faut de delta.aic) de celui-ci
# ou print(glmaic,2) --> affiche le meilleur mod�le et ceux �tant � 2 de celui-ci
#######################################################


print.aic.glm=function(resultat,nb_model=NULL,delta.aic=NULL,kappa=FALSE,
		nb_digit_aic=1){
	#, nb_digit_kappa=3) {
	
	if(is.null(delta.aic) & is.null(nb_model)) nb_model=1
	#option pour imprimer tous les mod�les
	if(!is.null(nb_model)) if(nb_model=="all") nb_model=length(resultat$aic)
	
	if(!is.null(delta.aic)){
		meilleur=as.numeric(resultat$aic)<(min(as.numeric(resultat$aic),na.rm=TRUE)+delta.aic)
		meilleur<-meilleur[!is.na(meilleur)]
		#le nombre de mod�le affich� sera au plus petit des 2
		nb_model=min(sum(meilleur),nb_model)  
	}
	
	
	triParAic = order(as.numeric(resultat$aic), decreasing = FALSE)
#	triParKappa = order(as.numeric(resultat$kappa), decreasing = TRUE)
	
	rangAic = rank(as.numeric(resultat$aic),ties.method ="min")
#	rangKappa = rank(-1*as.numeric(resultat$kappa),ties.method ="min")
	
	cat("meilleur mod�le selon AIC","\n")
	resParAic = data.frame(modele = resultat$modele[triParAic], 
			aic = round(as.numeric(resultat$aic[triParAic]),nb_digit_aic),
			# kappa = round(as.numeric(resultat$kappa[triParAic]),nb_digit_kappa), 
			rangAic = rangAic[triParAic])#, rangKappa = rangKappa[triParAic])
	
	print(resParAic[1:nb_model,])
	
#  if(kappa){
#  	cat("\n","meilleur mod�le selon Kappa","\n")
#  	resParKappa = data.frame(modele = resultat$modele[triParKappa], 
#      aic = round(as.numeric(resultat$aic[triParKappa]),nb_digit_aic),
#      kappa = round(as.numeric(resultat$kappa[triParKappa]),nb_digit_kappa), 
#      rangAic = rangAic[triParKappa], rangKappa = rangKappa[triParKappa])
#  	print(resParKappa[1:nb_model,])
# 	}
}



#######################################################
# quatri�me fonction
# d�finit la fonction g�n�rique plot pour les object de classe aic.glm
# trace un graph repr�sentant les delta(AIC) des mod�les en fonction de leur degr� de libert�
#
# exemple :
# plot(glmaic)
#######################################################


plot.aic.glm=function(resultat,deltaaic=4,main="AIC",pch="+",
		xlim=c(-1,(trunc(max(x)/5)+1)*5),
		ylim=c(-.1*(max(y)-min(y)),max(y)*1.02),
		xlab="ddl", ylab=c(expression(Delta (AIC))),las=1,
		jitter=FALSE,yaxs="i",xaxs="i",...) {
	
	x=as.numeric(resultat$ddl.null)-as.numeric(resultat$ddl.residu)	#delta(AIC)
	if (jitter==TRUE)x=jitter(x)		#jitter (si vrai) permet d'introduire un petit bruit pour �viter les superposition sur le graphique
	
	y=as.numeric(resultat$aic)-min(as.numeric(resultat$aic))	#calcul les delta(AIC)
	
	
	plot(cex=1.5,pch=pch,x=x,y=y,main=main,col="blue",xlab=xlab,ylab=ylab,frame.plot=F,las=las,xaxs=xaxs,yaxs=yaxs,xlim=xlim,ylim=ylim,yaxt="n",...)
#fait un graph sans l'axe des ordonn�es
	
	axis(1,c(-1,(trunc(max(x)/5)+1)*5),labels=FALSE)
	axis(2,c(-.1*(max(y)-min(y)),(trunc(max(y)/5)+1)*5),labels=FALSE)
	axis(2,round(seq(0,(trunc(max(y)/5)+1)*5,length.out=5)),las=1)
#trace l'axe des ordonn�es
	
	
	meilleur=(as.numeric(resultat$aic)<(min(as.numeric(resultat$aic))+0.00001))
	points(cex=1.5,pch=pch,x=as.numeric(resultat$ddl.null[meilleur][1])-as.numeric(resultat$ddl.residu[meilleur][1]),y=0,col="red",...)
#mets en �vidence le meilleur mod�le (delta(AIC)=0)
	
	abline(h=0,col="green")
	abline(h=deltaaic,lty=4,col="green")
# trace les lignes delta(AIC)=0 et 4 ; les mod�les compris entre 
# ces deux lignes sont consid�r�s comme des mod�les concurrents 
# aux meilleurs mod�les ; au-del� les mod�les sont loin des performances des meilleurs
	
	points(cex=1.5,pch=pch,col="red",x=0,y=-.05*(max(y)-min(y)))
	text(x=0,y=-.05*(max(y)-min(y)),"= meilleur mod�le",pos=4)
}


#######################################################
# cinqui�me fonction
# sous-s�lectionne les r�sultats pour un facteur donn�e
# si indice sort ers_fullment l'indice, sinon sort le jeu de donn�es s�lectionn�
# si inverse : s�lectionne les mod�le sans le facteur
# ou dicte le fonctionnement si plusieurs facteurs sont recherch�s (fonction "ou" ou "et")
#######################################################

selec_facteur=function(resultat,facteur,indice=TRUE,inverse=FALSE,ou=T)
{
	if (length(facteur)>1)
	{
		index=list()
		for(i in 1:length(facteur))
			index[[i]]=grep(facteur[i],resultat[,"modele"])
	} else
		index=grep(facteur,resultat$modele)
	
	if (length(facteur)>1)
		if(ou)
			index=unique(unlist(index)) else    # #recherche tous les mod�les o� au moins un des facteurs est pr�sent (fonctionnement OU)
			index=names(table(unlist(index))[table(unlist(index))==length(facteur)]) #recherche tous les mod�les o� tous les facteurs sont pr�sents (fonctionnement ET)
	
	
	if(inverse)
	{
		index=(1:dim(resultat)[1])[!((1:dim(resultat)[1]) %in% index)]
	}
	if(indice)
		return (index)
	else
		return (resultat[index,])
}