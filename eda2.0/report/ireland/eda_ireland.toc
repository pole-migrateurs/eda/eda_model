\contentsline {part}{I\hspace {1em}Main report}{6}{part.1}
\contentsline {chapter}{\numberline {1}Introduction}{7}{chapter.1}
\contentsline {chapter}{\numberline {2}Methods}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Principle of EDA}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Data requirements}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Biological data}{16}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}River topology}{16}{subsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.3.1}Fishing}{16}{subsubsection.2.1.3.1}
\contentsline {subsection}{\numberline {2.1.4}Supplementary variables calculated}{17}{subsection.2.1.4}
\contentsline {subsubsection}{\numberline {2.1.4.1}Segment characteristics}{17}{subsubsection.2.1.4.1}
\contentsline {subsubsection}{\numberline {2.1.4.2}River width}{17}{subsubsection.2.1.4.2}
\contentsline {section}{\numberline {2.2}Modelling}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Analysis of explanatory variables}{18}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Model selection}{18}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Stock and escapement}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Fluvial production predicted by EDA}{19}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Inclusion of lake production}{21}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Organisation of the work}{22}{section.2.4}
\contentsline {chapter}{\numberline {3}Results}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Electrofishing data}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Selection of explanatory variables}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Models}{29}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}River width model}{29}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Survey data model}{31}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Presence absence - $\Delta $ model}{31}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Positive densities - $\Gamma $ model}{35}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}Final - $\Delta \Gamma $ model}{41}{subsubsection.3.3.2.3}
\contentsline {section}{\numberline {3.4}Predictions}{42}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Stock}{42}{subsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.1.1}Predictions for 2011}{42}{subsubsection.3.4.1.1}
\contentsline {subsubsection}{\numberline {3.4.1.2}Predictions for the last 10 years}{44}{subsubsection.3.4.1.2}
\contentsline {subsection}{\numberline {3.4.2}Escapement}{47}{subsection.3.4.2}
\contentsline {chapter}{\numberline {4}Discussion}{53}{chapter.4}
\contentsline {section}{\numberline {4.1}Relationship between eel density and explanatory variables}{53}{section.4.1}
\contentsline {section}{\numberline {4.2}Comparison with other estimates of stock indicators}{53}{section.4.2}
\contentsline {section}{\numberline {4.3}Mortalities and management issues}{54}{section.4.3}
\contentsline {section}{\numberline {4.4}Bias and limits of the work}{54}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Using a better resolution GIS coverage to calculate water surface}{55}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Ensuring that electrofishing surveys have a national coverage}{57}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Inclusion of lake productivity estimates}{57}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}Conclusions on improving EDA}{57}{subsection.4.4.4}
\contentsline {section}{\numberline {4.5}Overall Conclusions}{58}{section.4.5}
\contentsline {chapter}{\numberline {5}Acknowledgments}{59}{chapter.5}
\contentsline {chapter}{\numberline {6}Glossary}{62}{chapter.6}
\contentsline {part}{II\hspace {1em}Annexes}{64}{part.2}
\contentsline {chapter}{{Annexe 1: Stock indicators }}{65}{section*.47}
\contentsline {chapter}{{Annexe 2: Homogeneity between \gls {ERS} and \gls {CCM} variables }}{67}{section*.49}
\contentsline {chapter}{{Annexe 3: Presence absence $\Delta $ model analysis }}{70}{section*.51}
\contentsline {chapter}{{Annexe 4: Density ($\Gamma $) model analysis }}{71}{section*.54}
