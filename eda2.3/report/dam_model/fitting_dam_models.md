---
title: "Test for models of dam effect in SUDOANG"
author: "Cedric and Maria"
date: "07 november 2019"
output: 
  html_document:
    number_sections: true
---



The code loads the datasets prepared for modelling in chunk delta and gamma.
The dataset for the model has been prepared in EDA_build.Rnw.
This vignette uses the purrr package to automate the building and testing of
models. To save time, unlike in eda2.0 not all combination of models are tested.




```r
load(file = paste0(ddatawd, "frsppt_2019.RData"))

rownames(frsppt) <- frsppt$ob_id # the first are in France
# we remove transport ="sure" and transport="possible categories
ddd <- frsppt[frsppt$year >= 1985,]


#columns=c("cumnbdamso",          
#		"cs_height_10_n","cs_height_08_n","cs_height_12_n","cs_height_15_n","cumnbdamp","cs_height_10_p",       
#		"cs_height_08_p", "cs_height_12_p",  "cs_height_15_p", "cs_height_10_pp", "cs_height_08_pp",  "cs_height_12_pp",      
#		"cs_height_15_pp", "cs_height_10_pps", "cs_height_08_pps", "cs_height_12_pps", "cs_height_15_pps",
#		"cs_height_10_pass0", "cs_height_10_pass1","cs_height_10_score0",
#		"cs_height_10_ppass0", "cs_height_10_ppass1","cs_height_10_pscore0",
#		"cs_height_10_pscore1","cs_height_10_FR",
#		"cs_height_10_SP","cs_height_10_PT")
test <- complete.cases(ddd[,c("year",
						"altitudem",
						"distanceseakm",
						"emu",
						"ef_fishingmethod",
						"densCS",
						"ef_wetted_area",
						"cs_height_10_p")])
ddd <- ddd[test,]

#str(frsppt_model_delta)		# 37088  obs. of  6 variables, 44018 obs. of  51 var M



# tests all model combination
# visreg does not accept language such as as.factor()
# transform data before
ddd$country <- as.factor(ddd$country)
# no eel should be there, or we consider it an artefact, but we want to keep the zeros
pb_transportdelta <- ddd$transport!="possible" & ddd$densCS>0
ddd <- ddd[!pb_transportdelta,]
ddg <- ddd[ddd$densCS>0,] # model gamma
pb_transportgamma <- ddd$transport!="possible" 
ddg <- ddg[!pb_transportgamma,]
ddgfr <- subset(ddg,country=='FR')
dddfr <- subset(ddd,country=='FR')
```

Notes for later
scoreonly=1 or score1 variables indicate the dams that have a score, i.e. they
are difficult to cross.
passonly=1 has a different meaning, it means that there is a pass, so the dam
should be easy to cross.

# description of the dataset


```r
describe(frsppt_model_delta)%>%html()
```

```
## Error in describe(frsppt_model_delta): object 'frsppt_model_delta' not found
```

# Model







```r
load(file=str_c(modelwd,"dam_test2.Rdata"))
load(file=str_c(modelwd,"dam_test1.Rdata"))
load(file=str_c(modelwd,"dddfr.Rdata"))
options(tibble.width =150)


# Not using AIC (as repeated) otherwise should use ,names_repair="unique" in unnest()
# unnest nicely unfolds the list of gla
# 
dam_test11 <- dam_test1%>%
		arrange(AIC)%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test11%>%select(1)%>%pull()
```

```
##  [1] "+s(cs_height_12_n, by = country, k = 3)"  
##  [2] "+s(cs_height_15_p, by = country, k = 3)"  
##  [3] "+s(cs_height_15_n, by = country, k = 3)"  
##  [4] "+s(cs_height_12_p, by = country, k = 3)"  
##  [5] "+s(cs_height_10_n, by = country, k = 3)"  
##  [6] "+s(cs_height_15_pp, by = country, k = 3)" 
##  [7] "+s(cs_height_15_pps, by = country, k = 3)"
##  [8] "+s(cs_height_10_p, by = country, k = 3)"  
##  [9] "+s(cs_height_12_pp, by = country, k = 3)" 
## [10] "+s(cs_height_12_pps, by = country, k = 3)"
## [11] "+s(cs_height_08_n, by = country, k = 3)"  
## [12] "+s(cs_height_10_pp, by = country, k = 3)" 
## [13] "+s(cs_height_10_pps, by = country, k = 3)"
## [14] "+s(cs_height_08_p, by = country, k = 3)"  
## [15] "+s(cs_height_08_pp, by = country, k = 3)" 
## [16] "+s(cs_height_08_pps, by = country, k = 3)"
```

```r
dam_test21 <- dam_test2%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test21%>%pull(vartested)
```

```
## [1] "+prop_height_score1xresid + prop_height_score0xresid"
## [2] "+prop_height_pass1xresid + prop_height_pass0xresid"  
## [3] "+cs_height_10_pass1 + cs_height_10_pass0"            
## [4] "+cs_height_10_score1 + cs_height_10_score0"          
## [5] "+resid + prop_height_pass0xresid"
```

```r
#summary for first model
modd_hsr <- (dam_test21%>%select(gammodel)%>%slice(1)%>%pull())[[1]] # "+prop_height_score1xresid + prop_height_score0xresid"
modd_hpr <- (dam_test21%>%select(gammodel)%>%slice(2)%>%pull())[[1]] # "+prop_height_pass1xresid + prop_height_pass0xresid"
modd_hp <- (dam_test21%>%select(gammodel)%>%slice(3)%>%pull())[[1]]  # "+cs_height_10_pass1 + cs_height_10_pass0" 
modd_hs <- (dam_test21%>%select(gammodel)%>%slice(4)%>%pull())[[1]]  # "+cs_height_10_score1 + cs_height_10_score0"    
# modèle avec pénalité
modd_hprr <- (dam_test21%>%select(gammodel)%>%slice(5)%>%pull())[[1]] # "+resid + prop_height_pass0xresid"
```




```r
load(file=str_c(modelwd,"dam_test6.Rdata"))
load(file=str_c(modelwd,"dam_test5.Rdata"))
load(file=str_c(modelwd,"dam_test4.Rdata"))
load(file=str_c(modelwd,"dam_test3.Rdata"))
load(file=str_c(modelwd,"ddgfr.Rdata"))
options(tibble.width =150)


# Not using AIC (as repeated) otherwise should use ,names_repair="unique" in unnest()
# unnest nicely unfolds the list of gla

#full model with interaction altitude and distance
dam_test31 <- dam_test3%>%
		arrange(AIC)%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test31%>%select(1)%>%pull()
```

```
##  [1] "+s(cs_height_08_n, by = country, k = 3)"  
##  [2] "+s(cs_height_08_p, by = country, k = 3)"  
##  [3] "+s(cs_height_10_n, by = country, k = 3)"  
##  [4] "+s(cs_height_10_p, by = country, k = 3)"  
##  [5] "+s(cs_height_12_n, by = country, k = 3)"  
##  [6] "+s(cs_height_12_p, by = country, k = 3)"  
##  [7] "+s(cs_height_08_pps, by = country, k = 3)"
##  [8] "+s(cs_height_08_pp, by = country, k = 3)" 
##  [9] "+s(cs_height_15_n, by = country, k = 3)"  
## [10] "+s(cs_height_15_p, by = country, k = 3)"  
## [11] "+s(cs_height_10_pps, by = country, k = 3)"
## [12] "+s(cs_height_10_pp, by = country, k = 3)" 
## [13] "+s(cs_height_12_pp, by = country, k = 3)" 
## [14] "+s(cs_height_12_pps, by = country, k = 3)"
## [15] "+s(cs_height_15_pp, by = country, k = 3)" 
## [16] "+s(cs_height_15_pps, by = country, k = 3)"
```

```r
#distance linear, using predicted height
dam_test41 <- dam_test4%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test41%>%pull(vartested)
```

```
## [1] "+prop_height_score0xresid + prop_height_score1xresid"
## [2] "+prop_height_pass1xresid + prop_height_pass0xresid"  
## [3] "+cs_height_10_pass1 + cs_height_10_pass0"            
## [4] "+cs_height_10_score1 + cs_height_10_score0"
```

```r
#same model than 4.1 but only for distance < 150 km
dam_test51 <- dam_test5%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test51%>%pull(vartested)
```

```
## [1] "+prop_height_score1xresid + prop_height_score0xresid"
## [2] "+prop_height_pass1xresid + prop_height_pass0xresid"  
## [3] "+cs_height_10_pass1 + cs_height_10_pass0"            
## [4] "+cs_height_10_score1 + cs_height_10_score0"
```

```r
# model without predictions, distance linear
dam_test61 <- dam_test6%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test61%>%pull(vartested)
```

```
## [1] "+prop_height_score1xresid + prop_height_score0xresid"
## [2] "+prop_height_pass1xresid + prop_height_pass0xresid"  
## [3] "+cs_height_10_pass1 + cs_height_10_pass0"            
## [4] "+cs_height_10_score1 + cs_height_10_score0"
```

```r
dam_test71 <- dam_test7%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

dam_test81 <- dam_test8%>%
		select(1,2,3,5,6)%>%
		unnest('cols'=c(gla))

#summary for first model
modg_hsr <- (dam_test41%>%select(gammodel)%>%slice(1)%>%pull())[[1]]
modg_hpr <- (dam_test41%>%select(gammodel)%>%slice(2)%>%pull())[[1]]
modg_hp <- (dam_test41%>%select(gammodel)%>%slice(3)%>%pull())[[1]]
modg_hs <- (dam_test41%>%select(gammodel)%>%slice(4)%>%pull())[[1]]

modg_hsrc150 <- (dam_test51%>%select(gammodel)%>%slice(1)%>%pull())[[1]]
modg_hprc150 <- (dam_test51%>%select(gammodel)%>%slice(2)%>%pull())[[1]]
modg_hpc150 <- (dam_test51%>%select(gammodel)%>%slice(3)%>%pull())[[1]]
modg_hsc150 <- (dam_test51%>%select(gammodel)%>%slice(4)%>%pull())[[1]]

modgp_hsr <- (dam_test61%>%select(gammodel)%>%slice(1)%>%pull())[[1]]
modgp_hpr <- (dam_test61%>%select(gammodel)%>%slice(2)%>%pull())[[1]]
modgp_hp <- (dam_test61%>%select(gammodel)%>%slice(3)%>%pull())[[1]]
modgp_hs <- (dam_test61%>%select(gammodel)%>%slice(4)%>%pull())[[1]]

mod7 <- (dam_test71%>%select(gammodel)%>%slice(1)%>%pull())[[1]]
mod8 <- (dam_test81%>%select(gammodel)%>%slice(1)%>%pull())[[1]]
```

# Variable analysis


```r
#i.m. stands for Inspect model d=delta
ggplot(ddd)+geom_boxplot(aes(x=densCS>0,y=cs_height_15_p))
```

![plot of chunk i.m.d.wrongheight](figure/i.m.d.wrongheight-1.png)

```r
ggplot(ddd)+geom_boxplot(aes(x=densCS>0,y=cs_height_08_pp))
```

![plot of chunk i.m.d.wrongheight](figure/i.m.d.wrongheight-2.png)

```r
ggplot(ddd)+geom_boxplot(aes(x=densCS>0,y=cs_height_10_p))
```

![plot of chunk i.m.d.wrongheight](figure/i.m.d.wrongheight-3.png)

```r
# get a vector of densities positives for very large height
# ddd%>%filter(cs_height_10_pass1>100 & densCS>0)%>%select("idsegment")%>%pull()%>%vsql()
# ddd%>%filter(cs_height_10_pass1>100 & densCS>0)%>%select("ob_id")%>%pull()%>%vsql()
```


```r
#i.m. stands for Inspect model g=gamma

#x11()
par(mfrow = c(1, 3))
ggplot(ddg)+geom_point(aes(x=densCS,y=cs_height_15_p))+facet_wrap(~country)
```

```
## Warning: Removed 14858 rows containing missing values (geom_point).
```

![plot of chunk i.m.g.wrongheight](figure/i.m.g.wrongheight-1.png)

```r
ggplot(ddg)+geom_point(aes(x=densCS,y=cs_height_08_pp))
```

```
## Warning: Removed 14858 rows containing missing values (geom_point).
```

![plot of chunk i.m.g.wrongheight](figure/i.m.g.wrongheight-2.png)

```r
ggplot(ddg)+geom_point(aes(x=densCS,y=cs_height_10_p))
```

```
## Warning: Removed 14858 rows containing missing values (geom_point).
```

![plot of chunk i.m.g.wrongheight](figure/i.m.g.wrongheight-3.png)

```r
# get a vector of densities positives for very large height
# ddd%>%filter(cs_height_10_pass1>100 & densCS>0)%>%select("idsegment")%>%pull()%>%vsql()
# ddd%>%filter(cs_height_10_pass1>100 & densCS>0)%>%select("ob_id")%>%pull()%>%vsql()
```



```r
par(mfrow = c(2, 4), mar = c(4, 3, 3, 2))
dotchart(ddd$densCS[order(ddd$densCS)], main = "densCS")
dotchart(ddd$year[order(ddd$year)], main = "year")
dotchart(ddd$ef_wetted_area[order(ddd$ef_wetted_area)], main = "ef_wetted_area")
```

```
## Warning in dotchart(ddd$ef_wetted_area[order(ddd$ef_wetted_area)],
## main = "ef_wetted_area"): 'x' is neither a vector nor a matrix: using
## as.numeric(x)
```

```r
dotchart(ddd$altitudem[order(ddd$altitudem)], main = "altitudem")
dotchart(ddd$distanceseakm[order(ddd$distanceseakm)], main = "distanceseakm")
dotchart(ddd$cs_height_10_p[order(ddd$cs_height_10_p)], main = "cs_height_10_p")
dotchart(ddd$cs_height_10_pass0[order(ddd$cs_height_10_pass0)], main = "cs_height_10_pass0")
dotchart(ddd$cs_height_10_pass1[order(ddd$cs_height_10_pass1)], main = "cs_height_10_pass1")
```

![plot of chunk i.m.d.corrgram](figure/i.m.d.corrgram-1.png)

```r
numvar<-c("densCS","year","ef_wetted_area","altitudem","distanceseakm",
		"cs_height_10_p","cs_height_10_pass0","cs_height_10_pass1")
corrgram(ddd[,numvar],lower.panel=panel.shade, upper.panel=panel.pie,order=TRUE)
```

![plot of chunk i.m.d.corrgram](figure/i.m.d.corrgram-2.png)

```r
# fonctions graphiques voir ? pairs.
# very long... uncomment to see
#panel.hist <- function(x, ...)
#{
#	usr <- par("usr"); on.exit(par(usr))
#	par(usr = c(usr[1:2], 0, 1.5) )
#	h <- hist(x, plot = FALSE)
#	breaks <- h$breaks; nB <- length(breaks)
#	y <- h$counts; y <- y/max(y)
#	rect(breaks[-nB], 0, breaks[-1], y, col = "cyan", ...)
#}
#panel.cor <- function(x, y, digits = 2, prefix = "", cex.cor, ...)
#{
#	usr <- par("usr"); on.exit(par(usr))
#	par(usr = c(0, 1, 0, 1))
#	r <- abs(cor(x, y,use="pairwise.complete.obs"))
#	txt <- format(c(r, 0.123456789), digits = digits)[1]
#	txt <- paste0(prefix, txt)
#	if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)
#	text(0.5, 0.5, txt, cex = cex.cor * r)
#}
#
#pairs(~densCS+year+ef_wetted_area+altitudem+distanceseakm+cs_height_10_p+
#				cs_height_10_pass0+cs_height_10_pass1,data=ddd,
#		panel = panel.smooth,
#		cex = 1, pch = 24, bg = "light blue",
#		diag.panel = panel.hist, cex.labels = 1, font.labels = 2)
#
#
#pairs(~densCS+year+ef_wetted_area+altitudem+distanceseakm+cs_height_10_p+
#				cs_height_10_pass0+cs_height_10_pass1,data=ddd, 
#		lower.panel = panel.smooth, 
#		upper.panel = panel.cor)

# dddfr
```


```r
par(mfrow = c(2, 4), mar = c(4, 3, 3, 2))
dotchart(ddg$densCS[order(ddg$densCS)], main = "densCS")
dotchart(ddg$year[order(ddg$year)], main = "year")
dotchart(ddg$ef_wetted_area[order(ddg$ef_wetted_area)], main = "ef_wetted_area")
dotchart(ddg$altitudem[order(ddg$altitudem)], main = "altitudem")
dotchart(ddg$distanceseakm[order(ddg$distanceseakm)], main = "distanceseakm")
dotchart(ddg$cs_height_10_p[order(ddg$cs_height_10_p)], main = "cs_height_10_p")
dotchart(ddg$cs_height_10_pass0[order(ddg$cs_height_10_pass0)], main = "cs_height_10_pass0")
dotchart(ddg$cs_height_10_pass1[order(ddg$cs_height_10_pass1)], main = "cs_height_10_pass1")
```

![plot of chunk i.m.g.corrgram](figure/i.m.g.corrgram-1.png)

```r
numvar<-c("densCS","year","ef_wetted_area","altitudem","distanceseakm",
		"cs_height_10_p","cs_height_10_pass0","cs_height_10_pass1")
corrgram(ddg[,numvar],lower.panel=panel.shade, upper.panel=panel.pie,order=TRUE)
```

![plot of chunk i.m.g.corrgram](figure/i.m.g.corrgram-2.png)

```r
# fonctions graphiques voir ? pairs.
# very long... uncomment to see
#panel.hist <- function(x, ...)
#{
#	usr <- par("usr"); on.exit(par(usr))
#	par(usr = c(usr[1:2], 0, 1.5) )
#	h <- hist(x, plot = FALSE)
#	breaks <- h$breaks; nB <- length(breaks)
#	y <- h$counts; y <- y/max(y)
#	rect(breaks[-nB], 0, breaks[-1], y, col = "cyan", ...)
#}
#panel.cor <- function(x, y, digits = 2, prefix = "", cex.cor, ...)
#{
#	usr <- par("usr"); on.exit(par(usr))
#	par(usr = c(0, 1, 0, 1))
#	r <- abs(cor(x, y,use="pairwise.complete.obs"))
#	txt <- format(c(r, 0.123456789), digits = digits)[1]
#	txt <- paste0(prefix, txt)
#	if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)
#	text(0.5, 0.5, txt, cex = cex.cor * r)
#}
#
#pairs(~densCS+year+ef_wetted_area+altitudem+distanceseakm+cs_height_10_p+
#				cs_height_10_pass0+cs_height_10_pass1,data=ddg,
#		panel = panel.smooth,
#		cex = 1, pch = 24, bg = "light blue",
#		diag.panel = panel.hist, cex.labels = 1, font.labels = 2)
#
#
#pairs(~densCS+year+ef_wetted_area+altitudem+distanceseakm+cs_height_10_p+
#				cs_height_10_pass0+cs_height_10_pass1,data=ddg, 
#		lower.panel = panel.smooth, 
#		upper.panel = panel.cor)

# ddgfr
```

# Model results


```r
require(stargazer)
library(mgcv)
stargazer(modd_hsr,modd_hpr,modd_hp,modd_hs, type='text',title='Testing the effect of score',
		no.pace=TRUE, keep.stat=c("adj.rsq","aic","n"),ci=TRUE, single.row=TRUE,
		keep=c("cs_height","ef_fishingmethod","emu","prop_height"))
```


Testing the effect of score
                                                                                               Dependent variable:                                                                      
                                                                                                   densCS > 0                                                                           
                                           (1)                                     (2)                                     (3)                                     (4)                  
emuFR_Arto                     -0.654*** (-0.898, -0.410)              -0.587*** (-0.831, -0.344)              -0.671*** (-0.915, -0.427)              -0.661*** (-0.903, -0.419)       
emuFR_Bret                       0.620*** (0.369, 0.871)                 0.914*** (0.682, 1.146)                 0.969*** (0.737, 1.200)                 0.748*** (0.505, 0.990)        
emuFR_Garo                       0.801*** (0.598, 1.005)                 0.767*** (0.565, 0.968)                 0.742*** (0.538, 0.946)                 0.780*** (0.577, 0.983)        
emuFR_Loir                       1.217*** (1.001, 1.432)                 1.422*** (1.212, 1.632)                 1.326*** (1.111, 1.541)                 1.214*** (1.000, 1.429)        
emuFR_Meus               -37.933 (-5,070,184.000, 5,070,108.000) -38.967 (-5,044,021.000, 5,043,943.000) -39.035 (-5,044,021.000, 5,043,943.000) -38.973 (-5,044,021.000, 5,043,943.000)
emuFR_Rhin               -37.732 (-3,036,805.000, 3,036,730.000) -38.896 (-3,016,772.000, 3,016,694.000) -38.925 (-3,016,772.000, 3,016,694.000) -38.805 (-3,016,772.000, 3,016,695.000)
emuFR_Rhon                     -1.578*** (-1.782, -1.375)              -1.543*** (-1.743, -1.343)              -1.448*** (-1.649, -1.248)              -1.551*** (-1.753, -1.349)       
emuFR_Sein                       1.477*** (1.266, 1.689)                 1.448*** (1.239, 1.656)                 1.408*** (1.197, 1.620)                 1.466*** (1.255, 1.678)        
ef_fishingmethodcoa              1.876*** (1.476, 2.276)                 1.736*** (1.370, 2.102)                 1.777*** (1.412, 2.143)                 1.680*** (1.315, 2.045)        
ef_fishingmethodiaa              0.198** (0.003, 0.392)                  0.231** (0.038, 0.423)                  0.246** (0.054, 0.438)                  0.249** (0.058, 0.441)         
ef_fishingmethodber              0.652*** (0.495, 0.809)                 0.621*** (0.465, 0.778)                 0.656*** (0.499, 0.812)                 0.678*** (0.521, 0.835)        
ef_fishingmethodgm               0.788*** (0.667, 0.910)                 0.777*** (0.656, 0.898)                 0.806*** (0.684, 0.928)                 0.789*** (0.667, 0.910)        
prop_height_score1xresid       -0.016*** (-0.019, -0.012)                                                                                                                               
prop_height_score0xresid       -0.025*** (-0.027, -0.024)                                                                                                                               
prop_height_pass1xresid                                                -0.022*** (-0.024, -0.020)                                                                                       
prop_height_pass0xresid                                                -0.024*** (-0.025, -0.023)                                                                                       
cs_height_10_pass1                                                                                             -0.029*** (-0.033, -0.025)                                               
cs_height_10_pass0                                                                                             -0.026*** (-0.027, -0.024)                                               
cs_height_10_score1                                                                                                                                       0.006 (-0.005, 0.018)         
cs_height_10_score0                                                                                                                                    -0.027*** (-0.028, -0.025)       
Observations                             29,520                                  30,026                                  30,026                                  30,026                 
Adjusted R2                               0.599                                   0.603                                   0.603                                   0.604                 
Note:                                                                                                                                                        *p<0.1; **p<0.05; ***p<0.01

Testing the effect of score
 

```r
require(stargazer)
library(mgcv)
stargazer(modg_hsr,modg_hpr,modg_hp,modg_hs, type='text',title='Testing the effect of score gamma model',
		no.pace=TRUE, keep.stat=c("adj.rsq","aic","n"),ci=TRUE, single.row=TRUE,
		keep=c("cs_height","ef_fishingmethod","emu","prop_height"))
```


Testing the effect of score gamma model
                                                                     Dependent variable:                                            
                                                                           densCS                                                   
                                    (1)                        (2)                        (3)                        (4)            
emuFR_Arto                0.308*** (0.121, 0.496)     0.234** (0.047, 0.420)    0.292*** (0.104, 0.481)    0.353*** (0.161, 0.544)  
emuFR_Bret                0.725*** (0.572, 0.879)    0.718*** (0.570, 0.866)    0.709*** (0.561, 0.858)    0.669*** (0.516, 0.823)  
emuFR_Garo                0.228*** (0.077, 0.379)     0.152** (0.002, 0.301)     0.193** (0.041, 0.345)    0.268*** (0.114, 0.422)  
emuFR_Loir                0.355*** (0.202, 0.508)    0.216*** (0.066, 0.366)    0.216*** (0.064, 0.369)    0.341*** (0.184, 0.497)  
emuFR_Rhon                 0.078 (-0.081, 0.236)      0.207** (0.047, 0.367)    0.219*** (0.057, 0.381)     0.088 (-0.073, 0.250)   
emuFR_Sein                0.535*** (0.389, 0.681)    0.562*** (0.417, 0.706)    0.602*** (0.456, 0.748)    0.560*** (0.411, 0.708)  
ef_fishingmethodcoa       1.868*** (1.715, 2.020)    1.919*** (1.767, 2.071)    1.853*** (1.700, 2.006)    1.835*** (1.679, 1.990)  
ef_fishingmethodiaa       0.218*** (0.090, 0.346)    0.222*** (0.095, 0.349)    0.219*** (0.090, 0.347)    0.240*** (0.110, 0.370)  
ef_fishingmethodber       0.655*** (0.527, 0.784)    0.740*** (0.612, 0.867)    0.752*** (0.624, 0.881)    0.686*** (0.555, 0.818)  
ef_fishingmethodgm       -0.339*** (-0.436, -0.241) -0.293*** (-0.389, -0.196) -0.332*** (-0.429, -0.234) -0.354*** (-0.453, -0.255)
prop_height_score0xresid -0.011*** (-0.012, -0.009)                                                                                 
prop_height_score1xresid -0.025*** (-0.031, -0.019)                                                                                 
prop_height_pass1xresid                             -0.025*** (-0.028, -0.022)                                                      
prop_height_pass0xresid                             -0.009*** (-0.011, -0.008)                                                      
cs_height_10_pass1                                                             -0.032*** (-0.036, -0.027)                           
cs_height_10_pass0                                                             -0.009*** (-0.010, -0.007)                           
cs_height_10_score1                                                                                       -0.016*** (-0.026, -0.007)
cs_height_10_score0                                                                                       -0.011*** (-0.013, -0.010)
Observations                       12,413                     12,413                     12,413                     12,413          
Adjusted R2                        0.225                      0.223                      0.235                      0.230           
Note:                                                                                                    *p<0.1; **p<0.05; ***p<0.01

Testing the effect of score gamma model
 

```r
# modèles avec distance de 150 km
stargazer(modg_hsrc150,modg_hprc150,modg_hpc150,modg_hsc150, type='text',
		title='Testing the effect of score gamma model on censored data',
		no.pace=TRUE, keep.stat=c("adj.rsq","aic","n"),ci=TRUE, single.row=TRUE,
		keep=c("cs_height","ef_fishingmethod","emu","prop_height"))
```


Testing the effect of score gamma model on censored data
                                                                     Dependent variable:                                            
                                                                           densCS                                                   
                                    (1)                        (2)                        (3)                        (4)            
emuFR_Arto                0.440*** (0.239, 0.642)    0.381*** (0.183, 0.579)    0.444*** (0.243, 0.645)    0.557*** (0.351, 0.763)  
emuFR_Bret                0.600*** (0.436, 0.763)    0.670*** (0.513, 0.828)    0.668*** (0.510, 0.827)    0.560*** (0.395, 0.725)  
emuFR_Garo                0.822*** (0.634, 1.010)    0.733*** (0.548, 0.918)    0.766*** (0.580, 0.952)    0.855*** (0.666, 1.044)  
emuFR_Loir                0.968*** (0.768, 1.168)    0.966*** (0.770, 1.161)    1.020*** (0.822, 1.219)    0.977*** (0.772, 1.182)  
emuFR_Rhon                 0.135 (-0.035, 0.305)     0.248*** (0.079, 0.417)    0.284*** (0.112, 0.456)     0.166* (-0.008, 0.339)  
emuFR_Sein                0.495*** (0.328, 0.662)    0.485*** (0.322, 0.649)    0.503*** (0.338, 0.669)    0.521*** (0.351, 0.691)  
ef_fishingmethodcoa       1.909*** (1.757, 2.062)    1.962*** (1.811, 2.112)    1.922*** (1.771, 2.074)    1.861*** (1.706, 2.017)  
ef_fishingmethodiaa       0.265*** (0.128, 0.402)    0.283*** (0.149, 0.417)    0.280*** (0.144, 0.415)    0.273*** (0.135, 0.412)  
ef_fishingmethodber       0.355*** (0.148, 0.561)    0.411*** (0.208, 0.613)    0.391*** (0.188, 0.595)    0.415*** (0.206, 0.625)  
ef_fishingmethodgm       -0.394*** (-0.519, -0.269) -0.352*** (-0.475, -0.229) -0.387*** (-0.511, -0.263) -0.400*** (-0.527, -0.272)
prop_height_score1xresid -0.009** (-0.016, -0.001)                                                                                  
prop_height_score0xresid -0.012*** (-0.015, -0.010)                                                                                 
prop_height_pass1xresid                             -0.021*** (-0.024, -0.017)                                                      
prop_height_pass0xresid                             -0.011*** (-0.013, -0.008)                                                      
cs_height_10_pass1                                                             -0.029*** (-0.035, -0.024)                           
cs_height_10_pass0                                                             -0.011*** (-0.014, -0.009)                           
cs_height_10_score1                                                                                        0.020*** (0.005, 0.035)  
cs_height_10_score0                                                                                       -0.017*** (-0.020, -0.014)
Observations                       7,215                      7,215                      7,215                      7,215           
Adjusted R2                        0.077                      0.102                      0.151                      0.125           
Note:                                                                                                    *p<0.1; **p<0.05; ***p<0.01

Testing the effect of score gamma model on censored data
 

```r
stargazer(mod8, type='text',title='Testing the effect of score gamma model',
		no.pace=TRUE, keep.stat=c("adj.rsq","aic","n"),ci=TRUE, single.row=TRUE,
		keep=c("cs_height","ef_fishingmethod","emu","prop_height"))
```


Testing the effect of score gamma model
                                       Dependent variable:    
                                             densCS           
emuFR_Arto                           -0.572 (-1.278, 0.134)   
emuFR_Bret                           0.572* (-0.064, 1.208)   
emuFR_Garo                           -0.430 (-1.177, 0.316)   
emuFR_Loir                           -0.389 (-1.139, 0.361)   
emuFR_Rhon                           -0.076 (-0.728, 0.576)   
emuFR_Sein                            0.329 (-0.322, 0.981)   
ef_fishingmethodcoa                  1.886*** (1.727, 2.045)  
ef_fishingmethodiaa                  0.226*** (0.090, 0.361)  
ef_fishingmethodber                  0.637*** (0.424, 0.851)  
ef_fishingmethodgm                 -0.464*** (-0.590, -0.339) 
emuFR_Adou distanceseakm           -0.015*** (-0.019, -0.011) 
emuFR_Arto distanceseakm           -0.030*** (-0.036, -0.025) 
emuFR_Bret distanceseakm           -0.008*** (-0.010, -0.006) 
emuFR_Garo distanceseakm           -0.008*** (-0.011, -0.004) 
emuFR_Loir distanceseakm              0.001 (-0.004, 0.005)   
emuFR_Rhon distanceseakm           -0.017*** (-0.020, -0.014) 
emuFR_Sein distanceseakm           -0.008*** (-0.011, -0.005) 
emuFR_Adou resid                   -0.039*** (-0.058, -0.021) 
emuFR_Arto resid                      0.014 (-0.008, 0.035)   
emuFR_Bret resid                   -0.043*** (-0.050, -0.035) 
emuFR_Garo resid                     -0.012 (-0.029, 0.005)   
emuFR_Loir resid                   -0.025*** (-0.037, -0.014) 
emuFR_Rhon resid                   -0.012*** (-0.019, -0.005) 
emuFR_Sein resid                   -0.031*** (-0.043, -0.019) 
emuFR_Adou prop_height_pass0xresid   0.016* (-0.001, 0.032)   
emuFR_Arto prop_height_pass0xresid   -0.004 (-0.022, 0.014)   
emuFR_Bret prop_height_pass0xresid   0.015*** (0.009, 0.021)  
emuFR_Garo prop_height_pass0xresid    0.008 (-0.006, 0.022)   
emuFR_Loir prop_height_pass0xresid    0.009 (-0.003, 0.020)   
emuFR_Rhon prop_height_pass0xresid -0.006** (-0.012, -0.0005) 
emuFR_Sein prop_height_pass0xresid    0.001 (-0.009, 0.010)   
Observations                                  7,215           
Adjusted R2                                   0.209           
Note:                              *p<0.1; **p<0.05; ***p<0.01

Testing the effect of score gamma model
 



The standard command plot.gam(all.terms=TRUE) crashes so I'm trying to 


```r
mmm <- modg_hp
par(par(mfrow = c(4, 4), mar = c(4, 3, 3, 2)))
plot(mmm,ylim=c(-5,5),pages=2)
```

![plot of chunk i.m.d.gamstdplot](figure/i.m.d.gamstdplot-1.png)

```r
#plot(mmm,all.terms=TRUE) marche pas
mmm$data <- as.data.frame(dddfr)
#plot(mmm,all.terms=TRUE)
#Error in if (NROW(tms) < NROW(data)) match(rownames(tms), rownames(data)) : 
#  argument is of length zero
stats::termplot(mmm,data=as.data.frame(dddfr),terms="ef_fishingmethod", se = TRUE)
```

![plot of chunk i.m.d.gamstdplot](figure/i.m.d.gamstdplot-2.png)

```r
#stats::termplot(mmm,data=as.data.frame(dddfr),terms="emu") error
par(mfrow=c(1,2))
stats::termplot(mmm,data=as.data.frame(dddfr),terms="cs_height_10_pass1",se=TRUE,ylim=c(-15,0),
		main="sum resid height with pass")
stats::termplot(mmm,data=as.data.frame(dddfr),terms="cs_height_10_pass0",se=TRUE,ylim=c(-15,0),
		main="sum resid height without pass")
```

![plot of chunk i.m.d.gamstdplot](figure/i.m.d.gamstdplot-3.png)

```r
dddfrm <-melt(dddfr,measure.vars=c("prop_height_pass1xresid","prop_height_pass0xresid"), variable.name="residheight")

ggplot(dddfrm)+geom_point(aes(x=densCS>0,y=value))+
		geom_boxplot(aes(x=densCS>0,y=value))+
		geom_smooth(aes(x=densCS>0,y=value), formula=densCS>0~value,   method="loess")+
		facet_wrap(~residheight )
```

![plot of chunk i.m.d.gamstdplot](figure/i.m.d.gamstdplot-4.png)

```r
mean(dddfr$prop_height_pass1xresid, na.rm=TRUE)
```

[1] 42.40757

```r
plot(dddfr$densCS>0,dddfr$prop_height_pass0xresid)
mean(dddfr$prop_height_pass0xresid, na.rm=TRUE)
```

[1] 119.9987
![plot of chunk i.m.d.gamstdplot](figure/i.m.d.gamstdplot-5.png)



```r
mmm <- modg_hprc150
plot(mmm)
```

![plot of chunk i.m.g.gamstdplot](figure/i.m.g.gamstdplot-1.png)

```r
#  argument is of length zero
stats::termplot(mmm,data=as.data.frame(ddgfr),terms="ef_fishingmethod", se = TRUE)
```

![plot of chunk i.m.g.gamstdplot](figure/i.m.g.gamstdplot-2.png)

```r
#stats::termplot(mmm,data=as.data.frame(dddfr),terms="emu") error
par(mfrow=c(1,2))
stats::termplot(mmm,data=as.data.frame(dddfr),terms="prop_height_pass1xresid",se=TRUE,ylim=c(-2,0),
		main="sum resid height with pass")
stats::termplot(mmm,data=as.data.frame(dddfr),terms="prop_height_pass0xresid",se=TRUE,ylim=c(-2,0),
		main="sum resid height without pass")
```

![plot of chunk i.m.g.gamstdplot](figure/i.m.g.gamstdplot-3.png)

```r
ddgfrm <-melt(ddgfr,measure.vars=c("prop_height_pass1xresid","prop_height_pass0xresid"), variable.name="residheight")

ggplot(ddgfrm)+geom_point(aes(x=value,y=densCS),alpha=0.5, size=0.5)+
		#geom_density(aes(x=densCS,y=value))+
		geom_smooth(aes(x=value,y=densCS),method="lm")+
		facet_wrap(~residheight )
```

![plot of chunk i.m.g.gamstdplot](figure/i.m.g.gamstdplot-4.png)

```r
mean(ddgfr$prop_height_pass1xresid, na.rm=TRUE)
```

[1] 11.55504

```r
plot(ddgfr$densCS,ddgfr$prop_height_pass0xresid)
mean(ddgfr$prop_height_pass0xresid, na.rm=TRUE)
```

[1] 27.31749
![plot of chunk i.m.g.gamstdplot](figure/i.m.g.gamstdplot-5.png)


```r
# GAMM
library(itsadug)
library(mgcv)


# TODO autocorrelation checks
# TODO check models
```

```r
mmmm <- modd_hpr
gamtabs(mmm, type="HTML")
```

<!-- html table generated in R 3.6.1 by xtable 1.8-4 package -->
<!-- Mon May 18 14:49:10 2020 -->
<table border=1>
<caption align="bottom">   </caption>
  <tr> <td> A. parametric coefficients </td> <td align="right"> Estimate </td> <td align="right"> Std. Error </td> <td align="right"> t-value </td> <td align="right"> p-value </td> </tr>
  <tr> <td> (Intercept) </td> <td align="right"> -2.2101 </td> <td align="right"> 0.0842 </td> <td align="right"> -26.2574 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> distanceseakm </td> <td align="right"> -0.0117 </td> <td align="right"> 0.0006 </td> <td align="right"> -20.4673 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> emuFR_Arto </td> <td align="right"> 0.3809 </td> <td align="right"> 0.1008 </td> <td align="right"> 3.7790 </td> <td align="right"> 0.0002 </td> </tr>
  <tr> <td> emuFR_Bret </td> <td align="right"> 0.6703 </td> <td align="right"> 0.0803 </td> <td align="right"> 8.3460 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> emuFR_Garo </td> <td align="right"> 0.7330 </td> <td align="right"> 0.0944 </td> <td align="right"> 7.7614 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> emuFR_Loir </td> <td align="right"> 0.9657 </td> <td align="right"> 0.0998 </td> <td align="right"> 9.6799 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> emuFR_Rhon </td> <td align="right"> 0.2484 </td> <td align="right"> 0.0863 </td> <td align="right"> 2.8796 </td> <td align="right"> 0.0040 </td> </tr>
  <tr> <td> emuFR_Sein </td> <td align="right"> 0.4853 </td> <td align="right"> 0.0836 </td> <td align="right"> 5.8071 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> ef_fishingmethodcoa </td> <td align="right"> 1.9615 </td> <td align="right"> 0.0766 </td> <td align="right"> 25.5986 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> ef_fishingmethodiaa </td> <td align="right"> 0.2827 </td> <td align="right"> 0.0684 </td> <td align="right"> 4.1353 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> ef_fishingmethodber </td> <td align="right"> 0.4109 </td> <td align="right"> 0.1033 </td> <td align="right"> 3.9761 </td> <td align="right"> 0.0001 </td> </tr>
  <tr> <td> ef_fishingmethodgm </td> <td align="right"> -0.3518 </td> <td align="right"> 0.0627 </td> <td align="right"> -5.6140 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> prop_height_pass1xresid </td> <td align="right"> -0.0209 </td> <td align="right"> 0.0018 </td> <td align="right"> -11.8419 </td> <td align="right"> &lt; 0.0001 </td> </tr>
  <tr> <td> prop_height_pass0xresid </td> <td align="right"> -0.0106 </td> <td align="right"> 0.0012 </td> <td align="right"> -8.8159 </td> <td align="right"> &lt; 0.0001 </td> </tr>
   <tr> <td> B. smooth terms </td> <td align="right"> edf </td> <td align="right"> Ref.df </td> <td align="right"> F-value </td> <td align="right"> p-value </td> </tr>
  <tr> <td> s(year) </td> <td align="right"> 4.9383 </td> <td align="right"> 4.9980 </td> <td align="right"> 29.8383 </td> <td align="right"> &lt; 0.0001 </td> </tr>
   <a name=tab.gam></a>
</table>



```r
library(visreg)
mmm<-mod7
summary(mmm)
mmm$data <- as.data.frame(ddgfr)
visreg(mmm,"prop_height_pass0xresid", by="emu")
visreg(fit=mmm,  "year")
visreg(fit=mmm,  "prop_height_pass1xresid")
visreg(fit=mmm,  "prop_height_pass0xresid")
trellis.par.set(strip.background=list(col="white"))
visreg(mmm, "emu")
#visreg(fit=mmm,  "year",
#		type = "conditional", 
#		xlab=expression(paste("Year (",y,")")),
#		scale = "response",
#		cond=list(distanceseakm=100,ef_fishingmethod="com",emu="FR_Loir",ef_wetted_area=600,prop_height_pass1xresid=0,prop_height_pass0xresid=0),
#		ylab=expression(paste("prob. mod. (",Delta,")",sep="")),
#		ylim=c(0.6,1),
#		points=list(cex=0.5, pch=16),
#		partial=TRUE,
#		overlay=TRUE) 
#visreg(fit=mmm,  "prop_height_pass1xresid",
#		type = "conditional", 
#		xlab=expression(paste("Propheight (",R(h~d),")")),
#		scale = "response",
#		cond=list(distanceseakm=0,ef_fishingmethod="com",emu="FR_Loir",ef_wetted_area=600,year=2015),
#		ylab=expression(paste("prob. mod. (",Delta,")",sep="")),
#		ylim=c(0,1),
#		points=list(cex=0.5, pch=16),
#		partial=FALSE,
#		overlay=TRUE) 
```







```r
require(PresenceAbsence)
threshold=0.4 
fr_model_delta <- frsppt_model_delta %>% filter(country=='FR') %>%
		select(cs_height_10_p, cs_height_10_pass0, cs_height_10_score0,
				densCS, year, ef_wetted_area, altitudem, distanceseakm,
				emu, ef_fishingmethod,op_id)
fr_model_delta <- fr_model_delta[complete.cases(fr_model_delta),]
# TODO find why missing cumulated height.....
DATA=data.frame(fr_model_delta[,"op_id"],fr_model_delta$densCS>0,predict(model_height_score,type="response"))
presence.absence.summary(DATA,main="Accuracy plot for presence absence",
		opt.thresholds=TRUE,
		opt.methods=c(1,2,4),
		req.sens=0.85,
		req.spec=0.85,
		truncate.tallest=TRUE,
		obs.prev=NULL,
		smoothing=1,
		vert.lines=FALSE,
		add.legend=TRUE,
		add.opt.legend=TRUE,
		legend.cex=0.6,
		opt.legend.cex=0.6,
		pch=NULL
)

matrice_confusion=cmx(DATA,threshold)
perc_deviance<-(1-deviance(model_height_score)/model_height_score$null.deviance)*100
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion)# correctement prédits
print(paste("correctly predicted",round(pbcl,2)))
correctly_predicted<-round(pbcl,2)
pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1]) # present correctement prédits
print(paste("present correctly predicted",correctly_predicted))
present_correctly_predicted<-round(pcp,2)
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2]) # absent correctement prédit
absents_correctly_predicted<-round(acp,2)
print(paste("absents correctly predicted",absents_correctly_predicted))
(K=Kappa(matrice_confusion))
```
