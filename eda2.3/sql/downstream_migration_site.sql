--- all starts from pulling data into the eda2.3 db chunk compare_with_existing_prod in edabuild
/*
con=dbConnect(RPostgres::Postgres(),
    dbname="eda2.3",
    host="localhost",
    port=5432,
    user= userlocal,
    password= passwordlocal)
dbExecute(con, "drop table if exists france.site")
st_write(deval4, con,  Id(schema="france", table = "site"))
dbDisconnect(con)
*/

-- add coordinates where missing
SELECT * FROM france.site;


WITH coords AS (
SELECT st_transform(st_centroid(geom), 4326) AS cc FROM france.rn WHERE idsegment =
(SELECT DISTINCT(idsegment) FROM france.site WHERE site='Bages_sigean'))

UPDATE france.site SET geom=cc FROM coords WHERE site='Bages_sigean'; --21


WITH coords AS (
SELECT st_transform(st_centroid(geom), 4326) cc FROM france.rn WHERE idsegment =
(SELECT DISTINCT(idsegment) FROM france.site WHERE site='Etang de l''Or'))

UPDATE france.site SET geom=cc FROM coords WHERE site='Etang de l''Or'; --13

