# Name : query_sudoang_data.R
# Date : 21/10/2021
# Authors: Maria Mateo
# R 4.0.4
#
# The following script is used to perform ad hoc queries to the SUDOANG database
#
##################################################################################

# -- Load libraries ---- ####
library(dplyr)
library(utils)

# -- Set worknig directory ---- ####
ddatawd <- paste0("C:/workspace/EDAdata/report2.3/data/")
tablewd <- "C:/workspace/EDAdata/report2.3/table/"


# -- Read data ----  ####
load(file=str_c(ddatawd,"table_silv.Rdata"))


# 1.	Observed biometric data of silver eels in Spain
# ###################################################### #

# unique(table_silv$emu)

silv_sp <- table_silv[table_silv$emu == "ES_Gali" | table_silv$emu == "ES_Basq" |
                        table_silv$emu == "ES_Minh" | table_silv$emu == "ES_Vale" |
                        table_silv$emu == "ES_Cata" | table_silv$emu == "ES_Astu" | 
                        table_silv$emu == "ES_Cast" | table_silv$emu == "ES_Anda" | 
                        table_silv$emu =="ES_Nava" | table_silv$emu == "ES_Cant" | 
                        table_silv$emu == "ES_Inne", ]

# dim(silv_sp): 115978 obs. and 93 variables
summary(silv_sp)

#   Average length and weight by EMU and year
mean_silv_sp <- silv_sp %>% group_by(year, emu) %>%
  summarise(nb_size_measured = sum(nb_size_measured), mean_weigth = mean(weight), mean_length = mean(length))

# -- Save data ---- ####
write.table(mean_silv_sp, paste0(tablewd,"mean_silv_sp.csv"), row.names = FALSE, sep=",", dec=".")
