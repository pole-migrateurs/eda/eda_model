# Le fichier a �t� charg� en plusieurs �tapes, Les scripts ont �t� laiss�s en place pour garder trace
# des �tapes. Commencer par le chargement du init, il faut peut �tre changer les chemins dans init.r pour
# sqldf et changer les chemins dans le .xml qui est dans eda/edaccm.
# pour lancer les r�sultats saumons, ligne 79 chargement du tableau des densit�s, la pr�diction choisie doit 
# �tre pass�e dans la variable "pred"
# Avant de lancer les cartes, il faut cr�er un shape avec la commande exportshape. Il faut aussi assigner le 
# nom du bassin versant � "bv".
# Deux autres fichiers sont n�cessaires, le fichier des d�bits class�s (ligne 105) et le fichier des ouvrages (ligne 109)
# L'ensemble des r�sultats est stock� dans le Rdata qui est charg� avant les cartes
# Author: cedric.briand
###############################################################################

# editer le fichier EDAload.xml, ce dernier n'est pas en contr�le de source
setwd("C:/Users/cedric.briand/Documents/workspace/EDA/")
#setwd("C:/Documents and Settings/vacataire.logrami.DREAL-CENTRE24/workspace/EDA/trunk/")
if (!(exists("baseODBC"))) source("EDACCM/init.r")
load(file = str_c(datawd,"/dataEDAlb2/allba.RData"))
# Passer directement � load(file = str_c(datawd,"/allba.RData"))
library(gplots)

# chargement des r�sultats du mod�le EDA
load(file=str_c(datawd,"/dataEDArht/baseEdaRHTmodel2.Rdata")) # resultmodel
#str(baseEdaRHTpate)
ba@data<-resultmodel@data
#R�cup�ration du nom du bv connaissant id_drain aval


#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(214811)]<-"Loire enti�re" 
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(225205)]<-"Loire amont (au dessus de l'Arroux)" 
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(219486)]<-"Loire amont (Confluence avec l'Allier)" 
#
#
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(209549)]<-"Blavet"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(213048)]<-"Mayenne"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(212340)]<-"Vilaine-Oust"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(216261)]<-"Vienne"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(218815)]<-"Creuse"  
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(221666)]<-"Gartempe" 
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(214861)]<-"Cher"  
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(219530)]<-"Allier" 
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(226479)]<-"Sioule"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(229500)]<-"Dore"  
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(232538)]<-"Alagnon"
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(224978)]<-"Arroux" 
#ba@data$bv[ba@data$id_drain%in%upstream_iddrain(218849)]<-"Vienne_amont"
#
#
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(230519)]<-"Gorre" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(231186)]<-"Birance" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(231012)]<-"Maulde" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(230218)]<-"Taurion" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(227807)]<-"Tarde" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(234012)]<-"Chapeauroux" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(233068)]<-"Lignon" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(232989)]<-"Ance du Nord" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(224388)]<-"Besbre" 
##ba@data$bv[ba@data$id_drain%in%upstream_iddrain(224978)]<-"Arroux" 

#
#t1<-Sys.time()
#ba<-upstream_segments(ba) # matrix upstream segments
#print(Sys.time()-t1)  #4.5 mins
#unique(ba@data$bv[!is.na(ba@data$bv)])
#
##save(ba,file=str_c(datawd,"/dataEDAlb2/ba.Rdata"))
#load(str_c(datawd,"/dataEDAlb2/ba.Rdata"))
# voir requete_de_travail_sea_hope.sql pour la table ci dessous, ajout de dmer, id_drain, et
#TODO programmer un truc pour selectionner le type de sortie
ba@data<-sqldf("select * from rht.prod_resultats_07_11_full",dbname="ouvragelb",host=getOption("sqldf.RPostgreSQL.host"))
ba@data$pred<-ba@data$prs_moy
ba@data$pred<-ba@data$ppsa_moy
ba@data$pred<-ba@data$ppst_moy
# ppst sont les production sans barrages
# prs production r�elle donn�es p�che �lectriques
# ppsa production potentielle actuelle (� partir d'un potentiel th�orique de prod � 7.5)
# la valeur pr�dite pour les mortalit�s doit s'appeller "pred"
###################################
# Export des fichiers shape
##################################
ba@data$bv[ba@data$id_drain%in%upstream_iddrain(216261)]<-"Vienne"
ba<-exportshape(ba,bv<-"Vienne",id_drain_aval=216261)
ba@data$bv[ba@data$id_drain%in%upstream_iddrain(219530)]<-"Allier"
ba<-exportshape(ba,bv<-"Allier",id_drain_aval=219530)
ba@data$bv[ba@data$id_drain%in%upstream_iddrain(224978)]<-"Arroux"
ba<-exportshape(ba,bv<-"Arroux",id_drain_aval=224978)
ba@data$bv[ba@data$id_drain%in%upstream_iddrain(214811)]<-"Loire enti�re"
ba<-exportshape(ba,bv<-"Loire enti�re",id_drain_aval=214811)

ba@data$bv[ba@data$id_drain%in%upstream_iddrain(219220)]<-"Loa" # loire amont
ba<-exportshape(ba,bv<-"Loa",id_drain_aval=219220)
####################################
# Chargement des donn�es de debit
####################################

debits_classes<-sqldf("select * from lb.debits_classes_final",dbname=getOption("sqldf.RPostgreSQL.dbname"),host=getOption("sqldf.RPostgreSQL.host"))
## save(debits_classes,file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/debits_classes.Rdata")
#load(file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/debits_classes.Rdata")
# la table mortalite est construite par tableau_mortalite.R
ba@datapate<-sqldf("select * from lb.table_mortalite_ouvrage",dbname=getOption("sqldf.RPostgreSQL.dbname"),host=getOption("sqldf.RPostgreSQL.host"))

# ci dessous renommage de certaines variables pour compatibilit� avec sea hope
ba@datapate$bv<-str_c(substring(ba@datapate$hydro_ref, 1, 1), tolower(substring(ba@datapate$hydro_ref, 2)))
ba@datapate<-chnames(ba@datapate,"ouv_id","id_roe")
ba@datapate$mort_sat<-as.numeric(ba@datapate$mort_sat)
ba@datapate$mort_ang<-as.numeric(ba@datapate$mort_ang)
# creation du shape ouvrage
# dans une console dos
# pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAlb2/shp/pate" -h w3.eptb-vilaine.fr -p 5432 -u postgres -g the_geom -r -k ouvragelb "select o.*,st_transform(o1.the_geom,3035) as the_geom,o.ouv_id as id_roe, initcap(hydro_ref) as bv from lb.table_mortalite_ouvrage o join lb.t_ouvrage_ouv o1 on o.ouv_id=o1.ouv_id;"

## table_lb_mortalite_ouvrage
##load_library("XLConnect")
##wb = loadWorkbook(str_c("C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/tableau_mortalite.xlsx"), create = FALSE)
##ba@datapate<-readWorksheet(wb, sheet = "mortalite_final")

####################################
# Calcul des mortalit�s
####################################
# les donn�es sont dans ba@data
#save(list=c("debits_classes","ba"),file = str_c(datawd,"/dataEDAlb2/allba.RData"))
load(file = str_c(datawd,"/dataEDAlb2/allba.RData"))

#quelques changements temporaires
taumin<-min(ba@datapate$mort_sat[ba@datapate$mort_sat>0],na.rm=TRUE)/100 # 5%
taumin<-ba@datapate$mort_sat/100
taumax<-max(ba@datapate$mort_sat,na.rm=TRUE)/100 # 100%
taumax<-ba@datapate$mort_sat/100
#ba@datapate$mortalite_sat[is.na(ba@datapate$mortalite_sat)]<-10 # il n'y a plus de NA
# liste des ba sans id_drain  ba@datapate[is.na(ba@datapate$id_drain),"id_roe"]
ba@datapate<-ba@datapate[!is.na(ba@datapate$id_drain),] # il y a des NA pour l'instant
# pour certains ouvrages, le d�bit d'�quipement est manquant.

# voir la progress bar pour voir la progression
# calcul des mortalites par ouvrage
MortaliteOuvrage<-calculMortaliteOuvrage2(Qmodule=ba@datapate$module,
		Qequipement=ba@datapate$eno_debitequipement,
		alphai=cbind(debits_classes$n_0305_10_rht[match(ba@datapate$hydro_ref ,debits_classes$hydro_ref )],
				debits_classes$n_0305_30_rht[match(ba@datapate$hydro_ref ,debits_classes$hydro_ref )],
				debits_classes$n_0305_50_rht[match(ba@datapate$hydro_ref ,debits_classes$hydro_ref )],
				debits_classes$n_0305_70_rht[match(ba@datapate$hydro_ref ,debits_classes$hydro_ref )],
				debits_classes$n_0305_90_rht[match(ba@datapate$hydro_ref ,debits_classes$hydro_ref )]),
		tau=cbind(taumin,ba@datapate$mort_sat/100,taumax))
ba@datapate$mortalite<-MortaliteOuvrage[,2]
ba@datapate$mortalitefavorable<-MortaliteOuvrage[,1]
ba@datapate$mortalitedefavorable<-MortaliteOuvrage[,3]

t1<-Sys.time()
# construit la matrice de ba@datadam
ba<-upstream_segments(ba) 
# calcule les mortalit�s
ba<-segment_mortality(ba)
print(Sys.time()-t1)  #10.15058 secs
#save(list=c("debits_classes","ba"),file = str_c(datawd,"/dataEDAlb2/allba_ppsa.RData"))
#save(list=c("debits_classes","ba"),file = str_c(datawd,"/dataEDAlb2/allba_ppst.RData"))
#save(list=c("debits_classes","ba"),file = str_c(datawd,"/dataEDAlb2/allba_prs.RData"))
#baseODBC<-c("ouvragelbdistant","postgres",passworddistant) 
#options(sqldf.RPostgreSQL.user = "postgres", 
#		sqldf.RPostgreSQL.password = passworddistant,
#		sqldf.RPostgreSQL.dbname = "ouvragelb",
#		sqldf.RPostgreSQL.host = "1.100.1.6",# 1.100.1.6 w3.eptb-vilaine.fr
#		sqldf.RPostgreSQL.port = 5432)
#load(file = str_c(datawd,"/dataEDAlb2/allba_ppsa.RData"))
#load(file = str_c(datawd,"/dataEDAlb2/allba_ppst.RData"))
#load(file = str_c(datawd,"/dataEDAlb2/allba_prs.RData"))
#ROE28711 = Poutes

map(ba,bv="Vienne",id_roe="ROE10604",legendlocation="topright",
 titlelegend="production smolt sans barrage",espece="smolts") #ROE16834
map(ba,bv="Allier",id_roe="ROE29781",legendlocation="topright",
		titlelegend="production smolt sans barrage",espece="smolts")
ba@data$bv[ba@data$id_drain%in%upstream_iddrain(214925)]<-"Loire"
ba<-exportshape(ba,bv<-"Loire",id_drain_aval=214925)
map(ba,bv="Loire",id_roe=NULL,legendlocation="topright",
		titlelegend="production smolt sans barrage",espece="smolts",zoom=6)
ba<-exportshape(ba,bv<-"Loire",id_drain_aval=214925)
map(ba,bv="Loire",id_roe=NULL,legendlocation="topright",
		titlelegend="production smolt sans barrage",espece="smolts",zoom=6)
map(ba,bv="Loa",id_roe=NULL,legendlocation="topright",
		titlelegend="production smolt sans barrage",espece="smolts",zoom=8,
		sortie1=FALSE,sortie2=FALSE,sortie3=FALSE)
#save(pate_layer_fort,file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/pate_layer_fort_ppsa.RData")
#save(pate_layer_fort,file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/pate_layer_fort_prs_allier.RData")

#save(pate_layer_fort,file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/pate_layer_fort_ppst.RData")
#save(pate_layer_fort,file="C:/Users/cedric.briand/Documents/workspace/p/devalpomi/R/data/pate_layer_fort_prs.RData")
#debug  de la fonction map, lancer les commandes ci-dessous 
# bv="Vienne";id_roe="ROE21331";object<-ba;legendlocation="topright";zoom=6
# titlelegend="production smolt sans barrage";espece="smolts"
# sortie1=TRUE;sortie2=TRUE;sortie3=TRUE;sortie4=TRUE

#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAlb2/shp/Vie" -h 1.100.1.6 -p 5432 -u postgres -g the_geom -r -k ouvragelb "select id_drain,ST_transform(ST_SimplifyPreserveTopology(the_geom,50),4326) as the_geom from rht.rhtvs2_loirebretagne where id_drain in (select rht.upstream_segments(216261)) or id_drain=216261 ;"
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAlb2/shp/Pate" -h w3.eptb-vilaine.fr -p 5432 -u postgres -g the_geom -r -k ouvragelb "select ouv_seq,ST_transform(the_geom,4326) as the_geom ,tt.* from lb.table_mortalite_ouvrage tt join lb.t_ouvrage_ouv o on o.ouv_id=tt.ouv_id;"
#pgsql2shp -f "C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAlb2/shp/ce" -h w3.eptb-vilaine.fr -p 5432 -u postgres -g geom -r -k ouvragelb "select  gid,geom as geom from shp.ce;"
