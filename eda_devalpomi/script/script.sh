# saving from old database 27/06/2022
cd eda/base/eda2.2
pg_dump -U postgres -h 1.100.1.6 -f "jointurestation.sql" --table rht.jointurestation ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "bv_hydro.sql" --table rht.bv_hydro ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "bv_hydro.sql" --table rht.bv_hydro ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "iabre.sql" --table rht.iabre ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "habitatgartempe.sql" --table rht.habitatgartempe ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "bomassi2.sql" --table rht.bomassi2 ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "habitatgartempe.sql" --table rht.habitatgartempe ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "jointurebomassifinal.sql" --table rht.jointurebomassifinal ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "production_reelle.sql" --table rht.production_reelle ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "potentiel_theorique_actuel.sql" --table rht.potentiel_theorique_actuel ouvragelb
pg_dump -U postgres -h 1.100.1.6 -f "potentiel_theorique_sans_barrage.sql" --table rht.potentiel_theorique_sans_barrage ouvragelb


psql -U postgres -c "create schema devalpomi" eda2.3
psql -U postgres  -f "jointurestation.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.jointurestation set schema devalpomi"  eda2.3
psql -U postgres  -f "bv_hydro.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.bv_hydro set schema devalpomi"  eda2.3
psql -U postgres  -f "iabre.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.iabre set schema devalpomi"  eda2.3
psql -U postgres  -f "habitatgartempe.sql" eda2.3
# require manual edition for compatibility
psql -U postgres  -c "ALTER TABLE rht.habitatgartempe set schema devalpomi"  eda2.3
psql -U postgres  -f "bomassi2.sql"  eda2.3
# require manual edition for compatibility
psql -U postgres  -c "ALTER TABLE rht.bomassi2 set schema devalpomi"  eda2.3
psql -U postgres  -f "habitatgartempe.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.habitatgartempe set schema devalpomi"  eda2.3
psql -U postgres  -f "jointurebomassifinal.sql" eda2.3
psql -U postgres  -c "ALTER TABLE rht.jointurebomassifinal set schema devalpomi"  eda2.3
psql -U postgres  -f "production_reelle.sql" eda2.3
psql -U postgres  -c "ALTER TABLE rht.production_reelle set schema devalpomi"  eda2.3
psql -U postgres  -f "potentiel_theorique_actuel.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.potentiel_theorique_actuel set schema devalpomi"  eda2.3
psql -U postgres  -f "potentiel_theorique_sans_barrage.sql"  eda2.3
psql -U postgres  -c "ALTER TABLE rht.potentiel_theorique_sans_barrage set schema devalpomi"  eda2.3