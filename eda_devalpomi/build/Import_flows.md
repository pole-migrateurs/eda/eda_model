---
title: "imporation of flow data"
author: "Cédric Briand, Maria Mateo, Nils Teichert"
date: "30/11/2020"
output: html_document
---










Data are retreived from the Banque Hydro



# Applying flows to basins

The daily flows are extracted from a subset of stations selected as
representative of the flow by local experts. The correspondance between hydro station
and geographical sector is as following in France.

Spain and Portugal : how do we do that ?


TODO : join idsegment and code_sous_sector, and apply a code_hydro_station to each segment


```r
sss <- read_excel("F:/eda/data/eda2.3/gt2/Station_hydro_et_sous_secteurs.xlsx")
kable(sss) %>% kable_styling() %>% scroll_box(width = "600px", height="200px")
```

<div style="border: 1px solid #ddd; padding: 0px; overflow-y: scroll; height:200px; overflow-x: scroll; width:600px; "><table class="table" style="margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> name </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> code_sous_secteur </th>
   <th style="text-align:left;position: sticky; top:0; background-color: #FFFFFF;"> code_station_hydro </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> Ariège amont </td>
   <td style="text-align:left;"> O13 </td>
   <td style="text-align:left;"> O1252510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hersvif 2 </td>
   <td style="text-align:left;"> O15 </td>
   <td style="text-align:left;"> O1442910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Hersvif 1 </td>
   <td style="text-align:left;"> O16 </td>
   <td style="text-align:left;"> O1662910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Ariège </td>
   <td style="text-align:left;"> O17 </td>
   <td style="text-align:left;"> O1712510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Garonne 2 </td>
   <td style="text-align:left;"> O20 </td>
   <td style="text-align:left;"> O1900010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Garonne 1 </td>
   <td style="text-align:left;"> O61 </td>
   <td style="text-align:left;"> O6140010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Cère </td>
   <td style="text-align:left;"> P19 </td>
   <td style="text-align:left;"> P1962910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Bave </td>
   <td style="text-align:left;"> P20 </td>
   <td style="text-align:left;"> P2054010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Corrèze 2 </td>
   <td style="text-align:left;"> P37 </td>
   <td style="text-align:left;"> P3502510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Corrèze 1 </td>
   <td style="text-align:left;"> P39 </td>
   <td style="text-align:left;"> P3922520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vézère 1 </td>
   <td style="text-align:left;"> P41 </td>
   <td style="text-align:left;"> P4161010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vézère 2 </td>
   <td style="text-align:left;"> P40 </td>
   <td style="text-align:left;"> P4161010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Vézère 3 </td>
   <td style="text-align:left;"> P32 </td>
   <td style="text-align:left;"> P4161010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Dordogne 1 </td>
   <td style="text-align:left;"> P51 </td>
   <td style="text-align:left;"> P5140010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Dordogne 2 </td>
   <td style="text-align:left;"> P50 </td>
   <td style="text-align:left;"> P5140010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 4 </td>
   <td style="text-align:left;"> Q51 </td>
   <td style="text-align:left;"> Q4801010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 5 </td>
   <td style="text-align:left;"> Q50 </td>
   <td style="text-align:left;"> Q4801010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 6 </td>
   <td style="text-align:left;"> Q48 </td>
   <td style="text-align:left;"> Q4801010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 7 </td>
   <td style="text-align:left;"> Q47 </td>
   <td style="text-align:left;"> Q4801010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 8 </td>
   <td style="text-align:left;"> Q44 </td>
   <td style="text-align:left;"> Q4801010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 1 </td>
   <td style="text-align:left;"> Q54 </td>
   <td style="text-align:left;"> Q5501010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 2 </td>
   <td style="text-align:left;"> Q53 </td>
   <td style="text-align:left;"> Q5501010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau 3 </td>
   <td style="text-align:left;"> Q52 </td>
   <td style="text-align:left;"> Q5501010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave de Pau aval </td>
   <td style="text-align:left;"> Q55 </td>
   <td style="text-align:left;"> Q5501010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Ossau </td>
   <td style="text-align:left;"> Q61 </td>
   <td style="text-align:left;"> Q6142920 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Aspe </td>
   <td style="text-align:left;"> Q63 </td>
   <td style="text-align:left;"> Q6332520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Aspe amont </td>
   <td style="text-align:left;"> Q62 </td>
   <td style="text-align:left;"> Q6332520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Aspe aval </td>
   <td style="text-align:left;"> Q65 </td>
   <td style="text-align:left;"> Q6332520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Oloron </td>
   <td style="text-align:left;"> Q71 </td>
   <td style="text-align:left;"> Q7002910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Oloron amont </td>
   <td style="text-align:left;"> Q70 </td>
   <td style="text-align:left;"> Q7002910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Saison amont </td>
   <td style="text-align:left;"> Q72 </td>
   <td style="text-align:left;"> Q7322520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Saison aval </td>
   <td style="text-align:left;"> Q73 </td>
   <td style="text-align:left;"> Q7322520 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Gave d'Oloron aval </td>
   <td style="text-align:left;"> Q74 </td>
   <td style="text-align:left;"> Q7412910 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Nive amont </td>
   <td style="text-align:left;"> Q91 </td>
   <td style="text-align:left;"> Q9164610 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Nive </td>
   <td style="text-align:left;"> Q92 </td>
   <td style="text-align:left;"> Q9312510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Nive amont 2 </td>
   <td style="text-align:left;"> Q90 </td>
   <td style="text-align:left;"> Q9312510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Nive aval </td>
   <td style="text-align:left;"> Q93 </td>
   <td style="text-align:left;"> Q9312510 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aude 3 </td>
   <td style="text-align:left;"> Y11 </td>
   <td style="text-align:left;"> Y1112010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aude 2 </td>
   <td style="text-align:left;"> Y12 </td>
   <td style="text-align:left;"> Y1232010 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Aude 1 </td>
   <td style="text-align:left;"> Y14 </td>
   <td style="text-align:left;"> Y1422020 </td>
  </tr>
</tbody>
</table></div>
# Daily flow extraction in France

data are collected from 
the [banque hydro](http://hydro.eaufrance.fr/) and extracted with the function 
extract_csv_hydro2. As the data are in a really weird format, it has to be read lines by lines
and the tables containing daily flows and other usefull information are extracted from the dataset, the code 
was initially created by Nils Teichert, thanks Nils !!!


```r
## function to extract flow (source Nils Teichert, MNHN) modified by Cédric
## to work with character vector (and avoid csv format problem)


extract_csv_hydro2 <- function(path_to_use){
	cat ("extracting ", path_to_use,"\n")
	options(warn=-1)
	con  <- file(path_to_use, open = "r")
	# this function returns a character vector, one element per line
	ext_tabs <- readLines(con,warn = FALSE)
	on.exit(close(con)) # closing the connexion
	ID_sel <- which(grepl("Code station", ext_tabs ))
	sta_sel <- strsplit(ext_tabs[ID_sel+1 ],";")[[1]][c(1,2)]
	ann_sel <- unlist(lapply(strsplit(ext_tabs[ID_sel+3 ],";"),function(X)rep(as.numeric(X[2]),31)))
	# data in the qjm format have 25 columns
	qjm_id <- which(sapply(strsplit(ext_tabs,";"),function(X) length(X)==25))
	qjm_sel <- strsplit(ext_tabs[qjm_id],";")
	tabQJM0 <-	as.data.frame(do.call(rbind, qjm_sel))
	colnames(tabQJM0) <- c("jour", "Jan", NA, "Feb", NA, "Mar", NA, "Apr", NA, "May", NA, "Jun", NA, "Jul", NA,
			"Aug", NA, "Sep", NA, "Oct", NA, "Nov", NA, "Dec", NA)
	tabQJM <- tabQJM0[!(tabQJM0[,1]=='Jour/Mois' | tabQJM0[,1]==""), !is.na(colnames(tabQJM0))]
	if(nrow(tabQJM)!=length(ann_sel)) stop(paste(path_to_use, "formatting problem the length of the QMJ table is different from the vector of years"))
	tabQJM <- tabQJM %>%
			mutate(code_hydro_station = sta_sel[1],	
					station_name=sta_sel[2],
					year =  ann_sel)%>%
			pivot_longer(c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"), names_to ="month", values_to ="Q") %>%
			mutate(month = as.character(match(month,month.abb)),
					date = lubridate::ymd(paste(year,month,jour, sep ="-")), 
					Q = as.numeric(Q)) %>% 
			arrange(date) %>% 
			select(code_hydro_station, station_name, date, year, month, jour, Q) %>% 
			filter(!is.na(date))
	options(warn=0)
	tabQJM
}



# extract files with a grep------------
ll <-list.files("F:/eda/data/eda2.3/gt2/debits")
debit_csv_files <- file.path("F:/eda/data/eda2.3/gt2/debits",ll[grep(".csv",ll)])

# Another elegant way of doing it-----------------
debit_csv_files <- fs::dir_ls("F:/eda/data/eda2.3/gt2/debits/", regexp = "\\.csv$")

# TODO before using : remove accented characters from file names

# extraction
debit_list_fr <- lapply(debit_csv_files, extract_csv_hydro2)

# passage au format table 
debit_sud_ouest <- bind_rows(debit_list_fr) %>% group_by(code_hydro_station) %>%  arrange(code_hydro_station, date) %>%
		dplyr::select(code_hydro_station, station_name, date, year, Q)
save(debit_sud_ouest, file = str_c(ddatawd, "debit_sud_ouest.Rdata"))
```

# Extraction of flow data in Spain and Portugal

In spain and portugal data come in a various format, though less complex than in France. Here is the code which first extract data to get columns code_hydro_station station_name     date        year     Q


```r
# Extract data from TER ------------------------------------
require(sf)
#code_hydro_station station_name     date        year     Q
path <-"F:/eda/data/eda2.3/gt2/flow"
ter1 <- readxl::read_excel(path=file.path(path,"TER_Riverflow_1996_2020.xlsx"),sheet=1)
ter2 <- readxl::read_excel(path=file.path(path,"TER_Riverflow_1996_2020.xlsx"),sheet=2)

#code_hydro_station station_name     date        year     Q
ter_flow<- bind_rows(
		ter1 %>% rename(date=Data, code_hydro_station=Variable, Q = Mitjana, station_name= Estacio) %>% 
				mutate(date=lubridate::ymd(date),year=lubridate::year(date))%>% 
				select(code_hydro_station, station_name, date, year, Q),
		ter2 %>% rename(date=Data, code_hydro_station=Variable, Q = Mitjana, station_name= Estacio) %>% 
				mutate(date=lubridate::ymd(date),year=lubridate::year(date))%>% 
				select(code_hydro_station, station_name, date, year, Q)
)
loc1 <- st_sfc(st_point(c(pull(ter1[1,"UTM X"]),pull(ter1[1,"UTM Y"]))))
st_crs(loc1) <- 25830
loc1 <- loc1 %>% st_transform(4326)

loc2 <- st_sfc(st_point(c(pull(ter2[1,"UTM X"]),pull(ter2[1,"UTM Y"]))))
st_crs(loc2) <- 25830
loc2 <- loc2 %>% st_transform(4326)

ter_station <-
		bind_rows(		
				ter1 %>% 
						head(1)%>%
						rename(code_hydro_station=Variable)%>%
						mutate(X=st_coordinates(loc1)[1], Y=st_coordinates(loc1)[2]) %>% 
						select(code_hydro_station, X, Y),
				
				ter2 %>% 
						head(1)%>%
						rename(code_hydro_station=Variable)%>%
						mutate(X=st_coordinates(loc2)[1], Y=st_coordinates(loc2)[2]) %>% 
						select(code_hydro_station, X, Y))


# Oria ----------------------------------

# flow2 simple table

ll <-list.files("F:/eda/data/eda2.3/gt2/flow/oria2")
ll <- ll[grep("bas",ll)]
ll <- ll[grep("csv",ll)]
debit_csv_files <- file.path("F:/eda/data/eda2.3/gt2/flow/oria2",ll)

oria2 <-  read_delim(debit_csv_files, delim=";" ,col_types="ccc", locale=locale("es", decimal_mark = ","))
# remove whitespace from string str_replace_all(x, space(), "") works also
oria2 <- oria2 %>% rename(code_hydro_station=sta_cod, Q=flow_m3_s)
oria2$Q <- as.numeric(str_replace_all(oria2$Q, fixed(" "), ""))
oria2$date <- lubridate::dmy(oria2$date)
oria2$code_hydro_station <- stringi::stri_sub(ll,8,length=nchar(ll))
oria2$code_hydro_station <- stringi::stri_sub(oria2$code_hydro_station,1,-15)


# other format table with month in column and day/year in rows :-(
ll <-list.files("F:/eda/data/eda2.3/gt2/flow/oria")
ll <- ll[grep("bas",ll)]
ll <- ll[grep("csv",ll)]


# file = ll[[1]]
# path = "F:/eda/data/eda2.3/gt2/flow/oria"
fn_extract_oria <- function(file, path){
	data <-  read_delim(file.path(path,file), delim=";" , locale=locale("es", decimal_mark = "."),col_types ="cnnnnnnnnnnnn")
	if (ncol(data)!=13) stop("Check file format, ncol should be 13")
	colnames(data) <- c("YD",1:12)
	data <- data %>% mutate_at(2:13, as.numeric)
	data1 <- data %>% 
			pivot_longer(cols=2:13,  values_to = "Q", names_to="M") %>%
			filter(!is.na(Q)) %>%
			mutate(YDM=str_c(YD,"/",M),
					date=lubridate::ydm(YDM)) %>%
			mutate(code_hydro_station = stringi::stri_sub(file,8,length=nchar(file)) ,
					code_hydro_station = stringi::stri_sub(code_hydro_station,1,-15)) %>%				
			select(code_hydro_station, date, Q)
	return(data1)
}
oria <- map(ll, ~fn_extract_oria(.x,path="F:/eda/data/eda2.3/gt2/flow/oria")) %>%
		bind_rows()

oria_flow <- rbind(oria,oria2)
oria_flow$station_name <- oria_flow$code_hydro_station
oria_flow$year <- lubridate::year(oria_flow$date)
# mondego

mond <- read_delim(file.path("F:/eda/data/eda2.3/gt2/flow/mondego","coimbra_daily_flow.csv"), delim=";" , skip=3, locale=locale("pt", decimal_mark = "."),col_types ="cnc")
mond$date <- strptime(mond$date, format= "%d/%m/%Y %H:%M")
mond <- mond[!is.na(mond$date),]
mond$date <- as.Date(mond$date)
mond$station_name<- "Açude ponte Coimbra"
mond$code_hydro_station <- "Mondego_coimbra 12G 01A"
mondego_flow <- mond
mondego_flow$year <- lubridate::year(mondego_flow$date)
save(oria_flow, mondego_flow,ter_flow, ter_station, file=str_c(ddatawd,"sp_flow.Rdata"))
```

# Calculating classified flows


```r
library(purrr)
load(file = str_c(ddatawd, "debit_sud_ouest.Rdata"))
load( file=str_c(ddatawd,"sp_flow.Rdata")) # oria_flow mondego_flow,ter_flow, ter_station
fn_class_flow <- function(data, iquantiles=c(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,0.95,0.99)){
	res <- data %>% as_tibble() %>%
			nest(data=c(-station_name, -code_hydro_station)) %>%
			mutate(quantiles = map(data, ~ quantile(.$Q, iquantiles , na.rm=TRUE)),
					# at this stage the result is a list, use bind_rows to get to tibble, and set_names() to rename
					quantiles = map(quantiles, ~ bind_rows(.) %>%  # use map(quantiles, ~ bind_rows(.)%>% gather()) for long format
									set_names(str_c("q_1004_",100*iquantiles))
					),
					year_min=  map(data, ~ min(.$year)),
					year_max=  map(data, ~ max(.$year))
			) %>%	 
			# remove data to allow to unnest and get to a simple table format (without list)
			select(code_hydro_station,station_name,quantiles,year_min, year_max) %>%	 
			unnest(cols = c(quantiles, year_min, year_max)) 
	return(res)
}

quantile_flow_sud_ouest <- debit_sud_ouest %>% fn_class_flow()
quantile_flow_ter <- ter_flow %>% fn_class_flow()
quantile_flow_oria <- oria_flow %>% fn_class_flow()
quantile_flow_mondego <- mondego_flow %>% fn_class_flow()


quantile_flow <- bind_rows(quantile_flow_sud_ouest,
		quantile_flow_ter,
		quantile_flow_oria,
		quantile_flow_mondego)
save(quantile_flow, file = str_c(ddatawd, "quantile_flow.Rdata"))
# quantile_flow %>% print(n=50,  width=Inf)
```

<table class="table" style="margin-left: auto; margin-right: auto;">
 <thead>
  <tr>
   <th style="text-align:left;"> code_hydro_station </th>
   <th style="text-align:left;"> station_name </th>
   <th style="text-align:right;"> q_1004_10 </th>
   <th style="text-align:right;"> q_1004_20 </th>
   <th style="text-align:right;"> q_1004_30 </th>
   <th style="text-align:right;"> q_1004_40 </th>
   <th style="text-align:right;"> q_1004_50 </th>
   <th style="text-align:right;"> q_1004_60 </th>
   <th style="text-align:right;"> q_1004_70 </th>
   <th style="text-align:right;"> q_1004_80 </th>
   <th style="text-align:right;"> q_1004_90 </th>
   <th style="text-align:right;"> q_1004_95 </th>
   <th style="text-align:right;"> q_1004_99 </th>
   <th style="text-align:right;"> year_min </th>
   <th style="text-align:right;"> year_max </th>
  </tr>
 </thead>
<tbody>
  <tr>
   <td style="text-align:left;"> O1252510 </td>
   <td style="text-align:left;"> L'Ariège à Foix </td>
   <td style="text-align:right;"> 51.00000 </td>
   <td style="text-align:right;"> 92.00000 </td>
   <td style="text-align:right;"> 134.000000 </td>
   <td style="text-align:right;"> 180.000000 </td>
   <td style="text-align:right;"> 234.00000 </td>
   <td style="text-align:right;"> 295.000000 </td>
   <td style="text-align:right;"> 379.000000 </td>
   <td style="text-align:right;"> 500.000000 </td>
   <td style="text-align:right;"> 762.00000 </td>
   <td style="text-align:right;"> 935.00000 </td>
   <td style="text-align:right;"> 1186.00000 </td>
   <td style="text-align:right;"> 1905 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> O1442910 </td>
   <td style="text-align:left;"> L'Hers Vif au Peyrat </td>
   <td style="text-align:right;"> 38.00000 </td>
   <td style="text-align:right;"> 64.00000 </td>
   <td style="text-align:right;"> 106.000000 </td>
   <td style="text-align:right;"> 204.000000 </td>
   <td style="text-align:right;"> 294.00000 </td>
   <td style="text-align:right;"> 382.000000 </td>
   <td style="text-align:right;"> 483.000000 </td>
   <td style="text-align:right;"> 606.000000 </td>
   <td style="text-align:right;"> 747.00000 </td>
   <td style="text-align:right;"> 939.00000 </td>
   <td style="text-align:right;"> 1130.00000 </td>
   <td style="text-align:right;"> 1962 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> O1662910 </td>
   <td style="text-align:left;"> L'Hers Vif à Calmont </td>
   <td style="text-align:right;"> 46.00000 </td>
   <td style="text-align:right;"> 119.00000 </td>
   <td style="text-align:right;"> 230.000000 </td>
   <td style="text-align:right;"> 297.000000 </td>
   <td style="text-align:right;"> 425.00000 </td>
   <td style="text-align:right;"> 587.000000 </td>
   <td style="text-align:right;"> 755.000000 </td>
   <td style="text-align:right;"> 935.000000 </td>
   <td style="text-align:right;"> 1112.00000 </td>
   <td style="text-align:right;"> 1231.00000 </td>
   <td style="text-align:right;"> 1334.35000 </td>
   <td style="text-align:right;"> 1996 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> O1712510 </td>
   <td style="text-align:left;"> L'Ariège à Auterive </td>
   <td style="text-align:right;"> 55.20000 </td>
   <td style="text-align:right;"> 126.00000 </td>
   <td style="text-align:right;"> 187.000000 </td>
   <td style="text-align:right;"> 265.000000 </td>
   <td style="text-align:right;"> 362.00000 </td>
   <td style="text-align:right;"> 473.000000 </td>
   <td style="text-align:right;"> 605.000000 </td>
   <td style="text-align:right;"> 778.000000 </td>
   <td style="text-align:right;"> 933.00000 </td>
   <td style="text-align:right;"> 1057.00000 </td>
   <td style="text-align:right;"> 1150.00000 </td>
   <td style="text-align:right;"> 1966 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> O1900010 </td>
   <td style="text-align:left;"> La Garonne à Portet-sur-Garonne </td>
   <td style="text-align:right;"> 32.00000 </td>
   <td style="text-align:right;"> 65.00000 </td>
   <td style="text-align:right;"> 100.000000 </td>
   <td style="text-align:right;"> 147.000000 </td>
   <td style="text-align:right;"> 211.00000 </td>
   <td style="text-align:right;"> 355.000000 </td>
   <td style="text-align:right;"> 567.000000 </td>
   <td style="text-align:right;"> 788.000000 </td>
   <td style="text-align:right;"> 1048.00000 </td>
   <td style="text-align:right;"> 1300.20000 </td>
   <td style="text-align:right;"> 1538.00000 </td>
   <td style="text-align:right;"> 1910 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> O6140010 </td>
   <td style="text-align:left;"> La Garonne à Lamagistère </td>
   <td style="text-align:right;"> 43.00000 </td>
   <td style="text-align:right;"> 111.00000 </td>
   <td style="text-align:right;"> 193.600000 </td>
   <td style="text-align:right;"> 272.800000 </td>
   <td style="text-align:right;"> 358.00000 </td>
   <td style="text-align:right;"> 462.000000 </td>
   <td style="text-align:right;"> 629.000000 </td>
   <td style="text-align:right;"> 853.000000 </td>
   <td style="text-align:right;"> 1159.80000 </td>
   <td style="text-align:right;"> 1360.00000 </td>
   <td style="text-align:right;"> 1579.00000 </td>
   <td style="text-align:right;"> 1966 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P1350010 </td>
   <td style="text-align:left;"> La Dordogne à Argentat </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 38.00000 </td>
   <td style="text-align:right;"> 92.000000 </td>
   <td style="text-align:right;"> 162.000000 </td>
   <td style="text-align:right;"> 275.00000 </td>
   <td style="text-align:right;"> 457.000000 </td>
   <td style="text-align:right;"> 625.000000 </td>
   <td style="text-align:right;"> 779.000000 </td>
   <td style="text-align:right;"> 1060.00000 </td>
   <td style="text-align:right;"> 1264.60000 </td>
   <td style="text-align:right;"> 1433.00000 </td>
   <td style="text-align:right;"> 1900 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P1962910 </td>
   <td style="text-align:left;"> La Cère à Biars-sur-Cère [Bretenoux] </td>
   <td style="text-align:right;"> 58.00000 </td>
   <td style="text-align:right;"> 127.00000 </td>
   <td style="text-align:right;"> 212.000000 </td>
   <td style="text-align:right;"> 310.000000 </td>
   <td style="text-align:right;"> 413.00000 </td>
   <td style="text-align:right;"> 603.000000 </td>
   <td style="text-align:right;"> 765.000000 </td>
   <td style="text-align:right;"> 1024.000000 </td>
   <td style="text-align:right;"> 1281.00000 </td>
   <td style="text-align:right;"> 1408.00000 </td>
   <td style="text-align:right;"> 1576.87000 </td>
   <td style="text-align:right;"> 1983 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P2054010 </td>
   <td style="text-align:left;"> La Bave à Frayssinhes [Le Martinet] </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 56.000000 </td>
   <td style="text-align:right;"> 212.000000 </td>
   <td style="text-align:right;"> 396.000000 </td>
   <td style="text-align:right;"> 617.00000 </td>
   <td style="text-align:right;"> 921.00000 </td>
   <td style="text-align:right;"> 980.00000 </td>
   <td style="text-align:right;"> 1913 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P3352520 </td>
   <td style="text-align:left;"> La Corrèze à Corrèze [Corrèze] </td>
   <td style="text-align:right;"> 17.00000 </td>
   <td style="text-align:right;"> 99.00000 </td>
   <td style="text-align:right;"> 232.000000 </td>
   <td style="text-align:right;"> 365.000000 </td>
   <td style="text-align:right;"> 581.00000 </td>
   <td style="text-align:right;"> 713.000000 </td>
   <td style="text-align:right;"> 882.000000 </td>
   <td style="text-align:right;"> 1022.800000 </td>
   <td style="text-align:right;"> 1106.00000 </td>
   <td style="text-align:right;"> 1152.00000 </td>
   <td style="text-align:right;"> 1209.00000 </td>
   <td style="text-align:right;"> 1990 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P3502510 </td>
   <td style="text-align:left;"> La Corrèze à Tulle [Pont des soldats] </td>
   <td style="text-align:right;"> 43.00000 </td>
   <td style="text-align:right;"> 95.00000 </td>
   <td style="text-align:right;"> 198.000000 </td>
   <td style="text-align:right;"> 359.000000 </td>
   <td style="text-align:right;"> 563.00000 </td>
   <td style="text-align:right;"> 782.400000 </td>
   <td style="text-align:right;"> 1029.000000 </td>
   <td style="text-align:right;"> 1202.000000 </td>
   <td style="text-align:right;"> 1341.00000 </td>
   <td style="text-align:right;"> 1398.00000 </td>
   <td style="text-align:right;"> 1456.00000 </td>
   <td style="text-align:right;"> 1957 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P3922520 </td>
   <td style="text-align:left;"> La Corrèze à Brive-la-Gaillarde [Pont du Buy] </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 78.000000 </td>
   <td style="text-align:right;"> 277.200000 </td>
   <td style="text-align:right;"> 995.60000 </td>
   <td style="text-align:right;"> 1248.00000 </td>
   <td style="text-align:right;"> 1554.00000 </td>
   <td style="text-align:right;"> 1918 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P4161010 </td>
   <td style="text-align:left;"> La Vézère à Montignac </td>
   <td style="text-align:right;"> 42.00000 </td>
   <td style="text-align:right;"> 110.00000 </td>
   <td style="text-align:right;"> 193.000000 </td>
   <td style="text-align:right;"> 306.000000 </td>
   <td style="text-align:right;"> 424.00000 </td>
   <td style="text-align:right;"> 575.000000 </td>
   <td style="text-align:right;"> 746.000000 </td>
   <td style="text-align:right;"> 935.000000 </td>
   <td style="text-align:right;"> 1226.00000 </td>
   <td style="text-align:right;"> 1318.00000 </td>
   <td style="text-align:right;"> 1621.00000 </td>
   <td style="text-align:right;"> 1915 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> P5140010 </td>
   <td style="text-align:left;"> La Dordogne à Bergerac </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 52.00000 </td>
   <td style="text-align:right;"> 128.000000 </td>
   <td style="text-align:right;"> 195.600000 </td>
   <td style="text-align:right;"> 270.00000 </td>
   <td style="text-align:right;"> 371.000000 </td>
   <td style="text-align:right;"> 522.000000 </td>
   <td style="text-align:right;"> 784.200000 </td>
   <td style="text-align:right;"> 1104.60000 </td>
   <td style="text-align:right;"> 1263.00000 </td>
   <td style="text-align:right;"> 1443.00000 </td>
   <td style="text-align:right;"> 1958 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q4801010 </td>
   <td style="text-align:left;"> Le Gave de Pau à Saint-Pé-de-Bigorre [Pont de Rieulhes] </td>
   <td style="text-align:right;"> 85.00000 </td>
   <td style="text-align:right;"> 132.00000 </td>
   <td style="text-align:right;"> 174.000000 </td>
   <td style="text-align:right;"> 222.000000 </td>
   <td style="text-align:right;"> 272.00000 </td>
   <td style="text-align:right;"> 335.000000 </td>
   <td style="text-align:right;"> 407.000000 </td>
   <td style="text-align:right;"> 503.000000 </td>
   <td style="text-align:right;"> 657.00000 </td>
   <td style="text-align:right;"> 788.00000 </td>
   <td style="text-align:right;"> 980.00000 </td>
   <td style="text-align:right;"> 1955 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q5501010 </td>
   <td style="text-align:left;"> Le Gave de Pau à Bérenx [Pont de Bérenx] </td>
   <td style="text-align:right;"> 16.00000 </td>
   <td style="text-align:right;"> 58.00000 </td>
   <td style="text-align:right;"> 173.000000 </td>
   <td style="text-align:right;"> 272.000000 </td>
   <td style="text-align:right;"> 378.00000 </td>
   <td style="text-align:right;"> 471.000000 </td>
   <td style="text-align:right;"> 554.000000 </td>
   <td style="text-align:right;"> 633.000000 </td>
   <td style="text-align:right;"> 767.00000 </td>
   <td style="text-align:right;"> 998.00000 </td>
   <td style="text-align:right;"> 1232.00000 </td>
   <td style="text-align:right;"> 1923 </td>
   <td style="text-align:right;"> 2017 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q6142910 </td>
   <td style="text-align:left;"> Le Gave d'Ossau à Oloron-Sainte-Marie [Oloron-Sainte-Croix] </td>
   <td style="text-align:right;"> 27.00000 </td>
   <td style="text-align:right;"> 63.00000 </td>
   <td style="text-align:right;"> 107.000000 </td>
   <td style="text-align:right;"> 157.000000 </td>
   <td style="text-align:right;"> 215.50000 </td>
   <td style="text-align:right;"> 304.000000 </td>
   <td style="text-align:right;"> 403.000000 </td>
   <td style="text-align:right;"> 529.000000 </td>
   <td style="text-align:right;"> 753.00000 </td>
   <td style="text-align:right;"> 1144.00000 </td>
   <td style="text-align:right;"> 1368.00000 </td>
   <td style="text-align:right;"> 1912 </td>
   <td style="text-align:right;"> 2013 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q6332510 </td>
   <td style="text-align:left;"> Le Gave d'Aspe à Bedous [Pont d'Escot] </td>
   <td style="text-align:right;"> 34.00000 </td>
   <td style="text-align:right;"> 77.00000 </td>
   <td style="text-align:right;"> 120.000000 </td>
   <td style="text-align:right;"> 172.000000 </td>
   <td style="text-align:right;"> 235.00000 </td>
   <td style="text-align:right;"> 333.000000 </td>
   <td style="text-align:right;"> 527.000000 </td>
   <td style="text-align:right;"> 747.000000 </td>
   <td style="text-align:right;"> 1190.00000 </td>
   <td style="text-align:right;"> 1329.00000 </td>
   <td style="text-align:right;"> 1499.29000 </td>
   <td style="text-align:right;"> 1948 </td>
   <td style="text-align:right;"> 2014 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q6502510 </td>
   <td style="text-align:left;"> Le Gave d'Aspe à Bidos </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 49.00000 </td>
   <td style="text-align:right;"> 116.000000 </td>
   <td style="text-align:right;"> 220.000000 </td>
   <td style="text-align:right;"> 341.00000 </td>
   <td style="text-align:right;"> 454.600000 </td>
   <td style="text-align:right;"> 604.000000 </td>
   <td style="text-align:right;"> 686.800000 </td>
   <td style="text-align:right;"> 838.90000 </td>
   <td style="text-align:right;"> 934.95000 </td>
   <td style="text-align:right;"> 1029.79000 </td>
   <td style="text-align:right;"> 2012 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q7002910 </td>
   <td style="text-align:left;"> Le Gave d'Oloron à Oloron-Sainte-Marie [Oloron-SNCF] </td>
   <td style="text-align:right;"> 47.00000 </td>
   <td style="text-align:right;"> 104.00000 </td>
   <td style="text-align:right;"> 179.000000 </td>
   <td style="text-align:right;"> 258.000000 </td>
   <td style="text-align:right;"> 339.00000 </td>
   <td style="text-align:right;"> 424.000000 </td>
   <td style="text-align:right;"> 520.000000 </td>
   <td style="text-align:right;"> 618.000000 </td>
   <td style="text-align:right;"> 742.00000 </td>
   <td style="text-align:right;"> 870.00000 </td>
   <td style="text-align:right;"> 1120.00000 </td>
   <td style="text-align:right;"> 1912 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q7322520 </td>
   <td style="text-align:left;"> Le Saison à Mauléon-Licharre [Pont du college] </td>
   <td style="text-align:right;"> 11.00000 </td>
   <td style="text-align:right;"> 58.00000 </td>
   <td style="text-align:right;"> 122.000000 </td>
   <td style="text-align:right;"> 265.800000 </td>
   <td style="text-align:right;"> 359.50000 </td>
   <td style="text-align:right;"> 507.000000 </td>
   <td style="text-align:right;"> 698.900000 </td>
   <td style="text-align:right;"> 892.000000 </td>
   <td style="text-align:right;"> 1082.30000 </td>
   <td style="text-align:right;"> 1202.65000 </td>
   <td style="text-align:right;"> 1338.53000 </td>
   <td style="text-align:right;"> 2007 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q7412910 </td>
   <td style="text-align:left;"> Le Gave d'Oloron à Escos </td>
   <td style="text-align:right;"> 18.00000 </td>
   <td style="text-align:right;"> 56.00000 </td>
   <td style="text-align:right;"> 116.000000 </td>
   <td style="text-align:right;"> 210.000000 </td>
   <td style="text-align:right;"> 324.00000 </td>
   <td style="text-align:right;"> 468.000000 </td>
   <td style="text-align:right;"> 613.000000 </td>
   <td style="text-align:right;"> 735.000000 </td>
   <td style="text-align:right;"> 924.00000 </td>
   <td style="text-align:right;"> 1266.00000 </td>
   <td style="text-align:right;"> 1358.00000 </td>
   <td style="text-align:right;"> 1922 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q9164610 </td>
   <td style="text-align:left;"> La Nive des Aldudes à Saint-Étienne-de-Baïgorry </td>
   <td style="text-align:right;"> 53.00000 </td>
   <td style="text-align:right;"> 133.00000 </td>
   <td style="text-align:right;"> 170.000000 </td>
   <td style="text-align:right;"> 283.600000 </td>
   <td style="text-align:right;"> 386.00000 </td>
   <td style="text-align:right;"> 500.000000 </td>
   <td style="text-align:right;"> 597.000000 </td>
   <td style="text-align:right;"> 734.000000 </td>
   <td style="text-align:right;"> 941.00000 </td>
   <td style="text-align:right;"> 1159.00000 </td>
   <td style="text-align:right;"> 1250.00000 </td>
   <td style="text-align:right;"> 1920 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Q9312510 </td>
   <td style="text-align:left;"> La Nive à Cambo-les-Bains </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 81.000000 </td>
   <td style="text-align:right;"> 305.600000 </td>
   <td style="text-align:right;"> 712.30000 </td>
   <td style="text-align:right;"> 1009.00000 </td>
   <td style="text-align:right;"> 1255.43000 </td>
   <td style="text-align:right;"> 1967 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Y1112010 </td>
   <td style="text-align:left;"> L'Aude à Belvianes-et-Cavirac </td>
   <td style="text-align:right;"> 4.00000 </td>
   <td style="text-align:right;"> 24.00000 </td>
   <td style="text-align:right;"> 54.000000 </td>
   <td style="text-align:right;"> 92.000000 </td>
   <td style="text-align:right;"> 185.00000 </td>
   <td style="text-align:right;"> 303.000000 </td>
   <td style="text-align:right;"> 423.000000 </td>
   <td style="text-align:right;"> 554.000000 </td>
   <td style="text-align:right;"> 748.00000 </td>
   <td style="text-align:right;"> 992.00000 </td>
   <td style="text-align:right;"> 1228.00000 </td>
   <td style="text-align:right;"> 1914 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Y1232010 </td>
   <td style="text-align:left;"> L'Aude à Carcassonne [Pont Neuf] </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 32.00000 </td>
   <td style="text-align:right;"> 80.000000 </td>
   <td style="text-align:right;"> 148.000000 </td>
   <td style="text-align:right;"> 246.00000 </td>
   <td style="text-align:right;"> 416.800000 </td>
   <td style="text-align:right;"> 581.000000 </td>
   <td style="text-align:right;"> 825.000000 </td>
   <td style="text-align:right;"> 1227.00000 </td>
   <td style="text-align:right;"> 1358.10000 </td>
   <td style="text-align:right;"> 1473.00000 </td>
   <td style="text-align:right;"> 1963 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Y1422020 </td>
   <td style="text-align:left;"> L'Aude à Marseillette </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 24.00000 </td>
   <td style="text-align:right;"> 89.000000 </td>
   <td style="text-align:right;"> 178.000000 </td>
   <td style="text-align:right;"> 301.00000 </td>
   <td style="text-align:right;"> 574.000000 </td>
   <td style="text-align:right;"> 875.000000 </td>
   <td style="text-align:right;"> 1215.000000 </td>
   <td style="text-align:right;"> 1442.00000 </td>
   <td style="text-align:right;"> 1595.00000 </td>
   <td style="text-align:right;"> 1732.83000 </td>
   <td style="text-align:right;"> 1986 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Y1612020 </td>
   <td style="text-align:left;"> L'Aude à Moussan [Moussoulens - écluse] </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.000000 </td>
   <td style="text-align:right;"> 1.00000 </td>
   <td style="text-align:right;"> 153.000000 </td>
   <td style="text-align:right;"> 350.000000 </td>
   <td style="text-align:right;"> 709.600000 </td>
   <td style="text-align:right;"> 1297.00000 </td>
   <td style="text-align:right;"> 1568.00000 </td>
   <td style="text-align:right;"> 1853.00000 </td>
   <td style="text-align:right;"> 1965 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> EA010_Girona_Cabal riu Ter </td>
   <td style="text-align:left;"> Aforament - Girona (Ter) </td>
   <td style="text-align:right;"> 1.37245 </td>
   <td style="text-align:right;"> 2.15000 </td>
   <td style="text-align:right;"> 2.848417 </td>
   <td style="text-align:right;"> 3.359437 </td>
   <td style="text-align:right;"> 3.92625 </td>
   <td style="text-align:right;"> 4.861292 </td>
   <td style="text-align:right;"> 6.480000 </td>
   <td style="text-align:right;"> 8.743583 </td>
   <td style="text-align:right;"> 16.99237 </td>
   <td style="text-align:right;"> 33.58850 </td>
   <td style="text-align:right;"> 90.99268 </td>
   <td style="text-align:right;"> 1996 </td>
   <td style="text-align:right;"> 2020 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> EA080_Torroella Montgri_Cabal riu Ter </td>
   <td style="text-align:left;"> Aforament - Torroella de Montgrí (Pont de Torroella) </td>
   <td style="text-align:right;"> 1.86221 </td>
   <td style="text-align:right;"> 2.74036 </td>
   <td style="text-align:right;"> 3.497700 </td>
   <td style="text-align:right;"> 4.097522 </td>
   <td style="text-align:right;"> 4.84000 </td>
   <td style="text-align:right;"> 5.924500 </td>
   <td style="text-align:right;"> 7.852685 </td>
   <td style="text-align:right;"> 12.202000 </td>
   <td style="text-align:right;"> 23.38120 </td>
   <td style="text-align:right;"> 38.40396 </td>
   <td style="text-align:right;"> 137.27639 </td>
   <td style="text-align:right;"> 2001 </td>
   <td style="text-align:right;"> 2020 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> leitzaran_oria </td>
   <td style="text-align:left;"> leitzaran_oria </td>
   <td style="text-align:right;"> 0.86500 </td>
   <td style="text-align:right;"> 1.15700 </td>
   <td style="text-align:right;"> 1.586000 </td>
   <td style="text-align:right;"> 2.073000 </td>
   <td style="text-align:right;"> 2.68600 </td>
   <td style="text-align:right;"> 3.493000 </td>
   <td style="text-align:right;"> 4.686000 </td>
   <td style="text-align:right;"> 6.580000 </td>
   <td style="text-align:right;"> 10.93000 </td>
   <td style="text-align:right;"> 16.52200 </td>
   <td style="text-align:right;"> 33.30170 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> belauntza_oria </td>
   <td style="text-align:left;"> belauntza_oria </td>
   <td style="text-align:right;"> 0.29500 </td>
   <td style="text-align:right;"> 0.39100 </td>
   <td style="text-align:right;"> 0.502000 </td>
   <td style="text-align:right;"> 0.613000 </td>
   <td style="text-align:right;"> 0.74400 </td>
   <td style="text-align:right;"> 0.906600 </td>
   <td style="text-align:right;"> 1.148600 </td>
   <td style="text-align:right;"> 1.551000 </td>
   <td style="text-align:right;"> 2.44820 </td>
   <td style="text-align:right;"> 3.87640 </td>
   <td style="text-align:right;"> 8.02312 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> araxes_oria </td>
   <td style="text-align:left;"> araxes_oria </td>
   <td style="text-align:right;"> 0.54880 </td>
   <td style="text-align:right;"> 0.68400 </td>
   <td style="text-align:right;"> 0.942000 </td>
   <td style="text-align:right;"> 1.285200 </td>
   <td style="text-align:right;"> 1.70400 </td>
   <td style="text-align:right;"> 2.309800 </td>
   <td style="text-align:right;"> 3.400400 </td>
   <td style="text-align:right;"> 5.553200 </td>
   <td style="text-align:right;"> 11.66720 </td>
   <td style="text-align:right;"> 17.26700 </td>
   <td style="text-align:right;"> 37.21136 </td>
   <td style="text-align:right;"> 2011 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> alegia_oria </td>
   <td style="text-align:left;"> alegia_oria </td>
   <td style="text-align:right;"> 0.69000 </td>
   <td style="text-align:right;"> 0.90500 </td>
   <td style="text-align:right;"> 1.221400 </td>
   <td style="text-align:right;"> 1.763600 </td>
   <td style="text-align:right;"> 2.52100 </td>
   <td style="text-align:right;"> 3.732000 </td>
   <td style="text-align:right;"> 5.948400 </td>
   <td style="text-align:right;"> 9.581200 </td>
   <td style="text-align:right;"> 18.83700 </td>
   <td style="text-align:right;"> 32.36800 </td>
   <td style="text-align:right;"> 69.44704 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> ibiur_oria </td>
   <td style="text-align:left;"> ibiur_oria </td>
   <td style="text-align:right;"> 0.00600 </td>
   <td style="text-align:right;"> 0.00900 </td>
   <td style="text-align:right;"> 0.016000 </td>
   <td style="text-align:right;"> 0.026000 </td>
   <td style="text-align:right;"> 0.03800 </td>
   <td style="text-align:right;"> 0.054000 </td>
   <td style="text-align:right;"> 0.080000 </td>
   <td style="text-align:right;"> 0.136000 </td>
   <td style="text-align:right;"> 0.28700 </td>
   <td style="text-align:right;"> 0.51735 </td>
   <td style="text-align:right;"> 1.25374 </td>
   <td style="text-align:right;"> 2009 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> amundarain_oria </td>
   <td style="text-align:left;"> amundarain_oria </td>
   <td style="text-align:right;"> 0.08400 </td>
   <td style="text-align:right;"> 0.12600 </td>
   <td style="text-align:right;"> 0.192000 </td>
   <td style="text-align:right;"> 0.275000 </td>
   <td style="text-align:right;"> 0.40100 </td>
   <td style="text-align:right;"> 0.593600 </td>
   <td style="text-align:right;"> 0.892200 </td>
   <td style="text-align:right;"> 1.419800 </td>
   <td style="text-align:right;"> 2.53000 </td>
   <td style="text-align:right;"> 3.89770 </td>
   <td style="text-align:right;"> 7.79586 </td>
   <td style="text-align:right;"> 2001 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> agauntza_oria </td>
   <td style="text-align:left;"> agauntza_oria </td>
   <td style="text-align:right;"> 0.15300 </td>
   <td style="text-align:right;"> 0.20000 </td>
   <td style="text-align:right;"> 0.268000 </td>
   <td style="text-align:right;"> 0.405000 </td>
   <td style="text-align:right;"> 0.58200 </td>
   <td style="text-align:right;"> 0.885400 </td>
   <td style="text-align:right;"> 1.401000 </td>
   <td style="text-align:right;"> 2.370400 </td>
   <td style="text-align:right;"> 4.79980 </td>
   <td style="text-align:right;"> 8.29680 </td>
   <td style="text-align:right;"> 17.69188 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> estanda_oria </td>
   <td style="text-align:left;"> estanda_oria </td>
   <td style="text-align:right;"> 0.06900 </td>
   <td style="text-align:right;"> 0.09200 </td>
   <td style="text-align:right;"> 0.130000 </td>
   <td style="text-align:right;"> 0.194200 </td>
   <td style="text-align:right;"> 0.29400 </td>
   <td style="text-align:right;"> 0.419000 </td>
   <td style="text-align:right;"> 0.632000 </td>
   <td style="text-align:right;"> 1.065000 </td>
   <td style="text-align:right;"> 2.36170 </td>
   <td style="text-align:right;"> 4.49925 </td>
   <td style="text-align:right;"> 11.97261 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> discharge_urumea </td>
   <td style="text-align:left;"> discharge_urumea </td>
   <td style="text-align:right;"> 1.61170 </td>
   <td style="text-align:right;"> 2.20400 </td>
   <td style="text-align:right;"> 3.048200 </td>
   <td style="text-align:right;"> 4.068000 </td>
   <td style="text-align:right;"> 5.23800 </td>
   <td style="text-align:right;"> 6.730000 </td>
   <td style="text-align:right;"> 8.611500 </td>
   <td style="text-align:right;"> 11.575800 </td>
   <td style="text-align:right;"> 18.41250 </td>
   <td style="text-align:right;"> 29.93160 </td>
   <td style="text-align:right;"> 61.28482 </td>
   <td style="text-align:right;"> 1992 </td>
   <td style="text-align:right;"> 2020 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> lasarte_oria </td>
   <td style="text-align:left;"> lasarte_oria </td>
   <td style="text-align:right;"> 3739.40000 </td>
   <td style="text-align:right;"> 4660.80000 </td>
   <td style="text-align:right;"> 5701.800000 </td>
   <td style="text-align:right;"> 7520.000000 </td>
   <td style="text-align:right;"> 9954.00000 </td>
   <td style="text-align:right;"> 13626.600000 </td>
   <td style="text-align:right;"> 19073.600000 </td>
   <td style="text-align:right;"> 29416.000000 </td>
   <td style="text-align:right;"> 53327.00000 </td>
   <td style="text-align:right;"> 86314.20000 </td>
   <td style="text-align:right;"> 197109.00000 </td>
   <td style="text-align:right;"> 2000 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
  <tr>
   <td style="text-align:left;"> Mondego_coimbra 12G 01A </td>
   <td style="text-align:left;"> Açude ponte Coimbra </td>
   <td style="text-align:right;"> 7.75000 </td>
   <td style="text-align:right;"> 10.41400 </td>
   <td style="text-align:right;"> 12.790000 </td>
   <td style="text-align:right;"> 15.468000 </td>
   <td style="text-align:right;"> 20.07000 </td>
   <td style="text-align:right;"> 28.564000 </td>
   <td style="text-align:right;"> 45.350000 </td>
   <td style="text-align:right;"> 81.692000 </td>
   <td style="text-align:right;"> 182.11200 </td>
   <td style="text-align:right;"> 318.25400 </td>
   <td style="text-align:right;"> 812.51780 </td>
   <td style="text-align:right;"> 1989 </td>
   <td style="text-align:right;"> 2019 </td>
  </tr>
</tbody>
</table>


