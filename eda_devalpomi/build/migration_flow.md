---
title: "Migration flow"
author: "Cédric, Nils, Maria"
date: "24/11/2020"
output: html_document
---







```r
# data from gave de Pau

# franchissement Gave de Pau
fg <- read_excel("D:/eda/data/eda2.3/modelisation_migration/debits_devalaison_gave_pau.xlsx", sheet="franch")
# debits classes periode etude
dcpe <- read_excel("D:/eda/data/eda2.3/modelisation_migration/debits_devalaison_gave_pau.xlsx",  sheet="debit_classe")
RPostgres::Postgres()
con <- dbConnect(RPostgres::Postgres(),
		dbname="eda2.3",
		host="localhost",
		port=5432,
		user= userlocal,
		password= passwordlocal)
# Le Gave de Pau à Bérenx [Pont de Bérenx]


flow<- dbGetQuery(con,"SELECT * FROM  sudoang.hydro_station_gt2_quantile where code_hydro_station='Q5501010'")



# building a graph to compare migration
f1<- flow %>% select (starts_with("q_1004")) %>% 
		select(-c("q_1004_75","q_1004_975")) %>%
		filter(row_number()==1)
d1 <- dcpe %>% 
		filter(Centile%in%c(10,20,30,40,50,60,70,80,90,95,99)) %>%
		select(Q) %>% t() 
# On peut ajouter 97.5 
colnames(d1) <- colnames(f1)
d2 <- rbind(d1, f1)
d2 <- cbind(d2, source= c("Q_1104_2006_2010","Q_1004_1923_2017"))
d3 <- d2 %>% pivot_longer(cols=-source,names_to='Qi', values_to='Q')

f1 <- f1[,-10] # 10 to 99 no 95

f2 <- flow %>% select (starts_with("q_1004"))%>%filter(row_number()==1) # tous les débits (not used)

f1 <-c(0,as.numeric(f1),10000) # first class is : 0 - Q10  ... last class is > Q90 (this makes 10 + 1 classes)
length(f1) # 11
# only class 10 20 ...90 99 are kept we need to remove Q95
fg$qcalss <- cut(fg$Qjournalier,f1)
# les débits classés 10 correspondent à 0, ...., dans les données de Nils
	
# ALTERNATIVE DEMANDEE PAR PIERRE, LES DEBITS SONT UTILISES SUR LA PERIODE DE REFERENCE


d1 <- c(0,d1[-10],10000)
# Alternative : utiliser les débits classés pendant la période d'étude
fg$qcalss <- cut(fg$Qjournalier,d1)

fg$qcalss <- as.integer(fg$qcalss)-1 # numbers between 0 and 10 11 classes

# tous les débits classés
dcpe1<- dcpe %>% 
		select(Q) %>% t() 
fg$qcalssfull <- cut(fg$Qjournalier,breaks=c(0,dcpe1,10000),labels=c(0,dcpe$Centile))
fg$qcalssfull <- as.numeric(as.character(fg$qcalssfull))
#fg$qcalssfull[fg$qcalssfull==9.9] <- 10
save(fg, flow, dcpe, d3, file="C:/workspace/edadata/sudoang/data_gave_pau.Rdata")
```



```r
load(file="C:/workspace/edadata/sudoang/data_gave_pau.Rdata")
# * fg daily migration with classified flows
# * flow classified flow at BerenQ
# * dcpe debits classés période d'étude data compiled in excel by Philippe Baran and classifying flows 
# during the study period
# d3 Comparision of quantiles computed for flow and dcpe (for graph)



# Graph comparing the migration in the gave de Pau during study period and for the whole hydro station recording period. The flows are comparable.
ggplot(d3)+geom_col(aes(x=Qi,y=Q,fill=source), position="dodge") + scale_fill_brewer(palette="Dark2")
```

![plot of chunk load_data](figure/load_data-1.png)

```r
ll <-list.files("D:/eda/data/eda2.3/modelisation_migration/")
ll <- ll[grep("extr",ll)]
for (name in ll){
	load(file.path("D:/eda/data/eda2.3/modelisation_migration/", name))
}



mig_class_flow <- 
		bind_rows(
				extr_Aulne %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Aulne', perc=N/sum(N,na.rm=TRUE)),
				extr_Elorn %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Elorn', perc=N/sum(N,na.rm=TRUE)),
				extr_Loire %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Loire', perc=N/sum(N,na.rm=TRUE)),
				extr_Orne %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Orne', perc=N/sum(N,na.rm=TRUE)),
				extr_Scorff %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Scorff', perc=N/sum(N,na.rm=TRUE)),
				extr_Sevre %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Sevre', perc=N/sum(N,na.rm=TRUE)),
				extr_Touques %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Touques', perc=N/sum(N,na.rm=TRUE)),
				extr_Vilaine %>% group_by(qcalss) %>% summarise(N=sum(N, na.rm= TRUE)) %>% mutate(loc='Vilaine', perc=N/sum(N,na.rm=TRUE)),
				fg %>% group_by(qcalss) %>% summarise(N=n())%>% mutate(loc='Gave de Pau', perc=N/sum(N,na.rm=TRUE))
		)

ggplot(mig_class_flow)+geom_point(aes(x=qcalss,y=perc,col=loc)) + geom_line(aes(x=qcalss,y=perc,col=loc, group=loc))
```

```
## Warning: Removed 2 rows containing missing values (geom_point).
```

```
## Warning: Removed 2 row(s) containing missing values (geom_path).
```

![plot of chunk load_data](figure/load_data-2.png)

```r
mig_class_flowdetail <- fg %>% group_by(qcalssfull) %>% summarise(N=n())%>% mutate(loc='Gave de Pau', perc=N/sum(N,na.rm=TRUE))
```

```
## Error: Must group by variables found in `.data`.
## * Column `qcalssfull` is not found.
```

Now we need to expand on this and just try to collect the surface of the basin or module from the stations.

* TODO get locations of stations (ask Nils)
* Extract and test the following variables in a mixture model (per basin structure)
*  Q50 (Cédric)
*  size of the upstream basin in km2
* basin_fragmentation as avg number of dams per km2
* basin_fragmentation2 as avg cumhight of dams per segment 
* size structure predicted above each station
* extract cumulated number of silver eel per size class above dam




```r
mig_class_flow <- mig_class_flow %>% group_by(loc) %>% arrange(qcalss) %>%
		mutate(C=cumsum(perc))
mig_class_flowdetail  <- mig_class_flowdetail %>% group_by(loc) %>% arrange(qcalssfull) %>%
		mutate(C=cumsum(perc))

ggplot(mig_class_flow)+
		geom_point(aes(x=qcalss,y=perc,col=loc)) +
		geom_line(aes(x=qcalss,y=perc,col=loc,group= loc)) + 
		scale_colour_tableau('Classic Cyclic') +
		theme_igray() 
```

```
## Warning: Removed 2 rows containing missing values (geom_point).
```

```
## Warning: Removed 2 row(s) containing missing values (geom_path).
```

![plot of chunk test_models](figure/test_models-1.png)

```r
ggplot(mig_class_flow)+
		geom_point(aes(x=qcalss,y=C,col=loc)) +
		geom_line(aes(x=qcalss,y=C,col=loc,group= loc))  +
		scale_colour_tableau('Classic Cyclic') +
		theme_igray()+
		scale_x_continuous("Quantiles", 
				breaks=c(0,2,4,6,8,9,10),minor_breaks=NULL,labels=c(
						"<10",
						"20-30",
						"40-50",
						"60-70",
						"80-90", 
						"90-99", 
						">99"))
```

```
## Warning: Removed 2 rows containing missing values (geom_point).

## Warning: Removed 2 row(s) containing missing values (geom_path).
```

![plot of chunk test_models](figure/test_models-2.png)

```r
ggplot(mig_class_flowdetail)+
		geom_point(aes(x=qcalssfull,y=C, col=loc), size=2)+
		geom_line(aes(x=qcalssfull,y=C,col=loc,group= loc),  lty=2)+
		scale_colour_tableau('Classic Cyclic') +
		theme_igray()+
		scale_x_continuous("Quantiles", 
				breaks=c(0,15,55,75,85,95,99),minor_breaks=NULL,
				labels=c("0-5","15-20","55-60","75-80","85-90","95-97.5",">99"))
```

![plot of chunk test_models](figure/test_models-3.png)

```r
g_binom <- glm(C ~ qcalss + loc, data = mig_class_flow, family = binomial(link = "logit"))
```

```
## Warning in eval(family$initialize): non-integer #successes in a binomial glm!
```

```r
plot(g_binom)
```

![plot of chunk test_models](figure/test_models-4.png)![plot of chunk test_models](figure/test_models-5.png)![plot of chunk test_models](figure/test_models-6.png)![plot of chunk test_models](figure/test_models-7.png)

```r
dd <- expand.grid(loc=unique(mig_class_flow$loc),qcalss=seq(from=0, to=10), stringsAsFactors = FALSE)
dd$p=boot::inv.logit(predict(g_binom,newdata=dd))
dd$upper=boot::inv.logit(predict(g_binom,newdata=dd) + 1.96*predict(g_binom,newdata=dd,se.fit = TRUE)$se.fit)
dd$lower=boot::inv.logit(predict(g_binom,newdata=dd) - 1.96*predict(g_binom,newdata=dd,se.fit = TRUE)$se.fit)
ddsub <- subset(dd,qcalss==5|qcalss==9|qcalss==10)
ddsub$lab <- str_c("Q50=",round(100*ddsub$p))
ddsub$lab[ddsub$qcalss==10] <- str_c("Q99=",round(100*ddsub$p[ddsub$qcalss==10]))
ddsub$lab[ddsub$qcalss==9] <- str_c("Q90=",round(100*ddsub$p[ddsub$qcalss==9]))


ggplot(dd) +
		geom_point(aes(x=qcalss,y=C,col=loc), data = mig_class_flow) +
		geom_line(aes(x=qcalss,y=p,col=loc, group=loc)) +
		geom_point(aes(x=qcalss,y=p), pch=22) +
		geom_ribbon(aes(x=qcalss, ymin=lower, ymax=upper,fill=loc), alpha=0.2) +
		geom_point(aes(x=qcalss,y=p,col=loc), data = ddsub, size=2.5, pch=21)  +
#		geom_label_repel(aes(x=qcalss,y=p, label=lab), data=ddsub,
#				segment.size = 0.5,
#				segment.color = "grey50",
#				alpha=1,
#				vjust = 0.5,
#				nudge_x=-10,
#				nudge_y=-0.1,
#				alpha=0.5) +	
		scale_x_continuous("Quantiles", breaks=c(0,2,4,6,8,9,10),minor_breaks=NULL,labels=c(
						"<10",
						"20-30",
						"40-50",
						"60-70",
						"80-90", 
						"90-99", 
						">99"))+
		facet_wrap(~loc) +
		scale_colour_tableau('Classic Cyclic') +
		scale_fill_tableau('Classic Cyclic') +
		theme_igray()
```

```
## Warning: Removed 2 rows containing missing values (geom_point).
```

![plot of chunk test_models](figure/test_models-8.png)

```r
reverse_cumsum <- function(X) c(X[1],diff(X))
# note below in practise Q95 is Q99 and Q99 is beyond Q99
# TODO find a way to deal with this...
dd0 <- dd %>% arrange(loc) %>% mutate(qcalss1=recode(qcalss,
						"0"="q_1004_10", 
						"1"="q_1004_20", 
						"2"="q_1004_30",
						"3"="q_1004_40", 
						"4"="q_1004_50", 
						"5"="q_1004_60", 
						"6"="q_1004_70", 
						"7"="q_1004_80",
						"8"="q_1004_90", 
						"9"="q_1004_95", 
						"10"="q_1004_99")) %>%
		group_by(loc)%>% mutate (p0=reverse_cumsum(p)) %>% ungroup()



ggplot(dd0) +
		geom_point(aes(x=qcalss,y=perc,col=loc), data = mig_class_flow) +
		geom_line(aes(x=qcalss,y=p0,col=loc, group=loc)) +
		scale_x_continuous(breaks=c(0,2,4,6,8,9,10),minor_breaks=NULL,labels=c(
						"<10",
						"20-30",
						"40-50",
						"60-70",
						"80-90", 
						"90-99", 
						">99"))+
		scale_colour_tableau('Classic Cyclic') +		
		theme_igray()
```

```
## Warning: Removed 2 rows containing missing values (geom_point).
```

![plot of chunk test_models](figure/test_models-9.png)

```r
dd0w <- dd0 %>%	
		select(-c("upper","lower")) %>%
		pivot_wider(id_cols=loc, names_from= qcalss1, values_from=p0)
dd0_mat <- dd0w %>% select(-loc) %>% as.matrix()
sumcol <- rowSums(dd0_mat)
dd0w[,2:12]<- dd0_mat / matrix(rep(sumcol, ncol(dd0_mat)),ncol=ncol(dd0_mat))
migration_flow_temp <- dd0w
# NOTE VERSION "old" corresponds to flow in the gave without using the classified flow during the period
# => use migration_flow_temp_old to get this file !!!
saveRDS(dd0w, file = str_c("C:/workspace/EDAdata/report2.3/data/migration_flow_temp.Rds"))
# use Sèvre dd0w[8,] for migration at large flow or Orne dd0[4,] for migration at low flow
# dd0w<- readRDS(file = str_c("C:/workspace/EDAdata/report2.3/data/migration_flow_temp.Rds"))
library(writexl)
write_xlsx(dd0w, path= "C:/workspace/EDAdata/report2.3/data/migration_flow_temp.xlsx")
```
