# Instructions to create devalpomi


## flow data

the flow data are built in  import_flowlb.Rmd
The latest version of the script uses API https://github.com/inrae/hubeau to download the data from hub
The station hydro are selected from the old dataset debit_classés


## Dam data

Main sript import loads data from BDOE, ROE and ICE 
[[SQL script]](https://forgemia.inra.fr/pole-migrateurs/eda/dbeel/-/blob/main/eda2.3/sql/import_dam_france.sql)

## Downstream migration mortalities (for eel)

See html for [Mortality turbine script](https://forgemia.inra.fr/pole-migrateurs/eda/eda_model/-/blob/main/eda_devalpomi/build/Mortalites_turbines_adour.Rmd)


## Shiny

See export_shiny trunk in [Mortality turbine script](https://forgemia.inra.fr/pole-migrateurs/eda/eda_model/-/blob/main/eda_devalpomi/build/Mortalites_turbines_adour.Rmd)