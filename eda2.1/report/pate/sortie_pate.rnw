\documentclass[a4paper]{article}
\usepackage{Sweave}
\usepackage[latin1]{inputenc}
\begin{document}
\title{ Analyse des r�sultats pour l'application du mod�le EDA2.1 aux mortalit�s dans les turbines}
\author{Briand C., Jouanin C., Baran P., Drouineau H., Gomes P., \\Berger V., Beaulaton L., Lambert P.\\ version de d�veloppement}
\date\today
\maketitle
\textit{Attention donn�es provisoires, ne pas diffuser !}
%==========================================
\section{Chargement des donn�es}
Les donn�es sont charg�es � l'aide de main\_rht\_result.R. 
Ici on utilise un raccourci en chargeant les Rdata,
les donn�es RHT et les donn�es du mod�le ont �t� charg�es au pr�alable � l'aide des classes BaseEdaRHTpate
et BaseEdaRHTmodel. Le calcul des valeurs de mortalit� se fait � partir de la mod�lisation du flux d�valant
par les turbines suivant trois hypoth�ses de courbes logistiques d�riv�es de l'�tude des ouvrages sur le
gave de Pau (\textit{Gomes} \textbf{pr�ciser la r�f�rence}, 2011) mutipli� par le coefficient de mortalit� estim�
� partir de formules pr�dictives (\textit{Larinier et al} \textbf{pr�ciser la r�f�rence}, 2011).
<< chargement, echo=TRUE, fig=FALSE, width=10, height=4 >>=
strftime(Sys.Date())
source("EDACCM/init.r")
source("EDARHT/function_PATE.R")
library(RColorBrewer)
load(file=str_c(datawd,"/dataEDArht/baseEdaRHTpate.Rdata"))
baseEdaRHTpatedebit<-new("BaseEdaRHTpatedebit",
		baseODBC="baseODBCrht",
		schema="rht",
		table="pate_debit_classe",
		prkey="id_drain",
		zonegeo="France")
baseEdaRHTpatedebit<-loaddb(baseEdaRHTpatedebit)
Qrate<-baseEdaRHTpatedebit@datapatedebit
pate<-baseEdaRHTpate@datapate
MortaliteOuvrage<-calculMortaliteOuvrage(Qmodule=baseEdaRHTpate@datapate$module,
		Qequipement=baseEdaRHTpate@datapate$q_equipement,
		alphai=cbind(Qrate$q75_rht[match(pate$bv,Qrate$bv)],Qrate$q90_rht[match(pate$bv,Qrate$bv)],Qrate$q95_rht[match(pate$bv,Qrate$bv)],Qrate$q975_rht[match(pate$bv,Qrate$bv)],Qrate$q99_rht[match(pate$bv,Qrate$bv)]),
		delta=cbind(c(-2.4,8.7877),c(-2.400580,5.767987),c(-4.558, 7)),
		tau=cbind(min(baseEdaRHTpate@datapate$mortalite70cm)/100,baseEdaRHTpate@datapate$mortalite70cm/100,max(baseEdaRHTpate@datapate$mortalite70cm)/100))
baseEdaRHTpate@datapate$mortalite<-MortaliteOuvrage[,2]
baseEdaRHTpate@datapate$mortalitefavorable<-MortaliteOuvrage[,1]
baseEdaRHTpate@datapate$mortalitedefavorable<-MortaliteOuvrage[,3]
t1<-Sys.time()
baseEdaRHTpate<-segment_mortality(baseEdaRHTpate)
print(Sys.time()-t1)
@
%==========================================
\section{Exemples de barrages}
Les scripts ci dessous ont �t� d�velopp�s pour visualiser la bonne position du bassin amont du barrage,
Les valeurs pr�sent�es dans les figures montrent les \textbf{ouvrage ROE} s�lectionn�s dans le cadre du calcul sur 
les bassins versants ateliers. La \textbf{zone surlign�e en noir} correspond au bassin versant situ� en amont d'un ouvrage.
Le script fait appel � la methode map en passant un identifiant d'ouvrage et retourne une carte du bassin.
La m�thode map utilise plusieurs fonction postgres, qui utilisent le parcours depuis la mer (ltree) pour identifier des parent�s g�ographiques
\begin{description}
	\item[rht.sea\_riversegment(id\_ numeric)] {Renvoit le  noeud mer du bassin versant }
	\item[rht.upstream\_segments(id\_ numeric)]{Renvoit les id drains du bassin versant amont}
\end{description}
<< Bassins_amont1, echo=TRUE, fig=TRUE, width=7, height=7 >>=
source("EDARHT/function_PATE.R")
map(baseEdaRHTpate,bv="Adour",id_roe="ROE44851")
@
\\
Figure~1.1.- Nombre d'anguilles argent�es � l'estuaire et au niveau de l'ouvrage de Baigts (ROE44851), le bassin en amont de l'ouvrage est surlign� en noir , les effectifs sont estim�s par le mod�le EDA2.1

\newpage
<< Bassins_amont2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Blavet",id_roe="ROE11400")
@
\newpage
<< Bassins_amont3, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Arc-Touloubre",id_roe="ROE42134")
@
\newpage
<< Bassins_amont4, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Charente-Boutonne-Tardoire-Bonnieure",id_roe="ROE50647")
@
\newpage
<< Bassins_amont5, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Vilaine-Oust",id_roe="ROE11542")
@
\newpage
<< Bassins_amont6, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Dronne",id_roe="ROE54318")
@
\newpage
<< Bassins_amont7, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Bresle",id_roe="ROE34300")
@
\newpage
<< Bassins_amont8, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Herault",id_roe="ROE48873")
@
\newpage
<< Bassins_amont9, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Mayenne",id_roe="ROE20586")
@
\newpage
<< Bassins_amont10, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Vire",id_roe="ROE7058")
@
%==========================================
\section{Carte des r�sultats par bassins}

<< mort1_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Adour",id_roe=NULL,typesortie=1)
@
Figure~2.1.- Taux de mortalit� par segment pour le bassin de l'Adour, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de l'\textbf{Adour} :
\\
<< mort1_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Adour",id_roe=NULL,typesortie=2)
@
\\
Figure~2.2.- Nombre d'anguilles mortes par segment pour le bassin de l'Adour, estim�es par le mod�le EDA2.1 turbines
\newpage
<< mort2_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Arc-Touloubre",id_roe=NULL,typesortie=1)
@
Figure~2.3.- Taux de mortalit� par segment pour le bassin de l'Arc, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de l' \textbf{Arc}  :
\\
<< mort2_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Arc-Touloubre",id_roe=NULL,typesortie=2)
@
\\
Figure~2.4.- Nombre d'anguilles mortes par segment pour le bassin de l'Arc, estim�es par le mod�le EDA2.1 turbines

\newpage
<< mort3_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Blavet",id_roe=NULL,typesortie=1)
@
\\
Figure~2.5.- Taux de mortalit� par segment pour le bassin du Blavet, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin du \textbf{Blavet} :
\\
<< mort3_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Blavet",id_roe=NULL,typesortie=2)
@
\\
Figure~2.6.- Nombre d'anguilles mortes par segment pour le bassin du Blavet, estim�es par le mod�le EDA2.1 turbines
\newpage

<< mort4_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Bresle",id_roe=NULL,typesortie=1)
@
\\
Figure~2.7.- Taux de mortalit� par segment pour le bassin de la Bresle, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de la \textbf{Bresle} :
\\
<< mort4_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Bresle",id_roe=NULL,typesortie=2)
@
\\
Figure~2.8.-  Nombre d'anguilles mortes par segment pour le bassin de la Bresle, estim�es par le mod�le EDA2.1 turbines

\newpage

<< mort5_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Charente-Boutonne-Tardoire-Bonnieure",id_roe=NULL,typesortie=1)
@
\\
Figure~2.9.- Taux de mortalit� par segment pour le bassin de la Charente, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de la \textbf{Charente} :
\\

<< mort5_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Charente-Boutonne-Tardoire-Bonnieure",id_roe=NULL,typesortie=2)
@
\\
Figure~2.10.-  Nombre d'anguilles mortes par segment pour le bassin de la Charente, estim�es par le mod�le EDA2.1 turbines

\newpage

<< mort6_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Dronne",id_roe=NULL,typesortie=1)
@
\\
Figure~2.11.- Taux de mortalit� par segment pour le bassin de la Dronne, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de la \textbf{Dronne} :
\\
<< mort6_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Dronne",id_roe=NULL,typesortie=2)
@
\\
Figure~2.12.- Nombre d'anguilles mortes par segment pour le bassin de la Dronne, estim�es par le mod�le EDA2.1 turbines
\newpage

<< mort7_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Herault",id_roe=NULL,typesortie=1)
@
\\
Figure~2.13.- Taux de mortalit� par segment pour le bassin de l'H�rault, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de l' \textbf{H�rault} :
\\
<< mort7_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Herault",id_roe=NULL,typesortie=2)
@
\\
Figure~2.14.- Nombre d'anguilles mortes par segment pour le bassin de l'H�rault, estim�es par le mod�le EDA2.1 turbines

\newpage

<< mort8_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Mayenne",id_roe=NULL,typesortie=1)
@
Figure~2.15.- Taux de mortalit� par segment pour le bassin de la Mayenne, estim�es par le mod�le EDA2.1 turbines
\\
Bassin de la \textbf{Mayenne} :
\\
\\
<< mort8_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Mayenne",id_roe=NULL,typesortie=2)
@
\\
Figure~2.16.- Nombre d'anguilles mortes par segment pour le bassin de la Mayenne, estim�es par le mod�le EDA2.1 turbines

\newpage

<< mort9_1, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Vilaine-Oust",id_roe=NULL,typesortie=1)
@
\\
Figure~2.17.- Taux de mortalit� par segment pour le bassin de la Vilaine, estim�es par le mod�le EDA2.1 turbines
\\
\\
Bassin de la \textbf{Vilaine}
\\
<< mort9_2, echo=FALSE, fig=TRUE, width=7, height=7 >>=
map(baseEdaRHTpate,bv="Vilaine-Oust",id_roe=NULL,typesortie=2)
@
\\
Figure~2.16.- Nombre d'anguilles mortes par segment pour le bassin de la Vilaine, estim�es par le mod�le EDA2.1 turbines

%==========================================
\section{Ouvrages s�lectionn�s sur les bassins versants, et r�sulats par ouvrage}
<<options, echo = FALSE>>=
library(xtable) # pour pouvoir produire des tables LaTeX dans R
funtab<-function(bv){
datapate<-baseEdaRHTpate@datapate[baseEdaRHTpate@datapate$bv==bv,c("id_roe","nom","mortalite50cm","mortalite70cm","mortalite90cm")]
rownames(datapate)<-1:nrow(datapate)
pate.table <- xtable(x = datapate, label = "ouvrage",caption = str_c("Donn�es d'ouvrage du bassin ",bv," 1 sur 3"))
digits(pate.table)[c(4:6)] <- c(1,1,1)
print(pate.table, file = str_c("table_",bv,"_1.tex"), size = "tiny", NA.string = ".")
datapate<-baseEdaRHTpate@datapate[baseEdaRHTpate@datapate$bv==bv,c("id_roe","q_equipement","h","dmer","module","qe_qm")]
rownames(datapate)<-1:nrow(datapate)
pate.table <- xtable(x = datapate, label = "ouvrage",caption = str_c("Donn�es d'ouvrage du bassin ",bv," 2 sur 3"))
digits(pate.table)[c(2:7)] <- c(0,rep(1,4),3)
print(pate.table, file = str_c("table_",bv,"_2.tex"), size = "tiny", NA.string = ".")
datapate<-baseEdaRHTpate@datapate[baseEdaRHTpate@datapate$bv==bv,c("id_roe","mortalite","mortalitedefavorable","mortalitefavorable")]
rownames(datapate)<-1:nrow(datapate)
pate.table <- xtable(x = datapate, label = "ouvrage",caption = str_c("Donn�es d'ouvrage du bassin ",bv," 3 sur 3"))
print(pate.table, file = str_c("table_",bv,"_3.tex"), size = "tiny", NA.string = ".")
}
funtab("Adour")
funtab("Arc-Touloubre")
funtab("Mayenne")
funtab("Herault")
funtab("Blavet")
funtab("Charente-Boutonne-Tardoire-Bonnieure")
#funtab("Vilaine-Oust")
funtab("Dronne")
funtab("Bresle")
@
La s�lection d'ouvrage a �t� r�alis�e par l'ONEMA et le p�le �cohydraulique, les donn�es manquantes 
ont �t� renseign�es par expertise. Les r�sultats sont pr�sent�s pour chaque bassin. Pour l'adour le nom de la microcentrale
et les mortalit�s estim�es dans les turbines de l'ouvrage sont la Table~1, la description des caract�ristiques des turbines est dans la Table~2, 
et les r�sultats sont dans la Table ~3.

\input{table_Adour_1.tex}
\input{table_Adour_2.tex}
\input{table_Adour_3.tex}
\input{table_Arc-Touloubre_1.tex}
\input{table_Arc-Touloubre_2.tex}
\input{table_Arc-Touloubre_3.tex}
\input{table_Mayenne_1.tex}
\input{table_Mayenne_2.tex}
\input{table_Mayenne_3.tex}
\input{table_Herault_1.tex}
\input{table_Herault_2.tex}
\input{table_Herault_3.tex}
\input{table_Blavet_1.tex}
\input{table_Blavet_2.tex}
\input{table_Blavet_3.tex}
\input{table_Charente-Boutonne-Tardoire-Bonnieure_1.tex}
\input{table_Charente-Boutonne-Tardoire-Bonnieure_2.tex}
\input{table_Charente-Boutonne-Tardoire-Bonnieure_3.tex}
%\input{table_Vilaine-Oust_1.tex}
%\input{table_Vilaine-Oust_2.tex}
%\input{table_Vilaine-Oust_3.tex}
\input{table_Dronne_1.tex}
\input{table_Dronne_2.tex}
\input{table_Dronne_3.tex}
\input{table_Bresle_1.tex}
\input{table_Bresle_2.tex}
\input{table_Bresle_3.tex}
\section{Analyse des r�sultats de mortalit�}
%Analyse de la coh�rence des r�sultats, fr�quence de mortalit� pour tous les segments situ�s en aval du premier ouvrage
%et ce pour l'ensemble de la France. 
%<< Bassins_amont10, echo=FALSE, fig=TRUE, width=7, height=7 >>=
%hist(apply(baseEdaRHTpate@rhtmortalities,1,function(X)prod(1-X)),main="Freq mort cum pour les segments en amont des bv ateliers")
%@
\end{document}
