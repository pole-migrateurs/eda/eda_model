\documentclass[a4paper]{article}
\usepackage[francais]{babel}
\usepackage{Sweave}
\usepackage[pdftex]{graphicx}
\usepackage[latin1]{inputenc}  
\usepackage[T1]{fontenc}
\graphicspath{{D:/CelineJouanin/workspace/EDAdata/dataEDArht/images/}} %D:/CelineJouanin/workspace/EDAdata/dataEDArht/images/E:/workspace/EDAdata/images/
\begin{document}
\title{ Analyse des r�sultats pour l'application du mod�le EDA2.1 aux mortalit�s dans les turbines\\ Application � la France}
\author{Briand C., Jouanin C., Baran P., Drouineau H., Gomes P., \\Berger V., Beaulaton L., Lambert P.\\ version de d�veloppement}
\date\today
\maketitle
\begin{center}
\textit{Attention donn�es provisoires, ne pas diffuser !}
\end{center}
%==========================================
\section{M�thode utilis�e pour le calcul des mortalit�s}
%==========================================
Les donn�es sont charg�es �  l'aide de main\_rht\_patefrance\_Result.R. 
La classe BaseRHTpatefrance  est cr�e pour modifier la m�thode turbine\_mortality. 
Le parcours des segments en amont d'un ouvrage donn�
 est le m�me et il est toujours stock� dans le slot datadam. Par contre, au lieu de stocker les r�sultats
 dans une matrice avec une ligne par id\_drain et une colonne par ouvrage, ici on ne garde qu'une colonne
 qui contient le nombre d'ouvrages en aval d'un id\_drain donn�. Le calcul des mortalit�s aux ouvrages s'�crit \textbf{(Equation \ref{survieouvrages})}
\begin{equation}
1-\prod_{k=1}^n (1-M)=1-(1-M)^n \label{survieouvrages}
\end{equation} 
Avec M=0.06 calcul�e comme la m�diane des mortalit�s aux ouvrages sur les bassins versant atelier.

%==========================================
\section{R�sultats au niveau de la France}
%==========================================
<< calcul1, echo=FALSE, fig=FALSE>>=
source("EDACCM/init.r")
load(str_c(datawd,"/dataEDArht/baseEdaRHTpatefrance4.Rdata"))# 3 sans altitudes
mort_per_seg<-100*round(1-mean(baseEdaRHTpatefrance@data$survie_finale_par_segment,na.rm=TRUE),3)
morttot<-round(sum(baseEdaRHTpatefrance@data$mortalite_totale))
prodtot<-round(sum(baseEdaRHTpatefrance@data$pred))
ugacourt<-substr(baseEdaRHTpatefrance@data$uga,1,4)
mortuga<-tapply(baseEdaRHTpatefrance@data$mortalite_totale,ugacourt,function(X) round(sum(X)))
produga<-tapply(baseEdaRHTpatefrance@data$pred,ugacourt,function(X) round(sum(X)))
mort_per_seg<-round(1-mean(baseEdaRHTpatefrance@data$survie_finale_par_segment,na.rm=TRUE),2)
mort_per_seg_uga<-tapply(baseEdaRHTpatefrance@data$survie_finale_par_segment,
		ugacourt,
		function(X) 100*round(1-mean(X, na.rm=T),3))
# polpulation amont
seg_amont<-baseEdaRHTpatefrance@data$id_drain%in%baseEdaRHTpatefrance@traveled_segments 
prodam<-sum(baseEdaRHTpatefrance@data[seg_amont,"pred"])
prodav<-prodtot-prodam
pprodav<-prodav/prodtot
prodamuga<-tapply(baseEdaRHTpatefrance@data[seg_amont,"pred"],ugacourt[seg_amont],function(X) round(sum(X)))
prodavuga<-produga-prodamuga
pprodavuga<-prodavuga/produga
@
<< table, echo=FALSE, fig=FALSE,include=FALSE>>=
library(xtable)
tableUGA<-rbind(mortuga,mort_per_seg_uga,100*round(mortuga/produga,3),100*round(pprodavuga,3),prodamuga)
rownames(tableUGA)<-c("Nombre arg.","tx_mort_segment","tx_mort","production aval","production amont")
mort_table <- xtable(x = tableUGA, label = "table:tableuga",caption = ("R�sultats par UGA"))
digits(mort_table) <- matrix(data=c(c(0,rep(0,ncol(mort_table)),
						c(0,rep(2,ncol(mort_table))),
						c(0,rep(2,ncol(mort_table))),
						c(0,rep(2,ncol(mort_table))),
						c(0,rep(0,ncol(mort_table))))),
						nrow=5,ncol=ncol(mort_table)+1,byrow=TRUE)
print(mort_table, file = "mort_table.tex", table.placement = "tbp",
caption.placement = "bottom", NA.string = ".")
@

<<label=barplot_uga,echo=FALSE,include=FALSE>>=
#graphique des mortalit�s aux ouvrages
barplot(mortuga,main="mortalit�s aux turbines en France",xlab="UGA",ylab="Nombre d'anguilles argent�es")
#par("din") pour voir la taille que l'on souhaite obtenir � la fin
@
% PARAGRAPHE MORTALITE TOTALE
La mortalite totale des anguilles argent�es par les turbines en France est de \Sexpr{morttot} ce qui compar�
� la production en anguilles argent�es du pays estim�e par le mod�le EDA2.1: \Sexpr{prodtot}, repr�sente
\Sexpr{round(100*morttot/prodtot,1)} pourcent de la production totale estim�e en France.
La mortalit� moyenne par segment du RHT est �lev�e, \Sexpr{mort_per_seg} mais beaucoup de segments
pour lequels une mortalit� a �t� calcul�e sont situ�s en amont et ne poss�dent pas d'anguilles.
%==========================================
\section{R�sultats par UGA}
%==========================================
% insertion de la Figure des mortalit�s  http://users.stat.umn.edu/~geyer/Sweave/foo.pdf
\begin{figure}
\begin{center}
<<label=barplot_uga1,fig=TRUE,echo=FALSE,width=7, height=5>>=
<<barplot_uga>>
@
\end{center}
\caption{Mortalit�s d''anguilles argent�es dans les turbines, estimation par UGA par le mod�le EDA2.1}
\label{fig:barplot_uga}
\end{figure}
% NOMBRE MORTES PAR UGA
\\
Les mortalit�s les plus importantes sont observ�es en Rh�ne-M�diterrann�e \Sexpr{round(tableUGA[1,"Rhon"])}, 
Seine-Normandie \Sexpr{round(tableUGA[1,"Sein"])} et Garonne \Sexpr{round(tableUGA[1,"Garo"])} 
\textbf{(Figure \ref{fig:barplot_uga})}. 
\\
Les r�sultats par UGA sont synth�tis�s au Tableau \ref{table:tableuga}. 
\begin{itemize}
\item Nombre Ar.=nombre d'anguilles argent�es mortes dans les tubines
\item tx\_mort\_segment=moyenne des taux de mortalit� par segment
\item tx\_mort=Nombre mortes/Production
\end{itemize}
\\
% insertion de la Table des mortalit�s
\input{mort_table.tex}
% SURVIES PAR SEGMENT
Le calcul des taux de survies par segment montre une frange littorale �pargn�e par les turbines pour laquelle la survie est de 100\% 
\textbf{(Figure \ref{fig:nbarmortes})}. Il est possible que la valeur de mortalit� appliqu�e aux grands axes fluviaux soit surestim�e du fait des mortalit�s faibles
mesur�es sur les tubines �quipant ces grands axes. Cependant, elle traduit quand m�me la position "aval" des turbines sur ces axes.
%FIGURE DES MORTALITES
\begin{figure}[h]
\centering
\fbox{
\includegraphics[width=0.8\textwidth]{nbarmortes.png}
}
\caption{Calcul du nombre d'anguilles mortes par segment hydrographique du RHT}
\label{fig:nbarmortes}
\end{figure}
\\
Les r�gions montagneuses de l'est de la France pr�sentent globalement des survies plus faibles \textbf{(Figure \ref{fig:survies})}
En terme de mortalit� par segments, les taux de mortalit� les plus importants sont trouv�s dans les UGA Rh�ne-M�diterrann�e \Sexpr{100*tableUGA[2,"Rhon"]}\% 
et Rhin-Meuse \Sexpr{100*tableUGA[2,"Rhin"]}\% \textbf{(Tableau \ref{table:tableuga})}. 
%FIGURE DES SURVIES
\begin{figure}[h]
\centering
\fbox{
\includegraphics[width=0.8\textwidth]{mort.png}
}
\caption{Calcul des taux de mortalit� par segment � la sortie du bassin versant}
\label{fig:survies}
\end{figure}

%http://en.wikibooks.org/wiki/LaTeX/Floats,_Figures_and_Captions.. h=Place the float here



\end{document}
