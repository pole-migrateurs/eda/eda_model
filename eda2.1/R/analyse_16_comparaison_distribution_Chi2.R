# Comparaison des distributions ERS et CCM suivant les classes de distance mer et de d'altitude
# 
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: Celine Jouanin
###############################################################################


#Comparaison des distributions ERS et CCM suivant les classes de distance mer et de d'altitude.
#Test du Chi2

codage<-function(data,nclass,rod)
#Fonction permettant le d�coupage en 4 classes d'effectifs �gaux
{
#nb de classe d�sir�e
	n<-nclass
rd<-rod
#calcul des bornes
	bornes<-quantile(data, probs = seq(0,1,1/n),include.lowest=T,na.rm = TRUE,names = TRUE)
#description des bornes et effectifs
	Amax<-aggregate(data,list(Nom=cut(data,bornes,include.lowest=T,label=F,right=T)),max)
	Amin<-aggregate(data,list(Nom=cut(data,bornes,include.lowest=T,label=F,right=T)),min)
	Afreq<-as.matrix(summary(as.factor(cut(na.omit(data),bornes, include.lowest=T,label=F,right=T))))
	limites<-as.data.frame(cbind(Amin[,1],Amin[,2],Amax[,2],Afreq))
	names(limites)<-c("Classe","Mini","Maxi","Effectif")
#calcul du nombre de valeurs manquantes
	manques<-length(data)-length(na.omit(data))
#impression des bornes
	cat(paste("D�coupage de la variable ",deparse(substitute(data))," - Nb de valeurs
							manquantes : ", manques, "\n"))
	print(limites)
#d�coupage de la variable
	varfac<-cut(data,bornes,include.lowest=T,label=F)
#transformation en facteur
	varfac<-as.factor(varfac)
	varfac<-paste("]",paste(round(limites[varfac,2],rd),round(limites[varfac,3],rd),sep="-"),"]",sep="")
}


rht$cl_dsea<-codage(rht$distance_sea,6,1)
rht$cl_dsea <- ordered(rht$cl_dsea, levels=c("]0-70.3]","]70.3-199.8]","]199.8-334.9]","]334.9-470.7]","]470.7-645.8]","]645.8-1138.1]"))
rht$cl_dsource<-codage(rht$distance_source,6,1)
rht$cl_dsource <- ordered(rht$cl_dsource, levels=c("]0-0.7]","]0.7-1.4]","]1.4-3]","]3-8.9]","]8.9-25.7]","]25.7-1045]"))
rht$cl_pagricultural<-codage(rht$p_agricultural,5,1)
rht$cl_punimpact<-codage(rht$p_unimpact,5,1)
rht$cl_elevmean<-codage(rht$elev_mean,8,1)
rht$cl_elevmean <- ordered(rht$cl_elevmean, levels=c("]-6.4-37.3]","]37.4-75]","]75.1-111.5]","]111.6-156.1]","]156.2-215.7]","]215.8-309]","]309.1-552.5]","]552.6-2982.9]"))

rht$cl_csnbdams<-codage(rht$cs_nbdams,6,1)
rht$cl_csnbdams <- ordered(rht$cl_csnbdams, levels=c("]0-7]","]8-21]","]22-35]","]36-52]","]53-76]","]77-266]"))

rht$cl_tjuilmoy<-codage(rht$tjuil_moy,6,1)
rht$cl_tjanmoy<-codage(rht$tjan_moy,6,1)
rht$cl_relativedistance<-codage(rht$relative_distance,6,3)
rht$cl_module<-codage(rht$module,4,4)
rht$cl_upcatcharea<-codage(rht$up_catchment_area,6,1)
rht$cl_riverarea<-codage(rht$riverarea,5,2)
rht$cl_riverarea<- ordered(rht$cl_riverarea,levels=c("]0-1470.92]","]1471.05-3601.35]","]3601.37-7468.67]","]7468.67-17678.74]","]17681.71-5356966.68]"))
rht$cl_pupnoimpact<-codage(rht$p_up_no_impact,5,2)
rht$cl_pupagricultural<-codage(rht$p_up_agricultural,5,2)

codage_cl_slope<-function(data){
	data$cl_slope<-rep(1,dim(data)[1])
	data$cl_slope[data$slope<=0.67]<-"[0-0.67]"
	data$cl_slope[data$slope>0.67]<-"]0.67-2.3]"
	data$cl_slope[data$slope>2.3]<-"]2.3-4.55]"
	data$cl_slope[data$slope>4.55]<-"]4.55-7.7]"
	data$cl_slope[data$slope>7.7]<-"]7.7-11.58]"
	data$cl_slope[data$slope>11.58]<-"]11.58-18.08]"
	data$cl_slope[data$slope>18.08]<-"]18.08-35.75]"
	data$cl_slope[data$slope>35.75]<-"]35.75-913]"
	data$cl_slope <- ordered(data$cl_slope, levels=c("[0-0.67]","]0.67-2.3]","]2.3-4.55]","]4.55-7.7]",
					"]7.7-11.58]","]11.58-18.08]","]18.08-35.75]","]35.75-913]"))
	return(data)
}
rht<-codage_cl_slope(rht)

codage_cl_purban<-function(data){
	data$cl_purban<-rep(1,dim(data)[1])
	data$cl_purban[data$p_urban<=0.02]<-"]0-0.02]"
	data$cl_purban[data$p_urban>0.02]<-"]0.02-0.05]"
	data$cl_purban[data$p_urban>0.05]<-"]0.05-0.13]"
	data$cl_purban[data$p_urban>0.13]<-"]0.13-1]"
	data$cl_purban[data$p_urban==0]<-"0"
	data$cl_purban <- ordered(data$cl_purban, levels=c("0","]0-0.02]","]0.02-0.05]","]0.05-0.13]","]0.13-1]"))
	return(data)
}
rht<-codage_cl_purban(rht)

codage_cl_pupurban<-function(data){
	data$cl_pupurban<-rep(1,dim(data)[1])
	data$cl_pupurban[data$p_up_urban<=0.01]<-"]0-0.01]"
	data$cl_pupurban[data$p_up_urban>0.01]<-"]0.01-0.03]"
	data$cl_pupurban[data$p_up_urban>0.03]<-"]0.03-0.05]"
	data$cl_pupurban[data$p_up_urban>0.05]<-"]0.05-1]"
	data$cl_pupurban[data$p_up_urban==0]<-"0"
	data$cl_pupurban <- ordered(data$cl_pupurban, levels=c("0","]0-0.01]","]0.01-0.03]","]0.03-0.05]","]0.05-1]"))
	return(data)
}
rht<-codage_cl_pupurban(rht)

codage_cl_minqmx<-function(data){
	data$cl_minqmx<-rep(1,dim(data)[1])
	data$cl_minqmx[data$minqmx<0.01]<-"1"
	data$cl_minqmx[data$minqmx<=0.01]<-"2"
	data$cl_minqmx[data$minqmx>0.07]<-"4"
	data$cl_minqmx <- ordered(data$cl_minqmx, levels=c("1","2","4"))
	return(data)
}
rht<-codage_cl_minqmx(rht)
summary(rht$cl_minqmx)

#129,142:156"cl_purban","cl_pupurban",
#cl_upcatcharea cex.names=0.65
#Suivant la surface en eau
for(i in c("cl_dsea","cl_dsource","cl_punimpact","cl_pupnoimpact",
		"cl_pagricultural","cl_pupagricultural","cl_elevmean","cl_tjuilmoy","cl_tjanmoy","cl_csnbdams","cl_relativedistance",
		"cl_slope","cl_module","cl_minqmx","cl_riverarea","cl_upcatcharea")){
	x11(); 
	nf<-layout(matrix(c(1,1,2,2), 2, 2, byrow=TRUE))
	all<-c(tapply(rht$riverareakm,rht[,i],sum)/sum(rht$riverareakm)*100,
			(tapply(rht$riverareakm,list(rht$ersdata,rht[,i]),sum)[2,]/sum(rht$riverareakm[rht$ersdata==2])*100))
	all<-all[!is.na(all)]
	par(mar=c(4.6,2.5,3.1,0.5))
	barplot(tapply(rht$riverareakm,rht[,i],sum)/sum(rht$riverareakm)*100,ylim=c(0,max(all)),las=2,col="grey",main=i,cex.names=1)
	legend("topleft",c("rht","ers"),col=c("grey","red"),bty="n",box.lty=1,fill=c("grey","red"))
	par(mar=c(6.2,2.5,1.5,0.5))
	ERS1<-tapply(rht$riverareakm,list(rht$ersdata,rht[,i]),sum)[2,]/sum(rht$riverareakm[rht$ersdata==2])*100
	barplot(ERS1,ylim=c(0,max(all)),las=2,col="red",cex.names=1)
}

x11(); 
nf<-layout(matrix(c(1,1,2,2), 2, 2, byrow=TRUE))
boxplot(rht$module,ers$module,main="module",names=c("RHT","ERS"),las=1)
boxplot(rht$minqmx,ers$minqmx,main="minqmx",names=c("RHT","ERS"),las=1)

#"cl_pupnoimpact","cl_purban","cl_pupurban","cl_slope",

#Test de Chi�
for(i in c("cl_dsea","cl_dsource","cl_punimpact",
		"cl_pagricultural","cl_pupagricultural","cl_elevmean","cl_tjuilmoy","cl_tjanmoy","cl_csnbdams","cl_relativedistance",
		"cl_module","cl_minqmx","cl_riverarea","cl_upcatcharea")){
RHT1<-tapply(rht$riverareakm,rht[,i],sum)/sum(rht$riverareakm)*100
ERS1<-tapply(rht$riverareakm,list(rht$ersdata,rht[,i]),sum)[2,]/sum(rht$riverareakm[rht$ersdata==2])*100
T1<-as.data.frame(list(RHT1,ERS1))
colnames(T1)<-c("RHT","ERS")
print(i)
print(chisq.test(T1,correct=T))
if(min(T1)<5){
	print(fisher.test(T1))
	}
T1m<-as.matrix(T1)
prop.test(T1m)
}

#Test Wilcoxon - Distribution
for(i in c("distance_sea","distance_source","p_unimpact","p_up_no_impact","p_urban","p_up_urban",
		"p_agricultural","p_up_agricultural","elev_mean","tjuil_moy","cs_nbdams","relative_distance",
		"slope","module","minqmx","riverarea","up_catchment_area")){
	print(i)
	print(wilcox.test(rht[,i],ers[,i],paired=F))
	x11();boxplot(rht[,i],ers[,i])
}


x11(); 
nf<-layout(matrix(c(1,1,2,2), 2, 2, byrow=FALSE))
par(mar=c(1,2.5,2,0.5))
boxplot(ers$module,las=2,main="module")
rug(ers$module,side=2)
boxplot(ers$minqmx,las=2,main="minqmx")
rug(ers$minqmx,side=2)

#Test Kolmogorv-Smirnov
for(i in c("distance_sea","distance_source","p_unimpact","p_up_no_impact","p_urban","p_up_urban",
		"p_agricultural","p_up_agricultural","elev_mean","tjuil_moy","tjan_moy","cs_nbdams","relative_distance",
		"slope","module","minqmx","riverarea","up_catchment_area")){
	print(i)
	print(ks.test(rht[,i],rht[,i][rht$ersdata==2],paired=F))
	x11();boxplot(rht[,i],rht[,i][rht$ersdata==2])
}

for(i in c("cl_dsea","cl_dsource","cl_punimpact","cl_pupnoimpact","cl_purban","cl_pupurban",
		"cl_pagricultural","cl_pupagricultural","cl_elevmean","cl_tjuilmoy","cl_tjanmoy","cl_csnbdams","cl_relativedistance",
		"cl_slope","cl_module","cl_minqmx","cl_riverarea","cl_upcatcharea")){
	print(i)
	print(ks.test(as.numeric(rht[,i]),as.numeric(rht[,i][rht$ersdata==2]),paired=F))
	x11();boxplot(rht[,i],rht[,i][rht$ersdata==2])
}

for (i in c("cl_dsea","cl_dsource","cl_punimpact","cl_pupnoimpact","cl_purban","cl_pupurban",
		"cl_pagricultural","cl_pupagricultural","cl_elevmean","cl_tjuilmoy","cl_tjanmoy","cl_csnbdams","cl_relativedistance",
		"cl_slope","cl_module","cl_minqmx","cl_riverarea","cl_upcatcharea")){
	p_clrht<-as.vector(table(rht[,i])/sum(table(rht[,i])))
	p_clers<-as.vector(table(rht[,i][rht$ersdata==2])/sum(table(rht[,i][rht$ersdata==2])))
	print(i)
	print(ks.test(p_clrht,p_clers))
}
