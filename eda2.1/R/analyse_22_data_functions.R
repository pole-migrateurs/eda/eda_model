# Fonctions communes pour rht, ers_full et ers
# Distance : analyse_distance_km
# Changement des noms : chnamesrht
# Calcul des proportions CLC :analyse_prop_clc_surfaceCLC
# Cr�ation des regroupement d'UGA : analyse_2_uga et analyse_2_fUGA
# Classe de barrages : analyse_2_cl_dams et analyse_2_cl_dams2
#
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: Celine Jouanin
###############################################################################

#Changing the names of the variables in rht
chnamesrht<-function(data){chnames(data,
		old_variable_name=c("dsource","dmer","cumnbbar","pente","altitude","up_area",
				"artificial_surfaces_11_13","artificial_vegetated_14","arable_land_21","permanent_crops_22","pastures_23","heterogeneous_agricultural_24","forest_31",
				"natural_32_33","wetlands_4","inland_waterbodies_51","marine_water_52",
				"up_artificial_surfaces_11_13","up_artificial_vegetated_14","up_arable_land_21","up_permanent_crops_22","up_pastures_23","up_heterogeneous_agricultural_24","up_forest_31",
				"up_natural_32_33","up_wetlands_4","up_inland_waterbodies_51","up_marine_water_52"),
		new_variable_name=c("distance_source","distance_sea","cumnbbar","slope","elev_mean","up_catchment_area",
				"art_11_13","art_14","arable_21","permcrop_22","pasture_23","hetagr_24","forest_31",
				"natural_32_33","wetlands_4","inwat_51","marwat_52",
				"up_art_11_13","up_art_14","up_arable_21","up_permcrop_22","up_pasture_23","up_hetagr_24","up_forest_31",
				"up_natural_32_33","up_wetlands_4","up_inwat_51","up_marwat_52"))
}

#Changing the distance variables into kilometers
analyse_distance_km<-function(data){
	#Longueur length
	data$length_km<-data$length/1000
	#Surface BV
	data$catchment_area_km<-data$catchment_area/10^6
#Distance from sea calculated ussing "dmer" (distance from sea at the upstream node)
	#dmer int�gre d�j� : plus the length/2 for each segments

	#Distance sea en km
	data$distance_sea<-data$distance_sea/1000
	#Distance source en km
	#data$distance_source[data$distance_sea==0&data$distance_source==0]<-data$d_source[data$distance_sea==0&data$distance_source==0]
	data$distance_source<-data$distance_source/1000
	#Distance relative en km
	data$relative_distance<-data$distance_sea/(data$distance_sea+data$distance_source)
    #Densit� de drainage
	#data$drainage_density<-/
	#Calcul du nb de barrages cumul�s par distance � la mer.
	#data$cnbdams_dsea<-data$cs_nbdams/data$distance_sea
	#Calcul des pentes moyennes  (pour les altitudes positives)
	data$work[data$elev_mean>=0&!is.na(data$elev_mean)]=sqrt(data$elev_mean[data$elev_mean>=0&!is.na(data$elev_mean)]*data$distance_sea[data$elev_mean>=0&!is.na(data$elev_mean)])+data$distance_sea[data$elev_mean>=0&!is.na(data$elev_mean)]
	#Remove ndbams=NA
	#data$nbdams[is.na(data$nbdams)]<-0
	return(data)
}

#Calcul des proportions pour la CLC avec catchment_area
analyse_prop_clc<-function(data){
	data$p_art_11_13<-data$art_11_13/data$catchment_area
	data$p_art_14<-data$art_14/data$catchment_area
	data$p_arable_21<-data$arable_21/data$catchment_area
	data$p_permcrop_22<-data$permcrop_22/data$catchment_area
	data$p_pasture_23<-data$pasture_23/data$catchment_area
	data$p_hetagr_24<-data$hetagr_24/data$catchment_area
	data$p_forest_31<-data$forest_31/data$catchment_area
	data$p_natural_32_33<-data$natural_32_33/data$catchment_area
	data$p_wetlands_4<-data$wetlands_4/data$catchment_area
	data$p_inwat_51<-data$inwat_51/data$catchment_area
	data$p_marwat_52<-data$marwat_52/data$catchment_area

	data$p_urban<-data$p_art_11_13+data$p_art_14
	data$p_agricultural<-data$p_arable_21+data$p_permcrop_22+data$p_hetagr_24
	data$p_no_impact<-data$p_forest_31+data$p_natural_32_33+data$p_pasture_23
	#data$p_pasture<- not changed
	data$p_wet<-data$p_wetlands_4+data$p_inwat_51+data$p_marwat_52

	data$p_up_art_11_13<-data$up_art_11_13/data$up_catchment_area
	data$p_up_art_14<-data$up_art_14/data$up_catchment_area
	data$p_up_arable_21<-data$up_arable_21/data$up_catchment_area
	data$p_up_permcrop_22<-data$up_permcrop_22/data$up_catchment_area
	data$p_up_pasture_23<-data$up_pasture_23/data$up_catchment_area
	data$p_up_hetagr_24<-data$up_hetagr_24/data$up_catchment_area
	data$p_up_forest_31<-data$up_forest_31/data$up_catchment_area
	data$p_up_natural_32_33<-data$up_natural_32_33/data$up_catchment_area
	data$p_up_wetlands_4<-data$up_wetlands_4/data$up_catchment_area
	data$p_up_inwat_51<-data$up_inwat_51/data$up_catchment_area
	data$p_up_marwat_52<-data$up_marwat_52/data$up_catchment_area

	data$p_up_urban<-data$p_up_art_11_13+data$p_up_art_14
	data$p_up_agricultural<-data$p_up_arable_21+data$p_up_permcrop_22+data$p_up_hetagr_24
	data$p_up_no_impact<-data$p_up_forest_31+data$p_up_natural_32_33+data$p_up_pasture_23+data$p_up_wetlands_4+data$p_up_inwat_51+data$p_up_marwat_52
	data$p_up_wet<-data$p_up_wetlands_4+data$p_up_inwat_51+data$p_up_marwat_52
	# adding the area from the catchment to the upstream catchment 
	# where the strahler order is one (it was not calculated)

	#Nouvelle variable regroupant no_impact et wet et qui correspond � up_no_impact
	data$p_unimpact<-data$p_no_impact+data$p_wet
	
	#Calcul des surfaces de la CLC
	data$SurfCLC<-data$art_11_13+data$art_14+data$arable_21+data$permcrop_22+data$pasture_23+
	    data$hetagr_24+data$forest_31+data$natural_32_33+data$wetlands_4+data$inwat_51+data$marwat_52
	data$SurfCLCup<-data$up_art_11_13+data$up_art_14+data$up_arable_21+data$up_permcrop_22+data$up_pasture_23+
		data$up_hetagr_24+data$up_forest_31+data$up_natural_32_33+data$up_wetlands_4+data$up_inwat_51+data$up_marwat_52

	data$Surf_agricultural<-data$arable_21+data$permcrop_22+data$hetagr_24
	data$Surf_urban<-data$art_11_13+data$art_14
	data$Surf_no_impact<-data$pasture_23+data$forest_31+data$natural_32_33
	data_Surf_wet<-data$wetlands_4+data$inwat_51+data$marwat_52
	data$Surf_unimpact<-data$art_11_13+data$art_14+data$arable_21+data$permcrop_22+data$pasture_23+
		data$hetagr_24+data$forest_31+data$natural_32_33+data$wetlands_4+data$inwat_51+data$marwat_52
	data$pSum<-data$p_art_11_13+data$p_art_14+data$p_arable_21+data$p_permcrop_22+
		data$p_hetagr_24+data$p_forest_31+data$p_natural_32_33+data$p_pasture_23+
		data$p_wetlands_4+data$p_inwat_51+data$p_marwat_52
	#Calcul d'incertitude entre la valeur exacte (CLC) et la valeur approch�e (catchment) 
	data$IncCLCcatch<-(data$SurfCLC-data$catchment_area)/data$SurfCLC
	return(data)
}

#Calcul des proportions pour la CLC avec la somme des surfaces de la CLC
analyse_prop_clc_surfaceCLC<-function(data){
	#Calcul des surfaces de la CLC
	data$SurfCLC<-data$art_11_13+data$art_14+data$arable_21+data$permcrop_22+data$pasture_23+
		data$hetagr_24+data$forest_31+data$natural_32_33+data$wetlands_4+data$inwat_51+data$marwat_52
	data$SurfCLCup<-data$up_art_11_13+data$up_art_14+data$up_arable_21+data$up_permcrop_22+data$up_pasture_23+
		data$up_hetagr_24+data$up_forest_31+data$up_natural_32_33+data$up_wetlands_4+data$up_inwat_51+data$up_marwat_52

	data$Surf_agricultural<-data$arable_21+data$permcrop_22+data$hetagr_24
	data$Surf_urban<-data$art_11_13+data$art_14
	data$Surf_no_impact<-data$pasture_23+data$forest_31+data$natural_32_33
	data_Surf_wet<-data$wetlands_4+data$inwat_51+data$marwat_52
	data$Surf_unimpact<-data$art_11_13+data$art_14+data$arable_21+data$permcrop_22+data$pasture_23+
		data$hetagr_24+data$forest_31+data$natural_32_33+data$wetlands_4+data$inwat_51+data$marwat_52

	#Calcul des proportions pour la CLC avec la surface de la CLC
	data$p_art_11_13<-data$art_11_13/data$SurfCLC
	data$p_art_14<-data$art_14/data$SurfCLC
	data$p_arable_21<-data$arable_21/data$SurfCLC
	data$p_permcrop_22<-data$permcrop_22/data$SurfCLC
	data$p_pasture_23<-data$pasture_23/data$SurfCLC
	data$p_hetagr_24<-data$hetagr_24/data$SurfCLC
	data$p_forest_31<-data$forest_31/data$SurfCLC
	data$p_natural_32_33<-data$natural_32_33/data$SurfCLC
	data$p_wetlands_4<-data$wetlands_4/data$SurfCLC
	data$p_inwat_51<-data$inwat_51/data$SurfCLC
	data$p_marwat_52<-data$marwat_52/data$SurfCLC

	data$p_urban<-data$p_art_11_13+data$p_art_14
	data$p_agricultural<-data$p_arable_21+data$p_permcrop_22+data$p_hetagr_24
	data$p_no_impact<-data$p_forest_31+data$p_natural_32_33+data$p_pasture_23
	#data$p_pasture<- not changed
	data$p_wet<-data$p_wetlands_4+data$p_inwat_51+data$p_marwat_52

	data$p_up_art_11_13<-data$up_art_11_13/data$SurfCLCup
	data$p_up_art_14<-data$up_art_14/data$SurfCLCup
	data$p_up_arable_21<-data$up_arable_21/data$SurfCLCup
	data$p_up_permcrop_22<-data$up_permcrop_22/data$SurfCLCup
	data$p_up_pasture_23<-data$up_pasture_23/data$SurfCLCup
	data$p_up_hetagr_24<-data$up_hetagr_24/data$SurfCLCup
	data$p_up_forest_31<-data$up_forest_31/data$SurfCLCup
	data$p_up_natural_32_33<-data$up_natural_32_33/data$SurfCLCup
	data$p_up_wetlands_4<-data$up_wetlands_4/data$SurfCLCup
	data$p_up_inwat_51<-data$up_inwat_51/data$SurfCLCup
	data$p_up_marwat_52<-data$up_marwat_52/data$SurfCLCup

	data$p_up_urban<-data$p_up_art_11_13+data$p_up_art_14
	data$p_up_agricultural<-data$p_up_arable_21+data$p_up_permcrop_22+data$p_up_hetagr_24
	data$p_up_no_impact<-data$p_up_forest_31+data$p_up_natural_32_33+data$p_up_pasture_23+data$p_up_wetlands_4+data$p_up_inwat_51+data$p_up_marwat_52
	data$p_up_wet<-data$p_up_wetlands_4+data$p_up_inwat_51+data$p_up_marwat_52
	# adding the area from the catchment to the upstream catchment 
	# where the strahler order is one (it was not calculated)

	#Nouvelle variable regroupant no_impact et wet et qui correspond � up_no_impact
	data$p_unimpact<-data$p_no_impact+data$p_wet

	data$pSum<-data$p_art_11_13+data$p_art_14+data$p_arable_21+data$p_permcrop_22+
		data$p_hetagr_24+data$p_forest_31+data$p_natural_32_33+data$p_pasture_23+
		data$p_wetlands_4+data$p_inwat_51+data$p_marwat_52
	#Calcul d'incertitude entre la valeur exacte (CLC) et la valeur approch�e (catchment) 
	data$IncCLCcatch<-(data$SurfCLC-data$catchment_area)/data$SurfCLC
	return(data)
}


#Cr�ation des variables UGA2(1 si Adour 0 sinon), UGA2 (avec 1 si Artois Picardie 0 sinon)...
analyse_uga<-function(data){
	#Tron�ons transfrontaliers
	data$uga[is.na(data$uga)]<-"foreign"
	UGA1<-rep(0,dim(data)[1])
	UGA1[data$uga%in%"Adour"]<-1
	data$UGA1<-UGA1
	UGA2<-rep(0,dim(data)[1])
	UGA2[data$uga%in%"Artois-Picardie"]<-1
	data$UGA2<-UGA2
	UGA3<-rep(0,dim(data)[1])
	UGA3[data$uga%in%"Bretagne"]<-1
	data$UGA3<-UGA3
	UGA4<-rep(0,dim(data)[1])
	UGA4[data$uga%in%"Corse"]<-1
	data$UGA4<-UGA4
	UGA5<-rep(0,dim(data)[1])
	UGA5[data$uga%in%"Garonne"]<-1
	data$UGA5<-UGA5
	UGA6<-rep(0,dim(data)[1])
	UGA6[data$uga%in%"Loire"]<-1
	data$UGA6<-UGA6
	UGA7<-rep(0,dim(data)[1])
	UGA7[data$uga%in%"Meuse"]<-1
	data$UGA7<-UGA7
	UGA8<-rep(0,dim(data)[1])
	UGA8[data$uga%in%"Rhin"]<-1
	data$UGA8<-UGA8
	UGA9<-rep(0,dim(data)[1])
	UGA9[data$uga%in%"Rhone-Mediterranee"]<-1
	data$UGA9<-UGA9
	UGA10<-rep(0,dim(data)[1])
	UGA10[data$uga%in%"Seine-Normandie"]<-1
	data$UGA10<-UGA10
#Regroupement de l'uga Rhin et Meuse
	UGA11<-rep(0,dim(data)[1])
	UGA11[data$uga%in%c("Rhin","Meuse")]<-1
	data$UGA11<-UGA11
#Regroupement de l'uga Artois-Picardie, Rhin et Meuse
	UGA12<-rep(0,dim(data)[1])
	UGA12[data$uga%in%c("Rhin","Meuse","Artois-Picardie")]<-1
	data$UGA12<-UGA12
	return(data)
}


#Cr�ation de la variable fUGA avec les UGA Rhin et Meuse regroup�s
analyse_regroupement_uga<-function(data){
	data$fUGA<-data$uga
	data$fUGA[data$uga%in%c("Rhin","Meuse")]<-"Rhin-Meuse"
	return(data)
}


#Classe de barrages (discr�tisation apr�s visualisation de l'histogramme pour la France)
analyse_cl_dams<-function(data){
	data$cl_cs_nbdams<-rep(0,dim(data)[1])
	data$cl_cs_nbdams[data$cs_nbdams<=15]<-2
	data$cl_cs_nbdams[data$cs_nbdams>15]<-3
	data$cl_cs_nbdams[data$cs_nbdams>25]<-4
	data$cl_cs_nbdams[data$cs_nbdams>35]<-5
	data$cl_cs_nbdams[data$cs_nbdams>45]<-6
	data$cl_cs_nbdams[data$cs_nbdams>55]<-7
	data$cl_cs_nbdams[data$cs_nbdams>65]<-8
	data$cl_cs_nbdams[data$cs_nbdams==0]<-1
	return(data)
}


#Classe de nombre de barrages cumul�s (Laurent)
analyse_cl_dams2<-function(data){
	data$cl_cs_nbdams2<-rep(0,dim(data)[1])
	data$cl_cs_nbdams2[data$cs_nbdams<=10]<-2
	data$cl_cs_nbdams2[data$cs_nbdams>10]<-3
	data$cl_cs_nbdams2[data$cs_nbdams>20]<-4
	data$cl_cs_nbdams2[data$cs_nbdams>30]<-5
	data$cl_cs_nbdams2[data$cs_nbdams>40]<-6
	data$cl_cs_nbdams2[data$cs_nbdams>50]<-7
	data$cl_cs_nbdams2[data$cs_nbdams==0]<-1
	return(data)
}


#D�termination de l'exutoire, facade maritime
exutoire<-function(data){
data$id_bv<-strtrim(data$chemin,6)
data$id_bvverif<-data$id_bv%in%data$id_drain[data$noeudmer==1]
data$id_bv[data$id_bvverif=="FALSE"]<-strtrim(data$chemin[data$id_bvverif=="FALSE"],5)
data$id_bvverif2<-data$id_bv%in%data$id_drain[data$noeudmer==1]
data$id_bv[data$id_bvverif2=="FALSE"]<-strtrim(data$chemin[data$id_bvverif2=="FALSE"],4)
data$id_bvverif3<-data$id_bv%in%data$id_drain[data$noeudmer==1]
#Probl�me 676 valeur dont on connait pas le BV car chemin="rien
#pour ces tron�ons ont affectera la facade correspondante � l'uga

ZONEGEO<-cbind(data$id_drain[data$noeudmer==1],data$zonegeo[data$noeudmer==1],data$uga[data$noeudmer==1])
ZONEGEO<-data.frame(ZONEGEO)
names(ZONEGEO)<-c("id_drain","zonegeo","uga")
ZONEGEO$id_drain<-as.numeric(ZONEGEO$id_drain)

ZONEGEOUGA<-data.frame(cbind(c("Golfe de Gascogne","Est","Bretagne","Mediterranee","Golfe de Gascogne","Golfe de Gascogne","Est","Est","Mediterranee","Manche"),
				c("Adour","Artois-Picardie","Bretagne","Corse","Garonne","Loire","Meuse","Rhin","Rhone-Mediterranee","Seine-Normandie")))
names(ZONEGEOUGA)<-c("zonegeo","uga")

data$facade<-rep(0,dim(data)[1])
data$facade<-ZONEGEO[match(data$id_bv,ZONEGEO$id_drain),"zonegeo"]
data$facade[data$id_bv=="rien"]<-ZONEGEOUGA[match(data$uga[data$id_bv=="rien"],ZONEGEOUGA$uga),"zonegeo"]
return(data)
}