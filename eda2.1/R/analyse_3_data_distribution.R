# Analyse de la distribution des diff�rentes variables dans le jeu de donn�es
#
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: Celine Jouanin
###############################################################################


ugaList<-levels(as.factor(ers$uga))


jpeg(filename=paste(datawd,"/dataEDArht/graphiques/NbElectrofishing_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
plot(table(ers$annee),xlab="Year",ylab="Nb of electrofishing sampling")
dev.off()

#if(secteur!="Crepe"){
##Nb operation by year and month
#library(ggplot2)
#x11()
#jpeg(filename=paste(datawd,"/dataEDArht/graphiques/NbOp_year_month_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
#dat<-ers
#g<-ggplot(data=dat)
#g<-g+stat_binhex(aes(annee,month),low="green", high="red")+ xlab("year")+ylab("month")
#print(g)
#dev.off()
#rm(dat)
#}

##Nb operation by year by wso_id
#library(ggplot2)
#jpeg(filename=paste(datawd,"/dataEDArht/graphiques/NbOp_year_wsoid_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
#g<-ggplot(data=ers)
#g+stat_binhex(aes(annee,wso_id),low="green", high="red")+ xlab("year")+ ylab("wso_id")
#dev.off()

##Etude Elevation en fonction de la distance � la mer par UGA (Rapport EDA)
	for(i in 1:length(ugaList)){
		jpeg(filename=paste(datawd,"/dataEDArht/graphiques/Elevation_Dsea_",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
		plot(rht$elev_mean[rht$uga%in%ugaList[i]]~rht$distance_sea[rht$uga%in%ugaList[i]],
				main=ugaList[i],xlab="Distance sea (km)",ylab="Elevation (m)",col=1,pch=21,bg=1)
#identify(rht$distance_sea[rht$uga%in%ugaList[i]],rht$elev_mean[rht$uga%in%ugaList[i]],rht$wso1_id[rht$uga%in%ugaList[i]],n=10,cex=0.65)
		points(rht$elev_mean[rht$uga%in%ugaList[i]&rht$ersdata==2]~rht$distance_sea[rht$uga%in%ugaList[i]&rht$ersdata==2],
				col="red",pch=21,bg="red") 
		#legend("topleft","ers",col="red",bty="n",pch=20,text.col="red")
		dev.off()
	
	}

##Etude Elevation en fonction de la distance � la source par UGA (Rapport EDA)
		for(i in 1:length(ugaList)){
			jpeg(filename=paste(datawd,"/dataEDArht/graphiques/Elevation_Dsource_",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
			plot(rht$elev_mean[rht$uga%in%ugaList[i]]~rht$distance_source[rht$uga%in%ugaList[i]],
				main=ugaList[i],xlab="Distance source (km)",ylab="Elevation (m)",col=1,pch=21,bg=1)
#identify(rht$distance_source[rht$uga%in%ugaList[i]],rht$elev_mean[rht$uga%in%ugaList[i]],rht$wso1_id[rht$uga%in%ugaList[i]],n=10,cex=0.65)
			points(rht$elev_mean[rht$uga%in%ugaList[i]&rht$ersdata==2]~rht$distance_source[rht$uga%in%ugaList[i]&rht$ersdata==2],
				col="red",pch=21,bg="red")
			#legend("topright","ers",col="red",bty="n",pch=20,text.col="red")
			dev.off()
		
		}

#Etude cs_nbdams en fonction de la distance � la mer par UGA (Rapport EDA)
ugaList<-levels(as.factor(rht$uga))
	for(i in 1:length(ugaList)){
		jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_Dsea_",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
		plot(rht$distance_sea[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],
			main=ugaList[i],xlab="Distance sea (km)",ylab="Cumulative number of dams",
			ylim=range(rht$cs_nbdams[rht$uga%in%ugaList[i]]),xlim=range(rht$distance_sea[rht$uga%in%ugaList[i]]),pch=21,bg=1)
#identify(rht$distance_sea[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],rht$wso1_id[rht$uga%in%ugaList[i]],n=10,cex=0.65)
		points(rht$distance_sea[rht$uga%in%ugaList[i]&rht$ersdata==2],rht$cs_nbdams[rht$uga%in%ugaList[i]&rht$ersdata==2],col="red",pch=21,bg="red")
		#legend("topleft","ers",col="red",bty="n",pch=20,text.col="red")
		dev.off()
	}


#Etude cs_nbdams en fonction de la distance � la source par UGA (Rapport EDA)
ugaList<-levels(as.factor(rht$uga))
for(i in 1:length(ugaList)){
	jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_Dsource_",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
	plot(rht$distance_source[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],
			main=ugaList[i],xlab="Distance source (km)",ylab="Cumulative number of dams",
			ylim=range(rht$cs_nbdams[rht$uga%in%ugaList[i]]),xlim=range(rht$distance_source[rht$uga%in%ugaList[i]]),pch=21,bg=1)
#identify(rht$distance_sea[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],rht$wso1_id[rht$uga%in%ugaList[i]],n=10,cex=0.65)
	points(rht$distance_source[rht$uga%in%ugaList[i]&rht$ersdata==2],rht$cs_nbdams[rht$uga%in%ugaList[i]&rht$ersdata==2],col="red",pch=21,bg="red")
	#legend("topright","ers",col="red",bty="n",pch=20,text.col="red")
	dev.off()
	}



#Etude cs_nbdams en fonction de l'elevation par UGA (Rapport EDA)
ugaList<-levels(as.factor(rht$uga))
	for(i in 1:length(ugaList)){
		jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_elevation_",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
		plot(rht$elev_mean[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],
			main=ugaList[i],xlab="Elevation (m)",ylab="Cumulative number of dams",
			ylim=range(rht$cs_nbdams[rht$uga%in%ugaList[i]]),xlim=range(rht$elev_mean[rht$uga%in%ugaList[i]]),pch=21,bg=1)
#identify(rht$distance_sea[rht$uga%in%ugaList[i]],rht$cs_nbdams[rht$uga%in%ugaList[i]],rht$wso1_id[rht$uga%in%ugaList[i]],n=10,cex=0.65)
		points(rht$elev_mean[rht$uga%in%ugaList[i]&rht$ersdata==2],rht$cs_nbdams[rht$uga%in%ugaList[i]&rht$ersdata==2],col="red",pch=21,bg="red")
		#legend("topleft","ers",col="red",bty="n",pch=20,text.col="red")
		dev.off()
	}


#Etude cs_nbdams en fonction de la distance � la mer par UGA suivant la pr�sence/absence (Rapport EDA)
ugaList<-levels(as.factor(rht$uga))
library(ggplot2)
for(i in 1:length(ugaList)){
	jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_Dsea_presence_absense",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
	dat<-rht[rht$uga%in%ugaList[i],]
	dat$electrofished[dat$ersdata==1]=FALSE
	dat$electrofished[dat$ersdata==2]=TRUE
	dat$ersdata=as.factor(dat$ersdata)
	dat$meand[is.na(dat$meand)]<-0.01
	g<-ggplot(dat,aes(x=distance_sea,y=cs_nbdams))
	g<-g+ opts(title=ugaList[i])+xlab("Distance sea (km)")+ylab("Cumulative number of dams")+
			geom_point(aes(col=electrofished,size=meand))+scale_colour_manual(values=c("FALSE"="red","TRUE"="black"))
	print(g)
	dev.off()
	rm(dat)
}


#Etude cs_nbdams en fonction de la distance � la source par UGA suivant la pr�sence/absence (Rapport EDA)
for(i in 1:length(ugaList)){
	jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_Dsource_presence_absense",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
	dat<-rht[rht$uga%in%ugaList[i],]
	dat$electrofished[dat$ersdata==1]=FALSE
	dat$electrofished[dat$ersdata==2]=TRUE
	dat$ersdata=as.factor(dat$ersdata)
	dat$meand[is.na(dat$meand)]<-0.01
	g<-ggplot(dat,aes(x=distance_source,y=cs_nbdams))
	g<-g+ opts(title=ugaList[i])+xlab("Distance source (km)")+ylab("Cumulative number of dams")+
			geom_point(aes(col=electrofished,size=meand))+scale_colour_manual(values=c("FALSE"="red","TRUE"="black"))
	print(g)
	dev.off()
	rm(dat)
}




#Etude cs_nbdams en fonction de l'altitude par UGA suivant la pr�sence/absence (Rapport EDA)
for(i in 1:length(ugaList)){
	jpeg(filename=paste(datawd,"/dataEDArht/graphiques/csnbdams_elev_mean_presence_absense",ugaList[i],".jpeg",sep=""), width = 480, height = 480,quality=100)
	dat<-rht[rht$uga%in%ugaList[i],]
	dat$electrofished[dat$ersdata==1]=FALSE
	dat$electrofished[dat$ersdata==2]=TRUE
	dat$ersdata=as.factor(dat$ersdata)
	dat$meand[is.na(dat$meand)]<-0.01
	g<-ggplot(dat,aes(x=elev_mean,y=cs_nbdams))
	g<-g+ opts(title=ugaList[i])+xlab("Elevation (m)")+ylab("Cumulative number of dams")+
			geom_point(aes(col=electrofished,size=meand))+scale_colour_manual(values=c("FALSE"="red","TRUE"="black"))
	print(g)
	dev.off()
	rm(dat)
	}

