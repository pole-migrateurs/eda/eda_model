# Hierarchical cluster analysis with the function varclus in Hmisc package
# To test the collinearity in the candidate predictors
#
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: Celine Jouanin
###############################################################################

#source("EDACCM/init.r")

soussecteur<-c("Adour","Artois-Picardie","Bretagne","Corse","Garonne","Loire","Meuse","Rhin","Rhone-Mediterranee","Seine-Normandie")
secteur<-select.list(c("France",soussecteur),multiple=FALSE)
load(file=paste(datawd,"/dataEDArht/ers_",secteur,rapport,".RData",sep=""))

ers<-ers[!ers$st_id%in%c("05331142","04290066",'06380208','06840051'),]
ers<-ers[!ers$id_drain=="300036",]


library(Hmisc)

mycol<-c("elev_mean","tjuil_moy","tjan_moy","slope","module","minqmx",
		"relative_distance","distance_sea","distance_source",
		"cs_nbdams","c_height","c_heightp","p_up_agricultural","p_up_no_impact","p_up_urban",
		"p_urban","p_unimpact","p_agricultural","up_catchment_area")

mycol<-c("elev_mean","tjuil_moy","tjan_moy","distance_sea",
		"cs_nbdams","cs_height","cs_heightp","ldistance_sea_dam","largeur")

mycol<-mycol[mycol%in%colnames(ers)]   #Pb cs_score =0 pour france !!!
mycol<-mycol[colSums(ers[mycol])!=0]
cor.test(rht$tjuil_moy,rht$elev_mean,use="complete")
#EMU
v<-as.formula(paste("~",mycol,collapse="+"))

vc<-varclus(v,data=ers[,mycol],similarity="spearman")
		
if(secteur%in%"France"){
png(filename=paste(imgwd,"Hierarchical_clustering_",secteur,".png",sep=""), width = 480, height = 480)
par(mar=c(0,4.5,1,1));plot.varclus(vc)
abline(h=0.64,col="red")
dev.off()
}


if(secteur%in%soussecteur){
#ugaList<-levels(as.factor(ers$uga))
x11();
bmp(filename=paste(imgwd,"Hierarchical_clustering_",secteur,".jpeg",sep=""), width = 480, height = 480)
par(mar=c(0,4.5,3.5,1));plot.varclus(vc)
abline(h=0.75,col="red")
mtext(secteur,side=3,padj=-1.7,font=2)
dev.off()
}

#Corvif (library AED)
require(AED)
mycol<-c("elev_mean","slope","tjuil_moy","tjan_moy","module","up_catchment_area",
		"relative_distance","distance_sea","distance_source",
		"p_up_agricultural","p_up_no_impact","p_up_urban",
		"p_urban","p_unimpact","p_agricultural","cs_heightp","cs_nbdams")
mycol<-mycol[!mycol%in%c("p_up_urban","p_agricultural")]

dataz<-ers[,mycol]
corvif(ers[,mycol])
mycol<-mycol[!mycol%in%c("p_up_agricultural")]
corvif(ers[,mycol])
mycol<-mycol[!mycol%in%c("module")]
corvif(ers[,mycol])
mycol<-mycol[!mycol%in%c("tjan_moy")]
corvif(ers[,mycol])
mycol<-mycol[!mycol%in%c("up_catchment_area")]
corvif(ers[,mycol])
mycol<-mycol[!mycol%in%c("p_up_no_impact")]
corvif(ers[,mycol])

#Nuage de points deux � deux
#see in analyse_51_correlation

#UGA
ugaList<-levels(as.factor(ers$fUGA))  #Rhin et Meuse regroup�s
for(i in 1:length(ugaList)){
v<-as.formula(paste("~",mycol,collapse="+"))
vc<-varclus(v,data=ers[ers$fUGA%in%ugaList[i],mycol],similarity="spearman")
x11();par(mar=c(0,4.5,3.5,1));plot.varclus(vc)
mtext(paste(ugaList[i]),side=3,padj=-1.7,font=2)
}


#clus8 <- cutree(vc$hclust,8)
#table(clus8)
#
#ddist<-dist(ers[,c("distance_sea","annee","cs_nbdams")],method="euclidian")
#hddist<-hclust(ddist,method="ward")
#plot(hddist,hang=-1,labels=ers[,"uga"])
#rm(mycol)



