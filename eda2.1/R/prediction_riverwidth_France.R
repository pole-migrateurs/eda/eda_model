# River width for Western EMU based on a raster layer
# 
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

#River width
rht$lll<-exp(2.122-0.076*sqrt(rht$slope))*rht$module^0.475

#Log10(Wet width + 1) = 0.22734+ 0.20045 (log10 catchment area) + 0.25939 (log 10 Shreve index)
#ccm$riverwidth<-((10^0.22734)*((ccm$up_area*100)^0.20045)*(ccm$shree^0.25939))-1  #m
#ccm$riverwidth[ccm$shree==1]<-0.8  #m

#up_area km�  --> *100 pour mettre en ha  Wet width in m
#shape_leng --> m
ccm$riverwidth<-ccm$western_width


Ndata<-dim(ers)[1]
a_width<-rep(0,Ndata)
for (i in 1:Ndata){
	if(ers$wso1_id[i]%in%ccm$wso1_id)
		a_width[i]<-as.numeric(ccm[which(ccm$wso1_id%in%ers$wso1_id[i]),"western_width"])
}

ers$a_width<-a_width

#Modele
# rem cedric wso_id est un num�rique ici...
# lm1<-lm(log(western_width)~shree+elev_mean+up_area+wso_id, data=ccm[!is.na(ccm$western_width),])
# la transformation exponentielle fout la zone dans les pr�dictions, pas assez de donn�es....
lm1<-lm(western_width~shree+alt_gradie+up_area, data=ccm[!is.na(ccm$western_width),])
pred.lm1<-predict.lm(lm1,newdata=ccm[!is.na(ccm$western_width),],type="response")
cor(pred.lm1,ccm$western_width[!is.na(ccm$western_width)])
x11();plot(pred.lm1~ccm[!is.na(ccm$western_width),"western_width"],xlim=c(0,max(ccm[!is.na(ccm$western_width),"western_width"]))
	,ylim=c(0,max(pred.lm1)))
abline(a=1,b=1,col='red')

lm2<-lm(log(western_width)~shree+up_area+wso_id, data=ccm[!is.na(ccm$western_width),])
pred.lm2<-exp(predict.lm(lm2,newdata=ccm[!is.na(ccm$western_width),],type="response"))
cor(pred.lm2,ccm$western_width[!is.na(ccm$western_width)])
x11();plot(pred.lm2~ccm[!is.na(ccm$western_width),"western_width"],xlim=c(0,max(ccm[!is.na(ccm$western_width),"western_width"]))
		,ylim=c(0,max(pred.lm2)))
abline(a=1,b=1,col='red')

lm3<-lm(log(western_width)~shree+up_area, data=ccm[!is.na(ccm$western_width),])
pred.lm3<-exp(predict.lm(lm3,newdata=ccm[!is.na(ccm$western_width),],type="response"))
cor(pred.lm3,ccm$western_width[!is.na(ccm$western_width)])
x11();plot(pred.lm3~ccm[!is.na(ccm$western_width),"western_width"],xlim=c(0,max(ccm[!is.na(ccm$western_width),"western_width"]))
		,ylim=c(0,max(pred.lm3)))
abline(a=1,b=1,col='red')

stepAIC(lm(log(western_width)~shree+up_area+wso_id+elev_mean+slope_mean+temp_mean+alt_gradie+strahler+distance_sea+relative_distance+distance_source,data=ccm[!is.na(ccm$western_width),]),trace=TRUE,)
lm4<-lm(log(western_width)~shree+up_area+elev_mean+temp_mean+alt_gradie+strahler+distance_source,data=ccm[!is.na(ccm$western_width),])
pred.lm4<-exp(predict.lm(lm4,newdata=ccm[!is.na(ccm$western_width),],type="response"))
cor(pred.lm4,ccm$western_width[!is.na(ccm$western_width)])
x11();plot(pred.lm4~ccm[!is.na(ccm$western_width),"western_width"],xlim=c(0,max(ccm[!is.na(ccm$western_width),"western_width"]))
		,ylim=c(0,max(pred.lm4)))
abline(a=1,b=1,col='red')

step5<-stepAIC(lm(log(western_width)~shree+up_area+wso_id+rvr_id+scheid+elev_mean+slope_mean+temp_mean+area_km2+alt_gradie+strahler+distance_sea+relative_distance+distance_source,data=ccm[!is.na(ccm$western_width),]),trace=TRUE,)
lm5<-lm(log(western_width)~shree+up_area+wso_id+elev_mean+temp_mean+area_km2+alt_gradie+strahler+relative_distance,data=ccm[!is.na(ccm$western_width),])
#gamchecklm(lm5,mydata=ccm[!is.na(ccm$western_width),])
pred.lm5<-exp(predict.lm(lm5,newdata=ccm[!is.na(ccm$western_width),],type="response"))
cor(pred.lm5,ccm$western_width[!is.na(ccm$western_width)])
x11();plot(pred.lm5~ccm[!is.na(ccm$western_width),"western_width"],xlim=c(0,max(ccm[!is.na(ccm$western_width),"western_width"]))
		,ylim=c(0,max(pred.lm5)))
abline(a=1,b=1,col='red')

AIC(lm1,lm2,lm3,lm4,lm5)

#Prediction
pred.ers<-predict.lm(lm1,newdata=ers[is.na(ers$a_width),],type="response")
ers$a_width[is.na(ers$a_width)]<-pred.ers

pred.ccm<-predict.lm(lm1,newdata=ccm[is.na(ccm$western_width),],type="response")
ccm$riverwidth[is.na(ccm$riverwidth)]<-pred.ccm  #change into:
#ccm$riverwidth[is.na(ccm$western_width)]<-pred.ccm
# C�dric : �a c'est pas tr�s �l�gant...
ccm$riverwidth[ccm$riverwidth<0]<-0

ccm$riverarea<-ccm$riverwidth*ccm$shape_leng #m*m  -->m�
ccm$riverareakm<-ccm$riverarea/10^6 #m*m/10^6  -->km�
# this was the problem...
# ccm[ccm$riverareakm>10000,c("riverareakm","shree","alt_gradie","up_area")]

ccm$riverwidth<-NA

#...............
#A v�rifier
#ccm$riverwidth[!is.na(ccm$western_width)]<-ccm$western_width # les valeurs observ�es
#ccm$riverwidth[is.na(ccm$western_width)]<-pred.ccm  # les valeurs predites l� ou il n�y a pas de valeurs observ�es.
#..............

ccm$nb_eel_per_segment<-ccm$mdxmpa/100*ccm$riverarea
print("total number of yellow eel")
print(sum(ccm$nb_eel_per_segment))
196629/sum(ccm$nb_eel_per_segment)   # The 2001-2007 yellow commercial catch was approx 196629 individuals per year
print("total number of silver eel")
print(sum(ccm$nb_eel_per_segment)*0.05)
# The Surface area is fluvial (3342ha) and lake (46602ha) � total 49944ha.  
3342*1e4/1e6
print("total water surface (km�)")
print(sum(ccm$riverareakm)) # water surface area in km2
print("number of silver eel per m�")
#print(100*sum(ccm$nb_eel_per_segment)*0.05/sum(ccm$riverarea)) # 0.33 anguille argent�e pour 100 m�
print(100*sum(ccm$nb_eel_per_segment)*0.05/sum(ccm$riverarea))
print("number of yellow eel per m�")
#print(100*sum(ccm$nb_eel_per_segment)/sum(ccm$riverarea)))  # 6.6 anguille jaune / 100 m� densite moyenne predite
print(100*sum(ccm$nb_eel_per_segment)/sum(ccm$riverarea))
#burishoole
(burish<-apply(ccm[ccm$wso_id==88600,c("nb_eel_per_segment","riverarea")],2,sum))
burish[1]/(burish[2]*10^6) #0.285  � comparer � 0.017 anguilles en 2003 soit 16 fois trop fort. Le max est 0.19.

print("number of yellow eel per 100 m� at the sea ")
print(100*sum(ccm$nb_eel_per_segment[ccm$distance_sea==0])/sum(ccm$riverarea[ccm$distance_sea==0]))

