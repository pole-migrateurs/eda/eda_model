# Response curves for the gamma model for ANGLIAN river basin
# 
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

# pr�paration des variables du mod�le

# l'utilisation de geom_smooth fait planter le package gam et on ne peut plus faire de predict sans relancer R
# donc je ne l'utilise que ci dessous
p1<-funresponsecurvebox(var="annee",preprint=TRUE) # ,preprint=TRUE preprint to see the interim result
p2<-funresponsecurvebox(var="month",preprint=TRUE,rug=FALSE, methodsmooth="lm")
p3<-funresponsecurvebox(var="distance_sea")
p4<-funresponsecurvebox("elev_mean")
p5<-funresponsecurvebox("tjuil_moy")
p6<-funresponsecurvebox("tjan_moy")
p7<-funresponsecurvebox("cs_nbdams")
p8<-funresponsecurvebox(var="p_up_urban")
p9<-funresponsecurvebox(var="p_up_no_impact")
p10<-funresponsecurvebox(var="p_up_agricultural")

p1<-funresponsecurvebox(var="annee") 
p2<-funresponsecurvebox(var="month")
p3<-funresponsecurvebox(var="distance_sea")
p4<-funresponsecurvebox("elev_mean")
p5<-funresponsecurvebox("tjuil_moy")
p6<-funresponsecurvebox("tjan_moy")
p7<-funresponsecurvebox("cs_nbdams")
p8<-funresponsecurvebox(var="p_up_urban")
p9<-funresponsecurvebox(var="p_up_no_impact")
p10<-funresponsecurvebox(var="p_up_agricultural")
graphics.off()


# ne lancer qu'� la fin puis relancer R.... Autrement les gam ne marchent plus

x11();print(p1)
x11();print(p2)
x11();print(p3)
x11();print(p4)
x11();print(p5)
x11();print(p6)
x11();print(p7)
x11();print(p8)
x11();print(p9)
x11();print(p10)
# RESTART R

#> x11();print(p2)
#Erreur dans smooth.construct.cr.smooth.spec(object, data, knots) : 
#		x has insufficient unique values to support 10 knots: reduce k.