# 
# 
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: C�line Jouanin
###############################################################################

source("EDACCM/init.r")
# A LANCER POUR LES MODELES
secteur<-select.list(c("France","Adour","Artois-Picardie","Bretagne","Corse","Garonne","Loire","Meuse","Rhin","Rhone-Mediterranee","Seine-Normandie"),multiple=FALSE)

load(file=paste(datawd,"/dataEDArht/rht_",secteur,".RData",sep=""))
load(file=paste(datawd,"/dataEDArht/ers_",secteur,".RData",sep=""))

library(mgcv)
library(hier.part)
#detach("package:mgcv")
#library(gam)  

source("EDAcommun/prg/outils/f_modele2.R") 

effet=list(c("s(annee)","factor(annee)"),
		c("s(month)"),
		c("factor(uga)","factor(facade)"),
		c("s(tjuil_moy)"),
		c("s(minqmx)","s(module)","s(distance_source)","s(up_catchment_area)"),
		c("s(p_urban)","s(p_up_urban)"),
		c("s(p_up_agricultural)","s(p_up_no_impact)","s(p_unimpact)","s(p_agricultural)","s(elev_mean)","s(slope)"),
		c("s(cs_nbdams)"),
		c("s(relative_distance)","s(tjan_moy)","s(distance_sea)"))
modele<-f_modele2(effet,y="d")
length(modele)


gamMGCVaic=function(modele,sub=NULL,poids=NULL,fichier_sauvergarde="sauve.RData",...) {
#  require(gam)
	
	resultat=data.frame(modele=rep(NA,length(modele)),distribution=NA,lien=NA,aic=NA,GCV=NA,ddl.null=NA,ddl.residu=NA,dat=NA,lik=NA)#,kappa=NA)
	cat("nombre de mod�le : ",length(modele),"\n")
	
	for (i in 1:length(modele)) {
		tmps.ini=Sys.time()         #top chrono
		gam1=gam(as.formula(modele[[i]]),subset=sub, weights=poids,...)#fait tourner le glm
		#gam1=gam(as.formula(modele[[i]]),poids=NULL,data=ers[select,],family=Gamma(link=log))
		tmps.fin=Sys.time()         #top chrono

		# stocke les informations du mod�le dans resultat		
		resultat[i,]=c(as.character(gam1$formula)[3],gam1$family$family,gam1$family$link,gam1$aic,gam1$gcv.ubre,gam1$df.null,gam1$df.residual,difftime(tmps.fin,tmps.ini,units="sec"),as.integer(logLik(gam1)))#, gam1.kappa)
		cat("le modele ",i," est fini\t\t|\tddl : ",gam1$df.null-gam1$df.residual, "\t|\tAIC : ",as.integer(gam1$aic),"\t|\ttemps mis : ",difftime(tmps.fin,tmps.ini,units="sec")," s\n")
		
		#sauvegarde les r�sultats en cas de crash
		save(resultat,file=fichier_sauvergarde)
		rm(gam1)
	}
	
	class(resultat)=c("aic.glm","data.frame")		#transforme resultat en object de classe aic.glm
	
	return(resultat)
}

source("EDAcommun/prg/outils/effet.R") 


select<-ers$d>0  #s�lection des effectifs non nuls
result=gamaic(modele,poids=NULL,data=ers[select,],family=Gamma(link=log))
print.aic.glm(resultat=result,nb_model=10,delta.aic=20000,kappa=FALSE,
		nb_digit_aic=1)
print(summary(as.factor(ers$uga)))