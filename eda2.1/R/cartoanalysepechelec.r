# cartoanalysepechelec.R
#
# EDA2.1 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand
###############################################################################

# au pr�alable http://trac.eptb-vilaine.fr:8066/trac/wiki/CookBook%20pgsql2shp
# creation du shape pour ccm21.riversegment_france
# lancer dans dos :
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_France" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='France');"
#C:\"Program Files"\PostgreSQL\8.4\bin\pgsql2shp -f "C:\eda\exports_shape\riversegments_Bretagne" -p 5433 -u postgres -P postgres -g the_geom -r -k eda2.0 "select the_geom, wso1_id from ccm21.riversegments where wso_id in (select wso_id from europe.wso where area='Bretagne');"
source("EDACCM/init.r")
library(RColorBrewer)

#-----------------------------------#
# Chargement des couches SIG
#-----------------------------------#
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
	install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
library(rgdal)
source("EDACCM/fonctions_graphiques.r")
library(lattice)
 trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
						 cutoff.tails=0.1,
						 alpha=1)))
#-----------------------------------#
# Graphique des pr�dictions sur les segments
#-----------------------------------#
# chargement de la couche de fond
# C:\"Program Files"\PostgreSQL\9.0\bin\pgsql2shp -f "E:\workspace\EDAdata\dataEDAccm\shape\bdmap2009" -p 5432 -u postgres -P postgres -g the_geom -r -k eda2.1 bdmap2009.station_geography
shpwd<-"C:/Users/cedric.briand/Documents/workspace/EDAdata/dataEDAccm/shape/"		
dsn=paste(shpwd,"bdmap2009.shp",sep="")
st<-readOGR(dsn,"bdmap2009")
# C:\"Program Files"\PostgreSQL\9.0\bin\pgsql2shp -f "E:\workspace\EDAdata\dataEDAccm\shape\rht_loire" -p 5432 -u postgres -P postgres -g the_geom -r -k eda2.1 "select t.id_drain, the_geom from rht.rht_topology t join uga2010.id_drain_uga u on t.id_drain=u.id_drain where uga='Loire';"

dsn=paste(shpwd,"rht_loire.shp",sep="")
rb<-readOGR(dsn,"rht_loire")

st<-st[(coordinates(st)[,2]>1885515&coordinates(st)[,2]<3262916)&coordinates(st)[,1]<4430137,]
# below I'm only joining with the model as there are less data
st1<-st[st@data$st_codecsp%in%bd_map@databdmap$st_codecsp,]# redimensionnement de l'object spatial 
# pas facile � trouver celui l� m�me si au final il est intuitif il ne faut pas redimensionner st@data mais st...
# ajout des colonnes dans le spatial, je remplace toutes les colonnes car la requete ne va chercher que le wso1_id
#st1<-mod_SpatialDataFrame(SpPDF=st1,  # un SpatialPolygonsDataFrame
#		data= md198$data ,  #  un dataframe
#		by.spdf="st_id",  # nom de la colonne contenant l'identifiant
#		by.data="st_id",   # nom de la colone contenant l'indentifiant de data
#		coldata=colnames(md198$data)[colnames(md198$data)!=c("st_id")])       # vecteur character des colonnes � int�ger
# str(st1@data)
# Cette fonction trace un graphique a partir d'un spatialpointdf
# et renvoit des points sur un fond de carte en cr�ant des couleurs
# progressives � partir d'un d�coupage en classe d'une variable continue
# elle utise aussi une colonne "nb" pour donner une taille de points diff�rents
x11(14,10)
mypalette<-rev(rainbow(6)) 
# carte pour la bretagne r�sidus
st1
st1@data$a_conserve<-as.factor(st1@data$a_conserve)
x11()
spplot(st,c("a_conserve"))
spplot(rb)