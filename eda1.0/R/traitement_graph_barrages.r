#####################################
# Traitement graphique (premi�re partie � charger
#####################################
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
source("EDAcommun/prg/init/fonctions_graphiques.r")
library(lattice)
library(RColorBrewer)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1)))
if(!exists("bassin"))
  bassin=readShapePoly("EDAcommun/data/couche_shp/bassins_hydro_modif_region.shp")
#
if(!exists("celb"))
celb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_polyline.shp")
if(!exists("rlb"))
rlb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_region.shp")


##############################
### CHARGMENT DE DONNEES
##############################
#traitement du data.frame noeuds

load(file="EDALB/data/extraction_parametre_Marion/noeud_complet.RData") # total


total$coefftr=total$cumul_expertise0*1+total$cumul_expertise1*3+ total$cumul_expertise2*7+total$cumul_expertise3*20+total$cumul_expertise4*55+total$cumul_expertise5*150
total$coefftr4=total$cumul_expertise0*0+total$cumul_expertise1*0.1+ total$cumul_expertise2*0.4+total$cumul_expertise3*2+total$cumul_expertise4*10
total$coeff=total$cumul_expertise0+total$cumul_expertise1+ total$cumul_expertise2+total$cumul_expertise3+total$cumul_expertise4
total$zone1=NA
total[is.na(total)]<-0

### traitement des cegenelin, il en manque malheureusement plein !
## d'abord ceux qui ne manquent pas
#total$cgenelin="4VIL"


total$zone=substr(as.character(total$cgenelin),1,2)
 total$zone1=substr(total$zone,0,1)
 total$zone1[total$zone==""]="Z"
 total$zone[total$zone=="J-"|total$zone=="J7"|total$zone=="J8"|total$zone=="J9"]="4VIL"
total$zone[total$zone=="J4"|total$zone=="J5"|total$zone=="J6"]="3BRE_sud"
total$zone[total$zone=="J2"|total$zone=="J3"]="1BRE_ouest"
total$zone[total$zone=="J0"|total$zone=="J1"]="0BRE_Nord"
total$zone1[total$zone1=="J"]=total$zone[total$zone1=="J"]
total$zone1[total$zone=="--"]="Z"
total$zone1[total$zone1=="K"|total$zone1=="L"|total$zone1=="M"]="5LOIRE"

total$zone1[total$zone1=="N"]="6SEVRE"  
total$zone1[total$zone1=="R"]="Z"
total$zone1[total$zone1==""]="Z"
total$zone1[total$zone1=="S"]="Z"
total$zone1[total$zone1=="Z"]=NA
#sum(total$bio09==-9999)# 933
total$bio09[total$bio09==-9999]=median(total$bio09[total$bio09!=-9999])
total$bio10[total$bio10==-9999]=median(total$bio09[total$bio10!=-9999])
total$bio11[total$bio11==-9999]=median(total$bio09[total$bio11!=-9999])
summary(as.factor(total$zone1)) #789

for (i in 1:length(unique(jeu_final$zone1))){
noeuds_mer_parzone=unique(jeu_final$noeud_mer[jeu_final$zone1==unique(jeu_final$zone1)[i]])
total[total$noeud_mer%in%noeuds_mer_parzone&is.na(total$zone1),"zone1"]=unique(as.character(unique(jeu_final$zone1)[i]))
}
summary(as.factor(total$zone1) )  #605
total$zone1[is.na(total$zone1)]="5LOIRE"
#!!!!!! ATTENTION LES INCONNUS =605 JE LES MET EN LOIRE  (c'est l� qu'ils ont le plus de proba d'�tre)
summary(as.factor(total$zone1) )  #605

total$zone1=as.factor(total$zone1)
total$presence_civelle=(total$civelle_pro+ total$civelle_amateur)!=0
total$effort=100
total$qualite_ROM_2002[total$qualite_ROM_2002==0]=3
require(gam)
total$annee=as.factor(total$annee)
total$methode_prospection=as.factor(total$methode_prospection)
total$presence_civelle=as.factor(total$presence_civelle)
total$methode_prospection=as.factor(total$methode_prospection)
total$qualite_ROM_2002=as.factor(total$qualite_ROM_2002)
levels(total$annee) <-modele_densite$xlevels$"as.factor(annee)" 
levels(total$zone1) <-modele_densite$xlevels$"zone1" 
levels(total$qualite_ROM_2002) <-modele_densite$xlevels$"as.factor(qualite_ROM_2002)"  
levels(total$methode_prospection) <-modele_densite$xlevels$"methode_prospection" 


total$pred_densite=predict.gam(modele_densite, newdata = total, type =  "response")
total$pred_presence= predict.gam(modele_densite_presence, newdata = total, type =  "response")
total$pred=total$pred_densite*total$pred_presence
#write.table (total,file="EDALB/pr�diction.txt", col.names=TRUE,sep=";")

total$pred_presence0.5=total$pred_presence
total$pred_presence0.5[total$pred_presence>0.5]=1
total$pred_presence0.5[total$pred_presence<=0.5]=0

#total1=subset(total,total$zone=="L6")
bretagne=total$zone%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
total1=subset(total,bretagne)
spdfpartiel=CrSpPtsDF("X","Y",total1)
(xlim1=as.numeric(bbox(spdfpartiel)[1,]))
(ylim1=as.numeric(bbox(spdfpartiel)[2,]))   
Bretagne=total$zone1%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
Loire=total$zone1%in%c("5LOIRE","6SEVRE")&!is.na(total$zone1)
#Correction de deux valeurs ab�rrantes
total[Bretagne&total$"Y"<2250000,"zone1"]= "6SEVRE"
#Correction de quatre valeurs ab�rrantes
total[Loire&total$"X"<200000,"zone1"]="1BRE_ouest" 
Bretagne=total$zone1%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
Loire=total$zone1%in%c("5LOIRE","6SEVRE")
total1=subset(total,Bretagne)
spdfpartiel=CrSpPtsDF("X","Y",total1)
(xlimBretagne=as.numeric(bbox(spdfpartiel)[1,]))
(ylimBretagne=as.numeric(bbox(spdfpartiel)[2,])) 
rm(total1)
total2=subset(total,Loire)
spdfpartiel=CrSpPtsDF("X","Y",total2)
(xlimLoire=as.numeric(bbox(spdfpartiel)[1,]))
(ylimLoire=as.numeric(bbox(spdfpartiel)[2,]))  
 rm(total2)
x11(2200,1600)
par("mar"=c(0.1,1.1,3.1,0.1))
 #layout(matrix(1:4,2,2))
 mypalette<-c(rev(brewer.pal(9,"YlGnBu")),brewer.pal(9,"YlOrRd")) 
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="cumul_expertise_pred",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                 fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(-1,1,5,10,20,30,50,100,200,353),#avec cut on supprime les quantiles
                cex=0.4,
                title="Cumul des expertises",
                xlim =  xlimBretagne,
                ylim = ylimBretagne,
                couleur_fond=c("grey95","grey95","paleturquoise3"),
                point=16,
                textbas=FALSE ,
                surface="celb",
                couleursurface="blue",
                legendlab="Expertises cumul�es",
                legendval=c(0,1,5,10,20,30,50,100,200,350) )
 
bar=read.table(file=paste("EDALB/data/extraction_parametre_Marion/bar4pred.txt",sep=""),header=TRUE)
bar$ppm=bar$Auteur=="Pierre Steinbach"|bar$Auteur=="PM Chapon" 
couleurs= c("blue","green","yellow","orange","red")
spdfbar=CrSpPtsDF(xlabel="Pt_LII_X",ylabel="Pt_LII_Y",data=bar)
mypalette= colorRampPalette(couleurs)
niveaux =cut(spdfbar[["expertise"]],5)
couleur=mypalette(length(levels(as.factor(niveaux))))[match(niveaux,levels(as.factor(niveaux)))]

points(spdfbar,
            spdfbar[["cumul_expertise"]],
            col=couleur,
            pch=15,
            cex=0.8
            )
couleur[!is.na(couleur)]<-"black"
points(spdfbar,
            spdfbar[["cumul_expertise"]],
            col=couleur,
            pch=22,
            cex=0.8
            )
couleur[is.na(couleur)]<-"grey10"
couleur[couleur=="black"]<-NA
points(spdfbar,
            spdfbar[["cumul_expertise"]],
            col=couleur,
            pch=20,
            cex=0.8
            )
# locator(1)            
legend(    x= 96180,
            y=2327377,
            title="expertises barrage",
            legend= c("inconnu","franchissable (1)","retard saisonnier(2)","difficile(3)","tr�s difficile(4)","infranchissable(5)"),
            pch=c(20,15,15,15,15,15,15),
            xjust=0,
            cex=1,
            col=c("black",couleurs),
            horiz=FALSE,
            bty="n" )

########################################
# LOIRE
########################################
x11(2200,1600)
par("mar"=c(0.1,1.1,3.1,0.1))
 #layout(matrix(1:4,2,2))
 mypalette<-c(rev(brewer.pal(9,"YlGnBu")),brewer.pal(9,"YlOrRd")) 
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="cumul_expertise_pred",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                 fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(-1,1,5,10,20,30,50,100,200,353),#avec cut on supprime les quantiles
                cex=0.4,
                title="Cumul des expertises",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3"),
                point=16,
                textbas=FALSE ,
                surface="celb",
                couleursurface="blue",
                legendlab="Expertises cumul�es",
                legendval=c(0,1,5,10,20,30,50,100,200,350) )
 



########################################
# LOIRE   (Barrages)
########################################
x11(2200,1600)
par("mar"=c(0.1,1.1,3.1,0.1))
# le fond
plot(get(c("rlb","bassin","celb")[1]),,col=c("grey95","grey95","paleturquoise3")[1])

for (i in 2:3){
plot(get(c("rlb","bassin","celb")[i]),col=c("grey95","grey95","paleturquoise3")[i],add=TRUE)
   }
  }

bar=read.table(file=paste("EDALB/data/extraction_parametre_Marion/bar4pred.txt",sep=""),header=TRUE)
bar$ppm=bar$Auteur=="Pierre Steinbach"|bar$Auteur=="PM Chapon" 
couleurs=c("#4C00FFFF","#00E5FFFF","#00FF4DFF","orange","red")
spdfbar=CrSpPtsDF(xlabel="Pt_LII_X",ylabel="Pt_LII_Y",data=bar)
mypalette= colorRampPalette(couleurs)
niveaux =cut(spdfbar[["expertise"]],5)
couleur=mypalette(length(levels(as.factor(niveaux))))[match(niveaux,levels(as.factor(niveaux)))]

points(spdfbar,
            spdfbar[["cumul_expertise"]],
            col=couleur,
            pch=15,
            cex=0.5
            )
couleur[!is.na(couleur)]<-"black"
couleur[is.na(couleur)]<-"grey10"
couleur[couleur=="black"]<-NA
points(spdfbar,
            spdfbar[["cumul_expertise"]],
            col=couleur,
            pch=20,
            cex=0.2
            )
# locator(1)            
legend(   x="bottomleft",
            title="expertises barrage",
            legend= c("inconnu","franchissable (1)","retard saisonnier(2)","difficile(3)","tr�s difficile(4)","infranchissable(5)"),
            pch=c(20,15,15,15,15,15,15),
            xjust=0,
            cex=1,
            col=c("black",couleurs),
            horiz=FALSE,
            bty="n" )