bassin=6

# r�cup�ration de la jointure tron�on barrage
#sql=paste("select * from  france.jointure where bassin='",bassin,"'")
#bar=fn_sql_0(sql,baseODBC=baseODBCbar,uid=uid,pwd=pwd,silent=FALSE)[[1]]
#bar=bar[,c("den_denivele","id_bdcarthage","ID_final","fba_valeurnote")]
#bar$den_denivele=as.numeric(as.character(bar$den_denivele))
#bar$den_denivele[bar$den_denivele==-98&!is.na(bar$den_denivele)]=NA 
#write.table(bar,file=paste("EDAcommun/data/amorce_mer/bassin final/source/bar",bassin,".txt",sep=""))  
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/bar",bassin,".txt",sep=""),header=TRUE)
# chargement des parcours  
  if(bassin<=3)
  {
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,".RData",sep=""))
  } else {
    #load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_noeudettroncon.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours1.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours2.RData",sep=""))
  }
#noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_",bassin,".txt",sep=""),header =TRUE)
noeuds$nb_barrages=as.double(NA)
noeuds$cumul_score=as.double(NA )
noeuds$pourc_NA_score=as.double(NA )
noeuds$cumul_denivele=as.double(NA)
noeuds$pourc_NA_denivele=as.double(NA)
noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,"_140000.txt",sep=""),header =TRUE)


 for (i in 140000:(dim(noeuds)[1])) {
  progress(i,dim(noeuds)[1])
       #cat(paste("i =", i, "\n"))
        noeud_en_cours=noeuds[i,]
        e=expression(
        parcoursCourant=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep="")))
        parcoursCourant=tryCatch(eval(e),
        error=paste("pas de parcours ",noeud_en_cours$Id_BDCARTHAGE))
        if ((class(parcoursCourant)=="data.frame")){
        ### Traitement du parcours courant
        # on vire la source (voir explication en haut) :
        #!cat(noeud_en_cours$Id_BDCARTHAGE)
        #!cat("\n")
        #index_troncon=
        #stopifnot(length(match(rev(parcoursCourant$troncon[-1]),bar$id_bdcarthage))<=
       #nrow(parcoursCourant))
        barrages_parcourus=merge(parcoursCourant[-1,],bar,by.x="troncon",by.y="id_bdcarthage")
        noeud_en_cours$nb_barrages=nrow(barrages_parcourus)
        #cat(paste(noeud_en_cours$nb_barrages,"\n"))
        if ( noeud_en_cours$nb_barrages >0) {
        noeud_en_cours$pourc_NA_score=sum(is.na(barrages_parcourus$fba_valeurnote))/noeud_en_cours$nb_barrages
        noeud_en_cours$pourc_NA_denivele=sum(is.na(barrages_parcourus$den_denivele))/noeud_en_cours$nb_barrages
         } else  {
         noeud_en_cours$pourc_NA_score=0
         noeud_en_cours$pourc_NA_denivele=0
         noeud_en_cours$cumul_score=0
         noeud_en_cours$cumul_denivele=0
         }
        if ( noeud_en_cours$pourc_NA_score!=1 & noeud_en_cours$nb_barrages >0){
        noeud_en_cours$cumul_score=sum(barrages_parcourus$fba_valeurnote,na.rm=TRUE )
        }
        if ( noeud_en_cours$pourc_NA_denivele!=1 & noeud_en_cours$nb_barrages >0){
        noeud_en_cours$cumul_denivele=sum(barrages_parcourus$den_denivele,na.rm=TRUE )
        }
         
        noeuds[i,]=noeud_en_cours
        } # end if exist parcours
        #parcoursCourant$troncon[-1] => le tron�on mer est NA
        # on r�cup�re les longeurs dans le data frame troncon_total :
        if (i/5000==round(i/5000))
   {
   write.table(noeuds,file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,"_",i,".txt",sep=""),row.names=FALSE)  
    }
  } #end boucle for
  
write.table(noeuds,file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),row.names=FALSE)  
  # toto=noeuds[1:150,]
  # fix(toto)



