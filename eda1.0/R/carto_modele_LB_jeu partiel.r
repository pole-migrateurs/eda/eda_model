#Carto_modele.R
# 04/08/2008 13:33:44
# traitement carto des mod�les

 


#####################################
# Traitement graphique (premi�re partie � charger
#####################################
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
source("EDAcommun/prg/init/fonctions_graphiques.r")
library(lattice)
library(RColorBrewer)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1)))
if(!exists("bassin"))
  bassin=readShapePoly("EDAcommun/data/couche_shp/bassins_hydro_modif_region.shp")
#
if(!exists("celb"))
celb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_polyline.shp")
if(!exists("rlb"))
rlb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_region.shp")

#####
# Chargement des RData
#######

load(file="EDALB/data/extraction_parametre_Marion/noeud_complet.RData") # total
load(file="EDALB/data/extraction_parametre_Marion/jeu_final_bar.RData")
load("EDALB/modele/modele_densite_denivele_50.Rdata")  #modele_densite 
load("EDALB/modele/modele_densite_presence_denivele_50.Rdata")  # modele_densite_presence  Il faut la mm variable barrage pour les 2 mod�les --> la moins pire pour les 2 = denivele (3�me meilleur pour densit� et pr�sence_absence)
                                              
 
select=
  x11(20,15)
mypalette<-rev(brewer.pal(9,"Spectral"))

fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="residus",
                mod=modele_densite,
                colonne_commune="code_station",
                data=jeu_final[select,],
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =seq(0, 1, 0.10), #ou NULL # voir quantile
                #cutn=c(0,0.5,1),#avec cut on supprime les quantiles
                cex=1,
                title="R�sidus du mod�le ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   

                    
##############################
### predictions sur les noeuds
##############################
#traitement du data.frame noeuds




total$coefftr=total$cumul_expertise0*1+total$cumul_expertise1*3+ total$cumul_expertise2*7+total$cumul_expertise3*20+total$cumul_expertise4*55+total$cumul_expertise5*150
total$coefftr4=total$cumul_expertise0*0+total$cumul_expertise1*0.1+ total$cumul_expertise2*0.4+total$cumul_expertise3*2+total$cumul_expertise4*10
total$coeff=total$cumul_expertise0+total$cumul_expertise1+ total$cumul_expertise2+total$cumul_expertise3+total$cumul_expertise4
total$zone1=NA
total[is.na(total)]<-0

### traitement des cegenelin, il en manque malheureusement plein !
## d'abord ceux qui ne manquent pas
#total$cgenelin="4VIL"


total$zone=substr(as.character(total$cgenelin),1,2)
 total$zone1=substr(total$zone,0,1)
 total$zone[total$zone=="J-"|total$zone=="J7"|total$zone=="J8"|total$zone=="J9"]="4VIL"
total$zone[total$zone=="J4"|total$zone=="J5"|total$zone=="J6"]="3BRE_sud"
total$zone[total$zone=="J2"|total$zone=="J3"]="1BRE_ouest"
total$zone[total$zone=="J0"|total$zone=="J1"]="0BRE_Nord"
total$zone1[total$zone1=="J"]=total$zone[total$zone1=="J"]
total$zone1[total$zone1=="K"|total$zone1=="L"|total$zone1=="M"|total$zone1=="-"]="5LOIRE"
total$zone1[total$zone1=="N"]="6SEVRE"  
total$zone1[total$zone1=="R"]="Z"
total$zone1[total$zone1==""]="Z"
total$zone1[total$zone1=="S"]="Z"
total$zone1[total$zone1=="Z"]=NA
#sum(total$bio09==-9999)# 933
total$bio09[total$bio09==-9999]=median(total$bio09[total$bio09!=-9999])
total$bio10[total$bio10==-9999]=median(total$bio09[total$bio10!=-9999])
total$bio11[total$bio11==-9999]=median(total$bio09[total$bio11!=-9999])
summary(as.factor(total$zone1)) #789

for (i in 1:length(unique(jeu_final$zone1))){
noeuds_mer_parzone=unique(jeu_final$noeud_mer[jeu_final$zone1==unique(jeu_final$zone1)[i]])
total[total$noeud_mer%in%noeuds_mer_parzone&is.na(total$zone1),"zone1"]=unique(as.character(unique(jeu_final$zone1)[i]))
}
summary(as.factor(total$zone1) )  #605
total$zone1[is.na(total$zone1)]="5LOIRE"
#!!!!!! ATTENTION LES INCONNUS =605 JE LES MET EN LOIRE  (c'est l� qu'ils ont le plus de proba d'�tre)
summary(as.factor(total$zone1) )  #605

total$zone1=as.factor(total$zone1)
total$presence_civelle=(total$civelle_pro+ total$civelle_amateur)!=0
total$effort=100
total$qualite_ROM_2002[total$qualite_ROM_2002==0]=3
require(gam)
total$annee=as.factor(total$annee)
total$methode_prospection=as.factor(total$methode_prospection)
total$presence_civelle=as.factor(total$presence_civelle)
total$methode_prospection=as.factor(total$methode_prospection)
total$qualite_ROM_2002=as.factor(total$qualite_ROM_2002)
levels(total$annee) <-modele_densite$xlevels$"as.factor(annee)" 
levels(total$zone1) <-modele_densite$xlevels$"zone1" 
levels(total$qualite_ROM_2002) <-modele_densite$xlevels$"as.factor(qualite_ROM_2002)"  
levels(total$methode_prospection) <-modele_densite$xlevels$"methode_prospection" 


total$pred_densite=predict.gam(modele_densite, newdata = total, type =  "response")
total$pred_presence= predict.gam(modele_densite_presence, newdata = total, type =  "response")
total$pred=total$pred_densite*total$pred_presence
#write.table (total,file="EDALB/pr�diction.txt", col.names=TRUE,sep=";")

total$pred_presence0.5=total$pred_presence
total$pred_presence0.5[total$pred_presence>0.5]=1
total$pred_presence0.5[total$pred_presence<=0.5]=0

total1=subset(total,total$zone=="L6")
spdfpartiel=CrSpPtsDF("X","Y",total1)
(xlim1=as.numeric(bbox(spdfpartiel)[1,]))
(ylim1=as.numeric(bbox(spdfpartiel)[2,]))   

# prediction combin�e
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_presence",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.10),#avec cut on supprime les quantiles
                cex=0.9,
                title="pr�dictions du mod�le (probabilit� de pr�sence de l'anguille) ",
                xlim =  xlim1,
                ylim = ylim1,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                ) 


###############################################
# Fonction pour faire les predictions pour ann�es et conditions particuli�res
###############################################
fun_calcul_pristine=function(data=total,annee,mod_presence,mod_densite,pec=TRUE,bar=TRUE){
if (bar) {
data[,c("cumul_expertise","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred",
"cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff")]=0
}
if(pec){
data$presence_civelle=FALSE        
}
data$annee=annee
data$pred_presence=predict(mod_presence,type="response",newdata=data)
data$pred_densite=predict(mod_densite,type="response",newdata=data)
data$pred=data$pred_presence* data$pred_densite
return(data[,c("pred_presence","pred_densite","pred")])
} 

total[,c("pred_presence_pr2005","pred_densite_pr2005","pred_pr2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=TRUE,bar=TRUE)
total[,c("pred_presence_2005","pred_densite_2005","pred_2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=F,bar=F)
total[,c("pred_presence_ssbar2005","pred_densite_ssbar2005","pred_ssbar2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=F,bar=T)
total[,c("pred_presence_sspec2005","pred_densite_sspec2005","pred_sspec2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=T,bar=F)   
total[,c("pred_presence_pr1989","pred_densite_pr1989","pred_pr1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=TRUE,bar=TRUE)
total[,c("pred_presence_1989","pred_densite_1989","pred_1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=F,bar=F)
total[,c("pred_presence_ssbar1989","pred_densite_ssbar1989","pred_ssbar1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=F,bar=T)
total[,c("pred_presence_sspec1989","pred_densite_sspec1989","pred_sspec1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=T,bar=F)  

# prediction combin�e
total$pred_2005_NA=total$pred_2005
total$pred_2005_NA[total$pourc_NA_score>=0.5]<-NA  
# prediction combin�e
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005_NA",
                mod=NULL,
                colonne_commune=NULL,
                data=total,    
                fond=c("rlb","bassin"),        #"celb"
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),#avec cut on supprime les quantiles
                cex=0.3,
                title="pr�dictions du mod�le (mod�le densit� x probabilit� de pr�sence) ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,    
                fond=c("rlb","bassin"),        #"celb"
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),#avec cut on supprime les quantiles
                cex=0.3,
                title="pr�dictions du mod�le (mod�le densit� x probabilit� de pr�sence) ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   

#sum(is.na(total$"pred_presence_pr2005"))      #0
#sum(is.na(total$"pred_densite_pr2005"))       #0
#sum(is.na(total$"pred_pr2005"))               #0
#sum(is.na(total$"pred_presence_2005")) #4153
#sum(is.na(total$"pred_densite_2005"))  #4153
#sum(is.na(total$"pred_2005"))          #4153
# creation d'un spatialpolygondataframe
sum(is.na(total$coeff))
###############################################
# Carto pristine
###############################################

#les noms des variables apparaissent sur la figure : je les change
# prediction combin�e
mypalette<-rev(brewer.pal(10,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),#avec cut on supprime les quantiles
                cex=0.3,
                title="pr�dictions du mod�le (mod�le d>0 * prob. presence) ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   


spdflb=CrSpPtsDF(xlabel="X",ylabel="Y",data=total)

spdflb@data=chnames(object=spdflb@data,                  
        old_variable_name=c("pred_pr2005","pred_2005","pred_ssbar2005","pred_sspec2005","pred_pr1989","pred_1989","pred_ssbar1989","pred_sspec1989"),          
        new_variable_name=c(
        "predictions_2005_pristines",      
        "predictions_2005",   
        "predictions_2005_sans_barrage",
        "predictions_2005_sans_peche",
         "predictions_1989_pristines",
         "predictions_1989",
         "predictions_1989_sans_barrage",
         "predictions_1989_sans_peche"
       )) 
        
# Calcul des densit�s m�dianes (on pourra calculer les nombres aussi)
#summary(spdflb)
#N2005=round(sum(BV_bre1@data$pr�dictions_2004*BV_bre1@data$S_EAU,na.rm=TRUE))
#N2005ss=round(sum(BV_bre1@data$pr�dictions_2004_sans_barrages*BV_bre1@data$S_EAU,na.rm=TRUE))
#N1989=round(sum(BV_bre1@data$pr�dictions_1980*BV_bre1@data$S_EAU,na.rm=TRUE))
#N1989ss=round(sum(BV_bre1@data$pr�dictions_1980_sans_barrages*BV_bre1@data$S_EAU,na.rm=TRUE))
(M2005=round(median(spdflb@data$predictions_2005,na.rm=TRUE),3))
(M2005ss=round(median(spdflb@data$predictions_2005_pristines,na.rm=TRUE),3))
(M2005bar=round(median(spdflb@data$predictions_2005_sans_barrage,na.rm=TRUE),3))
(M2005pec=round(median(spdflb@data$predictions_2005_sans_peche,na.rm=TRUE),3))
(M1989=round(median(spdflb@data$predictions_1989,na.rm=TRUE),3))
(M1989ss=round(median(spdflb@data$predictions_1989_pristines,na.rm=TRUE),3))
(M1989bar=round(median(spdflb@data$predictions_1989_sans_barrage,na.rm=TRUE),3))
(M1989pec=round(median(spdflb@data$predictions_1989_sans_peche,na.rm=TRUE),3))

l1 = list("sp.text", c(300000,2000000), paste("M�diane=",M2005),which = 1)
l2 = list("sp.text", c(300000,2000000), paste("M�diane=",M2005ss),which = 2)
l3 = list("sp.text", c(300000,2000000), paste("M�diane=",M1989),which = 3)
l4 = list("sp.text", c(300500,2000000), paste("M�diane=",M1989ss),which = 4)


mypalette<-rev(brewer.pal(10,"Spectral"))
trellis.par.get("regions") 
trellis.par.set(set = FALSE, regions =list(col =mypalette))
x11(20,15)

# construction d'une cl� (see ?xyplot)
thekey=list(rectangles = Rows(trellis.par.get("regions"),
                  c(1:10)), 
                  text = list(lab = as.character(c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88))),
                  title = "Densit�s/100 m2",
                  cex.title=0.9
                   )
spplot(spdflb,
        c("predictions_2005",
        "predictions_2005_pristines",
        "predictions_1989",
        "predictions_1989_pristines"),
        sp.layout=list(l1,l2,l3,l4),
        cuts=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),
        key = thekey,
        key.space="right",                                                            
        cex=0.3)

#spplot(spdflb,
#        c("predictions_2005",
#        "predictions_2005_pristines",
#        "predictions_1989",
#        "predictions_1989_pristines"),
#        sp.layout=list(l1,l2,l3,l4),
#        cuts=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),
#        key = thekey,
#        key.space="right",                                                            
#        cex=0.3)

# Calcul du pourcentage pristine par bassin


# je vire les pr�dictions pristine ou il n'y a pas de pr�diction en face
tempopr=total$pred_pr2005[is.na(total$"pred_2005")]  # pour stocker le result
tempossbar=total$pred_ssbar2005[is.na(total$"pred_2005")]
temposspech=tempopr=total$pred_sspec2005[is.na(total$"pred_2005")]
total$pred_pr2005[is.na(total$"pred_2005")]<-0
total$pred_ssbar2005[is.na(total$"pred_2005")]<-0
total$pred_sspec2005[is.na(total$"pred_2005")]<-0
# somme des densit�s
resultats=rbind(
  round(c(tapply(total$pred_2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_2005,na.rm=T)),2),
  round(c(tapply(total$pred_pr2005,total$zone1,sum),sum(total$pred_pr2005)),2),
  round(c(tapply(total$pred_ssbar2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_ssbar2005,na.rm=T)),2),
  round(c(tapply(total$pred_sspec2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_sspec2005,na.rm=T)),2)
  #round(c(tapply(total$pred_pr1989,total$zone1,sum),sum(total$pred_pr1989)),2) 
    )
  rownames(resultats)=c("predictions 2005","predictions pristines 2005","predictions sans barrages 2005","predictions sans peche 2005")
resultatsp=resultats[2:4,]
resultatsp[1,]=round(resultats[1,]/resultats[2,],2)
resultatsp[2,]=round(resultats[1,]/resultats[3,],2)
resultatsp[3,]=round(resultats[1,]/resultats[4,],2)
#resultatsp[4,]=round(resultats[1,]/resultats[5,],2)
print(resultats)
print(resultatsp)



write.table(resultats,file="EDALB/data/resultat_som_dens_2005_jeu_partiel.txt",sep=";")
write.table(resultatsp,file="EDALB/data/resultat_pourc_prist_2005_jeu_partiel.txt",sep=";")
#retour du jeu de donn�e � la normale
total$pred_pr2005[is.na(total$"pred_2005")]<-tempopr
total$pred_ssbar2005[is.na(total$"pred_2005")]<-tempossbar
total$pred_sspec2005[is.na(total$"pred_2005")]<-temposspred



# � faire sur jeu_final
jeu_final$pred_densite_presence=predict.gam(modele_densite_presence,newdata=jeu_final,type="response")
jeu_final[jeu_final$d>0,"pred_densite"]=predict.gam(modele_densite,newdata=jeu_final[jeu_final$d>0,],type="response")
jeu_final$pred=jeu_final$pred_densite*jeu_final$pred_densite_presence
plot(jeu_final$pred,jeu_final$d,ylim=c(0,50),xlim=c(0,10))
abline(0,1,col="green")
plot(jeu_final$pred,jeu_final$d,log="xy",xlim=c(1e-03,100))
abline(0,1,col="green")
cor.test(jeu_final$pred,jeu_final$d,method="spearman")                                         # 0.86 erreur!! = 0,69
cor.test(jeu_final$pred_densite_presence,jeu_final$d>0,method="spearman")                      # 0.79
cor.test(jeu_final$pred_densite[jeu_final$d>0],jeu_final$d[jeu_final$d>0],method="spearman")   # 0.70


################################
#   PREDICTIONS POUR DES VARIABLES
################################

don=total[1:1000,]
don=don[,c("annee","methode_prospection","effort","bio09","bio10","bio11","distance_mer","qualite_ROM_2002","zone1","presence_civelle","cumul_expertise","cumul_expertise0","cumul_expertise1","cumul_expertise2", "cumul_expertise3","cumul_expertise4","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred","cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff" )] 
don$annee=2005
don$"zone1"=don$"zone1"[15]   # Loire
don$"presence_civelle"=FALSE
don$"bio09"=median(total$"bio09")
don$"bio10"=median(total$"bio10")
don$"bio11"=median(total$"bio11")
don$"qualite_ROM_2002"=levels(don$"qualite_ROM_2002")[3]
#don$"methode_prospection"=levels(don$"methode_prospection")[1]
don[,c("distance_mer","cumul_expertise","cumul_expertise0","cumul_expertise1","cumul_expertise2", "cumul_expertise3","cumul_expertise4","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred","cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff" )]=rep(0,16) 
#write.table(total,file="EDALB/data/total_pour_pred_variable.txt", col.names=TRUE, sep=";")

funpredmodele=function(don,var="annee",niveautest=1982:2005,newvar1="mod pres_absxmod_pres",newvar2="ann�e", modele_densite_presence,modele_densite){
  l=length(niveautest)
  if(l>nrow(don)) stop("il faut cr�er un plus grand jeu de donn�es")
  don=don[1:l,]
  don[,var]=niveautest
  don$pred=predict(modele_densite_presence,newdata=don,type="response")*predict(modele_densite,newdata=don,type="response")
  don=chnames(object=don,                  
        old_variable_name=c(var,"pred"),          
        new_variable_name=c(
        newvar2,
        newvar1
       ))
       don=don[,c(newvar1,newvar2)] 
  return(don)
}


# install.packages("ggplot2")     #� lancer la premi�re fois
# aide http://had.co.nz/ggplot2/
#Ann�e (un facteur) il faut le passer en facteur pour appliquer   # marche pas avec don$annee dans niveautest du coup j'ai mis total$annee...Bon?
 don1<-funpredmodele(don,var="annee",newvar1="mod�le_complet_densit�s_100m2",newvar2="ann�e",niveautest=levels(total$annee),modele_densite_presence,modele_densite)
 don1[,"ann�e"]=as.factor(don1[,"ann�e"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=ann�e))
p + geom_bar() 

levels(total$annee)
# effort (max 2500)

don1<-funpredmodele(don,var="effort",newvar1="mod�le_complet_densit�s_100m�",newvar2="effort_m�",niveautest=seq(0,2500,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m�,x=effort_m�))
p + geom_line(colour = "blue", size = 1) 


# distance_mer
max(total$distance_mer) #969

don1<-funpredmodele(don,var="distance_mer",newvar1="mod�le_complet_densit�s_100m�",newvar2="distance_mer_km",niveautest=seq(0,1000,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m�,x=distance_mer_km))
p + geom_line(colour = "green", size = 1) 


# Cumul denivel�
#max(total$cumul_denivele,na.rm=T)  #215,07
don1<-funpredmodele(don,var="cumul_denivele",newvar1="mod�le_complet_densit�s_100m�",newvar2="cumul_denivele",niveautest=seq(0,216,length=100),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m�,x=cumul_denivele))
p + geom_line(colour = "palevioletred", size = 1) 

#Qualit� ROM (un facteur) il faut le passer en facteur pour appliquer 
 don1<-funpredmodele(don,var="qualite_ROM_2002",newvar1="mod�le_complet_densit�s_100m2",newvar2="qualite_ROM_2002",niveautest=1:5,modele_densite_presence,modele_densite)
 don1[,"qualite_ROM_2002"]=as.factor(don1[,"qualite_ROM_2002"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=qualite_ROM_2002))
p + geom_bar(aes(fill=qualite_ROM_2002)) 

#M�thode de prospection il faut le passer en facteur pour appliquer 
 don1<-funpredmodele(don,var="methode_prospection",newvar1="mod�le_complet_densit�s_100m2",newvar2="m�thode_prospection",niveautest=levels(don$methode_prospection),modele_densite_presence,modele_densite)
 don1[,"m�thode_prospection"]=as.factor(don1[,"m�thode_prospection"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=m�thode_prospection))
p + geom_bar(aes(fill=m�thode_prospection)) 

#Bio09
#max(total$bio09)  191
don1<-funpredmodele(don,var="bio09",newvar1="mod�le_complet_densit�s_100m�",newvar2="bio09",niveautest=seq(0,200,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m�,x=bio09))
p + geom_line(colour = "green", size = 1) 

#Bio10
#max(total$bio10) 191
don1<-funpredmodele(don,var="bio10",newvar1="mod�le_complet_densit�s_100m�",newvar2="bio10",niveautest=seq(0,200,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m�,x=bio10))
p + geom_line(colour = "green", size = 1) 

#pr�sence p�che civelle
 don1<-funpredmodele(don,var="presence_civelle",newvar1="mod�le_complet_densit�s_100m2",newvar2="pr�sence_peche_civelle",niveautest=c(FALSE,TRUE),modele_densite_presence,modele_densite)
 don1[,"pr�sence_peche_civelle"]=as.factor(don1[,"pr�sence_peche_civelle"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=pr�sence_peche_civelle))
p + geom_bar(aes(fill=pr�sence_peche_civelle)) 

#Zone

don1<-funpredmodele(don,var="zone1",newvar1="mod�le_complet_densit�s_100m2",newvar2="Zone_g�ographique",niveautest=levels(don$zone1),modele_densite_presence,modele_densite)
 don1[,"Zone_g�ographique"]=as.factor(don1[,"Zone_g�ographique"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=Zone_g�ographique))
p + geom_bar(aes(fill=Zone_g�ographique)) 

