bassin=4
noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),header=T)
hist(noeuds$cumul_score,100)
min(noeuds$cumul_score, na.rm=T)

lcscore=log(noeuds$cumul_score[noeuds$cumul_score>0])
hist(lcscore)
quantile(lcscore,na.rm=TRUE)
exp(quantile(lcscore,na.rm=TRUE))
max( noeuds$cumul_score,na.rm=T)