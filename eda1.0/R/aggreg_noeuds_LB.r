#aggr�gation des donn�es de base pour former un unique jeu de donn�es

#chargement de BDMAP et mise en forme

##chargement du fond de carte France  ( � �viter compte tenu de la gal�re que c'est, je charge un  Rdata des donn�es � la place
# 
#require(maptools)
#couche_noeud_2_km=readShapePoints("EDAcommun/data/couche_shp/noeuds2km_finalV2.shp")
#couche_noeud_2_kmdata=couche_noeud_2_km@data
#save(couche_noeud_2_kmdata,file="EDAcommun/data/couche_shp/donneesnoeuds2km_finalV2.RData")
load(file="EDAcommun/data/couche_shp/donneesnoeuds2km_finalV2.RData")
couche_noeud_2_kmdata$codenoeud=as.numeric(as.character(couche_noeud_2_kmdata$codenoeud))
noeuds=read.table("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_total.txt",header=TRUE,sep="\t")
noeuds$distance_source=noeuds$dist_source_max
noeuds$distance_relative=noeuds$distance_mer / (noeuds$distance_mer + noeuds$distance_source)
noeuds1=merge(noeuds,couche_noeud_2_kmdata,by.x="Id_BDCARTHAGE", by.y="ID_SOM_1")
noeuds1$"CODENOEUD"=noeuds1$"codenoeud"
noeuds1=noeuds1[noeuds1$bassin==4,c("Id_BDCARTHAGE","CODENOEUD", "POINT_X", "POINT_Y", "distance_mer", "noeud_mer", "distance_source", "distance_relative")]

# Le CGENELIN est tout foutu dans noeud1, je le r�cup�re ailleurs

contxt=read.table("EDAcommun/data/extraction_parametres/noeuds2km_contexte.txt",header=TRUE,sep="\t")
contxt=contxt[contxt$"Nom_bassin"=="Loire-Bretagne",]
contxt$"CODENOEUD"=contxt$"Codenoeud"
contxt=contxt[,c("CODENOEUD","ETAT2002")]
#chargement des barrages

barrages=read.table("EDALB/data/noeud_2km/noeud_source_bar4_pred.txt",header=TRUE,sep=" ")

#merge(bar4,troncons_source,by.x="Id_BDCARTHAGE",by.y="nouveau_noeud_aval",suffixes=c("","TR"))


#chargement des temp�ratures
temperatureBio09=read.table("EDAcommun/data/extraction_parametres/noeuds2km_temperaturebio09.txt",header=TRUE,sep="\t")
temperatureBio09=temperatureBio09[temperatureBio09$"NOM_BASSIN"=="Loire-Bretagne",]
temperatureBio09$bio09=temperatureBio09$RASTERVALU
temperatureBio09=temperatureBio09[,c("CODENOEUD","CGENELIN","bio09")]

temperatureBio10=read.table("EDAcommun/data/extraction_parametres/noeuds2km_temperaturebio10.txt",header=TRUE,sep="\t")
temperatureBio10=temperatureBio10[temperatureBio10$"NOM_BASSIN"=="Loire-Bretagne",]
temperatureBio10$bio10=temperatureBio10$RASTERVALU
temperatureBio10=temperatureBio10[,c("CODENOEUD","bio10")]

temperatureBio11=read.table("EDAcommun/data/extraction_parametres/noeuds2km_temperaturebio11.txt",header=TRUE,sep="\t")
temperatureBio11=temperatureBio11[temperatureBio11$"NOM_BASSIN"=="Loire-Bretagne",]
temperatureBio11$bio11=temperatureBio11$RASTERVALU
temperatureBio11=temperatureBio11[,c("CODENOEUD","bio11")]


temperature=merge(temperatureBio09,temperatureBio10,by="CODENOEUD",suffixes=c("",".y"))
temperature=merge(temperature,temperatureBio11,by="CODENOEUD",suffixes=c("",".y"))

temperature=temperature[,c("CODENOEUD","CGENELIN","bio09","bio10","bio11")]

altitude= read.table("EDAcommun/data/extraction_parametres/noeuds2km_altitude.txt",header=TRUE,sep="\t")
altitude$altitude=altitude$RASTERVALU
altitude=altitude[altitude$"NOM_BASSIN"=="Loire-Bretagne",c("CODENOEUD","altitude")]
#chargement des altitudes et pentes


#pente=read.table("EDAcommun/data/extraction_parametres/noeuds2km_pente.txt",header=TRUE,sep="\t")
#pente=pente[pente$"NOM_BASSIN"=="Loire-Bretagne",]
#pente$pente=pente$RASTERVALU   
#pente=pente[,c("CODENOEUD","pente")]
                      


#chargement p�che civelle
peche_civelle=read.table("EDAcommun/data/extraction_parametres/noeuds2km_civelle.txt",header=TRUE,sep="\t")
peche_civelle=peche_civelle[peche_civelle$"NOM_BASSIN"=="Loire-Bretagne",]

peche_civelle$civelle_pro=peche_civelle$profession
peche_civelle$civelle_amateur=peche_civelle$amateur
peche_civelle$"CODENOEUD"=peche_civelle$codenoeud
peche_civelle=peche_civelle[,c("CODENOEUD","civelle_pro","civelle_amateur")]


#aggr�gation

troncon=read.table("EDAcommun/data/extraction_parametres/TRONCON_HYDROGRAPHIQUE.csv",header=TRUE,sep=",")
total=merge(noeuds1,troncon[,c("Id_BDCARTHAGE","Code_Hydrographique_Cours_Eau")],by="Id_BDCARTHAGE")
total=merge(total,temperature,by="CODENOEUD")
total=merge(total,contxt,by="CODENOEUD")
total=merge(total,altitude,by="CODENOEUD")
total=merge(total,peche_civelle,by="CODENOEUD")
total=merge(total,barrages,by="Id_BDCARTHAGE")


#formatage des noms des champs



total$annee=2000
total$methode_prospection="compl�te"
total$cgenelin=total$Code_Hydrographique_Cours_Eau
total$effort=1
total$qualite_ROM_2002=total$ETAT2002
total$X=total$POINT_X
total$Y=total$POINT_Y
total$noeud_mer=total$noeud_mer.x
total$bassin = total$bassin.x
total$distance_mer=total$distance_mer.x
total$noeud_mer=total$noeud_mer.x

#simplifie le jeu de donn�es pour n'avoir que les champs n�cessaire
total=total[,c("annee","methode_prospection","effort","X","Y","bio09","bio10","bio11","altitude","distance_mer","noeud_mer","cgenelin","qualite_ROM_2002","civelle_pro","civelle_amateur","cumul_expertise","cumul_expertise0","cumul_expertise1","cumul_expertise2","cumul_expertise3","cumul_expertise4","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred","pourc_NA_score","pourc_NA_expertise","pourc_NA_denivele","cumul_denivele","nb_barragesppm")]



save(total,file="EDALB/data/extraction_parametre_Marion/noeud_complet.RData")
