#R�sultat variables mod�le

# Test du mod�le variable par variable (ajout d'une variable l'une apr�s l'autre et r�sultat des diff�rents mod�le dans EDA/Docs/Stat.xls
require(PresenceAbsence)
require(car)
modele_densite_presence=gam(d>0~as.factor(annee)+s(distance_mer)+presence_civelle+methode_prospection+s(effort)+s(bio09),data=jeu_final,family=binomial(link=logit))
summary(modele_densite_presence)
plot(modele_densite_presence,residuals=F,se=F,ask=F)
AIC(modele_densite_presence)
Anova(modele_densite_presence,test="F")

DATA=data.frame(jeu_final[,"code_operation"],jeu_final[,"d"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA))
Kappa(matrice_confusion)
presence.absence.summary(DATA)

#__________________________________________

#TEST BARRAGE
# Test du mod�le variable par variable 
require(PresenceAbsence)
require(car)
modele_densite_presence=gam(d>0~as.factor(annee)+s(distance_mer)+presence_civelle+methode_prospection+s(effort)+s(bio09)+s(coefftr),data=jeu_final,family=binomial(link=logit))
summary(modele_densite_presence)
plot(modele_densite_presence,residuals=F,se=F,ask=F)
AIC(modele_densite_presence)
Anova(modele_densite_presence,test="F")

DATA=data.frame(jeu_final[,"code_operation"],jeu_final[,"d"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA))
Kappa(matrice_confusion)
presence.absence.summary(DATA)
drop1(modele_densite_presence)
#Toutes les stats qu'on veut
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion) #Pourcentage de Bien Class�
pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1])   #Pr�sence Correctement Pr�dite
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2])   #
K=Kappa(matrice_confusion)
c(round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0))

#d=data.frame(drop1(modele_densite_presence))
#d[order(d$AIC,decreasing = T),]
#______________________________________________________________

# Test du mod�le avec jeu_final_bar (NA=50%)
require(PresenceAbsence)
require(car)
modele_densite_presence=gam(d>0~as.factor(annee)+s(distance_mer)+presence_civelle+methode_prospection+s(effort)+s(bio09)+s(cumul_expertise5)+s(coeff),data=jeu_final[jeu_final$pourc_NA_score<0.5,],family=binomial(link=logit))
summary(modele_densite_presence)
plot(modele_densite_presence,residuals=F,se=F,ask=F)
AIC(modele_densite_presence)
Anova(modele_densite_presence,test="F")

DATA=data.frame(jeu_final[jeu_final$pourc_NA_score<0.5,"code_operation"],jeu_final[jeu_final$pourc_NA_score<0.5,"d"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA))
Kappa(matrice_confusion)
presence.absence.summary(DATA)

#Toutes les stats qu'on veut
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion) #Pourcentage de Bien Class�
pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1])   #Pr�sence Correctement Pr�dite
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2])   #
K=Kappa(matrice_confusion)
c(round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0))

(pdev=(1-deviance(modele_densite_presence)/modele_densite_presence$null.deviance))
a_afficher=c(round(pdev*100),round(aic),round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0))
ex(a_afficher)

#____________
#Avec coeff Pierre
jeu_final$coefftr=jeu_final$cumul_expertise0*0+jeu_final$cumul_expertise1*0.1+ jeu_final$cumul_expertise2*0.4+jeu_final$cumul_expertise3*2+jeu_final$cumul_expertise4*10+jeu_final$cumul_expertise5*100
jeu_final$coefftr4=jeu_final$cumul_expertise0*0+jeu_final$cumul_expertise1*0.1+ jeu_final$cumul_expertise2*0.4+jeu_final$cumul_expertise3*2+jeu_final$cumul_expertise4*10
jeu_final$coeff=jeu_final$cumul_expertise0+jeu_final$cumul_expertise1+ jeu_final$cumul_expertise2+jeu_final$cumul_expertise3+jeu_final$cumul_expertise4
  select=rep(TRUE,nrow(jeu_final))
modele_densite_presence=gam(d>0~methode_prospection +s(distance_mer)+s(effort)+s(bio09)+presence_civelle+as.factor(annee)+s(coefftr4)+s(cumul_expertise5),data=jeu_final[select,],family=binomial(link=logit))
#drop1(modele_densite_presence)
summary(modele_densite_presence)
plot(modele_densite_presence,residuals=F,se=F,ask=F)
(aic=AIC(modele_densite_presence))
#coef(modele_densite_presence)
df=modele_densite_presence$df.residual
(pdev=(1-deviance(modele_densite_presence)/modele_densite_presence$null.deviance))
DATA=data.frame(jeu_final[select,"code_operation"],jeu_final[select,"d"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA))
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion)
pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1])
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2])
K=Kappa(matrice_confusion)
(a_afficher=c(round(df),round(aic),round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0)))
ex(a_afficher)
#presence.absence.summary(DATA)
