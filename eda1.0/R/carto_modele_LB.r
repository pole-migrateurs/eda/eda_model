#Carto_modele.R
# 04/08/2008 13:33:44
# traitement carto des mod�les

 


#####################################
# Traitement graphique (premi�re partie � charger
#####################################
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
source("EDAcommun/prg/init/fonctions_graphiques.r")
library(lattice)
library(RColorBrewer)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1)))
if(!exists("bassin"))
  bassin=readShapePoly("EDAcommun/data/couche_shp/bassins_hydro_modif_region.shp")
#
if(!exists("celb"))
celb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_polyline.shp")
if(!exists("rlb"))
rlb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb+bassin_region.shp")

#####
# MODELE
#######

load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet_traite.RData")
load("EDALB/modele/modele_densite_tt.Rdata") 
select=jeu_final$d>0 
  x11(20,15)
mypalette<-rev(brewer.pal(9,"Spectral"))

fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="residus",
                mod=NULL,
                colonne_commune="code_station",
                data=jeu_final[select,],
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =seq(0, 1, 0.10), #ou NULL # voir quantile
                #cutn=c(0,0.5,1),#avec cut on supprime les quantiles
                cex=1,
                title="R�sidus du mod�le ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   

                    
##############################
### predictions sur les noeuds
##############################
#traitement du data.frame noeuds

load(file="EDALB/data/extraction_parametre_Marion/noeud_complet.RData") # total
load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet_traite.RData")
load("EDALB/modele/modele_densite_tt.Rdata")  #modele_densite
# CEDRIC 10/08
#load("EDALB/modele/modele_densite_presence_cumul_exp_pred_tt.Rdata")  #  modele_densite_presence
load("EDALB/modele/modele_densite_presence_cumul_exp_pred_tt_CORRIGEEBIO10.Rdata")
# CEDRIC 10/08


total$coefftr=total$cumul_expertise0*1+total$cumul_expertise1*3+ total$cumul_expertise2*7+total$cumul_expertise3*20+total$cumul_expertise4*55+total$cumul_expertise5*150
total$coefftr4=total$cumul_expertise0*0+total$cumul_expertise1*0.1+ total$cumul_expertise2*0.4+total$cumul_expertise3*2+total$cumul_expertise4*10
total$coeff=total$cumul_expertise0+total$cumul_expertise1+ total$cumul_expertise2+total$cumul_expertise3+total$cumul_expertise4
total$zone1=NA
total[is.na(total)]<-0

### traitement des cegenelin, il en manque malheureusement plein !
## d'abord ceux qui ne manquent pas
#total$cgenelin="4VIL"


total$zone=substr(as.character(total$cgenelin),1,2)
 total$zone1=substr(total$zone,0,1)
 total$zone1[total$zone==""]="Z"
 total$zone[total$zone=="J-"|total$zone=="J7"|total$zone=="J8"|total$zone=="J9"]="4VIL"
total$zone[total$zone=="J4"|total$zone=="J5"|total$zone=="J6"]="3BRE_sud"
total$zone[total$zone=="J2"|total$zone=="J3"]="1BRE_ouest"
total$zone[total$zone=="J0"|total$zone=="J1"]="0BRE_Nord"
total$zone1[total$zone1=="J"]=total$zone[total$zone1=="J"]
total$zone1[total$zone=="--"]="Z"
total$zone1[total$zone1=="K"|total$zone1=="L"|total$zone1=="M"]="5LOIRE"

total$zone1[total$zone1=="N"]="6SEVRE"  
total$zone1[total$zone1=="R"]="Z"
total$zone1[total$zone1==""]="Z"
total$zone1[total$zone1=="S"]="Z"
total$zone1[total$zone1=="Z"]=NA
#sum(total$bio09==-9999)# 933
total$bio09[total$bio09==-9999]=median(total$bio09[total$bio09!=-9999])
total$bio10[total$bio10==-9999]=median(total$bio09[total$bio10!=-9999])
total$bio11[total$bio11==-9999]=median(total$bio09[total$bio11!=-9999])
summary(as.factor(total$zone1)) #789

for (i in 1:length(unique(jeu_final$zone1))){
noeuds_mer_parzone=unique(jeu_final$noeud_mer[jeu_final$zone1==unique(jeu_final$zone1)[i]])
total[total$noeud_mer%in%noeuds_mer_parzone&is.na(total$zone1),"zone1"]=unique(as.character(unique(jeu_final$zone1)[i]))
}
summary(as.factor(total$zone1) )  #605
total$zone1[is.na(total$zone1)]="5LOIRE"
#!!!!!! ATTENTION LES INCONNUS =605 JE LES MET EN LOIRE  (c'est l� qu'ils ont le plus de proba d'�tre)
summary(as.factor(total$zone1) )  #605

total$zone1=as.factor(total$zone1)
total$presence_civelle=(total$civelle_pro+ total$civelle_amateur)!=0
total$effort=100
total$qualite_ROM_2002[total$qualite_ROM_2002==0]=3
require(gam)
total$annee=as.factor(total$annee)
total$methode_prospection=as.factor(total$methode_prospection)
total$presence_civelle=as.factor(total$presence_civelle)
total$methode_prospection=as.factor(total$methode_prospection)
total$qualite_ROM_2002=as.factor(total$qualite_ROM_2002)
levels(total$annee) <-modele_densite$xlevels$"as.factor(annee)" 
levels(total$zone1) <-modele_densite$xlevels$"zone1" 
levels(total$qualite_ROM_2002) <-modele_densite$xlevels$"as.factor(qualite_ROM_2002)"  
levels(total$methode_prospection) <-modele_densite$xlevels$"methode_prospection" 


total$pred_densite=predict.gam(modele_densite, newdata = total, type =  "response")
total$pred_presence= predict.gam(modele_densite_presence, newdata = total, type =  "response")
total$pred=total$pred_densite*total$pred_presence
#write.table (total,file="EDALB/pr�diction.txt", col.names=TRUE,sep=";")

total$pred_presence0.5=total$pred_presence
total$pred_presence0.5[total$pred_presence>0.5]=1
total$pred_presence0.5[total$pred_presence<=0.5]=0

#total1=subset(total,total$zone=="L6")
bretagne=total$zone%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
total1=subset(total,bretagne)
spdfpartiel=CrSpPtsDF("X","Y",total1)
(xlim1=as.numeric(bbox(spdfpartiel)[1,]))
(ylim1=as.numeric(bbox(spdfpartiel)[2,]))   

# prediction combin�e
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_presence",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=0.5,
                title="pr�dictions du mod�le (probabilit� de pr�sence de l'anguille) ",
                xlim =  xlim1,
                ylim = ylim1,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                ) 


###############################################
# Fonction pour faire les predictions pour ann�es et conditions particuli�res
###############################################
fun_calcul_pristine=function(data=total,annee,mod_presence,mod_densite,pec=TRUE,bar=TRUE){
if (bar) {
data[,c("cumul_expertise","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred",
"cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff")]=0
}
if(pec){
data$presence_civelle=FALSE        
}
data$annee=annee
data$pred_presence=predict(mod_presence,type="response",newdata=data)
data$pred_densite=predict(mod_densite,type="response",newdata=data)
data$pred=data$pred_presence* data$pred_densite
return(data[,c("pred_presence","pred_densite","pred")])
} 

total[,c("pred_presence_pr2005","pred_densite_pr2005","pred_pr2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=TRUE,bar=TRUE)
total[,c("pred_presence_2005","pred_densite_2005","pred_2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=F,bar=F)
total[,c("pred_presence_ssbar2005","pred_densite_ssbar2005","pred_ssbar2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=F,bar=T)
total[,c("pred_presence_sspec2005","pred_densite_sspec2005","pred_sspec2005")]=fun_calcul_pristine(total,2005,modele_densite_presence,modele_densite,pec=T,bar=F)   
total[,c("pred_presence_pr1989","pred_densite_pr1989","pred_pr1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=TRUE,bar=TRUE)
total[,c("pred_presence_1989","pred_densite_1989","pred_1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=F,bar=F)
total[,c("pred_presence_ssbar1989","pred_densite_ssbar1989","pred_ssbar1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=F,bar=T)
total[,c("pred_presence_sspec1989","pred_densite_sspec1989","pred_sspec1989")]=fun_calcul_pristine(total,1989,modele_densite_presence,modele_densite,pec=T,bar=F)  

# prediction combin�e
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,    
                fond=c("rlb","bassin"),        #"celb"
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),#avec cut on supprime les quantiles
                cex=0.3,
                title="pr�dictions du mod�le (mod�le densit� x probabilit� de pr�sence) ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   

#sum(is.na(total$"pred_presence_pr2005"))      #0
#sum(is.na(total$"pred_densite_pr2005"))       #0
#sum(is.na(total$"pred_pr2005"))               #0
#sum(is.na(total$"pred_presence_2005")) #4153
#sum(is.na(total$"pred_densite_2005"))  #4153
#sum(is.na(total$"pred_2005"))          #4153
# creation d'un spatialpolygondataframe
sum(is.na(total$coeff))
###############################################
# Carto pristine
###############################################
##############################
# 10/2008
# C�dric Cartographie Bretagne et Loire de la probabilit� de pr�sence en conditions actuelles et pristines
################################
#total1=subset(total,total$zone=="L6")
#D�finition des zones g�ographiques
Bretagne=total$zone1%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
Loire=total$zone1%in%c("5LOIRE","6SEVRE")&!is.na(total$zone1)
#Correction de deux valeurs ab�rrantes
total[Bretagne&total$"Y"<2250000,"zone1"]= "6SEVRE"
#Correction de quatre valeurs ab�rrantes
total[Loire&total$"X"<200000,"zone1"]="1BRE_ouest" 
Bretagne=total$zone1%in%c("0BRE_Nord","1BRE_ouest","3BRE_sud","4VIL")
Loire=total$zone1%in%c("5LOIRE","6SEVRE")
total1=subset(total,Bretagne)
spdfpartiel=CrSpPtsDF("X","Y",total1)
(xlimBretagne=as.numeric(bbox(spdfpartiel)[1,]))
(ylimBretagne=as.numeric(bbox(spdfpartiel)[2,])) 
rm(total1)
total2=subset(total,Loire)
spdfpartiel=CrSpPtsDF("X","Y",total2)
(xlimLoire=as.numeric(bbox(spdfpartiel)[1,]))
(ylimLoire=as.numeric(bbox(spdfpartiel)[2,]))  
 rm(total2)
mypalette<-rev(brewer.pal(4,"Spectral"))
#PREDICTIONS PRISTINES BRETAGNE
x11(600,400)
total$predgraph=total$"pred_presence_pr2005"
total$predgraph[!Bretagne]<-2
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=1,
                title="Probabilit� de pr�sence d'anguilles",
                xlim =  xlimBretagne,
                ylim = ylimBretagne,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )
#PREDICTIONS ACTUELLES BRE 
total$predgraph=total$"pred_presence_2005"
total$predgraph[!Bretagne]<-2 
x11(600,400)        
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=0.8,
                title="Probabilit� de pr�sence d'anguilles ",
                xlim =  xlimBretagne,
                ylim = ylimBretagne,
                couleur_fond=c("grey95","grey95","paleturquoise3"),
                point=16,
                texbas=FALSE                
                ) 
#PREDICTIONS PRISTINES LOIRE
x11(600,400)
total$predgraph=total$"pred_presence_pr2005"
total$predgraph[!Loire]<-2
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=1,
                title="Probabilit� de pr�sence d'anguilles",
                xlim = xlimLoire,
                ylim = ylimLoire,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )
#PREDICTIONS ACTUELLES LOIRE   
total$predgraph=total$"pred_presence_2005"
total$predgraph[!Loire]<-2
x11(600,400)        
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=1,
                title="Probabilit� de pr�sence d'anguilles ",
                xlim =  xlimLoire,
                ylim = ylimLoire,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                ) 
#PREDICTIONS ACTUELLES LOIRE BRETAGNE  

x11(600,400)        
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_presence_2005",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=seq(0, 1, 0.25),#avec cut on supprime les quantiles
                cex=1,
                title="Probabilit� de pr�sence d'anguilles ",
                #xlim =  xlimLoire,
                #ylim = ylimLoire,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                ) 
                

                
#les noms des variables apparaissent sur la figure : je les change
# prediction combin�e
x11(600,400)    
mypalette<-rev(brewer.pal(10,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),#avec cut on supprime les quantiles
                cex=0.3,
                title="pr�dictions du mod�le (mod�le d>0 * prob. presence) ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )   

#PREDICTIONS COMBINEES LOIRE BRETAGNE  
x11(600,400) 
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_2005",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.01,0.05,0.1,1,5,10,70),#avec cut on supprime les quantiles
                cex=0.6,
                title="Pr�diction des densit�s pour 100 m2  ",
                xlim =  NULL,
                ylim = NULL, 
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )
#PREDICTIONS SANS BARRAGES LOIRE BRETAGNE  
 
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pred_densite_ssbar2005",
                mod=NULL,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                #probs = seq(0, 1, 0.25), #ou NULL # voir quantile
                cutn=c(0,0.01,0.05,0.1,1,5,10,70),#avec cut on supprime les quantiles
                cex=0.6,
                title="Densit�s d'anguilles ",
                #xlim =  xlimLoire,
                #ylim = ylimLoire,
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )                 
                
#PREDICTIONS COMBINEES BRETAGNE 
total$predgraph=total$"pred_2005"
total$predgraph[!Bretagne]<--100 
       
x11(600,400) 
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.01,0.05,0.1,1,5,10,70),#avec cut on supprime les quantiles
                cex=0.8,
                title="Pr�diction des densit�s (nb anguilles pour 100 m2)  ",
                xlim =  xlimBretagne,
                ylim = ylimBretagne, 
                couleur_fond=c("grey95","grey95"),
                point=16,
                textbas=FALSE ,
                surface="celb",
                couleursurface="steelblue",
                legendlab="Classes de densit�"               
                )                
 #PREDICTIONS COMBINEES BRETAGNE  SS BAR
total$predgraph=total$"pred_densite_ssbar2005"
total$predgraph[!Bretagne]<--100 
       
x11(600,400) 
mypalette<-rev(brewer.pal(9,"Spectral"))
fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="predgraph",
                mod=modele_densite,
                colonne_commune=NULL,
                data=total,
                fond=c("rlb","bassin","celb"),
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL ,#seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,0.01,0.05,0.1,1,5,10,70),#avec cut on supprime les quantiles
                cex=0.8,
                title="Pr�diction des densit�s pour 100 m2  ",
                xlim =  xlimBretagne,
                ylim = ylimBretagne, 
                couleur_fond=c("grey95","grey95","paleturquoise3")
                )                       
spdflb=CrSpPtsDF(xlabel="X",ylabel="Y",data=total)

spdflb@data=chnames(object=spdflb@data,                  
        old_variable_name=c("pred_pr2005","pred_2005","pred_ssbar2005","pred_sspec2005","pred_pr1989","pred_1989","pred_ssbar1989","pred_sspec1989"),          
        new_variable_name=c(
        "predictions_2005_pristines",      
        "predictions_2005",   
        "predictions_2005_sans_barrage",
        "predictions_2005_sans_peche",
         "predictions_1989_pristines",
         "predictions_1989",
         "predictions_1989_sans_barrage",
         "predictions_1989_sans_peche"
       )) 
        
# Calcul des densit�s m�dianes (on pourra calculer les nombres aussi)
#summary(spdflb)
#N2005=round(sum(BV_bre1@data$pr�dictions_2004*BV_bre1@data$S_EAU,na.rm=TRUE))
#N2005ss=round(sum(BV_bre1@data$pr�dictions_2004_sans_barrages*BV_bre1@data$S_EAU,na.rm=TRUE))
#N1989=round(sum(BV_bre1@data$pr�dictions_1980*BV_bre1@data$S_EAU,na.rm=TRUE))
#N1989ss=round(sum(BV_bre1@data$pr�dictions_1980_sans_barrages*BV_bre1@data$S_EAU,na.rm=TRUE))
(M2005=round(median(spdflb@data$predictions_2005,na.rm=TRUE),3))
(M2005ss=round(median(spdflb@data$predictions_2005_pristines,na.rm=TRUE),3))
(M2005bar=round(median(spdflb@data$predictions_2005_sans_barrage,na.rm=TRUE),3))
(M2005pec=round(median(spdflb@data$predictions_2005_sans_peche,na.rm=TRUE),3))
(M1989=round(median(spdflb@data$predictions_1989,na.rm=TRUE),3))
(M1989ss=round(median(spdflb@data$predictions_1989_pristines,na.rm=TRUE),3))
(M1989bar=round(median(spdflb@data$predictions_1989_sans_barrage,na.rm=TRUE),3))
(M1989pec=round(median(spdflb@data$predictions_1989_sans_peche,na.rm=TRUE),3))

l1 = list("sp.text", c(300000,2000000), paste("M�diane=",M2005bar),which = 1)
l2 = list("sp.text", c(300000,2000000), paste("M�diane=",M2005pec),which = 2)
l3 = list("sp.text", c(300000,2000000), paste("M�diane=",M1989bar),which = 3)
l4 = list("sp.text", c(300500,2000000), paste("M�diane=",M1989pec),which = 4)


mypalette<-rev(brewer.pal(10,"Spectral"))
trellis.par.get("regions") 
trellis.par.set(set = FALSE, regions =list(col =mypalette))
x11(20,15)

# construction d'une cl� (see ?xyplot)
thekey=list(rectangles = Rows(trellis.par.get("regions"),
                  c(1:10)), 
                  text = list(lab = as.character(c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88))),
                  title = "Densit�s/100 m2",
                  cex.title=0.9
                   )
spplot(spdflb,
        c("predictions_2005_sans_barrage",
        "predictions_2005_sans_peche",
        "predictions_1989_sans_barrage",
        "predictions_1989_sans_peche"),
        sp.layout=list(l1,l2,l3,l4),
        cuts=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),
        key = thekey,
        key.space="right",                                                            
        cex=0.3)

#spplot(spdflb,
#        c("predictions_2005",
#        "predictions_2005_pristines",
#        "predictions_1989",
#        "predictions_1989_pristines"),
#        sp.layout=list(l1,l2,l3,l4),
#        cuts=c(0,0.001,0.005,0.01,0.05,0.1,1,5,10,88),
#        key = thekey,
#        key.space="right",                                                            
#        cex=0.3)

# Calcul du pourcentage pristine par bassin


# je vire les pr�dictions pristine ou il n'y a pas de pr�diction en face
tempopr=total$pred_pr2005[is.na(total$"pred_2005")]  # pour stocker le result
tempossbar=total$pred_ssbar2005[is.na(total$"pred_2005")]
temposspech=tempopr=total$pred_sspec2005[is.na(total$"pred_2005")]
total$pred_pr2005[is.na(total$"pred_2005")]<-0
total$pred_ssbar2005[is.na(total$"pred_2005")]<-0
total$pred_sspec2005[is.na(total$"pred_2005")]<-0
# somme des densit�s
resultats=rbind(
  round(c(tapply(total$pred_2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_2005,na.rm=T)),2),
  round(c(tapply(total$pred_pr2005,total$zone1,sum),sum(total$pred_pr2005)),2),
  round(c(tapply(total$pred_ssbar2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_ssbar2005,na.rm=T)),2),
  round(c(tapply(total$pred_sspec2005,total$zone1,sum,na.rm=T),"total"=sum(total$pred_sspec2005,na.rm=T)),2),
  round(c(tapply(total$pred_pr1989,total$zone1,sum),sum(total$pred_pr1989)),2) 
    )
  rownames(resultats)=c("predictions 2005","predictions pristines 2005","predictions sans barrages 2005","predictions sans peche 2005","predictions pristines 1989")
resultatsp=resultats[2:4,]
resultatsp[1,]=round(resultats[1,]/resultats[2,],2)
resultatsp[2,]=round(resultats[1,]/resultats[3,],2)
resultatsp[3,]=round(resultats[1,]/resultats[4,],2)
resultatsp[4,]=round(resultats[1,]/resultats[5,],2)
print(resultats)
print(resultatsp)



write.table(resultats,file="EDALB/data/resultat_som_dens_1989.txt",sep=";")
write.table(resultatsp,file="EDALB/data/resultat_pourc_prist_1989.txt",sep=";")
#retour du jeu de donn�e � la normale
total$pred_pr2005[is.na(total$"pred_2005")]<-tempopr
total$pred_ssbar2005[is.na(total$"pred_2005")]<-tempossbar
total$pred_sspec2005[is.na(total$"pred_2005")]<-temposspred



# � faire sur jeu_final
jeu_final$pred_densite_presence=predict.gam(modele_densite_presence,newdata=jeu_final,type="response")
jeu_final[jeu_final$d>0,"pred_densite"]=predict.gam(modele_densite,newdata=jeu_final[jeu_final$d>0,],type="response")
jeu_final$pred=jeu_final$pred_densite*jeu_final$pred_densite_presence
plot(jeu_final$pred,jeu_final$d,ylim=c(0,50),xlim=c(0,10))
abline(0,1,col="green")
plot(jeu_final$pred,jeu_final$d,log="xy",xlim=c(1e-03,100))
abline(0,1,col="green")
cor.test(jeu_final$pred,jeu_final$d,method="spearman")                                         # 0.86 erreur!! = 0,69
cor.test(jeu_final$pred_densite_presence,jeu_final$d>0,method="spearman")                      # 0.79
cor.test(jeu_final$pred_densite[jeu_final$d>0],jeu_final$d[jeu_final$d>0],method="spearman")   # 0.70
#avec pearson pour comparaison ga�lle
cor.test(jeu_final$pred,jeu_final$d,method="pearson")                                         # 0.60
cor.test(jeu_final$pred_densite_presence,jeu_final$d>0,method="pearson")                      # 0.82
cor.test(jeu_final$pred_densite[jeu_final$d>0],jeu_final$d[jeu_final$d>0],method="pearson")   # 0,60

################################
#   PREDICTIONS POUR DES VARIABLES
################################

don=total[1:1000,]
don=don[,c("annee","methode_prospection","effort","bio09","bio10","bio11","distance_mer","qualite_ROM_2002","zone1","presence_civelle","cumul_expertise","cumul_expertise0","cumul_expertise1","cumul_expertise2", "cumul_expertise3","cumul_expertise4","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred","cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff" )] 
don$annee=2005
don$"zone1"=don$"zone1"[15]   # Loire
don$"presence_civelle"=FALSE
don$"bio09"=median(total$"bio09")
don$"bio10"=median(total$"bio10")
don$"bio11"=median(total$"bio11")
don$"qualite_ROM_2002"=levels(don$"qualite_ROM_2002")[3]
#don$"methode_prospection"=levels(don$"methode_prospection")[1]
don[,c("distance_mer","cumul_expertise","cumul_expertise0","cumul_expertise1","cumul_expertise2", "cumul_expertise3","cumul_expertise4","cumul_expertise5","nb_barrages","cumul_score","cumul_expertise_pred","cumul_denivele","nb_barragesppm","coefftr","coefftr4","coeff" )]=rep(0,16) 
write.table(total,file="EDALB/data/total_pour_pred_variable.txt", col.names=TRUE, sep=";")

funpredmodele=function(don,var="annee",niveautest=1982:2005,newvar1="mod pres_absxmod_pres",newvar2="ann�e", modele_densite_presence,modele_densite){
  l=length(niveautest)
  if(l>nrow(don)) stop("il faut cr�er un plus grand jeu de donn�es")
  don=don[1:l,]
  don[,var]=niveautest
  don$pred=predict(modele_densite_presence,newdata=don,type="response")*predict(modele_densite,newdata=don,type="response")
  don=chnames(object=don,                  
        old_variable_name=c(var,"pred"),          
        new_variable_name=c(
        newvar2,
        newvar1
       ))
       don=don[,c(newvar1,newvar2)] 
  return(don)
}


# install.packages("ggplot2")     #� lancer la premi�re fois
# aide http://had.co.nz/ggplot2/
#Ann�e (un facteur) il faut le passer en facteur pour appliquer 
 don1<-funpredmodele(don,var="annee",newvar1="mod�le_complet_densit�s_100m2",newvar2="ann�e",niveautest=1982:2005,modele_densite_presence,modele_densite)
 don1[,"ann�e"]=as.factor(don1[,"ann�e"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=ann�e))
p + geom_bar() 


# effort (max 2500)

don1<-funpredmodele(don,var="effort",newvar1="mod�le_complet_densit�s_100m2",newvar2="effort_m2",niveautest=seq(0,2500,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=effort_m2))
p + geom_line(colour = "blue", size = 1) 


# distance_mer
#max(total$distance_mer) 969

don1<-funpredmodele(don,var="distance_mer",newvar1="mod�le_complet_densit�s_100m2",newvar2="distance_mer_km",niveautest=seq(0,1000,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=distance_mer_km))
p + geom_line(colour = "green", size = 1) 


# Cumul expertise pr�dites
#max(total$cumul_expertise_pred,na.rm=T)  353,5
don1<-funpredmodele(don,var="cumul_expertise_pred",newvar1="mod�le_complet_densit�s_100m2",newvar2="cumul_notes_expertise_m�diane_pour_valeurs_manquantes",niveautest=seq(0,354,length=100),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=cumul_notes_expertise_m�diane_pour_valeurs_manquantes))
p + geom_line(colour = "palevioletred", size = 1) 

#Qualit� ROM (un facteur) il faut le passer en facteur pour appliquer 
 don1<-funpredmodele(don,var="qualite_ROM_2002",newvar1="mod�le_complet_densit�s_100m2",newvar2="qualite_ROM_2002",niveautest=1:5,modele_densite_presence,modele_densite)
 don1[,"qualite_ROM_2002"]=as.factor(don1[,"qualite_ROM_2002"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=qualite_ROM_2002))
p + geom_bar(aes(fill=qualite_ROM_2002)) 

#M�thode de prospection il faut le passer en facteur pour appliquer 
 don1<-funpredmodele(don,var="methode_prospection",newvar1="mod�le_complet_densit�s_100m2",newvar2="m�thode_prospection",niveautest=levels(don$methode_prospection),modele_densite_presence,modele_densite)
 don1[,"m�thode_prospection"]=as.factor(don1[,"m�thode_prospection"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=m�thode_prospection))
p + geom_bar(aes(fill=m�thode_prospection)) 

#Bio09
#max(total$bio09)  191
don1<-funpredmodele(don,var="bio09",newvar1="mod�le_complet_densit�s_100m2",newvar2="bio09",niveautest=seq(0,200,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=bio09))
p + geom_line(colour = "green", size = 1) 

#Bio10
#max(total$bio10) 191
don1<-funpredmodele(don,var="bio10",newvar1="mod�le_complet_densit�s_100m2",newvar2="bio10",niveautest=seq(0,200,length=1000),modele_densite_presence,modele_densite)
p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=bio10))
p + geom_line(colour = "green", size = 1) 

#pr�sence p�che civelle
 don1<-funpredmodele(don,var="presence_civelle",newvar1="mod�le_complet_densit�s_100m2",newvar2="pr�sence_peche_civelle",niveautest=c(FALSE,TRUE),modele_densite_presence,modele_densite)
 don1[,"pr�sence_peche_civelle"]=as.factor(don1[,"pr�sence_peche_civelle"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=pr�sence_peche_civelle))
p + geom_bar(aes(fill=pr�sence_peche_civelle)) 

#Zone

don1<-funpredmodele(don,var="zone1",newvar1="mod�le_complet_densit�s_100m2",newvar2="Zone_g�ographique",niveautest=levels(don$zone1),modele_densite_presence,modele_densite)
 don1[,"Zone_g�ographique"]=as.factor(don1[,"Zone_g�ographique"])
 p<-ggplot(don1,aes(y=mod�le_complet_densit�s_100m2,x=Zone_g�ographique))
p + geom_bar(aes(fill=Zone_g�ographique)) 

