 
bassin=4


 #r�cup�ration de la jointure tron�on barrage
#sql=paste("select * from  france.loirebretagnetroncon ")
#bar=fn_sql_0(sql,baseODBC=baseODBCbar,uid=uid,pwd=pwd,silent=FALSE)[[1]]
## tous les barrages y compris les barrages qui viennent d'une autre source
#
#
#bar=bar[,c("den_denivele","id_bdcarthage","ID_final","fba_valeurnote")]
#bar$den_denivele=as.numeric(as.character(bar$den_denivele))
#bar$den_denivele[bar$den_denivele==-98&!is.na(bar$den_denivele)]=NA 
#write.table(bar,file=paste("EDAcommun/data/amorce_mer/bassin final/source/barloirebretagne",".txt",sep=""))  

bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/barloirebretagne.txt",sep=""),header=TRUE)
bar$den_denivele=as.numeric(as.character(bar$den_denivele))
bar$den_denivele[bar$den_denivele==-98&!is.na(bar$den_denivele)]=NA
bar=bar[order(bar$ouv_id),]

# au final je ne prends que les expersise et je recalcules les score2 (je les remplace) (r�union du 10 juillet)
#bar[is.na(as.numeric(bar$ouv_id)),"score1"]=bar[is.na(as.numeric(bar$ouv_id)),"score"]      # PM
#bar[!is.na(as.numeric(bar$ouv_id)),"score1"]=bar[!is.na(as.numeric(bar$ouv_id)),"expertise"]      # P
bar$haut=NA
  bar[bar$den_denivele<=0.5 & !is.na(bar$den_denivele),"haut"]<-1
  bar[bar$den_denivele>0.5 & bar$den_denivele<=1& !is.na(bar$den_denivele),"haut"]<-2
  bar[bar$den_denivele>1 & bar$den_denivele<=2& !is.na(bar$den_denivele),"haut"]<-3
  bar[bar$den_denivele>2 &!is.na(bar$den_denivele),"haut"]<-4
  
  # calcul du score2
  bar$score2=  round(bar$expertise-bar$haut+ bar$den_denivele,2)
  bar$score2=bar$score2+ 1.6  # min(bar$score2,na.rm=TRUE)
  
  # calcul de la note d'expertise
 
  bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"]=
    round(bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"])
  bar[!is.na(bar$expertise) &  bar$expertise>=4.5 &  bar$expertise<5,"expertise"]=5

  hist(bar$expertise,50)
   hist(bar$score2,50)     

# chargement des parcours  
  if(bassin<=3)
  {
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,".RData",sep=""))
  } else {
    #load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_noeudettroncon.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours1.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours2.RData",sep=""))
  }
noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_",bassin,".txt",sep=""),header =TRUE)
bar$vect=as.character(NA)
bar$baraval=as.character(NA)
bar$nb_barrages=as.double(NA)
bar$cumul_score2=as.double(NA )
bar$cumul_expertise=as.double(NA )
bar$pourc_NA_expertise=as.double(NA )
bar$pourc_NA_score2=as.double(NA )
bar$cumul_denivele=as.double(NA)
bar$pourc_NA_denivele=as.double(NA) 
 
 for (i in 1:(dim(bar)[1])) {
  #progress(i,dim(noeuds)[1])
       cat(paste("i =", i, "\n"))
        noeud_en_cours=noeuds[noeuds$"Id_BDCARTHAGE"==bar[i,"nouveau_noeud_aval"],]
        bar_en_cours=bar[i,]
        if (nrow(noeud_en_cours)==1){
        parcoursCourant=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep=""))
        ### Traitement du parcours courant
        # on vire la source (voir explication en haut) :
        #!cat(noeud_en_cours$Id_BDCARTHAGE)
        #!cat("\n")
        #index_troncon=
        #stopifnot(length(match(rev(parcoursCourant$troncon[-1]),bar$id_bdcarthage))<=
       #nrow(parcoursCourant))
        barrages_parcourus=merge(parcoursCourant[-1,],bar,by.x="troncon",by.y="id_bdcarthage")
      
        bar_en_cours$nb_barrages=nrow(barrages_parcourus)
        cat(paste(bar_en_cours$nb_barrages,"\n"))
        if ( bar_en_cours$nb_barrages >0) {
        barrages_parcourus=barrages_parcourus[order(barrages_parcourus$dmer_amont),]
        bar_en_cours$vect=concat(barrages_parcourus$ouv_id)
        bar_en_cours$baraval=barrages_parcourus$ouv_id[length(barrages_parcourus$ouv_id)]
        bar_en_cours$pourc_NA_expertise   =sum(is.na(barrages_parcourus$expertise))/bar_en_cours$nb_barrages
        bar_en_cours$pourc_NA_score2  =sum(is.na(barrages_parcourus$score2))/bar_en_cours$nb_barrages
        bar_en_cours$pourc_NA_denivele=sum(is.na(barrages_parcourus$den_denivele))/bar_en_cours$nb_barrages
         } else  {
         bar_en_cours$pourc_NA_expertise=0
         bar_en_cours$pourc_NA_denivele=0
         bar_en_cours$pourc_NA_score2=0
         bar_en_cours$cumul_expertise=0
         bar_en_cours$cumul_score2=0
         bar_en_cours$cumul_denivele=0
         bar_en_cours$vect=""
         bar_en_cours$baraval="0"
         }
        if ( bar_en_cours$pourc_NA_expertise!=1 & bar_en_cours$nb_barrages >0){
        bar_en_cours$cumul_expertise=sum(barrages_parcourus$expertise,na.rm=TRUE )
        }
        if ( bar_en_cours$pourc_NA_score2!=1 & bar_en_cours$nb_barrages >0){
        bar_en_cours$cumul_score2=sum(barrages_parcourus$score2,na.rm=TRUE )
        }
        if ( bar_en_cours$pourc_NA_denivele!=1 & bar_en_cours$nb_barrages >0){
        bar_en_cours$cumul_denivele=sum(barrages_parcourus$den_denivele,na.rm=TRUE )
        }
       
         
        bar[i,]=bar_en_cours
        #parcoursCourant$troncon[-1] => le tron�on mer est NA
        # on r�cup�re les longueurs dans le data frame troncon_total :
        if (i/1000==round(i/1000))
   {
   write.table(bar,file=paste("EDAcommun/data/amorce_mer/bassin final/source/bardetail",bassin,"_",i,".txt",sep=""),row.names=FALSE)  
    }
    }#end if
  } #end boucle for
  
write.table(bar,file=paste("EDAcommun/data/amorce_mer/bassin final/source/bardetail",bassin,".txt",sep=""),row.names=FALSE)  
  # toto=noeuds[1:150,]
  # fix(toto)



