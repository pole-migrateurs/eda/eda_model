load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet_traite.RData")
require(gam)
require(PresenceAbsence)
require(car)

jeu_final$nb300=jeu_final$nb_150+jeu_final$nb_150_300
jeu_final$d_300=jeu_final$nb_300/jeu_final$effort*100

select=rep(TRUE,nrow(jeu_final))
   
modele_densite_presence=gam(d_300>0~methode_prospection+s(distance_mer)+s(effort)+s(bio10)+as.factor(annee)+s(cumul_expertise_pred),data=jeu_final[select,],family=binomial(link=logit))
#dfd=data.frame(drop1(modele_densite_presence))
#dfd[order(dfd$AIC,decreasing = T),]
summary(modele_densite_presence)
#x11()
#plot(modele_densite_presence,residuals=F,se=F,ask=F)

DATA=data.frame(jeu_final[select,"code_operation"],jeu_final[select,"effectif"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA,threshold = 0.5))
(aic=AIC(modele_densite_presence))
#coef(modele_densite_presence)
df=modele_densite_presence$df.residual
(pdev=(1-deviance(modele_densite_presence)/modele_densite_presence$null.deviance))
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion)
pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1])
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2])
(K=Kappa(matrice_confusion))
(a_afficher=c(round(df),round(aic),round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0)))
ex(a_afficher)
presence.absence.summary(DATA)
#save(modele_densite_presence,file= "EDALB/modele/d_300_modele_densite_presence.Rdata")


#MODELE DENSITE
select=jeu_final$d_300>0 
modele_densite=gam(d_300~as.factor(annee)+s(distance_mer)+methode_prospection+presence_civelle+as.factor(qualite_ROM_2002)+zone1+s(bio10)+s(cumul_expertise_pred),data=jeu_final[select,],family=Gamma(link=log))

#dfd=data.frame(drop1(modele_densite))
#dfd[order(dfd$AIC,decreasing = T),]
#summary(modele_densite)
plot(modele_densite,residuals=F,se=T,ask=F)
#AIC(modele_densite)
#Anova(modele_densite)
(aic=AIC(modele_densite))
#coef(modele_densite_presence)
df=modele_densite$df.residual
(pdev=(1-deviance(modele_densite)/modele_densite$null.deviance))
(a_afficher=c(round(df),round(aic)))
ex(a_afficher)
save(modele_densite,file= "EDALB/modele/d_300_modele_densite.Rdata")