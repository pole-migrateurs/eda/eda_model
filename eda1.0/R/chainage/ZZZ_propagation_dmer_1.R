# Nom fichier :        propagation_dmer_1.R
# Projet :             EDA\traitement geographique\chainage
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             Laurent Beaulaton
# Contact :            laurent.beaulaton@onema.fr
# Date de creation :    22/02/2008 11:24:15
# Description :         propagation des nouvelles dmer sur 1 troncon, v�rifie si cela entraine des modif des noeuds d�j� fait et fait les modif
# Data needed
# Packages needed :     
# remarques
#
#**********************************************************************

propagation_dmer_1=function(troncon_a_verif)
{
  #regarde si cela entraine des modifs
  if(sum(noeud_dist[as.character(troncon_a_verif$Id_BDCARTHAGE_amont),"dmer"]>troncon_a_verif$dmer_amont)>0)
  {
    #noeud n�cessitant la propagation des nouvelles dmer
    noeud_refait=noeud_total[as.character(troncon_a_verif$Id_BDCARTHAGE_amont),][noeud_total[as.character(troncon_a_verif$Id_BDCARTHAGE_amont),"dmer"]>troncon_a_verif$dmer_amont,"Id_BDCARTHAGE"]
    #propagation
    troncon_refait=troncon_a_verif[troncon_a_verif$Id_BDCARTHAGE_amont %in% noeud_a_refaire,]
#    troncon_total[troncon_total$troncon %in% troncon_a_verif[troncon_a_verif$Id_BDCARTHAGE_amont %in% noeud_a_refaire,"troncon"],]=troncon_a_verif[troncon_a_verif$Id_BDCARTHAGE_amont %in% noeud_a_refaire,]
  }  else 
  {
    noeud_refait=NULL
    troncon_refait=NULL
  }
  
  reponse=list(troncon=troncon_refait,noeud=noeud_refait)
  
  return(reponse)
}

