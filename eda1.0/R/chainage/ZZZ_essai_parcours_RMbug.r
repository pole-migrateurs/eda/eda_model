# Nom fichier :        essai_parcours_object2.R
# Projet :             EDA/EDAcommun
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             Laurent Beaulaton
# Contact :            laurent.beaulaton@onema.fr
# Date de creation :    04/04/2008 17:22:09
# Description :         parcours du r�seau � l'aide programmation data.frame de data.frame
# Data needed
# Packages needed :     
# remarques
#
#**********************************************************************


##########
## chargement des donn�es et config
#base_access="EDAcommun/data/data.mdb"
#
#
##chargement des troncon bdcarthage et des amorces "mer"
#require(RODBC)
#canal=odbcConnectAccess(base_access)
#troncon_longueur=sqlFetch(canal,"troncon_longueur")
#odbcClose(canal)
#
##d�termine quels troncons sont orient�
#troncon_oriente=troncon_longueur$Sens=="Sens du tron�on"
#
##formatage de la table noeud
#noeuds=read.table("EDAcommun/data/amorce_mer/Noeuds_bassin_final.txt",header=TRUE,sep="\t",as.is=TRUE)
#noeuds[noeuds$Id_BDCARTHAGE=="437000561","situation"]="ras" #correction erreur
#noeuds[noeuds$situation=="ras","distance_mer"]=-999
#noeuds$noeud_mer=noeuds$Id_BDCARTHAGE
#noeuds[noeuds$situation=="ras","noeud_mer"]=NA
#noeuds=noeuds[,c("Id_BDCARTHAGE","situation","distance_mer","bassin","noeud_mer")]
#rownames(noeuds)=noeuds$Id_BDCARTHAGE
#noeuds$niveau=-999
#noeuds[noeuds$situation=="mer","niveau"]=0
#noeuds[noeuds$situation=="frontiere","niveau"]=0
#noeuds[noeuds$distance_mer==-999,"distance_mer"]=999999
#
#table(noeuds$situation)
#table(noeuds$bassin)
#
##formatage de la table troncon_total
#troncon_total=data.frame(id=c(as.character(troncon_longueur[,"Id_BDCARTHAGE"]),as.character(troncon_longueur[!troncon_oriente,"Id_BDCARTHAGE"])),noeud_amont=c(as.character(troncon_longueur$Id_BDCARTHAGE_Noeud_Initial),as.character(troncon_longueur[!troncon_oriente,"Id_BDCARTHAGE_Noeud_Final"])),noeud_aval=c(as.character(troncon_longueur$Id_BDCARTHAGE_Noeud_Final),as.character(troncon_longueur[!troncon_oriente,"Id_BDCARTHAGE_Noeud_Initial"])),longueur=c(troncon_longueur$longueur,troncon_longueur[!troncon_oriente,"longueur"]))
#troncon_total=data.frame(id=troncon_total$id,noeud_amont=troncon_total$noeud_amont,noeud_aval=troncon_total$noeud_aval,longueur=troncon_total$longueur)
#troncon_total$id=as.character(troncon_total$id)
#troncon_total$noeud_amont=as.character(troncon_total$noeud_amont)
#troncon_total$noeud_aval=as.character(troncon_total$noeud_aval)
#
##save(noeuds, troncon_total, troncon_longueur ,file="EDAcommun/data/amorce_mer/noeuds_troncon_total.RData")
#load(file="EDAcommun/data/amorce_mer/noeuds_troncon_total.RData")

#
##cr�ation du niveau 0
#noeuds_a_faire=as.character(noeuds[noeuds$niveau==0,"Id_BDCARTHAGE"])
#for(n in 1:length(noeuds_a_faire))
#{
#  progress(n,length(noeuds_a_faire))
#  d=data.frame(noeud=noeuds_a_faire[n],troncon=NA)
#  d$noeud=as.character(d$noeud)
#  d$troncon=as.character(d$troncon)
#  assign(paste("parcours_",noeuds_a_faire[n],sep=""),d)
#  
#}
#
##sauvegarde niveau 0 entier
##save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")) ,file="EDAcommun/data/amorce_mer/niveau_0.RData")

#load(file="EDAcommun/data/amorce_mer/niveau_0.RData")
#
#
##sauvegarde des couches entieres
#noeuds_entier=noeuds
#troncon_total_entier=troncon_total
#
##d�coupage du niveau 0 en 6 RData, 1 par bassin
#for(bassin_AE in 1:6)
#{
#  progress(bassin_AE,6)             
#  #s�lection des noeuds
#  noeuds=noeuds_entier[noeuds_entier$bassin==bassin_AE,]
#  #selection des troncons
#  troncon_total=troncon_total_entier[troncon_total_entier$noeud_aval %in% noeuds$Id_BDCARTHAGE,]
#  #enregistrement et selection des parcours
#  save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/amorce_mer/niveau_0_",bassin_AE,".RData",sep=""))  
#}

for(bassin_AE in 6:6)
{
 #load(file=paste("EDAcommun/data/amorce_mer/niveau_0_",bassin_AE,".RData",sep=""))
  min_niveau=0
  max_niveau=100000



##pour relancer au niveau 335 
 load(file=paste("EDAcommun/data/amorce_mer/niveau_461_",bassin_AE,"mg.RData",sep=""))
noeuds[noeuds$niveau>459,c("situation","distance_mer","niveau","noeud_mer")]=matrix(rep(c("ras",999999,-999,NA),sum(noeuds$niveau>459)),ncol=4,byrow=TRUE)
noeuds$distance_mer=as.numeric(noeuds$distance_mer)
noeuds$noeud_mer=as.integer(noeuds$noeud_mer)
noeuds$niveau=as.integer(noeuds$niveau)
min_niveau=459
  
  for(niveau in min_niveau:max_niveau)
  {
    cat(paste("\nB",bassin_AE,"\tN",niveau," :\n"))
    noeuds_parcouru=data.frame(Id_BDCARTHAGE=integer(1),distance_mer=numeric(1),noeud_mer=integer(1),niveau=integer(1)) #data.frmae que l'on rempli avec les nouveaux noeuds parcourus que l'on restockera + tard dans noeuds
    noeuds_a_faire=noeuds[noeuds$niveau==niveau,]
    if(dim(noeuds_a_faire)[1]<1) break
    index_troncon=(1:dim(troncon_total)[1])[troncon_total$noeud_aval %in% noeuds_a_faire$Id_BDCARTHAGE] ### !!!!! match fait le premier match uniquement
    troncon_a_faire=troncon_total[index_troncon,]
    for(n in 1:dim(noeuds_a_faire)[1])
    {
      progress(n,dim(noeuds_a_faire)[1])
      noeud_en_cours=noeuds_a_faire[n,]
      #tron�ons voisins du noeud
      voisin=troncon_a_faire[troncon_a_faire$noeud_aval %in% noeud_en_cours$Id_BDCARTHAGE,c("noeud_amont","longueur","id")] 
      
      #controle si pas de voisin identique (ie : deux tron�ons m�nent au m�me noeud amont)
      if(length(unique(voisin$noeud_amont))<dim(voisin)[1])
      {
        detect=names(table(voisin$noeud_amont)[table(voisin$noeud_amont)>1]) #r�cup�re le nom du noeud amont coupable
        for (i in 1:length(detect))
        {
          indice=voisin$noeud_amont == detect[i]
          mini=which.min(voisin[indice,"longueur"]) #s�lectionne la distance la plus courte (donne l'indice du premier)
          doublon=indice[indice]     #que les doublon
          doublon[mini]=FALSE        #tout ce qui n'est pas mini = TRUE         
          n_ligne=(1:length(indice))[indice] #numero de ligne des doublons
          voisin=voisin[-n_ligne[doublon],] #on supprime les lignes doublons
        }
      }
      if(dim(voisin)[1]==0)
      { 
        noeuds[match(noeud_en_cours$Id_BDCARTHAGE,noeuds$Id_BDCARTHAGE),"situation"]="source"
      } else
      {
        if(sum(!voisin$noeud_amont %in% noeuds_parcouru$Id_BDCARTHAGE)>0) #cr�e les noeuds_parcouru manquant
        {
          #cherche l'id dans BDCARTHAGE
          id=match(voisin$noeud_amont[!voisin$noeud_amont %in% noeuds_parcouru$Id_BDCARTHAGE],noeuds$Id_BDCARTHAGE)
          # comme on est sur un bassin il peut manquer un ou plusieurs noeuds
          id=id[!is.na(id)]
          # @cedric modif pour �viter plantage !
          if (length(id)!=0){
          noeuds_parcouru[(dim(noeuds_parcouru)[1]+1):(dim(noeuds_parcouru)[1]+ length(id)),]=noeuds[id,c("Id_BDCARTHAGE","distance_mer","noeud_mer","niveau")]
          } else warnings(paste("n=",n))
        }
        # numero de ligne dans noeuds_parcouru[] des noeuds voisins : id
        id=match(voisin$noeud_amont,noeuds_parcouru$Id_BDCARTHAGE) 
        # selection des noeuds amont ayant une distance mer superieure � la distance actuelle 
        # noeud en cours = noeud dont on traite les noeuds � l'amont
        indice=noeuds_parcouru[id,"distance_mer"]>(noeud_en_cours[,"distance_mer"]+voisin$longueur)
        indice[is.na(indice)]=FALSE      #s'il n'y a pas de match, id sera � na donc indice sera � NA, c'est que le noeud est hors bassin 
        # remplacement dans noeuds 
        if (sum(indice)>0){
          # de la distance mer et du niveau qui indique en combien de noeuds on atteind la distance la plus courte vers ce noeud
          noeuds_parcouru[id[indice],c("distance_mer","niveau","noeud_mer")]=data.frame(noeud_en_cours[,"distance_mer"]+voisin[indice,"longueur"],niveau+1,noeud_en_cours$noeud_mer)
          # acces � l'object parcour du noeud en cours 
          parcours_noeud_en_cours=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep=""))
          for (i in 1:sum(indice)){ # nombre de voisins diminuant la distance
            id_voisin=voisin[indice,"noeud_amont"][i]
            parcours_voisin=parcours_noeud_en_cours
            d=dim(parcours_voisin)[1]+1
            # incr�mentation du data.frame en incluant une nouvelle ligne
            parcours_voisin[d,]=c(voisin[indice,"noeud_amont"][i],voisin[indice,"id"][i])
            assign(paste("parcours_",id_voisin,sep=""),parcours_voisin)
          }     
       } 
      
    }
   }
   #r�int�gre les noeuds parcouru dans noeuds
   # supprime la premi�re ligne qi sert � rien
   noeuds_parcouru=noeuds_parcouru[-1,]
   id_noeud=match(noeuds_parcouru$Id_BDCARTHAGE,noeuds$Id_BDCARTHAGE)
   noeuds[id_noeud,c("distance_mer","niveau","noeud_mer")]=noeuds_parcouru[,c("distance_mer","niveau","noeud_mer")]
   if (niveau/50==round(niveau/50))
   {
   save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/amorce_mer/niveau_",niveau+1,"_",bassin_AE,"mg.RData",sep=""))
  }
  } # boucle for sur le niveau

  
  save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/amorce_mer/niveau_",niveau+1,"_",bassin_AE,".RData",sep=""))
  write.table(noeuds,paste("EDAcommun/data/amorce_mer/niveau_",niveau,"_",bassin_AE,".txt",sep=""),sep="\t",row.names=FALSE,col.names=TRUE)

  rm(list=c("noeuds", "troncon_total", ls(pattern="parcours_")))
}


write.table(noeuds,file="EDAcommun/data/amorce_mer/export_noeuds_loire.txt",sep="\t",row.names=FALSE,col.names=TRUE)
