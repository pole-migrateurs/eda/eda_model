# Nom fichier :        parcours_source.R
# Projet :             EDA/EDAcommun
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             C�dric Briand
# Contact :            cedric.biand@lavilaine.com
# Date de creation :    24/04/2008 11:08:23
# Description :         parcours du r�seau � l'aide programmation data.frame 
# Data needed
# Packages needed :     
# remarques
#
#**********************************************************************



#fonction pour renvoyer la position de a dans b
#attention vecteur non ordonn�
ind=function(a,b){
    index=(1:length(b))[b%in%a]
    return(index)
}

# retourne l'index des valeurs r�p�t�es d'un vecteur
indrepeated=function(a){
    sol=match(unique(a),a)     #index des valeurs uniques
    rep=a[-sol]  # valeurs r�p�t�es
    return(ind(rep,a))   # index des valeurs r�p�t�es
}
# renvoit l'index des valeurs apparaissant une seule fois
induk=function(a){
    sol=match(unique(a),a)     #index des valeurs uniques
    return(sol)   # index des valeurs r�p�t�es
}

# parcours source : explication
# dans la table parcours les transformations sont les suivantes
        # noeud troncon
        # Mer NA          source 0             source 0
        # N3  t3    =>    N2    t1     =>      N2    t1
        # N2 t2           N3    t2             N3    t2+t1
        # source t1       mer    t3            mer    t3+t2+t1
 # a partir de l� les distances sont ajout�es dans dist_source_cum
 # et remplac�es si plus grandes dans dist_source_max 

# chargement des donn�es par bassin
#fichier=c("niveau_721_5")
##fichier="niveau_191_1"

#chargement des sources et des noeuds parcourus
source=read.table("EDAcommun/data/amorce_mer/bassin final/noeud_source.txt",sep="\t", header = TRUE)
parcouru=read.table("EDAcommun/data/amorce_mer/bassin final/noeuds_parcouru.txt",sep="\t", header = TRUE)
fichier=list("amorce_mer/niveau_192_1gilda","amorce_mer/niveau_658_2ss","amorce_mer/niveau_610_3ss",c("niveau_1055_4_59689ss","parcours_1055_4"),c("niveau_721_5_46728ss","parcours_721_5"),c("amorce_mer/niveau_843_6parcours1","amorce_mer/niveau_843_6parcours2"))

#chargement des longueurs des troncons
troncon_parcouru=read.table("EDAcommun/data/amorce_mer/bassin final/troncon_parcouru.txt",sep="\t", header = TRUE)
troncon_parcouru$id=troncon_parcouru$Id_BDCARTHAGE


for (bassin in 5:6) {
  progress(bassin,6)
  for(z in 1:length(fichier[[bassin]]))
    load(file=paste("EDAcommun/data/",fichier[[bassin]][z],".RData",sep=""))
  noeuds=parcouru[parcouru$bassin==bassin,]
  noeuds[noeuds$situation=="source","situation"]="ras"
  noeuds[noeuds$Id_BDCARTHAGE %in% source[source$CompteDenouveau_noeud_amont==1,"nouveau_noeud_amont"],"situation"]="source"
  noeuds[,c("dist_source_max")]=as.double(-1)
  noeuds[,"dist_source_cum"]=as.numeric(0)
  noeuds[,"noeud_source"]=as.character(NA)
  noeuds_source=noeuds[noeuds$situation=="source",]
  noeuds[noeuds$situation=="source","dist_source_max"]=0
  noeuds[noeuds$situation=="source","dist_source_cum"]=0
  noeuds[noeuds$situation=="source","noeud_source"]=  noeuds[noeuds$situation=="source","Id_BDCARTHAGE"]#eux m�mes
  troncon_total=troncon_parcouru[troncon_parcouru$bassin==bassin,]
  troncon_total$rang_strahler=0
  
 ###############################################################################
 #                   PARCOURS DES NOEUDS A PARTIR DES SOURCE
 ###############################################################################
  
  
#parcours ligne par ligne
  for (i in 1:(dim(noeuds_source)[1])) {
        progress(i,dim(noeuds_source)[1])

        #recuperation du data.frame
        #!cat(paste("i =", i, "\n"))
        noeud_en_cours=noeuds_source[i,]
        parcoursCourant=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep=""))
        ### Traitement du parcours courant
        # on vire la source (voir explication en haut) :
        #!cat(noeud_en_cours$Id_BDCARTHAGE)
        #!cat("\n")
        index_noeud=match(rev(parcoursCourant$noeud)[-1],noeuds$Id_BDCARTHAGE)
        
        index_troncon=match(rev(parcoursCourant$troncon[-1]),troncon_total$id)  #parcoursCourant$troncon[-1] => le tron�on mer est NA
        #troncon_total[index_troncon,"rang_strahler"]=fun_strahler_cum(troncon_total[index_troncon,"rang_strahler"])
        
        id_troncontotal=troncon_total$id[index_troncon]
        # on r�cup�re les longeurs dans le data frame troncon_total :
        longueurs=troncon_total[index_troncon,"longueur"]
        
        
        # il y a des doublons dans tron�on
        # pour tester qu'il est bien sain de prendre la premi�re instance de chaque tron�on (a mettre en commentaire ensuite)
        # , cr�ation et parcours d'un data frame des valeurs r�p�t�es
        # et recherche de valeurs diff�rentes
        #if (length(index_noeud)!=length(index_troncon)){
#            dftest=data.frame("id"=as.character(id_troncontotal[indrepeated(id_troncontotal)]),"l"=longueurs[indrepeated(id_troncontotal)])
#            for (j in 1:nrow(dftest)) {
#                  if (sum(dftest[dftest$id%in%dftest$id[j],"l"])/2 !=dftest[j,"l"]) {
#                         stop(paste("j=",j,"il existe des distances diff�rentes pour une m�me valeur de tron�on"))
#                  }
#            }
#        }
        
        #longueurs=longueurs[induk(id_troncontotal)]
        stopifnot(length(longueurs)==(nrow(parcoursCourant)-1))
        longueurs=cumsum(longueurs)  # parcours de la source vers la mer
        #!cat("longueurs = \n")
        #!cat(longueurs)
       #!cat("\n")
        ## r�int�gration des distances source max
        # ----------------------------------------
        # tous les noeuds dont la distance source est sup�rieur � l'actuelle
        longueurs_sup=noeuds[index_noeud,"dist_source_max"]< longueurs
        index_noeuds=match(noeuds[index_noeud,][longueurs_sup,"Id_BDCARTHAGE"],noeuds$Id_BDCARTHAGE)
        #!cat(longueurs_sup)
        #!cat("\n")        
        if(sum(longueurs_sup)>0){
          noeuds[index_noeuds,"dist_source_max"]=longueurs[longueurs_sup]
          # on affecte les longueurs max
          noeuds[index_noeuds,"noeud_source"]=noeud_en_cours$Id_BDCARTHAGE
          # on affecte l'identifiant du noeud source en cours
        }
        #-------------------------------------------
        # cumul des distances source en amont d'un noeud (distance de tous les parcours source -mer)
        noeuds[index_noeud,"dist_source_cum"]=noeuds[index_noeud,"dist_source_cum"]+ longueurs
        #!stopifnot(!is.na(noeuds[index_noeuds,"noeud_source"]))
       
    } # end boucle dans bassin
    
    if(bassin<4)
    {
      save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/",fichier[[bassin]][z],"s.RData",sep=""))
    } else {
      save(list=c("noeuds", "troncon_total"),file=paste("EDAcommun/data/",fichier[[bassin]][z],"s_noeudettroncon.RData",sep=""))
      parcours=ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]
      save(list=parcours[1:(length(parcours)/2)], file=paste("EDAcommun/data/",fichier[[bassin]][z],"sparcours1.RData",sep=""))
      save(list=parcours[(length(parcours)/2+1):length(parcours)],file=paste("EDAcommun/data/",fichier[[bassin]][z],"sparcours2.RData",sep=""))
    }
    rm(list=c("noeuds", "troncon_total", ls(pattern="parcours_"))) 
 
} # end boucle bassin 
# 
# ###############################################################################
# #                   PARCOURS DES NOEUDS HORS SOURCE MER
# ###############################################################################
# 
# 
#    index=noeuds$niveau!=-999 #=> les noeuds sont accessibles depuis la mer
#    index1=noeuds$dist_source_max!=-1     #=> les noeuds sont sur les parcours source mer
#
#    
#
#    cat(paste("noeuds parcourus :" ,round(100*sum(index&index1)/nrow(noeuds)),"%\n"))
#    cat(paste("noeuds hors parcours amont aval :", round(100*sum(index&!index1)/nrow(noeuds)),"%\n"))
#    cat(paste("noeuds non parcourus depuis la mer :" ,round(100*sum(!index)/nrow(noeuds)),"%\n"))
#    noeuds_a_faire=noeuds[index&!index1,]     # les candidats pour une deuxi�me moulinette
#    noeuds_parcouru=noeuds[index&index1,]   # les noeuds sur un parcours source mer
#    
#    if(dim(noeuds_a_faire)[1]<1) break  # on arr�te le bassin et on passe au suivant
#    #index_troncon=(1:dim(troncon_total)[1])[troncon_total$noeud_aval %in% noeuds_a_faire$Id_BDCARTHAGE] ### !!!!! match fait le premier match uniquement
#    #troncon_a_faire=troncon_total[index_troncon,] 
#   
#    for(n in 1:dim(noeuds_a_faire)[1])
#    {
#      progress(n,dim(noeuds_a_faire)[1])
#      #!cat(paste("n=",n,"\n"))
#      noeuds_en_cours=noeuds_a_faire[n,]
#      #!cat(paste(noeud_en_cours$Id_BDCARTHAGE,"\n"))
#      # au dela du premier passage dans la boucle while, le noeuds_en_cours peut contenir plusieurs noeuds
#      jonction_trouvee=FALSE
#      liste_recherche=list()
#      niveau=0
#      while( !jonction_trouvee){
#          niveau=niveau+1
#          
#          voisin=troncon_total[troncon_total$noeud_aval %in% noeuds_en_cours$Id_BDCARTHAGE,]
#          voisin=voisin[induk(voisin$noeud_amont),]      # pour limiter les doublons et le grossissement de l'object
#          liste_recherche[[niveau]]=voisin                #sert pour le parcours � l'envers � la fin
#          #!cat(paste("niveau =", niveau,"voisin="))
#          #!cat(voisin$noeud_amont)
#          #!cat("\n")
#          
#          #contr�le si pas de voisin identique (ie : deux tron�ons m�nent au m�me noeud amont)
##          if(length(unique(voisin$noeud_amont))<dim(voisin)[1])
##          {
##                detect=names(table(voisin$noeud_amont)[table(voisin$noeud_amont)>1]) #r�cup�re le nom du noeud amont coupable
##                for (i in 1:length(detect))
##                {
##                  indice=voisin$noeud_amont == detect[i]
##                  mini=which.min(voisin[indice,"longueur"]) #s�lectionne la distance la plus courte (donne l'indice du premier)
##                  doublon=indice[indice]     #que les doublon
##                  doublon[mini]=FALSE        #tout ce qui n'est pas mini = TRUE         
##                  n_ligne=(1:length(indice))[indice] #numero de ligne des doublons
##                  voisin=voisin[-n_ligne[doublon],] #on supprime les lignes doublons
##                } # end for
##          } # end if
#          if(dim(voisin)[1]==0)
#          { 
#            warning(paste(noeuds_a_faire[n,"Id_BDCARTHAGE"] ,"=>en amont de ce noeud on arrive � une source",
#                    "hors ce noeud n'a pas �t� retenu dans les parcours source-mer =>>???")) 
#          } else
#          {
#          if(sum(voisin$noeud_amont %in% noeuds_parcouru$Id_BDCARTHAGE)>0)
#              { # il existe au moins un noeud sur un parcours
#            
#                 id=match(voisin$noeud_amont[voisin$noeud_amont %in% noeuds_parcouru$Id_BDCARTHAGE],noeuds$Id_BDCARTHAGE)
#                  # comme on est sur un bassin il peut manquer un ou plusieurs noeuds
#                  id=id[!is.na(id)]
#                  noeuds_sur_parcours=noeuds[id,]    #liste des voisins amont appartenant � un parcours
#    
#                  # DETECTION DES NOEUDS SUR LE MEME PARCOURS => ON VIRE LE NOEUD LE PLUS EN AVAL
#    
#                  if (length(id )>1&length(id)!=length(unique(noeuds_sur_parcours[,"noeud_source"])))
#                  { # il faut au moins deux noeuds
#                      parcours_possibles=unique(noeuds_sur_parcours[,"noeud_source"])
#                      
#                         for (m in parcours_possibles){
#                               nspm=noeuds_sur_parcours[noeuds_sur_parcours$noeud_source==m,]
#                               nsp=noeuds_sur_parcours[noeuds_sur_parcours$noeud_source!=m,]
#                               nspm=nspm[which.min(nspm[,"dist_source_max"]),]
#                               noeuds_sur_parcours=rbind(nsp,nspm)
#                                  # on vire parmis ce parcours possible les noeuds aval
#                                  warnings(paste(
#                                    "noeud :",noeuds_a_faire[n,"Id_BDCARTHAGE"]," de source",m,
#                                    "deux noeuds sur le m�me parcours"))
#                         } # end for m
#                  } # end if
#                  
#                  
#                  # SELECTION DU NOEUD AYANT LA DISTANCE SOURCE MAX
#                  
#                  noeuds_sur_parcours=
#                        noeuds_sur_parcours[which.max(noeuds_sur_parcours[,"dist_source_max"]),]
#                  stopifnot (nrow( noeuds_sur_parcours)==1)               
#                         
#                  # acces au data.frame parcours du noeud source du parcours selectionn� 
#                  parcoursCourant=get(paste("parcours_",noeuds_sur_parcours$noeud_source,sep=""))   # mer en premier
#                  
#                  # Pb de source
#                  if (sum(parcoursCourant$noeud%in%noeuds_sur_parcours$Id_BDCARTHAGE)==0){
#                      Warnings(paste("noeud",noeuds_en_cours$Id_BDCARTHAGE,
#                      "Le noeud de croisement n'a pas la source enregistr�e (plusieurs parcours)"))
#                      break 
#                      } 
#                                                          
#                  
#                  # CREATION D'UN NOUVEAU PARCOURS AVAL PASSANT PAR LE NOEUD NON PARCOURU
#                  
#
#
#                  # ci dessous idem parcours source
#                  index_noeud=match(rev(parcoursCourant$noeud)[-1],noeuds$Id_BDCARTHAGE)
#                  Source=rev(parcoursCourant$noeud)[1]          
#                  index_troncon=match(rev(parcoursCourant$troncon[-1]),troncon_total$id)  #parcoursCourant$troncon[-1] => le tron�on mer est NA
#                  id_troncontotal=troncon_total$id[index_troncon]               
#                  if (noeuds_sur_parcours$Id_BDCARTHAGE==noeuds_sur_parcours$noeud_source)
#                  {
#                      # Si le croisement est un noeud source, le traitement fait disparaitre l'id noeud source
#                      #=> traitement de ce cas sp�cial
#                      longueur_crois=0
#                  } else
#                  {
#                    # on r�cup�re les longeurs dans le data frame troncon_total :
#                    longueurs=troncon_total[index_troncon,"longueur"]
#                    longueurs=cumsum(longueurs)  # parcours de la source vers la mer
#                    # r�cup�ration de la longueur cumul�e au noeud de croisement avec un parcours
#                    longueur_crois=longueurs[rev(parcoursCourant$noeud)[-1]==noeuds_sur_parcours$Id_BDCARTHAGE]
#                  }
#                  noeud_crois=noeuds_sur_parcours$Id_BDCARTHAGE  # vecteur des noeuds depuis le croisement en cours de cr�ation
#                  #!cat(paste("noeud_croisement"=noeud,"longueur=",longueurcum,"\n"))
#                  
#                  # remplace l'object parcours courant
#                  for (i in (niveau):1){
#                   # niveau =niveaux de la boucle while
#                   # on utilise les noeuds amont et aval du tron�on pour refaire le trajet dans l'autre sens
#                       indexvoisin=match(noeud_crois[length(noeud_crois)],liste_recherche[[i]]$noeud_amont)
#                       noeud_crois=c(noeud_crois,liste_recherche[[i]]$noeud_aval[indexvoisin])  #vecteur
#                       longueur_crois=c(longueur_crois,liste_recherche[[i]]$longueur[indexvoisin])# vecteur
#                  }
#                  noeud_crois=noeud_crois[-1]   # on ne garde pas la premi�re ligne qui correspondait � l'intersection
#                  longueur_crois=cumsum(longueur_crois)[-1] # on ne garde pas la premi�re ligne qui correspondait � l'intersection
#                  #!cat("longueurcum=")
#                  #!cat(longueurcum)
#                  #!cat("\n")
#                  #!cat(noeud)
#                  #!cat("\n")
#                  index_noeud=match(noeud_crois,noeuds$Id_BDCARTHAGE)  
#                  longueurs_sup=noeuds[index_noeud,"dist_source_max"]< longueur_crois
#                  index_noeuds=match(noeuds[index_noeud,][longueurs_sup,"Id_BDCARTHAGE"],noeuds$Id_BDCARTHAGE)
#                    
#                  if(sum(longueurs_sup)>0){
#                      noeuds[index_noeuds,"dist_source_max"]=longueur_crois[longueurs_sup]
#                      #!cat("distances source max \n")
#                      #!cat(noeuds[index_noeuds,"dist_source_max"])
#                      # on affecte les longueurs max depuis le croisement
#                      noeuds[index_noeuds,"noeud_source"]=Source
#                      # la source a �t� r�cup�r�e sur le parcours au dessus
#                 } # end boucle if
#                #-------------------------------------------
#                # cumul des distances source en amont d'un noeud (distance de tous les parcours source -mer)
#                noeuds[index_noeud,"dist_source_cum"]=noeuds[index_noeud,"dist_source_cum"]+ longueur_crois
#                #!print(noeuds[index_noeud,"dist_source_cum"])       
#                
#                    # on sort de la boucle while
#               jonction_trouvee=TRUE
#
#          } else 
#          { #pas de noeud sur le parcours on augmente d'une �tape
#              id=match(voisin$noeud_amont,noeuds$Id_BDCARTHAGE) 
#              noeuds_en_cours=noeuds[id,]
#              jonction_trouvee=FALSE
#              if (niveau>50)  jonction_trouvee=TRUE
#               
#          } # end boucle if testant l'arriv�e � un croisement avec un parcours mer-source
#        } #  end boucle else testant l'arriv�e � une source
#    } # boucle while jonction_trouvee
#  } # boucle for sur le niveau
#      save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/amorce_mer/",fichier[z],"ss.RData",sep=""))
#       write.table(noeuds,file=paste("EDAcommun/data/amorce_mer/",fichier[z],"ss.txt",sep=""),sep="\t",row.names=FALSE,col.names=TRUE)
#    rm(list=c("noeuds", "troncon_total", ls(pattern="parcours_")))
#} # end boucle for Z entre bassins
#        



###############################################
# calcul des rangs de strahler
###############################################

source("EDAcommun/prg/outils/ajout_data.R")

#noeud_mer_a_tester=300041066

fun_strahler=function(list_rang)  #rang du troncon amont (A) et du troncon aval(B)
{
  list_rang=sort(list_rang,decreasing=TRUE)[1:2]
  rang_A=list_rang[1]
  rang_B=list_rang[2]
  reponse=ifelse(rang_A==rang_B,rang_A+1,max(rang_A,rang_B))
  return(reponse)
}

#chargement des r�sultats de l'exploration source des noeuds
noeuds_source=read.table("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_total.txt",header=TRUE,sep="\t")
troncons_source=read.table("EDAcommun/data/amorce_mer/bassin final/source/troncon_parcouru_source.txt",header=TRUE,sep="\t")
nb_troncon_confluence=read.table("EDAcommun/data/amorce_mer/bassin final/source/nb_troncon_confluence.txt",header=TRUE,sep="\t")
noeuds_source_uniquement=unique(data.frame(noeud_source=noeuds_source$noeud_source,noeud_mer=noeuds_source$noeud_mer))
noeuds_source_uniquement=noeuds_source_uniquement[!is.na(noeuds_source_uniquement$noeud_source),]
noeuds_source=merge(noeuds_source,nb_troncon_confluence,by.x="Id_BDCARTHAGE",by.y="nouveau_noeud_aval",all.x=TRUE)
noeuds_source[is.na(noeuds_source$nb_troncon),"nb_troncon"]=0
noeud_source_confluence=read.table("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_confluence.txt",header=TRUE,sep="\t")

#cr�ation d'une barre de progression pour le bassin !!!! version 2.7.0. de R n�cessaire
pb_bassin <- tkProgressBar("bassin en cours", "1/6",1, 6, 1)
  
for (bassin in 3:6) {
setTkProgressBar(pb_bassin,bassin,label=paste(bassin,"/6",sep=""))

  if(bassin<=3)
  {
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,".RData",sep=""))
  } else {
    #load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_noeudettroncon.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours1.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours2.RData",sep=""))
  }
  
  troncon_total=troncons_source[troncons_source$bassin==bassin,]
  troncon_total$rang_strahler=0
  troncon_total$niveau=0
  noeuds=noeuds_source[noeuds_source$bassin==bassin,]
  noeud_source_confluence_bassin=noeud_source_confluence[noeud_source_confluence$bassin==bassin,]

  noeud_a_tester=unique(noeuds$noeud_mer)

troncon_bassin_final=data.frame(Id_BDCARTHAGE=NA, nouveau_sens=NA, nouveau_noeud_amont=NA, dmer_amont=NA, nouveau_noeud_aval=NA, dmer_aval=NA, longueur=NA, bassin=NA, noeud_mer=NA, dist_source_max_amont=NA, dist_source_cum_amont=NA, noeud_source_amont=NA, dist_source_max_aval=NA, dist_source_cum_aval=NA, noeud_source_aval=NA, rang_strahler=NA, niveau=NA, parcouru=NA)

  #cr�ation d'une barre de progression pour les neouds � tester !!!! version 2.7.0. de R n�cessaire
pb_noeud <- tkProgressBar("noeud en cours", paste("1/",length(noeud_a_tester),sep=""),1, length(noeud_a_tester), 1)
  
  for(num_noeud_a_tester in 1:length(noeud_a_tester))
  {
    setTkProgressBar(pb_noeud,num_noeud_a_tester,label=paste(noeud_a_tester[num_noeud_a_tester],"\t:\t",num_noeud_a_tester,"/",length(noeud_a_tester),sep=""))
    noeud_mer_a_tester=noeud_a_tester[num_noeud_a_tester]
    noeuds_selec=noeuds[noeuds$noeud_mer==noeud_mer_a_tester,]
        if(sum(!is.na(noeuds_selec$noeud_source))==0) next  #pour �liminer les ptits bassin o� le parcours a merd�
    troncon_selec=troncon_total[troncon_total$noeud_mer==noeud_mer_a_tester,]
    noeuds_source_selec=noeuds_source_uniquement[noeuds_source_uniquement$noeud_mer==noeud_mer_a_tester,]
    noeud_source_confluence_selec=noeud_source_confluence_bassin[noeud_source_confluence_bassin$noeud_mer==noeud_mer_a_tester,]
    
    troncon_selec$parcouru=0
#cherche les troncons qui ne sont jamais parcouru dans les parcours_source (d�fluence, canaux ...)    
 #cr�ation d'une barre de progression pour les neouds � tester !!!! version 2.7.0. de R n�cessaire
pb_noeud_source <- tkProgressBar("noeud en cours de nettoyage", paste("1/",length(noeuds_source_selec$noeud_source),sep=""),1, length(noeuds_source_selec$noeud_source), 1)

    for(num_noeuds_source_selec_a_tester in 1:length(noeuds_source_selec$noeud_source))
    {
      setTkProgressBar(pb_noeud_source,num_noeuds_source_selec_a_tester,label=paste(noeuds_source_selec$noeud_source[num_noeuds_source_selec_a_tester],"\t:\t",num_noeuds_source_selec_a_tester,"/",length(noeuds_source_selec$noeud_source),sep=""))
      noeuds_source_selec_a_tester=noeuds_source_selec$noeud_source[num_noeuds_source_selec_a_tester]
      parcours_courant=get(paste("parcours_",noeuds_source_selec_a_tester,sep=""))
      troncon_selec[troncon_selec$Id_BDCARTHAGE %in% parcours_courant$troncon,"parcouru"]=troncon_selec[troncon_selec$Id_BDCARTHAGE %in% parcours_courant$troncon,"parcouru"]+1      
    }
close(pb_noeud_source)    
    troncon_vire=troncon_selec[troncon_selec$parcouru==0,]        #les troncons en question
    noeuds_selec[noeuds_selec$Id_BDCARTHAGE %in% troncon_vire$nouveau_noeud_aval,"nb_troncon"]=noeuds_selec[noeuds_selec$Id_BDCARTHAGE %in%  troncon_vire$nouveau_noeud_aval,"nb_troncon"]-1    
    troncon_selec=troncon_selec[troncon_selec$parcouru>0,]


    test_fin=data.frame(noeud_source=noeuds_source_selec$noeud_source,fin=FALSE) #pour les test de sortie, regarde si test si toutes les rivi�res sont arriv�e � la mer
    fin = FALSE
    niveau=0
      #cr�ation d'une barre de progression pour les niveaux � tester !!!! version 2.7.0. de R n�cessaire
  pb_niveau <- tkProgressBar("niveau en cours","0",1,trunc(max(troncon_selec$parcouru)/2), 1)
    while(fin==FALSE)
    {
      setTkProgressBar(pb_niveau,niveau,label=niveau)
      niveau=niveau+1
      for(noeuds_source_selec_a_tester in test_fin[!test_fin$fin,"noeud_source"])
#      noeuds_source_selec_a_tester=test_fin[!test_fin$fin,"noeud_source"][1]
#      noeuds_source_selec_a_tester="100001076"
      {
        if(niveau==1) #initialisation des parcours source et premier parcours
        {
          parcours_courant=get(paste("parcours_",noeuds_source_selec_a_tester,sep=""))
          parcours_courant=(parcours_courant[(dim(parcours_courant)[1]):2,])
          parcours_courant=merge(parcours_courant,noeuds_selec[,c("Id_BDCARTHAGE","nb_troncon")],by.x="noeud",by.y="Id_BDCARTHAGE",sort=FALSE)
          parcours_courant$niveau=NA
          parcours_courant$strahler=0
          #controle
          stopifnot(troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[1,"troncon"],"rang_strahler"]==0)
          #ne garde dans parcours courants que les troncons appartenant � la rivi�re de la source
          troncon_a_conserver=troncon_selec[troncon_selec$noeud_source_amont==noeuds_source_selec_a_tester,"Id_BDCARTHAGE"]
          parcours_courant=parcours_courant[parcours_courant$troncon %in% troncon_a_conserver,]          
          troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[1,"troncon"],c("rang_strahler","niveau")]=c(1,niveau)
          parcours_courant[1,c("niveau","strahler")]=c(1,1)
          #assign(paste("troncon_",parcours_courant[1,"troncon"],sep=""),noeuds_source_selec_a_tester)
          i=1
          while(ifelse(i<dim(parcours_courant)[1],parcours_courant[i+1,"nb_troncon"]==1,FALSE))  # tous les noeuds souvent jusqu'� la prochaine confluence
          {
            i=i+1
            troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[i,"troncon"],c("rang_strahler","niveau")]=c(1,niveau)
            parcours_courant[i,c("niveau","strahler")]=c(niveau,1)
            #assign(paste("troncon_",parcours_courant[i,"troncon"],sep=""),noeuds_source_selec_a_tester)
          }
          assign(paste("parcours_source_",noeuds_source_selec_a_tester,sep=""),parcours_courant)
        } else 
        { #niveau > 1
          #on r�cup�re le parcours
          parcours_courant=get(paste("parcours_source_",noeuds_source_selec_a_tester,sep=""))
          #on fait d'abord le premier noeud non encore parcouru ie le premier du niveau = NA et on prend le num�ro de ligne (i)
          i=as.integer(row.names(parcours_courant[is.na(parcours_courant$niveau),])[1])
          troncon_amont=noeud_source_confluence_selec[noeud_source_confluence_selec$nouveau_noeud_aval==parcours_courant[i,"noeud"],"troncon_Id_BDCARTHAGE"]  #liste des troncons amont � la confluence

          index_troncon=troncon_selec$Id_BDCARTHAGE %in% troncon_amont
          stopifnot(sum(index_troncon)>0)
          if(sum(index_troncon)==1)
          {

            troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[i,"troncon"],c("niveau","rang_strahler")]=c(niveau,troncon_selec[troncon_selec$Id_BDCARTHAGE %in% troncon_amont, "rang_strahler"])
            parcours_courant[i,c("niveau","strahler")]=c(niveau,troncon_selec[troncon_selec$Id_BDCARTHAGE %in% troncon_amont, "rang_strahler"])
            while(ifelse(i<dim(parcours_courant)[1],parcours_courant[i+1,"nb_troncon"]<=1,FALSE))  # tous les noeuds souvent jusqu'� la prochaine confluence
              {
                i=i+1
                troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[i,"troncon"],c("niveau","rang_strahler")]=c(niveau,parcours_courant[i-1,"strahler"])
                parcours_courant[i,c("niveau","strahler")]=c(niveau,rang_strahler)
              } #end while
              assign(paste("parcours_source_",noeuds_source_selec_a_tester,sep=""),parcours_courant)
          } else { #endif sum(index_troncon) ==1
            if(sum(troncon_selec[index_troncon, "niveau"]==0)==0) #test si les troncon amont ont d�j� �t� parcouru
            {
              rang_strahler=fun_strahler(troncon_selec[troncon_selec$Id_BDCARTHAGE %in% troncon_amont, "rang_strahler"])
              troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[i,"troncon"],c("niveau","rang_strahler")]=c(niveau,rang_strahler)
              parcours_courant[i,c("niveau","strahler")]=c(niveau,rang_strahler)
              
              while(ifelse(i<dim(parcours_courant)[1],parcours_courant[i+1,"nb_troncon"]==1,FALSE))  # tous les noeuds suivant jusqu'� la prochaine confluence
              {
                i=i+1
                 #troncon_amont=noeud_source_confluence_selec[noeud_source_confluence_selec$nouveau_noeud_aval==parcours_courant[i,"noeud"],"troncon_Id_BDCARTHAGE"]  #liste des troncons amont � la confluence
                #rang_strahler=troncon_selec[troncon_selec$Id_BDCARTHAGE %in% troncon_amont, "rang_strahler"]
                troncon_selec[troncon_selec$Id_BDCARTHAGE==parcours_courant[i,"troncon"],c("niveau","rang_strahler")]=c(niveau,rang_strahler)
                parcours_courant[i,c("niveau","strahler")]=c(niveau,rang_strahler)
              } #end while
              assign(paste("parcours_source_",noeuds_source_selec_a_tester,sep=""),parcours_courant)
            } #end if(sum(troncon_selec[index_troncon, "niveau"]==0)==0)
          } #end if sum(index_troncon)
        } #end else niveau=1
    test_fin[test_fin$noeud_source==noeuds_source_selec_a_tester,"fin"]=(parcours_courant[dim(parcours_courant)[1],"strahler"]>0)
        } #end for  noeuds_source_selec_a_tester
        fin=(sum(test_fin$fin)==dim(test_fin)[1])
#        cat(paste("\nniveau : ",niveau,"\n",sep=""))
#        if(niveau==1) fin=TRUE
      } #end while
close(pb_niveau)
#r�int�gration des troncons qui ne sont jamais parcouru dans les parcours_source (d�fluence, canaux ...)  
# lui affecte le m�me rang de strahler que le troncon en amont de la d�fluence
#dans le cas des zones complexes (type marais) tous ne pourrons pas �tre "rattrap�" ainsi
#pb bas� sur noeud aval, hors il peut y avoir plusieurs troncons terminant sur le noeud aval !!!
 #     recuperable=troncon_selec[troncon_selec$nouveau_noeud_aval %in% troncon_vire$nouveau_noeud_amont,c("nouveau_noeud_aval","rang_strahler")]
#      
#      troncon_vire[troncon_vire$nouveau_noeud_amont %in% recuperable$nouveau_noeud_aval,]
#      troncon_vire[,"rang_strahler"]=troncon_selec[troncon_selec$nouveau_noeud_aval %in% troncon_vire$nouveau_noeud_amont,"rang_strahler"]
#      troncon_selec=ajout_data(troncon_selec,troncon_vire)
#r�integre le tout dans le jeu de donnee du bassin
  troncon_bassin_final=ajout_data(troncon_bassin_final,troncon_selec)
  } # end for(noeud_mer_a_tester
  close(pb_noeud)
  
  #m�moriser troncon_bassin_final + parcours ???
  troncon_bassin_final=troncon_bassin_final[-1,]
#  hist(troncon_bassin_final$rang_strahler)
#  plot(as.factor(troncon_bassin_final$rang_strahler),troncon_bassin_final$dmer_aval)
  save(troncon_bassin_final,file=paste("EDAcommun/data/amorce_mer/bassin final/source/bassin_",bassin,"_strahler.RData",sep=""))
    if(bassin<=3)
  {
    save(list=ls(pattern="parcours_source_"),file=paste("EDAcommun/data/amorce_mer/bassin final/source/bassin_",bassin,"_parcours_strahler.RData",sep=""))
  } else {
    liste_parcours=ls(pattern="parcours_source_")
    save(list=liste_parcours[1:trunc(length(liste_parcours)/2)],file=paste("EDAcommun/data/amorce_mer/bassin final/source/bassin_",bassin,"_parcours_strahler_1.RData",sep=""))
    save(list=liste_parcours[(trunc(length(liste_parcours)/2)+1):length(liste_parcours)],file=paste("EDAcommun/data/amorce_mer/bassin final/source/bassin_",bassin,"_parcours_strahler_1.RData",sep=""))
  }
} # end for bassin

#################
troncon_selec[,c("Id_BDCARTHAGE","rang_strahler", "niveau", "parcouru")]
write.table(troncon_selec,file="EDAcommun/data/amorce_mer/bassin final/source/bassin_test_strahler2.txt",sep="\t",row.names=FALSE)

write.table(troncon_bassin_final,file="EDAcommun/data/amorce_mer/bassin final/source/troncon_bassin_final_4_prov.txt",sep="\t",row.names=FALSE)
