# Nom fichier :        parcours_amont.R
# Projet :             EDA\traitement geographique\chainage
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             Laurent Beaulaton
# Contact :            laurent.beaulaton@onema.fr
# Date de creation :    15/02/2008 23:50:14
# Description :         prg pour parcours de la mer vers l'amont
# Data needed
# Packages needed :     RODBC 
# remarques
#
#**********************************************************************

##########
# chargement des donn�es et config
base_access="E:\\Mes documents\\Mes dossiers\\bd carthage\\bd carthage propre france\\export csv\\bd1.mdb"
source("EDAcommun/prg/traitement geographique/chainage/dedoublonnage_bord_mer.R")
source("EDAcommun/prg/traitement geographique/chainage/equivalence_noeud_troncon.R")
source("EDAcommun/prg/outils/ajout_data.R")
source("EDAcommun/prg/traitement geographique/chainage/parcours_1.R")
source("EDAcommun/prg/traitement geographique/chainage/doublons_noeud.R")
source("EDAcommun/prg/traitement geographique/chainage/propagation_dmer.R")
source("EDAcommun/prg/traitement geographique/chainage/propagation_dmer_1.R")

#chargement des troncon bdcarthage et des amorces "mer"
require(RODBC)
canal=odbcConnectAccess(base_access)
troncon_longueur=sqlFetch(canal,"troncon_longueur")
bord_mer=sqlFetch(canal,"noeud_mer")
odbcClose(canal)

#table des troncon restant � parcourir
troncon_longueur_restant=troncon_longueur


#constitution d'amorce aval (suppression des noeuds plus amont sur une m�me rivi�re)
# quand HA aura fini, ce cela stock� proprement directement dans la base
noeud_dist=dedoublonnage_bord_mer(bord_mer)
#noeud_dist=data.frame(Id_BDCARTHAGE=c(446000389,446000379),dmer=c(0,0),Id_BDCARTHAGE_mer=c(446000389,446000379),i=c(0,0))
#rownames(noeud_dist)=noeud_dist$Id_BDCARTHAGE

#pr�paration de la tables finale faisant les "chemins" 
troncon_=data.frame(Id_BDCARTHAGE_aval=-99, Id_BDCARTHAGE_amont=noeud_dist$Id_BDCARTHAGE, troncon=-99, longueur=0 , fn_dist=1)
troncon_$dmer_aval=-99
troncon_$dmer_amont=noeud_dist[as.character(troncon_$Id_BDCARTHAGE_amont),"dmer"]
troncon_$Id_BDCARTHAGE_mer=noeud_dist[as.character(troncon_$Id_BDCARTHAGE_amont),"Id_BDCARTHAGE_mer"]

nouveau_noeud=unique(noeud_dist) # dernier noeud int�gr�

#d�but boucle
reste=TRUE
i=0

while(reste){
  i=i+1
  cat(i)
  
  #selection des noeuds valides de 2� ordre
  nouveau_troncon_a_valider_amont=extract_troncon_depuis_noeud(noeud_ori=nouveau_noeud,troncon_a_extraire=troncon_longueur_restant,sens="amont")
  nouveau_troncon_a_valider_aval=extract_troncon_depuis_noeud(noeud_ori=nouveau_noeud,troncon_a_extraire=troncon_longueur_restant,sens="aval")

  if(dim(nouveau_troncon_a_valider_amont)[1]+dim(nouveau_troncon_a_valider_aval)[1]>0) 
  {  
    if(dim(nouveau_troncon_a_valider_amont)[1]>0)
    {
      troncon_integre1=parcours_1(nouveau_troncon_a_valider=nouveau_troncon_a_valider_amont,sens="amont")
      if(dim(nouveau_troncon_a_valider_aval)[1]>0)
      {
        troncon_integre2=parcours_1(nouveau_troncon_a_valider=nouveau_troncon_a_valider_aval,sens="aval")
        troncon_integre=ajout_data(troncon_integre1, troncon_integre2)
        rownames(troncon_integre)=NULL
      } else troncon_integre=troncon_integre1
    } else troncon_integre=parcours_1(nouveau_troncon_a_valider=nouveau_troncon_a_valider_aval,sens="aval")
    
    
    #gestion des doublons
    a=doublons_noeud(troncon_candidat=troncon_integre, noeud_existant=noeud_dist, troncon_existant=troncon_)    
    troncon_final=a$troncon
    noeud_final=a$noeud
    rm(a)
    noeud_final[noeud_final$Id_BDCARTHAGE %in% noeud_dist$Id_BDCARTHAGE,"i"]= noeud_dist[as.character(noeud_final[noeud_final$Id_BDCARTHAGE %in% noeud_dist$Id_BDCARTHAGE,"Id_BDCARTHAGE"]),"i"]

#--> propagation
propagation_dmer(noeud_a_verifier=noeud_final[noeud_final$i==i,])
noeud_final[noeud_final$Id_BDCARTHAGE_mer == 446000396,]
    #retrait des troncons parcourus
    troncon_longueur_restant=troncon_longueur_restant[!(troncon_longueur_restant$Id_BDCARTHAGE %in% troncon_final$troncon),]
    
  #
  #  if(dim(nouveau_troncon_a_valider_aval)[1]>0)
  #  {
  #  #errrr
  #  }
    
    nouveau_noeud = noeud_final[!(noeud_final$Id_BDCARTHAGE %in% noeud_dist$Id_BDCARTHAGE),]
  
  
    
    troncon_=troncon_final
    noeud_dist=noeud_final
    noeud_dist[noeud_dist$Id_BDCARTHAGE %in% nouveau_noeud$Id_BDCARTHAGE, "i"]=i
  } else reste=FALSE
  
  cat(paste("\t:\t",dim(nouveau_noeud)[1],"\t:\t",dim(troncon_longueur_restant)[1],"\n"))
}
  
require(RODBC)
canal=odbcConnectAccess(base_access)
sqlSave(canal,troncon_,"troncon_",rownames=FALSE)
sqlSave(canal,noeud_dist,"noeud_dist",rownames=FALSE)
odbcClose(canal)
