# Nom fichier :        parcours_source1.R
# Projet :             EDA/EDAcommun
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             C�dric Briand
# Contact :            cedric.biand@lavilaine.com
# Date de creation :    24/04/2008 11:08:23
# Description :         parcours du r�seau � l'aide programmation data.frame 
#                       PARCOURS DES SOURCES SUR LES PARCOURS LES PLUS COURTS SOURCE MER
#                        PUIS RECHERCHE (VERS L'AMONT) D'UN CROISEMENT AVEC UN PARCOURS SOURCE MER
#                       CETTE VERSION RECHERCHE UNE INSTANCE UNIQUE DE TOUS LES VOISINS
#                      +2 >> essai d'optimisation du code
# Data needed
# Packages needed :     
# remarques
#
#**********************************************************************

#fonction pour renvoyer la position de a dans b
#attention vecteur non ordonn�
ind=function(a,b){
    index=(1:length(b))[b%in%a]
    return(index)
}

# retourne l'index des valeurs r�p�t�es d'un vecteur
indrepeated=function(a){
    sol=match(unique(a),a)     #index des valeurs uniques
    rep=a[-sol]  # valeurs r�p�t�es
    return(ind(rep,a))   # index des valeurs r�p�t�es
}
# renvoit l'index des valeurs apparaissant une seule fois
induk=function(a){
    sol=match(unique(a),a)     #index des valeurs uniques
    return(sol)   # index des valeurs r�p�t�es
}

# parcours source : explication
# dans la table parcours les transformations sont les suivantes
        # noeud troncon
        # Mer NA          source 0             source 0
        # N3  t3    =>    N2    t1     =>      N2    t1
        # N2 t2           N3    t2             N3    t2+t1
        # source t1       mer    t3            mer    t3+t2+t1
 # a partir de l� les distances sont ajout�es dans dist_source_cum
 # et remplac�es si plus grandes dans dist_source_max 

# chargement des donn�es par bassin
#fichier=c("niveau_721_5")
#fichier="niveau_191_1"
#fichier=c("niveau_658_2","niveau_610_3")
#fichier=c("niveau_610_3")
#fichier="niveau_191_1"      ,"niveau_610_3"
fichier="niveau_441_6gilda"

for (z in 1:length(fichier)) {
  load(file=paste("EDAcommun/data/amorce_mer/",fichier[z],".RData",sep=""))
  noeuds[,c("dist_source_max")]=as.double(-1)
  noeuds[,"dist_source_cum"]=as.numeric(0)
  noeuds[,"noeud_source"]=as.character(NA)
  noeuds_source=noeuds[noeuds$situation=="source",]
  noeuds[noeuds$situation=="source","dist_source_max"]=0
  noeuds[noeuds$situation=="source","dist_source_cum"]=0
  noeuds[noeuds$situation=="source","noeud_source"]=  noeuds[noeuds$situation=="source","Id_BDCARTHAGE"]#eux m�mes
  
 ###############################################################################
 #                   PARCOURS DES NOEUDS A PARTIR DES SOURCE
 ###############################################################################
  
  
#parcours ligne par ligne
  for (i in 1:(dim(noeuds_source)[1])) {
        progress(i,dim(noeuds_source)[1])

        #recuperation du data.frame
        #!cat(paste("i =", i, "\n"))
        noeud_en_cours=noeuds_source[i,]
        parcoursCourant=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep=""))
        ### Traitement du parcours courant
        # on vire la source (voir explication en haut) :
        #!cat(noeud_en_cours$Id_BDCARTHAGE)
        #!cat("\n")
        index_noeud=match(rev(parcoursCourant$noeud)[-1],noeuds$Id_BDCARTHAGE)
        
        index_troncon=match(rev(parcoursCourant$troncon[-1]),troncon_total$id)  #parcoursCourant$troncon[-1] => le tron�on mer est NA
        
        
        id_troncontotal=troncon_total$id[index_troncon]
        # on r�cup�re les longeurs dans le data frame troncon_total :
        longueurs=troncon_total[index_troncon,"longueur"]
        
        
        # il y a des doublons dans tron�on
        # pour tester qu'il est bien sain de prendre la premi�re instance de chaque tron�on (a mettre en commentaire ensuite)
        # , cr�ation et parcours d'un data frame des valeurs r�p�t�es
        # et recherche de valeurs diff�rentes
        #if (length(index_noeud)!=length(index_troncon)){
#            dftest=data.frame("id"=as.character(id_troncontotal[indrepeated(id_troncontotal)]),"l"=longueurs[indrepeated(id_troncontotal)])
#            for (j in 1:nrow(dftest)) {
#                  if (sum(dftest[dftest$id%in%dftest$id[j],"l"])/2 !=dftest[j,"l"]) {
#                         stop(paste("j=",j,"il existe des distances diff�rentes pour une m�me valeur de tron�on"))
#                  }
#            }
#        }
        
        #longueurs=longueurs[induk(id_troncontotal)]
        stopifnot(length(longueurs)==(nrow(parcoursCourant)-1))
        longueurs=cumsum(longueurs)  # parcours de la source vers la mer
        #!cat("longueurs = \n")
        #!cat(longueurs)
       #!cat("\n")
        ## r�int�gration des distances source max
        # ----------------------------------------
        # tous les noeuds dont la distance source est sup�rieur � l'actuelle
        longueurs_sup=noeuds[index_noeud,"dist_source_max"]< longueurs
        index_noeuds=match(noeuds[index_noeud,][longueurs_sup,"Id_BDCARTHAGE"],noeuds$Id_BDCARTHAGE)
        #!cat(longueurs_sup)
        #!cat("\n")        
        if(sum(longueurs_sup)>0){
          noeuds[index_noeuds,"dist_source_max"]=longueurs[longueurs_sup]
          # on affecte les longueurs max
          noeuds[index_noeuds,"noeud_source"]=noeud_en_cours$Id_BDCARTHAGE
          # on affecte l'identifiant du noeud source en cours
        }
        #-------------------------------------------
        # cumul des distances source en amont d'un noeud (distance de tous les parcours source -mer)
        noeuds[index_noeud,"dist_source_cum"]=noeuds[index_noeud,"dist_source_cum"]+ longueurs
        #!stopifnot(!is.na(noeuds[index_noeuds,"noeud_source"]))
       
    } # end boucle dans bassin
    


    save(list=c("noeuds", "troncon_total", ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")]),file=paste("EDAcommun/data/",fichier[z],"s.RData",sep=""))
    # rm(list=c("noeuds", "troncon_total", ls(pattern="parcours_"))) 
 
 
 
 ###############################################################################
 #                   PARCOURS DES NOEUDS HORS SOURCE MER
 ###############################################################################
 
    ##|||||||||||||||||||||||||||||||||||||||||||||||||
    # pour lancement temporaire, d�commenter
    # pour cette boucle on a pas besoin des parcours, test pour savoir si cette version sans
    # les parcours acc�l�re les temps de calcul
    #z=1
#    fichier="niveau_1055_4"
#    load(file=paste("EDAcommun/data/amorce_mer/niveau_1055_4_1000ss.RData",sep=""))
#     #on enl�ve les parcours qui ne sont pas utilis�s
#    save(list= ls(pattern="parcours_")[ls(pattern="parcours_") %in% paste("parcours_",noeuds$Id_BDCARTHAGE,sep="")],file=paste("EDAcommun/data/parcours_",substr(fichier[z],8,nchar(fichier[z])),".RData",sep=""))
#    rm(list=c( ls(pattern="parcours_")))
    ##|||||||||||||||||||||||||||||||||||||||||||||||||
    
    
    
    
    index=noeuds$niveau!=-999 #=> les noeuds sont accessibles depuis la mer
    index1=noeuds$dist_source_max!=-1     #=> les noeuds sont sur les parcours source mer

    

    #!cat(paste("noeuds parcourus :" ,round(100*sum(index&index1)/nrow(noeuds)),"%\n"))
    #!cat(paste("noeuds hors parcours amont aval :", round(100*sum(index&!index1)/nrow(noeuds)),"%\n"))
    #!cat(paste("noeuds non parcourus depuis la mer :" ,round(100*sum(!index)/nrow(noeuds),2),"%\n"))
    noeuds_a_faire=noeuds[index&!index1,]     # les candidats pour une deuxi�me moulinette
    noeuds_parcouru=noeuds[index&index1,]   # les noeuds sur un parcours source mer
    noeuds_parcouru=noeuds_parcouru$Id_BDCARTHAGE
    #index_troncon=(1:dim(troncon_total)[1])[troncon_total$noeud_aval %in% noeuds_a_faire$Id_BDCARTHAGE] ### !!!!! match fait le premier match uniquement
    #troncon_a_faire=troncon_total[index_troncon,] 
   
    liste_recherche=list()
    
    # �������������������� ATTENTION VERIFIER LE n   �������������������������
    for(n in 1:dim(noeuds_a_faire)[1])
    {
      progress(n,dim(noeuds_a_faire)[1])
      #! cat(paste("n=",n,"\n"))
      noeuds_en_cours=noeuds_a_faire[n,]
     #! cat(paste(noeuds_en_cours$Id_BDCARTHAGE,"\n"))
      # au dela du premier passage dans la boucle while, le noeuds_en_cours peut contenir plusieurs noeuds
      jonction_trouvee=FALSE
      
      niveau=0
      parcours_unique=vector()
      while( !jonction_trouvee){
      
          niveau=niveau+1          
          voisin=troncon_total[troncon_total$noeud_aval %in% noeuds_en_cours$Id_BDCARTHAGE,]
          voisin=voisin[!voisin$noeud_amont%in%parcours_unique,] 
                        
          parcours_unique=c(parcours_unique,voisin$noeud_amont)
          parcours_unique=parcours_unique[induk(parcours_unique)]      # pour limiter les doublons et le grossissement de l'object
          liste_recherche[[niveau]]=voisin                #sert pour le parcours � l'envers � la fin
          #!cat(paste("niveau =", niveau,"voisin="))
          #!cat(voisin$noeud_amont)
          #!cat("\n")
          
          #contr�le si pas de voisin identique (ie : deux tron�ons m�nent au m�me noeud amont)
#          if(length(unique(voisin$noeud_amont))<dim(voisin)[1])
#          {
#                detect=names(table(voisin$noeud_amont)[table(voisin$noeud_amont)>1]) #r�cup�re le nom du noeud amont coupable
#                for (i in 1:length(detect))
#                {
#                  indice=voisin$noeud_amont == detect[i]
#                  mini=which.min(voisin[indice,"longueur"]) #s�lectionne la distance la plus courte (donne l'indice du premier)
#                  doublon=indice[indice]     #que les doublon
#                  doublon[mini]=FALSE        #tout ce qui n'est pas mini = TRUE         
#                  n_ligne=(1:length(indice))[indice] #numero de ligne des doublons
#                  voisin=voisin[-n_ligne[doublon],] #on supprime les lignes doublons
#                } # end for
#          } # end if
          if(!is.null(voisin$noeud_amont))  #du fait de la r�indexation voisin peut �tre null data.frame with n rows
          { 
          # SELECTION DU MEILLEUR CROISEMENT SI EXISTE
          ########################################################
          if(sum(voisin$noeud_amont %in% noeuds_parcouru)>0)
              { # il existe au moins un noeud sur un parcours
            
                 id=match(voisin$noeud_amont[voisin$noeud_amont %in% noeuds_parcouru],noeuds$Id_BDCARTHAGE)
                  # comme on est sur un bassin il peut manquer un ou plusieurs noeuds
                  id=id[!is.na(id)]
                  noeuds_sur_parcours=noeuds[id,]    #liste des voisins amont appartenant � un parcours
    
                  # DETECTION DES NOEUDS SUR LE MEME PARCOURS => ON VIRE LE NOEUD LE PLUS EN AVAL
    
                  if (length(id )>1&length(id)!=length(unique(noeuds_sur_parcours[,"noeud_source"])))
                  { # il faut au moins deux noeuds
                      parcours_possibles=unique(noeuds_sur_parcours[,"noeud_source"])
                      
                         for (m in parcours_possibles){
                               nspm=noeuds_sur_parcours[noeuds_sur_parcours$noeud_source==m,]
                               nsp=noeuds_sur_parcours[noeuds_sur_parcours$noeud_source!=m,]
                               nspm=nspm[which.min(nspm[,"dist_source_max"]),]
                               noeuds_sur_parcours=rbind(nsp,nspm)
                                  # on vire parmis ce parcours possible les noeuds aval
                                  warning(paste(
                                    "noeud :",noeuds_a_faire[n,"Id_BDCARTHAGE"]," de source",m,
                                    "deux noeuds sur le m�me parcours"))
                         } # end for m
                  } # end if
                  
                  
                  # SELECTION DU NOEUD AYANT LA DISTANCE SOURCE MAX
                  
                  noeuds_sur_parcours=
                        noeuds_sur_parcours[which.max(noeuds_sur_parcours[,"dist_source_max"]),]
                  stopifnot (nrow( noeuds_sur_parcours)==1)               
                         
                 
                  # CREATION D'UN NOUVEAU PARCOURS AVAL ENTRE LE CROISEMENT ET LE NOEUD NON PARCOURU

                    # r�cup�ration de la longueur cumul�e au noeud de croisement avec un parcours
                  longueur_crois=noeuds_sur_parcours$dist_source_max
                  noeud_crois=noeuds_sur_parcours$Id_BDCARTHAGE  # vecteur des noeuds depuis le croisement en cours de cr�ation
                  #!cat(paste("noeud_croisement"=noeud_crois,"longueur=",longueurcum,"\n"))
                                    
                  for (i in (niveau):1){
                   # niveau =niveaux de la boucle while
                   # on utilise les noeuds amont et aval du tron�on pour refaire le trajet dans l'autre sens
                       indexvoisin=match(noeud_crois[length(noeud_crois)],liste_recherche[[i]]$noeud_amont)
                       noeud_crois=c(noeud_crois,liste_recherche[[i]]$noeud_aval[indexvoisin])  #vecteur
                       longueur_crois=c(longueur_crois,liste_recherche[[i]]$longueur[indexvoisin])# vecteur
                  }
                  noeud_crois=noeud_crois[-1]   # on ne garde pas la premi�re ligne qui correspondait � l'intersection
                  longueur_crois=cumsum(longueur_crois)[-1] # on ne garde pas la premi�re ligne qui correspondait � l'intersection
                  #!cat("longueurcum=")
                  #!cat(longueurcum)
                  #!cat("\n")
                  #!cat(noeud)
                  #!cat("\n")
                  
                    # REMPLACEMENT DES DISTANCES SUPERIEURES ET  CALCUL DU CUMUL DES DISTANCES SOURCE

                  index_noeud=match(noeud_crois,noeuds$Id_BDCARTHAGE)  
                  longueurs_sup=noeuds[index_noeud,"dist_source_max"]< longueur_crois
                  index_noeuds=match(noeuds[index_noeud,][longueurs_sup,"Id_BDCARTHAGE"],noeuds$Id_BDCARTHAGE)


                    
                  if(sum(longueurs_sup)>0){
                      noeuds[index_noeuds,"dist_source_max"]=longueur_crois[longueurs_sup]
                      #!cat("distances source max \n")
                      #!cat(noeuds[index_noeuds,"dist_source_max"])
                      # on affecte les longueurs max depuis le croisement
                      noeuds[index_noeuds,"noeud_source"]=noeuds_sur_parcours$noeud_source
                      # la source a �t� r�cup�r�e sur le parcours au dessus
                 } # end boucle if
                #-------------------------------------------
                # cumul des distances source en amont d'un noeud (distance de tous les parcours source -mer)
                noeuds[index_noeud,"dist_source_cum"]=noeuds[index_noeud,"dist_source_cum"]+ longueur_crois
                #!print(noeuds[index_noeud,"dist_source_cum"])       
                noeuds_parcouru=c(noeuds_parcouru,noeuds[index_noeud,"Id_BDCARTHAGE"])  # on rajoute tous noeuds au noeud pr�c�dent
                noeuds_parcouru=unique(noeuds_parcouru) # on vire les doublons
                    # on sort de la boucle while
               jonction_trouvee=TRUE
          ########################################################
          } else  
          { #pas de noeud sur le parcours on augmente d'une �tape
              id=match(voisin$noeud_amont,noeuds$Id_BDCARTHAGE) 
              noeuds_en_cours=noeuds[id,]
              jonction_trouvee=FALSE
              #!cat("passe par l� \n")
              if (niveau>50)  jonction_trouvee=TRUE
               
          } # end boucle if testant l'arriv�e � un croisement avec un parcours mer-source
          ########################################################
        }  else  # end boucle if voisin !=0
          {
          #!cat("passe par ici \n")
              jonction_trouvee=TRUE  # on ne continue pas on a tout parcouru pas de source tagg�e en amont
          }
          #  end boucle else testant l'arriv�e � une source
    } # boucle while jonction_trouvee
     if (n/1000==round(n/1000))
   {
   save(list=c("noeuds", "troncon_total"),file=paste("EDAcommun/data/",fichier[z],"_",n,"ss.RData",sep=""))
  }
    
  } # boucle for sur le niveau
      save(list=c("noeuds", "troncon_total"),file=paste("EDAcommun/data/",fichier[z],"_",n,"ss.RData",sep=""))
       write.table(noeuds,file=paste("EDAcommun/data/",fichier[z],"_",n,"ss.txt",sep=""),sep="\t",row.names=FALSE,col.names=TRUE)
      #rm(list=c("noeuds", "troncon_total", ls(pattern="parcours_")))
} # end boucle for Z entre bassins
        
 

