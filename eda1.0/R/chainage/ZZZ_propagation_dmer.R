# Nom fichier :        propagation_dmer.R
# Projet :             EDA\traitement geographique\chainage
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             Laurent Beaulaton
# Contact :            laurent.beaulaton@onema.fr
# Date de creation :    22/02/2008 11:24:15
# Description :         propagation des nouvelles dmer sur tous les troncons, v�rifie si cela entraine des modif des noeuds d�j� fait et fait les modif
# Data needed
# Packages needed :     
# remarques
#
#**********************************************************************

propagation_dmer=function(noeud_a_verifier)
{
  #impl�mente les nouvelles dmer sur les troncons correspondant
  sens_="amont"
  troncon_a_verifier=extract_troncon_depuis_noeud(noeud_ori=noeud_a_verifier,troncon_a_extraire=troncon_longueur,sens="aval")
  troncon_a_verif=parcours_1(nouveau_troncon_a_valider=troncon_a_verifier,sens=sens_,noeud_dist_=noeud_dist)
  a=propagation_dmer_1(troncon_a_verif)
}

