# Nom fichier :        tag_source.R
# Projet :             EDA/EDAcommun
# Version :            1.0
# Organisme :          ONEMA
# Auteur :             C�dric Briand
# Contact :            cedric.biand@lavilaine.com
# Date de creation :    24/04/2008 11:08:23
# Description :
# Data needed
# Packages needed :
# remarques
#
#**********************************************************************
 fichier="niveau_192_1gilda"
 load(file=paste("EDAcommun/data/amorce_mer/",fichier,".RData",sep=""))
   #=> les noeuds sont sur les parcours source mer
   #!cat(paste("noeuds parcourus :" ,round(100*sum(index&index1)/nrow(noeuds)),"%\n"))
    #!cat(paste("noeuds hors parcours amont aval :", round(100*sum(index&!index1)/nrow(noeuds)),"%\n"))
    #!cat(paste("noeuds non parcourus depuis la mer :" ,round(100*sum(!index)/nrow(noeuds),2),"%\n"))

count=list()
nb=0
for (i in rev(sort(unique(noeuds$niveau))[-1] ))
{
 cat(paste("i=",i,"\n"))
# i=  rev(sort(unique(noeuds$niveau))[-1] )[2]
  index=noeuds$niveau==i & noeuds$situation!="source"
  noeuds_a_faire=noeuds[index,]
   if(dim(noeuds_a_faire)[1]!=0) {
    for(n in 1:dim(noeuds_a_faire)[1])
    {
      #progress(n,dim(noeuds_a_faire)[1])

      noeuds_en_cours=noeuds_a_faire[n,]
      cat(paste(noeuds_en_cours$Id_BDCARTHAGE)," ")

     voisin=troncon_total[troncon_total$noeud_aval %in% noeuds_en_cours$Id_BDCARTHAGE,]
     voisin_noeud=noeuds[noeuds$Id_BDCARTHAGE%in%voisin$noeud_amont,]
        if ((noeuds_en_cours$distance_mer) > max(voisin_noeud$distance_mer,na.rm=TRUE))
            {  noeuds_en_cours$situation="source"
            cat(paste("=","source "))
            nb=nb+1
            }
         cat("\n")
         } #for n
    count[[i]]=nb
    cat("\n")
  } #if
} # fori