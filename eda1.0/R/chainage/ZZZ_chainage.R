#setwd(choose.dir(caption="choisir un dossier de travail"))
#base_access=choose.files(filters=matrix(c("base Access","*.mdb"),ncol=2))
#bord_mer=file.choose()
setwd("E:/Mes documents/Mes dossiers/bd carthage/bd carthage propre france/export csv")
base_access="E:\\Mes documents\\Mes dossiers\\bd carthage\\bd carthage propre france\\export csv\\bd1.mdb"
#bord_mer="E:\\Mes documents\\Mes dossiers\\bd carthage\\bd carthage propre france\\export csv\\noeud_mer.csv"


require(RODBC)
canal=odbcConnectAccess(base_access)
#list_tables=sqlTables(canal)
troncon_total=sqlFetch(canal,"TRONCON_HYDROGRAPHIQUE")
troncon_relie=sqlFetch(canal,"troncon relie")
noeud_valide_2=sqlFetch(canal,"noeud_mer")
odbcClose(canal)

troncon_relie_U=unique(troncon_relie$Id_BDCARTHAGE)
troncon_total_U=unique(troncon_total[,c("Id_BDCARTHAGE","Id_BDCARTHAGE_Noeud_Initial","Id_BDCARTHAGE_Noeud_Final")])

#extraction des noeuds valides depuis la table des troncons reli�s
noeud_valide_1=unique(c(troncon_total_U[troncon_total_U$Id_BDCARTHAGE %in% troncon_relie_U,"Id_BDCARTHAGE_Noeud_Initial"],troncon_total_U[troncon_total_U$Id_BDCARTHAGE %in% troncon_relie_U,"Id_BDCARTHAGE_Noeud_Final"]))
#extraction des noeuds valides depuis les bords de mers
#noeud_valide_2=read.csv(bord_mer,header=TRUE)$Id_BDCARTHAGE
noeud_valide=unique(c(noeud_valide_1, noeud_valide_2))
#--> 293 226 noueds

condition_sortie=TRUE
i=0

while(condition_sortie){
  i=i+1
  cat(i)
  #selection des noeuds valides de 2� ordre
  nouveau_noeud_valide=unique(troncon_total_U[troncon_total_U$Id_BDCARTHAGE_Noeud_Final %in% noeud_valide,"Id_BDCARTHAGE_Noeud_Initial"])
  
  #compte ceux qui ne sont pas d�j� valide
  somme_nouveau=sum(!nouveau_noeud_valide %in% noeud_valide)
  cat(paste("\t: ",somme_nouveau,"\n"))
  condition_sortie=somme_nouveau>0
  
  #nouvelle liste de noeud_valide
  noeud_valide=unique(c(nouveau_noeud_valide,noeud_valide))
}

#62 it�rations --> 446507

#troncon valide
troncon_valide=troncon_total_U[troncon_total_U$Id_BDCARTHAGE_Noeud_Final %in% noeud_valide,]
dim(troncon_valide)[1]/dim(troncon_total_U)[1]
dim(troncon_valide)[1]-dim(troncon_relie)[1]

canal=odbcConnectAccess(base_access)
sqlSave(canal,troncon_valide,"troncon_valide3",rownames=FALSE)
sqlSave(canal,data.frame(Id_BDCARTHAGE=noeud_valide),"noeud_valide3",rownames=FALSE)
odbcClose(canal)