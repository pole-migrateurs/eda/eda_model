#tab1=barglobal   
#tab2=noeudglobal     
#X_tab1="ouv_abscisse" 
#Y_tab1="ouv_ordonnee"
#val_tab1="ouv_id" 
#X_tab2="X" 
#Y_tab2="Y"
#new_val_tab2="ouv_id" 
#default_new_val_tab2=NA



sql=paste("select * from t_ouvrage_ouv ")
barglobal=fn_sql_0(sql,baseODBC=baseODBCbar,uid=uid,pwd=pwd,silent=FALSE)[[1]]
if (class(barglobal)!="data.frame") stop("probl�me au chargement de la table ouvrage")
barglobal=killfactor(barglobal)
  # chargement de la base barrage
#! je ne sais pas si il va falloir charger directement sur la base access la base de
# 

canal=odbcConnectAccess("EDAcommun/data/Cedric_bd1.mdb")
noeudglobal=sqlFetch("channel"=canal,"new_obstacles_uniques")
odbcClose(canal)
noeudglobal=killfactor(noeudglobal)
noeudglobalLB=noeudglobal[noeudglobal$Bassin=="Loire-Bretagne",]
corresXY=function(tab1,  # data.frame
                  tab2,     #data.frame
                  X_tab1,  #texte id du pivot
                  Y_tab1,
                  val_tab1, #texte id de la colonne sur laquelle la valeur est demand�e
                  X_tab2,  #texte id du pivot
                  Y_tab2,
                  new_val_tab2, #nom de la nouvelle colonne
                  default_new_val_tab2=NA)
                  {
    les_doubles=vector
    tab2[,new_val_tab2]=default_new_val_tab2
    for (i in 1:nrow(tab1)){
            progress(i,nrow(tab1))
          for (j in 1:nrow(tab2)){
            if (tab1[i,X_tab1]==tab2[j,X_tab2] & tab1[i,Y_tab1]==tab2[j,Y_tab2]) 
                {
                if (!is.na(tab2[j,new_val_tab2]))
                    {
                  cat(paste(tab1[j,val_tab1],":",tab2[j,new_val_tab2],"\n"))
                  les_doubles=c(les_doubles,paste(tab1[j,val_tab1],":",tab2[j,new_val_tab2]))
                    }
            tab2[j,new_val_tab2]=tab1[j,val_tab1]
                }
            }  # boucle for j   
          } # boucle for i   
    return(list("dfresult"=tab2,"vectdoubles"=les_doubles))
}
barglobal=corresXY(tab1=noeudglobalLB,  # data.frame
                  tab2=barglobal,     #data.frame
                  X_tab1="X",  #texte id du pivot
                  Y_tab1="Y",
                  val_tab1="ID_final", #texte id de la colonne sur laquelle la valeur est demand�e
                  X_tab2="ouv_abscisse",  #texte id du pivot
                  Y_tab2="ouv_ordonnee",
                  new_val_tab2="ID_final", #nom de la nouvelle colonne
                  default_new_val_tab2=NA) 

save(barglobal,file=paste("EDAcommun/data/liste_corresp_barrages.RData",sep=""))
#les donn�es vont �tre r�int�gr�es par Marion