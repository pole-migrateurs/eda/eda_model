############################################################
# manipulation des donn�es
############################################################

#chargement des donn�es
load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet.RData")

  ers_full$X=as.numeric(ers_full$X)
 ers_full$Y=as.numeric(ers_full$Y)
#correction jeu de donn�es
#ers_full[ers_full$distance_source_station<0,"distance_source_station"]=NA
#ers_full[ers_full$domaine_piscicole=="S","domaine_piscicole"]="s"
#ers_full[ers_full$domaine_piscicole=="","domaine_piscicole"]=NA

ers_full[ers_full$effectif==0,c("nb_150","nb_150_300","nb_300_450","nb_450_600","nb_600_750","nb_750_900","nb_900")]=0

#distance relative
ers_full$distance_relative = ers_full$distance_mer / (ers_full$distance_mer + ers_full$distance_source)

#presence de p�cheurs
ers_full$presence_civelle=(ers_full$civelle_pro+ ers_full$civelle_amateur)!=0
ers_full$presence_fluviaux=(ers_full$pro_fluviaux)!=0
ers_full$presence_amat_engin=ers_full$amat_engin!=0

#selection des m�thodes de prospection
#table(ers_full$methode_prospection)
ers_full=ers_full[!(ers_full$methode_prospection %in% c("EPA","partielle sur toute la largeur","placettes","traits","autres","faci�s","ambiances")),]
ers_full$methode_prospection=as.factor(as.character(ers_full$methode_prospection))

tapply(ers_full$profondeur,ers_full$methode_prospection,quantile,na.rm=TRUE,prob=c(0,0.5,0.75,.95,1))


#permet de remplacer les valeurs manquantes d'une colonne par la m�diane
funremplace_val_manq_autres_stations=function(data,nomcolonnearemplacer,nomcolonnestation){
  # calcul des m�dianes sur toutes ope des stations
 med=tapply(data[,nomcolonnearemplacer],data[,nomcolonnestation],median,na.rm=TRUE)
 med=med[!is.na(med)]
 # recherche de la correpondance entre les num�ros de station
 # des lignes sans valeur et les medianes
 index=match(as.numeric(as.character(data[,nomcolonnestation]))[is.na(data[,nomcolonnearemplacer])],as.numeric(names(med)))
 # quelles donn�es sont remplacables
 index2=!is.na(index)
 print(paste("nombre de valeurs remplac�es =", sum(!is.na(index))," sur",length(index)))
 data[,nomcolonnearemplacer][is.na(data[,nomcolonnearemplacer])][index2]<-med[index[index2]]
 return( data)
}

#remplace les surfaces manquantes
ers_full<-funremplace_val_manq_autres_stations(ers_full,"surface_station", "code_station")

#cr�e un champ effort pour relativiser le nb de poisson pris
ers_full[ers_full$methode_prospection=="compl�te","effort"]=ers_full[ers_full$methode_prospection=="compl�te","surface_station"]
ers_full[ers_full$methode_prospection=="partielle sur berges","effort"]=ers_full[ers_full$methode_prospection=="partielle sur berges","longueur_station"]*2 #0,5m de la berge + 1,5m d'efficacit�
ers_full[ers_full$methode_prospection=="Stratifi�e par Points (grand milieu)","effort"]=ers_full[ers_full$methode_prospection=="Stratifi�e par Points (grand milieu)","surface_station"] # (Belliard et al., 2007)


# pour avoir toujours le m�me nb de donn�es            
jeu_complet=apply(ers_full[,!(names(ers_full)%in% c("estime","largeur_station","profondeur_station","longueur_station","surface_station"))],1,function(x) sum(is.na(x)))==0  & (ers_full$effort < 2500) & (ers_full$annee>1980) & (ers_full$annee<2008) #& (ers_full$bassin!=1) #& (ers_full$bassin!=6)
                              
#nb de lignes compl�tes
sum(jeu_complet)





#extrait les p�ches compl�tes o� l'effectif a �t� estim�
possibilite_estime=!is.na(ers_full[jeu_complet,"estime"]) & ers_full[jeu_complet,"methode_prospection"]=="compl�te" 
a_reinserer=ers_full[jeu_complet,][possibilite_estime,]
a_reinserer$methode_prospection="compl�te.estime"
#hypo = efficacit� idem sur chaque classe de taille
a_reinserer$nb_150=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_150 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_150_300=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_150_300 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_300_450=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_300_450 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_450_600=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_450_600 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_600_750=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_600_750 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_750_900=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_750_900 / a_reinserer$effectif * a_reinserer$estime))
a_reinserer$nb_900=ifelse(a_reinserer$effectif==0,0,round(a_reinserer$nb_900 / a_reinserer$effectif * a_reinserer$estime))

#constitue un jeu final o� les p�che compl�te ont � la fois le premier passage et l'effectif estim� (doublon des op�rations donc)
levels(ers_full$methode_prospection)=c(levels(ers_full$methode_prospection),"compl�te.estime")
jeu_final=ajout_data(ers_full[jeu_complet,],a_reinserer)
#jeu_final=a_reinserer     #options que les p�che compl�tes estim�es
plot(ers_full$effectif,ers_full$estime)

table(jeu_final$methode_prospection)

#calcul des densit�s (#/100m�)
jeu_final$d_150=jeu_final$nb_150/jeu_final$effort*100
jeu_final$d_150_300=jeu_final$nb_150_300/jeu_final$effort*100
jeu_final$d_300_450=jeu_final$nb_300_450/jeu_final$effort*100
jeu_final$d_450_600=jeu_final$nb_450_600/jeu_final$effort*100
jeu_final$d_600_750=jeu_final$nb_600_750/jeu_final$effort*100
jeu_final$d_750_900=jeu_final$nb_750_900/jeu_final$effort*100
jeu_final$d_900=jeu_final$nb_900/jeu_final$effort*100
jeu_final$d=jeu_final$effectif/jeu_final$effort*100
jeu_final$nb_450=rowSums(jeu_final[,c("nb_450_600","nb_600_750","nb_750_900","nb_900")],na.rm=T)
jeu_final$d_450=rowSums(jeu_final[,c("d_450_600","d_600_750","d_750_900","d_900")],na.rm=T)

# choix des stations ou le transport des civelles a �t� effectu�

annee_transport=c(1997,1998,1998,1998,1999)
station=c('04410033','04180035','04180047','04450013','04030012')
 codes_ope_a_retirer=vector()
for (i in 1:length(station)){
codes_ope=jeu_final$code_operation[as.character(jeu_final$code_station)%in%station[i] ]

annees=jeu_final$annee[as.character(jeu_final$code_station)%in%station[i] ]>annee_transport[i]

 cat(paste("i=",i,"nbope=",length(codes_ope),"nbope>date=",sum(jeu_final$code_operation%in%codes_ope[annees]),"\n"))
 codes_ope_a_retirer=c(codes_ope_a_retirer,codes_ope[annees])

}
# soit on vire la station
#jeu_final=jeu_final[!jeu_final$code_operation%in%codes_ope_a_retirer,]
# soit on rajoute un facteur
jeu_final$transport=FALSE
jeu_final[jeu_final$code_operation%in%codes_ope_a_retirer,"transport"]=TRUE
#write.table(jeu_final,file="EDALB/data/extraction_parametre_Marion/jeu_final.txt",col.names = TRUE,sep=";")

jeu_final$coefftr=jeu_final$cumul_expertise0*1+jeu_final$cumul_expertise1*3+ jeu_final$cumul_expertise2*7+jeu_final$cumul_expertise3*20+jeu_final$cumul_expertise4*55+jeu_final$cumul_expertise5*150
jeu_final$coefftr4=jeu_final$cumul_expertise0*0+jeu_final$cumul_expertise1*0.1+ jeu_final$cumul_expertise2*0.4+jeu_final$cumul_expertise3*2+jeu_final$cumul_expertise4*10

jeu_final$coeff=jeu_final$cumul_expertise0+jeu_final$cumul_expertise1+ jeu_final$cumul_expertise2+jeu_final$cumul_expertise3+jeu_final$cumul_expertise4



jeu_final$zone=substr(jeu_final$cgenelin,1,2)

jeu_final$zone1=substr(jeu_final$zone,0,1)

jeu_final$zone[jeu_final$zone=="J-"|jeu_final$zone=="J7"|jeu_final$zone=="J8"|jeu_final$zone=="J9"]="4VIL"
jeu_final$zone[jeu_final$zone=="J4"|jeu_final$zone=="J5"|jeu_final$zone=="J6"]="3BRE_sud"

jeu_final$zone[jeu_final$zone=="J2"|jeu_final$zone=="J3"]="1BRE_ouest"
jeu_final$zone[jeu_final$zone=="J0"|jeu_final$zone=="J1"]="0BRE_Nord"
jeu_final$zone1[jeu_final$zone1=="J"]=jeu_final$zone[jeu_final$zone1=="J"]
jeu_final$zone1[jeu_final$zone1=="K"|jeu_final$zone1=="L"|jeu_final$zone1=="M"|jeu_final$zone1=="-"]="5LOIRE"
jeu_final$zone1[jeu_final$zone1=="N"]="6SEVRE"
#jeu_final$zone1[jeu_final$zone1=="Z"]="7Iles"
summary(as.factor(jeu_final$zone1))
jeu_final$zone1=as.factor(jeu_final$zone1)


#save(jeu_final,file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet_traite.RData")
load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet_traite.RData")
############################################################
# statistiques descriptives et graph
############################################################
corjeufinal=jeu_final[,c(
"surface_station" ,
"cumul_expertise",
"cumul_expertise0" ,
"cumul_expertise1",
"cumul_expertise2",
"cumul_expertise3",
"cumul_expertise4",
"cumul_expertise5",
"nb_barrages",
"cumul_score", 
"bio01",
"bio08",
"bio09" ,                 
"bio10",
"bio11",           
"altitude"  ,
 "pente",
 "amat_ligne",
 "amat_engin" ,            
"distance_mer",
 "nb_barrages" , 
"distance_source",   
"pro_fluviaux" ,
"civelle_pro"  ,
"distance_relative")]
 cor=symnum(clS <-cor(corjeufinal,use = "complete"))
print(cor)
#corr�lation entre variables temp�ratures

# correlation barrages
corjeufinal=jeu_final[,c(
"nb_barrages",
"cumul_score",
"cumul_expertise", 
"bio01",
"bio08",
"bio09" ,                 
"bio10",
"bio11",                
"distance_mer",
"distance_relative")]
 cor=symnum(clS <-cor(corjeufinal,use = "complete"))
print(cor)



#corr�lation temp-altitude
plot(jeu_final$altitude, jeu_final$bio01)
cor.test(jeu_final$altitude, jeu_final$EXTRACT_BI,method="spearman")

plot(ers_full$altitude,ers_full$pente)
cor.test(ers_full$altitude,ers_full$pente,method="spearman")

plot(jeu_final$distance_relative,jeu_final$bio10)
cor.test(ers_full$dist_mer,ers_full$altitude,method="spearman")

plot(ers_full$distance_mer,ers_full$nb_barrages_aval)
cor.test(ers_full$distance_mer,ers_full$nb_barrages_aval,method="spearman")

plot(jeu_final$annee,jeu_final$altitude)
cor.test(ers_full$dist_mer,ers_full$nb_150,method="spearman")

plot(ers_full$altitude,log(ers_full$nb_150+1))
cor.test(ers_full$altitude,ers_full$nb_150,method="spearman")

plot(ers_full$nb_barrages_aval,log(ers_full$nb_150+1))
cor.test(ers_full$nb_barrages_aval,ers_full$densite,method="spearman")

(table(ers_full$surface_station))

#stat sur les NA par variable
apply(ers_full[,],2,function(x) sum(is.na(x)))

hist(jeu_final$distance_mer)
hist(jeu_final$distance_source_station)
hist(jeu_final$distance_relative)


plot(jeu_final$distance_mer,jeu_final$altitude,col=ifelse(jeu_final$nb_450==0,"black","blue"),pch="+")
plot(jeu_final$distance_mer,jeu_final$altitude,col=ifelse(jeu_final$nb_150_300==0,"black","blue"),pch="+")
points(jeu_final$distance_mer,jeu_final$altitude,col=ifelse(jeu_final$nb_150==0,NA,"red"))

plot(jeu_final$distance_mer,jeu_final$nb_barrages_aval,col=ifelse(jeu_final$nb_450==0,"black","blue"),pch="+")
plot(jeu_final$distance_mer,jeu_final$nb_barrages_aval,col=ifelse(jeu_final$nb_150_300==0,"black","blue"),pch="+")
points(jeu_final$distance_mer,jeu_final$nb_barrages_aval,col=ifelse(jeu_final$nb_150==0,NA,"red"))

par(mar=c(4,4,0,0)+.1,cex=2)
hist(jeu_final$annee,xlab="year",ylab="# of fishing operation",main="")
hist(ers_full$annee,xlab="year",ylab="# of fishing operation",main="")

table(jeu_final$bassin)

table(jeu_final$amat_ligne)

plot(a_reinserer$effectif,a_reinserer$estime)
cor.test(a_reinserer$effectif,a_reinserer$estime)
summary(lm(a_reinserer$estime~a_reinserer$effectif))

with(jeu_final[jeu_final$effectif>0,],hist(effectif/surface_station*100))

############################################################
# mod�le
############################################################
summary(jeu_final)

### Stat sur expertise
#hist(jeu_final$pourc_NA_expertise)         
#plot((1:dim(jeu_final)[1])/dim(jeu_final)[1],sort(jeu_final$pourc_NA_score,decreasing=TRUE),ylab="%NA score",xlab="%op�rations")
#matrix(c(seq(0,4500,500)[-1],sort(jeu_final$pourc_NA_expertise,decreasing=FALSE)[seq(0,4500,500)]),nrow=2,byrow=T)   
#donc en virant 79 � 85% de NA on vire 1000 � 1500 stations

### Stat sur score
#hist(jeu_final$pourc_NA_score)         
#plot(sort(jeu_final$pourc_NA_score,decreasing=TRUE),(1:dim(jeu_final)[1])/dim(jeu_final)[1],xlab="%NA score",ylab="%op�rations")
#matrix(c(seq(0,4500,500)[-1],sort(jeu_final$pourc_NA_score,decreasing=FALSE)[seq(0,4500,500)]),nrow=2,byrow=T)  
#donc en virant 79 � 85% de NA on vire 1000 � 1500 stations
#sort(jeu_final�pourc_NA_score,decreasing=False)
 # matrix(c(seq(0,4500,500)[-1],sort(jeu_final$pourc_NA_score,decreasing=FALSE)[seq(0,4500,500)]),nrow=2,byrow=T) 
### Stat sur d�nivel�
#hist(jeu_final$pourc_NA_denivele)         
#plot(sort(jeu_final$pourc_NA_denivele,decreasing=TRUE),(1:dim(jeu_final)[1])/dim(jeu_final)[1],xlab="%NA denivele",ylab="%op�rations")
#matrix(c(seq(0,4500,500)[-1],sort(jeu_final$pourc_NA_denivele,decreasing=TRUE)[seq(0,4500,500)]),nrow=2,byrow=T) 
#donc en virant 70 � 80% de NA on vire 1000 � 1500 stations

matrix(c(seq(0,4500,500)[-1],sort(jeu_final[,"pourc_NA_score"],decreasing=FALSE)[seq(0,4500,500)]),nrow=2,byrow=T)  
#On choisit de ne garder que 500 stations (NA=421%) 

#### Cr�ation d'un nouveau jeu de donn�es jeu_final_bar qui ne prend en compte que les stations qui ont 50% de NA ou moins
station_score_na=(jeu_final$pourc_NA_score<0.5)#Qu'on prenne pourc_NA sur score ou expertise revient au mm -->tr�s bonne corr�lation

# mod�le complet
#


#distribution densit� et nb
hist(jeu_final[,"d_450_600"],breaks=1000)
hist(jeu_final[,"d_450_600"],xlim=c(0,10),breaks=1000)
hist(jeu_final[jeu_final$nb_450_600>0,"d_450_600"],xlim=c(0,10),1000)
plot(table(jeu_final[,"nb_450_600"]))
plot(table(jeu_final[jeu_final$nb_450_600>0,"nb_450_600"]))
# --> on choisit de mod�liser densit� avec mod�le delta-gamma

source("EDAcommun/prg/outils/aide_modele.R")

runif(1) #pb initialisation de random.seed

plot(table(jeu_final[jeu_final$methode_prospection=="partielle sur berges","effort"]))
######################################################
# mod�le delta = regr�ssion logistique
######################################################
require(gam)
require(PresenceAbsence)
require(car)
#as.factor(annee)+s(distance_mer)+presence_civelle+methode_prospection+s(effort)+coeff+cumul_expertise5+s(bio01)+s(cumul_expertise_pred)+s(nb_barragesppm)

#select=jeu_final$pourc_NA_score<0.5
select=rep(TRUE,nrow(jeu_final))
# 09 = temp�rature du trimestre le plus sec
# sur la proba de pr�sence, le facteur qualit� ROM n'explique presque rien    
modele_densite_presence=gam(d>0~methode_prospection+s(distance_mer)+s(effort)+s(bio10)+as.factor(annee)+s(cumul_expertise_pred),data=jeu_final[select,],family=binomial(link=logit))
#dfd=data.frame(drop1(modele_densite_presence))
#dfd[order(dfd$AIC,decreasing = T),]
summary(modele_densite_presence)
#x11()
#plot(modele_densite_presence,residuals=F,se=F,ask=F)

DATA=data.frame(jeu_final[select,"code_operation"],jeu_final[select,"effectif"]>0,predict(modele_densite_presence,type="response"))
(matrice_confusion=cmx(DATA,threshold = 0.6))
(aic=AIC(modele_densite_presence))
#coef(modele_densite_presence)
df=modele_densite_presence$df.residual
(pdev=(1-deviance(modele_densite_presence)/modele_densite_presence$null.deviance))
pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion)

pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1]) # present correctement pr�dits
print(paste("presents correctement pr�dits",pcp))
acp=matrice_confusion[2,2]/sum (matrice_confusion[,2]) # absent correctement pr�dit
print(paste("absents correctement pr�dits",acp))
(K=Kappa(matrice_confusion))
print(paste("kappa"=K)
(a_afficher=c(round(df),round(aic),round(as.numeric(K[1]),3),round(100*c(pbcl,pcp,acp),0)))
ex(a_afficher)
presence.absence.summary(DATA)
#save(modele_densite_presence,file= "EDALB/modele/modele_densite_presence_cumul_exp_pred_tt_CORRIGEEBIO10.Rdata")

######################################################
#modele densit�
######################################################
#select=jeu_final$pourc_NA_score<0.5&jeu_final$d>0
select=jeu_final$d_450>0 
# 10 = temp�rature du trimestre le plus chaud    
#
modele_densite=gam(d_450~as.factor(annee)+s(distance_mer)+methode_prospection+presence_civelle+as.factor(qualite_ROM_2002)+zone1+s(bio10)+s(cumul_expertise_pred),data=jeu_final[select,],family=Gamma(link=log))

#dfd=data.frame(drop1(modele_densite))
#dfd[order(dfd$AIC,decreasing = T),]
#summary(modele_densite)
plot(modele_densite,residuals=F,se=T,ask=F)
#AIC(modele_densite)
#Anova(modele_densite)
(aic=AIC(modele_densite))
#coef(modele_densite_presence)
df=modele_densite$df.residual
(pdev=(1-deviance(modele_densite)/modele_densite$null.deviance))
(a_afficher=c(round(df),round(aic)))
ex(a_afficher)


test_distribution(modele_densite,graph=1)
test_distribution(modele_densite,graph=2)
plot(modele_densite$y,modele_densite$y-modele_densite$fitted.value,xlim=c(0,20))
plot(modele_densite$y,modele_densite$fitted.value)
abline(a=0,b=1)
save(modele_densite,file= "EDALB/modele/modele_densite_cumul_exp_pred_450_tt.Rdata")

# mod�le delta = regr�ssion logistique 
modele_150_presence=gam(nb_150>0 ~ s(distance_mer)+cumul_expertise0+s(cumul_expertise1)+s(cumul_expertise2)+s(cumul_expertise3)+s(cumul_expertise4)+(cumul_expertise5),data=jeu_final,family=binomial(link=logit))
summary(modele_150_presence)
plot(modele_150_presence,page=1,se=FALSE,scale=0)


plot(rep(seq(0,1000,10),3),predict(modele_150,newdata=expand.grid(distance_mer=seq(0,1000,10),surface_station=c(10,100,1000)),type="response"))

jeu_450=jeu_final[jeu_final$nb_450_600>0,]
modele_450_positive=gam(d_450_600 ~ s(distance_mer) +s(bio11)+s(cumul_expertise)+s(annee),data=jeu_450,family=Gamma(link=log))
summary(modele_150_positive)
plot(modele_150_positive,page=1,se=FALSE,residuals=FALSE,scale=0)
test_distribution(modele_150_positive,graph=1)
plot(modele_150_positive$y,modele_150_positive$y-modele_150_positive$fitted.value,xlim=c(0,20))
plot(modele_150_positive$y,modele_150_positive$fitted.value,xlim=c(0,20))

##################"
# analyse des mod�les m�thode laurent
#####################
source("EDAcommun/prg/outils/effet.R")  
f_modele_bis(effet1=c(
 "s(distance_mer)","s(effort)","methode_prospection","s(annee)","moyen_prospection","as.factor(objectif_peche)","s(bio01)","s(bio08)","s(bio09)","s(bio10)","s(bio11)","s(altitude)","s(pente)","as.factor(presence_civelle)","as.factor(qualite_ROM_2002)","s(distance_relative,k=4)","s(distance_mer)","s(distance_source)","as.factor(noeud_mer))"),
 effet2=c("s(nb_barrages)","s(cumul_score)","s(cumul_denivele)","s(cumul_expertise_pred)","s(nb_barragesppm)","s(cumul_expertise5)+s(coefftr4)","s(coefftr)","s(cumul_expertise)","s(nombre_barrages_expertis�s)"),
 y="d")
 
 modele=f_modele_bis(effet1=c(
 "s(distance_mer)","s(effort)","methode_prospection","as.factor(annee)","s(bio01)","s(bio08)","s(bio09)","s(bio10)","s(bio11)","s(altitude)","s(pente)","as.factor(presence_civelle)","as.factor(qualite_ROM_2002)","s(distance_relative,k=4)","s(distance_mer)","s(distance_source)","as.factor(noeud_mer))" ) ,
 effet2=c("s(nb_barrages)","s(cumul_score)","s(cumul_denivele)","s(cumul_expertise_pred)","s(nb_barragesppm)","s(coefftr)","s(cumul_expertise)","s(coefftr)"),
 y="d")
 
result=gamaic(modele,poids=NULL,data=jeu_final[select,],family=Gamma(link=log)) 
print.aic.glm(resultat=result,nb_model=10,delta.aic=NULL,kappa=FALSE,
  nb_digit_aic=1)
 # On oublie moyen prospection
 #   table(jeu_final[,"moyen_prospection"],jeu_final[,"methode_prospection"])
 # On oublie objectif p�che   
 #   table(jeu_final[,"methode_prospection"],jeu_final[,"objectif_peche"])



   