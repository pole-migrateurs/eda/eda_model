 
bassin=4


 #r�cup�ration de la jointure tron�on barrage
#sql=paste("select * from  france.jointure where bassin='",bassin,"'")
#bar=fn_sql_0(sql,baseODBC=baseODBCbar,uid=uid,pwd=pwd,silent=FALSE)[[1]]
#bar=bar[,c("den_denivele","id_bdcarthage","ID_final","fba_valeurnote")]
#bar$den_denivele=as.numeric(as.character(bar$den_denivele))
#bar$den_denivele[bar$den_denivele==-98&!is.na(bar$den_denivele)]=NA 
#bar[bar$ID_final==4826,]
##b0=bar[indrepeated(bar$ID_final),]
##nrow(b0)
#
#write.table(bar,file=paste("EDAcommun/data/amorce_mer/bassin final/source/bar",bassin,".txt",sep=""))  
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/bar",bassin,".txt",sep=""),header=TRUE)
bar=bar[order(bar$ID_final),]

######
#traitement des donn�es
# Je r�cup�re uniquement les expertises => modif de la vue
# 
   
#######################################
 bar$haut=NA
 bar[bar$den_denivele<=0.5 & !is.na(bar$den_denivele),"haut"]<-1
  bar[bar$den_denivele>0.5 & bar$den_denivele<=1& !is.na(bar$den_denivele),"haut"]<-2
  bar[bar$den_denivele>1 & bar$den_denivele<=2& !is.na(bar$den_denivele),"haut"]<-3
  bar[bar$den_denivele>2 &!is.na(bar$den_denivele),"haut"]<-4
  
  # calcul du score2
  bar$score2=  round(bar$fba_valeurnote-bar$haut+ bar$den_denivele,2)
  bar$score2=bar$score2+ 1.6  # min(bar$score2,na.rm=TRUE)
  
  # calcul de la note d'expertise
  bar$expertise=bar$fba_valeurnote
  bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"]=
    round(bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"])
  bar[!is.na(bar$expertise) &  bar$expertise>=4.5 &  bar$expertise<5,"expertise"]=5
  hist(bar$expertise,50)
   hist(bar$score2,50)
 


# chargement des parcours  
  if(bassin<=3)
  {
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,".RData",sep=""))
  } else {
    #load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_noeudettroncon.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours1.RData",sep=""))
    load(file=paste("EDAcommun/data/amorce_mer/bassin final/source/source",bassin,"_parcours2.RData",sep=""))
  }
noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_",bassin,".txt",sep=""),header =TRUE)
noeuds$nb_barrages=as.double(NA)
noeuds$cumul_score=as.double(NA )
noeuds$cumul_expertise=as.double(NA)
noeuds$cumul_expertise0=as.double(NA)
noeuds$cumul_expertise1=as.double(NA)
noeuds$cumul_expertise2=as.double(NA)
noeuds$cumul_expertise3=as.double(NA)
noeuds$cumul_expertise4=as.double(NA)
noeuds$cumul_expertise5=as.double(NA)
noeuds$cumul_denivele=as.double(NA)
noeuds$pourc_NA_score=as.double(NA)
noeuds$pourc_NA_expertise=as.double(NA)
noeuds$pourc_NA_denivele=as.double(NA)
#noeuds=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,"_45619.txt",sep=""),header =TRUE)
 for (i in 1:(dim(noeuds)[1])) {
  #progress(i,dim(noeuds)[1])
       cat(paste("i =", i, "\n"))
        noeud_en_cours=noeuds[i,]
        parcoursCourant=get(paste("parcours_",noeud_en_cours$Id_BDCARTHAGE,sep=""))
        ### Traitement du parcours courant
        # on vire la source (voir explication en haut) :
        #!cat(noeud_en_cours$Id_BDCARTHAGE)
        #!cat("\n")
        #index_troncon=
        #stopifnot(length(match(rev(parcoursCourant$troncon[-1]),bar$id_bdcarthage))<=
       #nrow(parcoursCourant))
        barrages_parcourus=merge(parcoursCourant[-1,],bar,by.x="troncon",by.y="id_bdcarthage")
        noeud_en_cours$nb_barrages=nrow(barrages_parcourus)
        cat(paste(noeud_en_cours$nb_barrages,"\n"))
        if ( noeud_en_cours$nb_barrages >0) {
        noeud_en_cours$pourc_NA_score=round(sum(is.na(barrages_parcourus$score2))/noeud_en_cours$nb_barrages,3)
        noeud_en_cours$pourc_NA_denivele=round(sum(is.na(barrages_parcourus$den_denivele))/noeud_en_cours$nb_barrages,3)
        noeud_en_cours$pourc_NA_expertise=round(sum(is.na(barrages_parcourus$expertise))/noeud_en_cours$nb_barrages,3)
        
         } else  {
         noeud_en_cours$pourc_NA_score=0
         noeud_en_cours$pourc_NA_denivele=0
         noeud_en_cours$pourc_NA_expertise=0
         noeud_en_cours$cumul_score=0
         noeud_en_cours$cumul_denivele=0
         noeud_en_cours$cumul_expertise=0
         noeud_en_cours$cumul_expertise0=0
         noeud_en_cours$cumul_expertise1=0
         noeud_en_cours$cumul_expertise2=0
         noeud_en_cours$cumul_expertise3=0
         noeud_en_cours$cumul_expertise4=0
         noeud_en_cours$cumul_expertise5=0
         }
         
        if ( noeud_en_cours$pourc_NA_score!=1 & noeud_en_cours$nb_barrages >0){
        noeud_en_cours$cumul_score=sum(barrages_parcourus$score2,na.rm=TRUE )
        }
        if ( noeud_en_cours$pourc_NA_denivele!=1 & noeud_en_cours$nb_barrages >0){
        noeud_en_cours$cumul_denivele=sum(barrages_parcourus$den_denivele,na.rm=TRUE )
        }
        if ( noeud_en_cours$pourc_NA_expertise!=1 & noeud_en_cours$nb_barrages >0){
        noeud_en_cours$cumul_expertise=
        sum(barrages_parcourus[,"expertise"],na.rm=TRUE )
        noeud_en_cours$cumul_expertise0=
          sum(barrages_parcourus$expertise==0 &!is.na(barrages_parcourus$expertise))
        noeud_en_cours$cumul_expertise1=
          sum(barrages_parcourus$expertise==1 &!is.na(barrages_parcourus$expertise))
        noeud_en_cours$cumul_expertise2=
          sum(barrages_parcourus$expertise==2 &!is.na(barrages_parcourus$expertise))
        noeud_en_cours$cumul_expertise3=
         sum(barrages_parcourus$expertise==3 &!is.na(barrages_parcourus$expertise))
        noeud_en_cours$cumul_expertise4=
         sum(barrages_parcourus$expertise==4 &!is.na(barrages_parcourus$expertise))
        noeud_en_cours$cumul_expertise5=
        sum(barrages_parcourus$expertise==5 &!is.na(barrages_parcourus$expertise))
        } 
        noeuds[i,]=noeud_en_cours
        #parcoursCourant$troncon[-1] => le tron�on mer est NA
        # on r�cup�re les longeurs dans le data frame troncon_total :
        if (i/1000==round(i/1000))
   {
   write.table(noeuds,file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,"_",i,".txt",sep=""),row.names=FALSE)  
    }
  } #end boucle for
  
write.table(noeuds,file=paste("EDAcommun/data/amorce_mer/bassin final/source/noeud_source_bar",bassin,".txt",sep=""),row.names=FALSE)  
  # toto=noeuds[1:150,]
  # fix(toto)



