# traitement barrage
# prediction quand il n'y a pas de barrage et analyse graphique  (pour les NA barrages, pr�diction des scores m�dians)
# analyse graphique au milieu pour le r�sultat du dernier calcul du cumul des scores

# chargement de tous les barrages
#sql=paste("select * from  france.t_barunique_buq")
#bar0=fn_sql_0(sql,baseODBC=baseODBCbar,uid=uid,pwd=pwd,silent=FALSE)[[1]]
#write.table(bar0,file=paste("EDAcommun/data/amorce_mer/bassin final/source/t_barunique_buq.txt",sep=""),row.names=FALSE)
bar0=read.table(file="EDAcommun/data/amorce_mer/bassin final/source/t_barunique_buq.txt",header=TRUE)
#chargement des barrages avec reconstitution des scores et d�nivel�s
bar=read.table(file=paste("EDAcommun/data/amorce_mer/bassin final/source/bar",4,".txt",sep=""),header=TRUE)

bar=bar[order(bar$ID_final),]
 bar$haut=NA
 bar[bar$den_denivele<=0.5 & !is.na(bar$den_denivele),"haut"]<-1
  bar[bar$den_denivele>0.5 & bar$den_denivele<=1& !is.na(bar$den_denivele),"haut"]<-2
  bar[bar$den_denivele>1 & bar$den_denivele<=2& !is.na(bar$den_denivele),"haut"]<-3
  bar[bar$den_denivele>2 &!is.na(bar$den_denivele),"haut"]<-4
  
  # calcul du score2
  bar$score2=  round(bar$fba_valeurnote-bar$haut+ bar$den_denivele,2)
  bar$score2=bar$score2+ 1.6  # min(bar$score2,na.rm=TRUE)
  
  # calcul de la note d'expertise
  bar$expertise=bar$fba_valeurnote
  bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"]=
    round(bar[!is.na(bar$expertise) &  bar$expertise<4.5,"expertise"])
  bar[!is.na(bar$expertise) &  bar$expertise>=4.5 &  bar$expertise<5,"expertise"]=5
  hist(bar$expertise,50)
   hist(bar$score2,50)
 
# jointure sur la base de l'id_final
# il n'y a pas de doublons dans bar
#bar[indrepeated("ID_final"),]
bar1=merge(bar0,bar,by="ID_final")

#alt=read.table(file="EDAcommun/data/extraction_parametres/noeuds2km_altitude.txt",header=TRUE)
#alt=alt[alt$"NOM_BASSIN"=="Loire-Bretagne",]
#alt$zone=substr(alt$CODEHYDRO,1,2)

bar1$zone=bar1$Code_Hydrographique_Cours_Eau
bar1$zone=substr(bar1$Code_Hydrographique_Cours_Eau,1,2)
bar1$zone[is.na(bar1$zone)]="--"
bar1$zone1=substr(bar1$zone,0,1)
#bar1$zone1[bar1$zone1=="J"]=bar1$zone[bar1$zone1=="J"]
#bar1$zone1[bar1$zone1=="J6"|bar1$zone1=="J7"|bar1$zone1=="J8"|bar1$zone1=="J9"]="J6"
bar1$zone1[bar1$zone1=="Z"]="-"
summary(as.factor(bar1$zone1))
 # passer ici � la partie statistique ligne 330
#unique(alt$zone1)

# on laisse tomber il manque la moiti� des tron�ons


#bar2=merge(bar1,alt,by.x="Id_BDCARTHAGE_Noeud_Initial",by.y="CODENOEUD")
#####################################
# Traitement graphique
#####################################
necessary = c('sp', 'maptools','rgdal')
if(!all(necessary %in% installed.packages()[, 'Package']))
install.packages(c('sp', 'maptools','rgdal'), dep = T)
library(sp)
library(maptools)
source("EDAcommun/prg/init/fonctions_graphiques.r")
library(lattice)
library(RColorBrewer)
trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
                                      cutoff.tails=0.1,
                                      alpha=1)))
#load(file="EDALB/data/extraction_parametre_Marion/jeu_donnee_complet.RData")         # ers_full
#x11()
#show.settings()


#

  if(!exists("CE"))
CE=readShapeLines("EDAcommun/data/couche_shp/COURS_EAU123.shp")
  if(!exists("France"))
France=readShapePoly("EDAcommun/data/couche_shp/FRANCE.SHP")
CE1=CE[CE[["CLASSIFICA"]]==1,]
if(!exists("bassin"))
  bassin=readShapePoly("EDAcommun/data/couche_shp/bassins_hydro_modif_region.shp")
 
if (!exists("bassinlb"))
bassinlb = readShapePoly("EDAcommun/data/couche_shp/contour_LB.shp")
if(!exists("CElb"))
CElb =readShapeLines ("EDAcommun/data/couche_shp/Cours d'eau principaux_lb.shp")


bar=read.table(file=paste("EDALB/data/extraction_parametre_Marion/bar4pred.txt",sep=""),header=TRUE)
bar$ppm=bar$Auteur=="Pierre Steinbach"|bar$Auteur=="PM Chapon"



  x11(20,15)
mypalette<-rev(brewer.pal(9,"Spectral"))

fun_graph_mod(  xlabel="Pt_LII_X",
                ylabel="Pt_LII_Y",
                type="score2",
                mod=NULL,
                colonne_commune=NULL,
                data=bar,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =seq(0, 1, 0.10), #ou NULL # voir quantile
                #cutn=c(0,0.5,1),#avec cut on supprime les quantiles
                cex=1,
                title="Scores des barrages ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")   
                 
fun_graph_mod(  xlabel="Pt_LII_X",
                ylabel="Pt_LII_Y",
                type="expertise",
                mod=NULL,
                colonne_commune=NULL,
                data=bar,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL,# seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(-1,0,1,2,3,4,5),#avec cut on supprime les quantiles
                cex=1,
                title="Expertise des barrages ",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")
spdf@data=spdf@data [is.na( spdf@data$donnees_affichee),]
 sppot
names(spdf) 
 points(spdf,
            col="black",
            pch=12,
            cex=0.1
            )
### ZOOMS

                            
barVil=subset(bar,bar$zone=="J6"|bar$zone=="J7"|bar$zone=="J8"|bar$zone=="J9")
spdVil=CrSpPtsDF("Pt_LII_X","Pt_LII_Y" ,barVil)
xlimvil=as.numeric(bbox(spdVil)[1,])
ylimvil=as.numeric(bbox(spdVil)[2,])                
                 
fun_graph_mod(  xlabel="Pt_LII_X",
                ylabel="Pt_LII_Y",
                type="expertise",
                mod=NULL,
                colonne_commune=NULL,
                data=bar,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL,# seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(-1,0,1,2,3,4,5),#avec cut on supprime les quantiles
                cex=1,
                title="Expertises des barrages ",
                xlim =  xlimvil,
                ylim = ylimvil,
                couleur_fond="grey80")   


barVil=subset(bar,bar$zone=="L6"|bar$zone=="L7"|bar$zone=="L8")
spdVil=CrSpPtsDF("Pt_LII_X","Pt_LII_Y" ,barVil)
xlimvil=as.numeric(bbox(spdVil)[1,])
ylimvil=as.numeric(bbox(spdVil)[2,])                
 #zoom
 xlimvil=c(xlimvil[1],xlimvil[1]+50000)
 ylimvil=c(ylimvil[1],ylimvil[1]+50000)
fun_graph_mod(  xlabel="Pt_LII_X",
                ylabel="Pt_LII_Y",
                type="expertise",
                mod=NULL,
                colonne_commune=NULL,
                data=bar,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs =NULL,# seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(-1,0,1,2,3,4,5),#avec cut on supprime les quantiles
                cex=1,
                title="Expertises des barrages ",
                xlim =  xlimvil,
                ylim = ylimvil,
                couleur_fond="grey80")  
                                    
# la fonction balance l'object spdf dans l'environnement principal
# probl�mes de doubles ?
# attention il faut virer les coordonn�es
spdf@coords=spdf@coords[is.na( spdf@data$donnees_affichee),]
spdf@data=spdf@data [is.na( spdf@data$donnees_affichee),]

 plot(spdf,
            col="black",
            pch=12,
            cex=0.1
            )
baro=bar[order(bar$Pt_LII_X ),]
baro=baro[baro$zone=="J7",]

spdf1=CrSpPtsDF("Pt_LII_X","Pt_LII_Y" ,bar)
  x11(20,15)
plot(CElb)
trellis.par.set(sp.theme())
spplot(spdf1,c("zone1"))
 
 
########################
# carto barrages noeuds
########################

noeudsbar=read.table(file=paste("EDALB/data/noeud_2km/noeud_source_bar4_pred.txt",sep=""),header=TRUE)                     
if (!exists("noeudsxy"))noeudsxy=read.table(file=paste("EDAcommun/data/extraction_parametres/noeuds_parcouruXY.txt",sep=""),header=TRUE)
noeudsxy=merge(noeudsbar,noeudsxy, by="Id_BDCARTHAGE" )
 
 #mypalette<-rev(brewer.pal(9,"Spectral"))
 # mypalette<-brewer.pal(9,"YlGnBu")
 #mypalette<-brewer.pal(9,"YlOrRd")
 x11(22,16)
 layout(matrix(1:4,2,2))
 mypalette<-c(rev(brewer.pal(9,"YlGnBu")),brewer.pal(9,"YlOrRd"))
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="cumul_expertise",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,5,10,20,30,50,100,200,353),#avec cut on supprime les quantiles
                cex=0.3,
                title="Cumul des expertises",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")
                
  fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="nb_barragesppm",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,5,10,20,40,60,80,160),
                cex=0.3,
                title="Nb Barrages expertis�s",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")                
  
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="cumul_expertise_pred",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,5,10,20,30,50,100,200,353),#avec cut on supprime les quantiles
                cex=0.3,
                title="Cumul expertise (+predictions)",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")                  
                
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="nb_barrages",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=c(0,5,10,20,40,60,80,160),
                cex=0.3,
                title="Nb Barrages total",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")  

corjeufinal=noeudsbar[,c(
"nb_barrages",
"nb_barragesppm",
"cumul_score",
"cumul_expertise",
"cumul_expertise_pred",
"cumul_denivele")]
 cor=symnum(clS <-cor(corjeufinal,use = "complete"))
print(cor)
x11()
plot(noeudsbar$nb_barrages, noeudsbar$cumul_expertise_pred)
cor.test(noeudsbar$nb_barrages, noeudsbar$cumul_expertise_pred,method="spearman")  
                
cor.test(noeudsbar$nb_barrages, noeudsbar$cumul_score,method="pearson")  


cor.test(noeudsbar$nb_barragesppm, noeudsbar$cumul_expertise,method="pearson")  

 x11(22,16)
 mypalette<-rev(brewer.pal(9,"YlGnBu"))
 layout(matrix(1:4,2,2))
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pourc_NA_expertise",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=seq(0,1,0.1),
                cex=0.3,
                title="Pourcentage_NA_expertise",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")
                
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pourc_NA_score",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
               probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=seq(0,1,0.1),
                cex=0.3,
                title="Pourcentage_NA_score",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")
                
 fun_graph_mod(  xlabel="X",
                ylabel="Y",
                type="pourc_NA_denivele",
                mod=NULL,
                colonne_commune=NULL,
                data=noeudsxy,
                fond=CElb,
                couleurs=mypalette,
                space="Lab",                   #c("rgb", "Lab")
                probs = NULL, #seq(0, 1, 0.10), #ou NULL # voir quantile
                cutn=seq(0,1,0.1),
                cex=0.3,
                title="Pourcentage_NA_denivele",
                xlim =  NULL,
                ylim = NULL,
                couleur_fond="grey80")  
                   
 #####################################
# Analyse statistique
#####################################

require(gam)
runif(1) #pb initialisation de random.seed
# pour avoir toujours le m�me nb de donn�es 
bar2=bar1[, c("ID_final" ,"den_denivele", "Cours_eau","Id_BDCARTHAGE","Navigabilit�","Id_BDCARTHAGE_Noeud_Initial","Id_BDCARTHAGE_Noeud_Final","Pt_LII_X",                  "Pt_LII_Y","Auteur","Nom_obstac","X","Y","Hauteur_ch","Cgenelin" ,"fba_valeurnote","haut","score2","expertise","zone","zone1"  )]
bar2$Navigabilit�[bar2$Navigabilit�=="Inconnue"]="Non navigable" 
bar2$Navigabilit�=as.factor(bar2$Navigabilit�)
bar2$zone1=as.factor(bar2$zone1)  
#summary(bar2)
a_retenir=apply(bar2[,!(names(bar2)%in% c())],1,function(x) sum(is.na(x)))==0   #& (ers_full$bassin!=1) #& (ers_full$bassin!=6)
bar3=bar2[a_retenir,]

modele_expertise=glm(expertise~Navigabilit�+zone1,data=bar3,family=poisson(link = "log"))
summary(modele_expertise)
hist(resid(modele_expertise))
plot(modele_expertise)
pred=predict(modele_expertise,newdata=bar2,type="response")
hist(pred)
 medianes_obs=tapply(bar2$exp,bar2$zone1,median,na.rm=T)
 medianes_pr�dites=tapply(pred,bar2$zone1,median)
 nb_obs=tapply(pred,bar2$zone1,function(X) sum(!is.na(X)))
 (result=cbind("pred"=medianes_pr�dites,"obs"=medianes_obs,"count"=nb_obs))
 write.table(result,file=paste("EDALB/data/extraction_parametre_Marion/result_pred_exp_bar.txt",sep=""))
median(bar3$expertise)   #3
median(bar3$score2)     #2.9
bar2$pred=pred


write.table(bar2,file=paste("EDALB/data/extraction_parametre_Marion/bar4pred.txt",sep=""))

#Pour MArion et son SIG
write.table(bar2,file=paste("EDALB/data/extraction_parametre_Marion/bar4predcarto.txt",sep=""),sep = ";",row.names =FALSE) 

bar2[is.na(bar2$expertise),"expertise"]=-9999
write.xls( bar2, file=paste("EDALB/data/extraction_parametre_Marion/bar4pred.xls",sep=""),      colNames = TRUE, 
           sheet = 1, 
           from = 1, 
           rowNames = NA )
 