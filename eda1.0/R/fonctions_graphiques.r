# fonctions graphiques et utilitaires
xlimloire=c(51660,773417)
ylimloire=c(1952000,2436000)
xlimbre=c(51660,500000)
ylimbre=c(2102000,2436000)

# creation d'un object de la classe points � partir d'un data.frame

# retire les lignes qui ont NA dans les coordonn�es X et Y

CrSpPtsDF<-function(xlabel,ylabel,data){
  xpos=match(xlabel,colnames(data))
  ypos=match(ylabel,colnames(data))
  # on vire les donn�es manquantes pour les coordonn�es
  data=data[complete.cases(data[c(xpos,ypos)]),]
  coord=data[,c(xpos,ypos)]
  datasp=data[,-c(xpos,ypos)]
  if (class(datasp)=="numeric") # plus qu'une colonne
   {
   datasp<-as.data.frame(datasp)
   colnames(datasp)=colnames(data)[3]
   }
   
  spdf=SpatialPointsDataFrame(SpatialPoints(coord),data)
  return(spdf)
}
# spdf=CrSpPtsDF(xlabel="ouv_abscisse",ylabel="ouv_ordonnee",data=barglobal[,-c(2,14)])
#

# Fonction qui modifie un object SpatialPolygonsDataFrame
# pour int�grer de nouvelles colonnes s�lectionn�es � partir d'un data.frame externe
# en fonction d'un identifiant trouv� dans unes des colonnes du slot data
# du  SpatialPolygonsDataFrame

mod_SpatialDataFrame<-function(SpPDF,  # un SpatialPolygonsDataFrame
                                        data,  #  un dataframe
                                        label_SpPDF,  # nom de la colonne contenant l'identifiant
                                        label_data,   # nom de la colone contenant l'indentifiant de data
                                        coldata=colnames(data)       # vecteur character des colonnes � int�ger
                                        ){

  # slotNames(SpPDF)    # pour info -> noms des Slots
  df=SpPDF@data # extraction du slot data de l'object S4 (SpatialPolygonsDataFrame)
  if (sum(coldata %in% colnames(df))>0) stop(paste(label_data,"est d�j� pr�sent dans SpPDF"))
  df<-merge(df,data,by.X=label_SpPDF,by.Y=label_data)
  SpPDF@data<-df
return(SpPDF)
}

# Cette fonction trace un graphique a partir d'un spatialpointdf
# et renvoit des points sur un fond de carte en cr�ant des couleurs
# progressives � partir d'un d�coupage en classe d'une variable continue
# elle utise aussi une colonne "nb" pour donner une taille de points diff�rents
plot_SpPtsDF=function(fond,
                      xlim = NULL,
                      ylim = NULL,
                      donnee,
                      colonne,
                      couleurs,
                      space=c("rgb", "Lab"),
                      probs,
                      cutn=NULL,         # ignore les quantiles (probs)
                      cex=1,
                      title,
                      text=NULL,
                      couleur_fond="black",
                      point=19,
                      textbas=TRUE,
                      surface=NULL,
                      couleursurface=NULL,
                      legendlab=NULL,
                      legendval=NULL,
                     ...
                      ){
   
   if (is.null(xlim)){
   xlim=bbox(get(fond[1]))[1,]
   }
     if (is.null(ylim)){
   ylim=bbox(get(fond[1]))[2,]
   }
   plot(get(fond[1]),xlim=xlim,ylim=ylim,col=couleur_fond[1])
  if (length(fond)>1){
  for (i in 2:length(fond)){
  plot(get(fond[i]),col=couleur_fond[i],add=TRUE)
   }
  }
   title(title)
   if (! is.null(text)) mtext(text,side=1,cex=0.8,line=2)
   mypalette= colorRampPalette(couleurs)
   #cat(!is.null(cutn))
   if (is.null(cutn)){
    cat("calcul des quantiles \n")
    quant=unique(quantile(x=donnee[[colonne]],probs,na.rm=TRUE))
    niveaux= cut(donnee[[colonne]],quant)  
    couleur=mypalette(length(quant))[match(niveaux,levels(as.factor(niveaux)))]
   } else   {
   cat("calcul des coupures \n")
      niveaux =cut(donnee[[colonne]],cutn)
      couleur=mypalette(length(levels(as.factor(niveaux))))[match(niveaux,levels(as.factor(niveaux)))]
    } 
   if (! is.null(donnee[["nb"]])){
   cex1=cex
   cex=2*donnee[["nb"]]/max(donnee[["nb"]])-0.5 +cex
   }  else cex1=cex
      points(donnee,
            donnee[[colonne]],
            col=couleur,
            pch=point,
            cex=cex
            )
       if(is.null(legendval)) legendval= levels(niveaux)          
      if (is.null(cutn)) {      
      legend(    x= "bottomleft",
                title=ifelse(is.null(legendlab), paste("Quantiles"),legendlab),
                legend= legendval,
                pch=point,
                xjust=0,
                cex=0.8,
                col=c(mypalette(length(quant))),
                horiz=FALSE,
                bty="n" )
      if (textbas){
      mtext(paste("Quantiles",paste(probs,collapse="|")),side=1,cex=0.6)
      } 
      } else {
       
            legend(    x= "bottomleft",
                title=ifelse(is.null(legendlab), paste("Classes"),legendlab),
                legend= legendval,
                pch=point,
                xjust=0,
                cex,
                col=c(mypalette(length(levels(as.factor(niveaux))))),
                horiz=FALSE,
                bty="n" )
      if (textbas){
      mtext(paste("Coupure",paste(levels(niveaux),collapse="|")),side=1,cex=0.6)
      } 
      }
              
      if (! is.null(donnee[["nb"]])){
      legend(    x= "topleft",
                title=paste("Nb"),
                legend= round(c(min(donnee[["nb"]]),
                            max(donnee[["nb"]])),0),
                pch=point,
                col=c("grey","grey"),
                cex=0.8,
                pt.cex=c(2*min(donnee[["nb"]])/max(donnee[["nb"]])+cex1-0.5,
                1.5+cex1),
                horiz=FALSE,
                bty="n" )
      }
      # �criture d'une couche au dessus du graphe
      if (!is.null(surface)){
      plot(get(surface),col=couleursurface,add=TRUE)
      }
                
}

addpointscut_SpPtsDF=function(
                      donnee,
                      colonne,
                      space=c("rgb", "Lab"),
                      probs,
                      couleurs,
                      cex=1,                                   
                    ...
                      ){
    mypalette= colorRampPalette(couleurs)
    quant=unique(quantile(x=donnee[[colonne]],probs,na.rm=TRUE))
    niveaux= cut(donnee[[colonne]],quant)
    couleur=mypalette(length(quant))[match(niveaux,levels(as.factor(niveaux)))]               
       points(donnee,
            donnee[[colonne]],
            col=couleur,
            pch=19,
            cex=cex
            )              
} 

   
addpoints_SpPtsDF=function(
                      donnee,
                      colonne,
                      couleur,
                      cex=1,
                      label,
                      xlegend=NULL ,  #"bottomright"
                    ...
                      ){
       points(donnee,
            donnee[[colonne]],
            col=couleur,
            pch=19,
            cex=cex
            )
      if (!is.null(xlegend)){
      legend(    x= xlegend,
                legend= label,
                pch=c(19),
                xjust=0,
                cex=cex,
                col=couleur,
                horiz=FALSE,
                bty="n" ) 
                }             
}   
   
# cette fonction extrait les r�sidus d'un mod�le mod appliqu� � un dataframedonne
# elle cherche tous les objects ayant un x+y =unique (station)
# calcule les r�sidus moyens sur les stations et le nombre d'observations
# cr�e un spatialpointsdataframe et l'affiche � l'aide de la fonction
# plot_SpPtsDF dont elle repasse un certain nombre d'arguments
fn<-function(modele,nb=100){
if (nchar( modele$formula[3])>nb) {
  mod=paste(modele$formula[2],modele$formula[1],
      substr(modele$formula[3], start=1,stop=nb),"\n",
      substr(modele$formula[3], start=nb+1,stop=nchar(modele$formula[3])))
  
  } else {
  mod=paste(modele$formula[2],modele$formula[1],modele$formula[3])
  }
return(mod)
}

fun_graph_mod=function(           xlabel,
                                  ylabel,
                                  type="residus",    #residus, predites
                                  mod=NULL,                      # NULL
                                  data,
                                  colonne_commune=NULL,           # NULL
                                  fond,
                                  couleurs,
                                  probs,
                                  cutn=NULL,         # ignore les quantiles
                                  cex=1,
                                  space,
                                  title,
                                  xlim =  NULL,                      
                                  ylim = NULL ,
                                  couleur_fond="black",
                                  point=19,
                                  textbas=TRUE,
                                  surface=NULL,
                                  couleursurface=NULL,
                                  legendlab=NULL,
                                  legendval=NULL,                                 
                                  ...){
  switch(type,
  "residus"={
    data$donnees_affichee=residuals(mod)
            },
   "predites"=
  {
  cat("prediction du mod�le \n")
  data$donnees_affichee=predict(mod,data,type="response")
  } ,
  {   #par d�faut last si rien n'est trouv� avant type="nomcolonne"
  data$donnees_affichee=data[,type]
  }
  )
   
  if (!is.null(colonne_commune)) {
    station=data[,colonne_commune]
    data$station=as.factor(station )
    x=  tapply(as.numeric(data[,xlabel]),data$station,mean,na.rm=TRUE)
    y=  tapply(as.numeric(data[,ylabel]),data$station,mean,na.rm=TRUE)
    donnees_affichee=tapply(data$donnees_affichee,data$station,mean)
    nb=tapply(data$donnees_affichee,data$station,length)
    newdata=as.data.frame(cbind(x,y,donnees_affichee,nb))
    colnames(newdata)=c(xlabel,ylabel,colnames(newdata[c(3,4)]))
  }   else {
    x=data[,xlabel]
    y=data[,ylabel]
    donnees_affichee=as.numeric(data$donnees_affichee)   
    newdata=as.data.frame(cbind(x,y,donnees_affichee))
    colnames(newdata)=c(xlabel,ylabel,colnames(newdata[3]))
    #newdata$bidon=NA    #pour faire un data.frame pour CrSpPtsDF
  }
  
  # creation d'un spatialpointsdataframe
  spdf=CrSpPtsDF(xlabel,ylabel,data=newdata)
  spdf<<-spdf
 # if (!is.null(mod) ) text=fn(mod,100) else text=""
 text=""

  plot_SpPtsDF(         fond,
                        xlim,
                        ylim,
                        donnee=spdf,  #un object de classe SpPtsDF
                        colonne="donnees_affichee",     # la colonne qu'on souhaite afficher
                        couleurs,
                        space,                   #c("rgb", "Lab")
                        probs,
                        cutn,
                        cex, 
                        title=title,
                        text,
                        couleur_fond=couleur_fond,
                        point,
                        textbas,
                        surface,
                        couleursurface,
                        legendlab,
                        legendval
                        )
  return=spdf
}                      

#' eda_carto performs a loading and merging of the spatial data on the coldata columns
#' @return nothing
#' @param shpname name of the corresponding 
#' @author cedric
#' @debug shpname="dbeel.shp" # does not work with wrbd_stationdbeel.shp I don't know why.....
#' load(file=paste(datawd,"/dataEDAccm/ers_full_Western.RData",sep=""))
#' data<-dbeel@datadbeel
#' data$newdata=NA
#' prkey="op_id"
#'  res<-joinshptodata(shpname="dbeel.shp",shptype="points",data=dbeel@datadbeel,prkey="op_id",coldata=c("newdata"))
joinshptodata<-function(shpname,shptype="points",data,prkey,coldata=colnames(data),...) {
			trellis.par.set(set = FALSE, regions = list(col = bpy.colors(n =100,
									cutoff.tails=0.1,
									alpha=1)))		
			dsn=paste(shpwd,shpname,sep="") # shpwd is defined in the xml file at the root
			switch(shptype, points =		myshp<-readShapePoints(dsn),
					poly=myshp<-readShapePoly(dsn),
					lines=myshp<-readShapeLines(dsn))
			shp@data[,prkey]=as.character(shp@data[,prkey])
			data[,prkey]<-tolower(	data[,prkey])
			newshpdata<-mod_SpatialDataFrame(SpPDF=shp,
					data,  #  un dataframe
					label_SpPDF=prkey,  # nom de la colonne contenant l'identifiant
					label_data=prkey,   # nom de la colone contenant l'indentifiant de data
					coldata=coldata       # vecteur character des colonnes � int�ger
			)	
		
			return(newshpdata)
		}

#exemple  (d�commenter pour lancer)
#BV_bre=readShapePoly("BV-Bges-BRETAGNE_region.shp")    # class sp
#names(BV_bre)<-c("vue","nom_bassin","surface")
#slotNames(BV_bre)
# names(BV_bre@data)
#bre2=mod_SpatialDataFrame (SpPDF=BV_bre,   #un SpatialPolygonsDataFrame
#                                        data=barglobal,   # un dataframe
#                                        label_SpPDF="nom_bassin",   #nom de la colonne contenant l'identifiant
#                                        label_data="ouv_id",   # nom de la colone contenant l'indentifiant de data  correpondant au nom contenu dans le spdf
#                                        coldata=c("pred.gam2","expertise"))
# slotNames(bre2)
# names(bre2@data)
# bre[["densite"]]
# spplot(bre,"densite")

############################################
# exploration de la classe S4 et des objects graphes
#  ! les methodes ne sont pas d�finies suivant les standards S4
############################################
# class(bre2)
# polybre2=bre2@polygons
#    class(polybre2)    # => pas facile d'extraire ... voir methodes en dessous

#  ID=polybre2@ID
#getSpPPolygonsData(bre)
#LabptSlots=getSpPPolygonsLabptSlots(bre)
#IDSlots=getSpPPolygonsIDSlots(bre)
#getPolygonsIDSlot
#Slots= getSpPpolygonsSlot(bre)
#getPolygonsLabptSlot
#str(bre)
#methods(sp)
#showClass("SpatialPolygons")
#showClass("Spatial") # object p�re
#showClass("SpatialPolygonsDataFrame")
#showMethods("SpatialPolygonsDataFrame")
#showMethods("SpatialPolygons")
#showMethods("Spatial")
#getMethods("SpatialPoints")

