fumodifnoteglobal=function(noteglobal){
    vecteur=c("BERGE",
             "DIVERSITE",
             "PROFIL",
              "RUGOSITE")
    vecteurchange=c(0.5,1,1,1)
    for ( i in 1:4){
    notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote==vecteur[i]]=
    notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote==vecteur[i]]+vecteurchange[i]
    }
    ouv=notesglobal$ouv_id
    for (i in 1: nrow(notesglobal)){
      progress(i,nrow(notesglobal))
      selectouv=notesglobal$ouv_id==notesglobal$ouv_id[i]
      notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="SCORE" & selectouv]=
      max(notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="BERGE" & selectouv],0)+
      max(notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="DIVERSITE"&selectouv],0)+
      max(notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="PROFIL"&selectouv],0)+
      max(notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="RUGOSITE"&selectouv],0)+
      max(notesglobal$fba_valeurnote[notesglobal$fba_tyn_typenote=="HAUTEUR"&selectouv],0)
    # je passe par le max car il y a des numeric(0) qui foutent la merde
    }
    return(notesglobal)
}