------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
-- Arrange_restocking_data                                                                          			--
--  																											--
-- This script is used to clean restocking data 																--
-- X:\Ålen\2_ Data collection DCF EUMAP\Ålutsättningsdata - Restocking\Ålutsättningar_ny_total_20230116_JS.xlsx	--
--																												--
-- @ Torbjörn Säterberg(torbjorn.saterberg@slu.se) 2022-12-15													--
------------------------------------------------------------------------------------------------------------------
--
-- First, the restocking data has been inserted into rawdata from its own csv database Restocking(Originally in varchar form)
-- Second, data has been cleaned and geometries have been added!

-- set empty values to null
UPDATE rawdata.restockingeda SET antal=NULL where antal='';
update rawdata.restockingeda SET antal=NULL where antal=-9;
UPDATE rawdata.restockingeda SET belopp=NULL where belopp='';
UPDATE rawdata.restockingeda SET belopp=NULL where belopp=' ';
UPDATE rawdata.restockingeda SET belopp=10000 where belopp='10 000';
UPDATE rawdata.restockingeda SET belopp=NULL where belopp=-9;
ALTER TABLE rawdata.restockingeda ALTER COLUMN belopp TYPE float4 USING belopp::float4;
UPDATE rawdata.restockingeda SET vikt=NULL where vikt='';
UPDATE rawdata.restockingeda SET vikt=NULL where vikt=' ';
UPDATE rawdata.restockingeda SET vikt=NULL where vikt='-9.00';
ALTER TABLE rawdata.restockingeda ALTER COLUMN vikt TYPE float4 USING vikt::float4;
UPDATE rawdata.restockingeda SET koordxut=NULL where koordxut='';
UPDATE rawdata.restockingeda SET koordxut=NULL where koordxut=' ';
UPDATE rawdata.restockingeda SET koordxut='6196353' where koordxut='6196353.545';
ALTER TABLE rawdata.restockingeda ALTER COLUMN koordxut TYPE int8 USING koordxut::int8;
UPDATE rawdata.restockingeda SET koordyut=NULL where koordyut='';
UPDATE rawdata.restockingeda SET koordyut=NULL where koordyut=' ';
UPDATE rawdata.restockingeda SET koordyut='1355519' where koordyut='1355519.597';
ALTER TABLE rawdata.restockingeda ALTER COLUMN koordyut TYPE int8 USING koordyut::int8;
UPDATE rawdata.restockingeda SET koordx=NULL where koordx='';
UPDATE rawdata.restockingeda SET koordx=NULL where koordx=' ';
ALTER TABLE rawdata.restockingeda ALTER COLUMN koordx TYPE int8 USING koordx::int8;
UPDATE rawdata.restockingeda SET koordy=NULL where koordy='';
UPDATE rawdata.restockingeda SET koordy=NULL where koordy=' ';
ALTER TABLE rawdata.restockingeda ALTER COLUMN koordy TYPE int8 USING koordy::int8;

-- add columns to table
ALTER TABLE rawdata.restockingeda ADD COLUMN geom geometry, -- geometry for provided coordinates
add column snapped_geom geometry, -- geometry for coordinates snapped to river network
add column 	idx SERIAL PRIMARY key, -- there is no key in the original data (add that here)
add column idsegment varchar(12); -- idsegments from sweden.rn table 

-- add geom based on coordinates in restocking "database"
UPDATE rawdata.restockingeda set geom = ST_POINT(koordyut, koordxut,3021);
UPDATE rawdata.restockingeda set geom = ST_POINT(koordy, koordx,3021) where geom is null; -- add koodinates to some restockings that do not have exact location

-- Add geometry for closest point in rivernetwork
with snapped_geom as ( 
select idx, shortest_dist_point.idsegment, ST_Closestpoint(shortest_dist_point.geom, st_transform(r.geom,3035)) snapped_geom
from rawdata.restockingeda r  
cross join lateral (
select 
geom, 
idsegment
from sweden.rn srn
order by st_transform(r.geom,3035) <-> srn.geom
limit 1) shortest_dist_point)
update rawdata.restockingeda r2 
set 
snapped_geom = snapped_geom.snapped_geom, 
idsegment = snapped_geom.idsegment
from snapped_geom
where 
r2.idx = snapped_geom.idx;  

