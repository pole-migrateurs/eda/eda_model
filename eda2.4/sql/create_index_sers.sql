ALTER TABLE sers.dag_datum_dim 
ADD PRIMARY KEY(dag_nyckel);

ALTER TABLE sers.elfiske_altitud_dim  
ADD PRIMARY KEY(altitud_nyckel);

ALTER TABLE sers.elfiske_avromrklass_dim 
ADD PRIMARY KEY(avromrklass_nyckel);

ALTER TABLE sers.elfiske_fangst_fact
ADD PRIMARY KEY(elffangst_nyckel);

ALTER TABLE sers.elfiske_lokal_dim
ADD PRIMARY KEY(elflok_nyckel);

ALTER TABLE sers.elfiske_utfiske_fact 
ADD PRIMARY KEY(elfutf_nyckel);

ALTER TABLE sers.elprovfiske_dim 
ADD PRIMARY KEY(elfisk_nyckel);

ALTER TABLE sers.fiskregion_dim 
ADD PRIMARY KEY(fiskregion_nyckel);

ALTER TABLE sers.huvudavromr_dim
ADD PRIMARY KEY(huvudavromr_nyckel);

ALTER TABLE sers.langdintervall_dim
ADD PRIMARY KEY(langdinterv_nyckel);

ALTER TABLE sers.provfiske_syfte_dim  
ADD PRIMARY KEY(prfsyfte_nyckel);

ALTER TABLE sers.provfiske_utforare_dim  
ADD PRIMARY KEY(prfutf_nyckel);

ALTER TABLE sers.sotvattenarter_dim  
ADD PRIMARY KEY(arter_nyckel);

ALTER TABLE sers.typavpop_dim
ADD PRIMARY KEY(typavpop_nyckel);

ALTER TABLE sers.vattendrag_dim 
ADD PRIMARY KEY(vattendrag_nr);

ALTER TABLE sers.vattendragbredd_dim
ADD PRIMARY KEY(vdragbredd_nyckel);




