

-- Load excel-file with hydropower information
drop table if exists rawdata.hydropower;
create table rawdata.hydropower(
Namn VARCHAR,	
�gare VARCHAR,	
Normalprod DOUBLE PRECISION,	
Effekt DOUBLE PRECISION,	
Byggt VARCHAR,
Nedlagt INT,
Aggregat INT,
Turbinlev VARCHAR,
Genlev VARCHAR,
Turbintyp VARCHAR,
Station VARCHAR,
Ombyggt VARCHAR,
Fallh DOUBLE PRECISION,
Regler VARCHAR,
Vattenf DOUBLE PRECISION,
Medelv DOUBLE PRECISION,
Magasin DOUBLE PRECISION,
Vattendrag VARCHAR,
Ort VARCHAR,
Kommun VARCHAR,
L�n VARCHAR,
"Lat" NUMERIC,
"Long" NUMERIC
);

copy rawdata.hydropower from 'C:\hydrografi_i_natverk\Vattenkraft\vattenkraft 2021-02-10.csv' (FORMAT 'csv', HEADER, delimiter ";", ENCODING 'WIN1252');

-- Add geometry to table (these points are locations of hydropower buildings)
ALTER TABLE rawdata.hydropower 
  ADD geom geography(POINT,4326);

update rawdata.hydropower 
set 
geom = ST_SetSRID(ST_MakePoint("Long","Lat"),4326);



-- sweden.rn definition

-- Drop table

-- DROP TABLE sweden.rn;

/*
 Creation of the sweden.rn table 
 */

drop table if exists sweden.rna;
CREATE TABLE sweden.rna (fictitious boolean,
innetwork int4,
	CONSTRAINT c_pk_rnaidsegment PRIMARY KEY (idsegment)
)
INHERITS (dbeel_rivers.rna);
CREATE INDEX indexidsegmentrna ON sweden.rna USING btree (idsegment);
ALTER TABLE sweden.rna ADD CONSTRAINT c_fk_idsegment FOREIGN KEY (idsegment)
REFERENCES sweden.rn(idsegment) ON UPDATE CASCADE ON DELETE CASCADE;
/* TODO CREATE INDEXES ON TABLE rna
CREATE INDEX indexcountry ON sweden.rn USING btree (country);
CREATE INDEX indexidsegmentrrn ON sweden.rn USING btree (idsegment);
CREATE INDEX indexnextdownidsegmentrrn ON sweden.rn USING btree (nextdownidsegment);
CREATE INDEX indexsourcern ON sweden.rn USING btree (source);
CREATE INDEX indextargetrn ON sweden.rn USING btree (target);
CREATE INDEX rn_geom_ix ON sweden.rn USING gist (geom);
*/
-- Insert values of idsegments from table rn to table rna


INSERT INTO sweden.rna(idsegment) SELECT idsegment FROM sweden.rn;
-- Updated Rows 1458830


SELECT count(*) FROM  sweden.rn; 
UPDATE sweden.rn SET seaidsegment = NULL;
UPDATE sweden.rn SET seaidsegment = idsegment WHERE issea ; --4957
UPDATE sweden.rn SET path= text2ltree(idsegment) WHERE issea ; --4957
SELECT path FROM  sweden.rn WHERE path  IS NOT NULL;
SELECT path FROM  sweden.rn WHERE issea;



-- update from the table created in R for path and distancesea
CREATE INDEX ON sweden.temp_rna_distance USING btree(idsegment);
UPDATE sweden.rn set path=temp_rna_distance.path::ltree FROM
sweden.temp_rna_distance WHERE temp_rna_distance.idsegment=rn.idsegment; --Updated Rows 1353007

UPDATE sweden.rna set distanceseam=temp_rna_distance.distanceseam::numeric FROM 
sweden.temp_rna_distance WHERE temp_rna_distance.idsegment=rna.idsegment; --Updated Rows  1353007

UPDATE sweden.rna set shreeve=temp_stream_order.shreeve FROM 
sweden.temp_stream_order WHERE temp_stream_order.idsegment=rna.idsegment; --Updated Rows  1353007

UPDATE sweden.rna set strahler=temp_stream_order.strahler FROM 
sweden.temp_stream_order WHERE temp_stream_order.idsegment=rna.idsegment; --Updated Rows  1353007


CREATE OR REPLACE VIEW sweden.rn_rna
AS SELECT rn.idsegment,
    rn.source,
    rn.target,
    rn.lengthm,
    rn.nextdownidsegment,
    rn.path,
    rn.isfrontier,
    rn.issource,
    rn.seaidsegment,
    rn.issea,
    rn.geom,
    rn.isendoreic,
    rn.isinternational,
    rn.country,
    rna.altitudem,
    rna.distanceseam,
    rna.distancesourcem,
    rna.cumnbdam,
    rna.medianflowm3ps,
    rna.surfaceunitbvm2,
    rna.surfacebvm2,
    rna.strahler,
    rna.shreeve,
    rna.codesea,
    rna.name,
    rna.pfafriver,
    rna.pfafsegment,
    rna.basin,
    rna.riverwidthm,
    rna.riverwidthmsource,
    rna.temperature,
    rna.temperaturejan,
    rna.temperaturejul,
    rna.wettedsurfacem2,
    rna.wettedsurfaceotherm2,
    rna.lengthriverm,
    rna.emu,
    rna.cumheightdam,
    rna.slope,
    rna.dis_m3_pyr_riveratlas,
    rna.dis_m3_pmn_riveratlas,
    rna.dis_m3_pmx_riveratlas,
    rna.drought,
    rna.drought_type_calc
   FROM sweden.rn
     JOIN sweden.rna ON rna.idsegment = rn.idsegment;


----------------------------------------
-- Assign basin to each river segment --
----------------------------------------

-- catchment area numbers for river segments
drop table if exists temp_basin;
create table temp_basin as
SELECT distinct rn.idsegment, sb.haro--, row_number () over (
		--PARTITION BY rn.idsegment)  
FROM sweden.rn rn, rawdata.sub_basin sb 
where ST_INTERSECTS(rn.geom, ST_Transform(sb.geom,3035));

-- find catchment area for segments crossing catchment area borders(choose catchment area associated with nextdownidsegment)
create temporary table cross_border_segments as(
with temp_double_idsegments as( -- segments that cross catchment areas
(select idsegment
from temp_basin
group by idsegment having count(*) >1)) 
select next_idsegment.idsegment, sb.haro  
from (
select srn.idsegment, srn2.geom
from
temp_double_idsegments
left join sweden.rn srn on temp_double_idsegments.idsegment=srn.idsegment
left join sweden.rn srn2 on srn.nextdownidsegment =srn2.idsegment) next_idsegment, rawdata.sub_basin sb
where ST_intersects(next_idsegment.geom, st_transform(sb.geom,3035))); 

drop table if exists cross_border_segments2;
create temporary table cross_border_segments2 as(
with temp_double_idsegments 
as( -- segments that cross catchment areas
select idsegment
from 
cross_border_segments
group by idsegment having count(*) >1) 
select next_idsegment.idsegment, sb.haro  
from (
select srn.idsegment, srn3.geom
from
temp_double_idsegments
left join sweden.rn srn on temp_double_idsegments.idsegment=srn.idsegment
left join sweden.rn srn2 on srn.nextdownidsegment =srn2.idsegment
left join sweden.rn srn3 on srn2.nextdownidsegment=srn3.idsegment -- check if segment two steps away belongs to one catchment area only
) next_idsegment, rawdata.sub_basin sb
where ST_intersects(next_idsegment.geom, st_transform(sb.geom,3035))); 

-- there are 5 segments that have cross boarder segments 2 segments away. Here I manually change those!!!
delete from cross_border_segments2 
where idsegment='SW1309611' and haro=77000;

delete from cross_border_segments2 
where idsegment='SW1309501' and haro=77000;

delete from cross_border_segments2 
where idsegment='SW482546' and haro=32000;

delete from cross_border_segments2
where idsegment='SW599885' and haro=32000;

delete from cross_border_segments2
where idsegment= 'SW505613' and haro = 26027;

-- drop duplicate segments in each subtable
delete from temp_basin
where idsegment in
(select idsegment
from temp_basin
group by idsegment having count(*) >1)

delete from cross_border_segments
where idsegment in
(select idsegment
from 
cross_border_segments
group by idsegment having count(*) >1);

-- update sweden.rna table with catchment area
update sweden.rna srna
set basin = basins_temp.haro 
from (select *
from temp_basin 
union
select * 
from cross_border_segments
union
select*
from cross_border_segments2)
basins_temp
where srna.idsegment = basins_temp.idsegment;

--- end of river basin update ------
------------------------------------
------------------------------------
