
/* ---------------------------------
 * CREATE_EEL_ELECTROFISHING_TABLE -
 * ---------------------------------
 * 
 * This script is used to create a table with eel density data from SERS to be used in EDA 2.4. It uses tables in SERS. 
 * Important!!!! Run tasks (in map DatTransSERS) in order to get the most up to data SERS data in EDA2.4 database.
 * 
 * @ Torbj�rn S�terberg (torbjorn.saterberg@slu.se) 2023-01-24
 */

-------------------------------------
-- Create eel electrofishing table --
-------------------------------------

-----------------------------------------------------------
-----------------------------------------------------------
--         				eel_dens    					 --
--														 --
-- A table contain all eel density estimates from        --
-- the sers database. If eel_dens is null infers         --
-- that no eels were caught during that electrofishing   --
-- operation.                                            --
-----------------------------------------------------------
-----------------------------------------------------------
DROP TABLE IF EXISTS rawdata.eel_dens CASCADE; 
CREATE TABLE rawdata.eel_dens AS(
SELECT ed.elfisk_nyckel id,
	   eld.elflok_nyckel id_local,
	   eld.elflok_xkoorlok x_coord_RT90,
	   eld.elflok_ykoorlok y_coord_RT90,
	   ST_POINT(eld.elflok_ykoorlok,eld.elflok_xkoorlok,3021) geom,
	   ed.antutfis number_of_passes,
	   eel_dens.storfisk eel_density,
	   eel_utf.utfiske1 pass1,
	   eel_utf.utfiske2 pass2,
	   eel_utf.utfiske3 pass3,
	   eel_utf.utfiske4 pass4,
	   ed.area as "area",
	   ed.bredd AS river_width,
	   ed.lokalbre AS river_width_loc,
	   ed.avfiskbr AS full_width_pass, -- has the full width been electrofished? If N full width has not been electrofished.
	   ed.fiskedat AS fishing_date,
	   ed.substr1 AS substr1,
	   ed.substr2 AS substr2,
	   ed.substr3 AS substr3,
	   ed.vattenha AS water_flow,
	   ed.vattenni AS water_level,
	   ed.grumligh AS water_turbidity,
	   ed.vtnfarg AS water_colour,
	   ed.maxdjup AS water_depth_max,
	   ed.medeldju AS water_depth_mean,
	   ed.bottento AS topography,
	   ed.vattente AS water_temp,
	   ed.lufttemp AS air_temp,
	   ed.beskuggn AS shade_level, 
	   ed.vandhind AS migration_obstacle,
	   ed.utforare AS responsible_for_inventory,
	   vd.vattendrag_namn AS watercourse_name
	   	   
FROM 
sers.ELPROVFISKE_DIM ed -- ALL Electrofishing operations in the SERS database
LEFT JOIN 
		 (SELECT DISTINCT ON(elfisk_nyckel) *
		  FROM
		  sers.ELFISKE_UTFISKE_FACT 
		  WHERE sers.ELFISKE_UTFISKE_FACT.arter_nyckel =153 
		  ORDER BY elfisk_nyckel) 
		  AS eel_utf -- PASSES where eels have been caught
ON ed.ELFISK_NYCKEL = EEL_UTF.ELFISK_NYCKEL
LEFT JOIN 
		 (SELECT *
		  FROM
		  sers.elfiske_fangst_fact
		  WHERE arter_nyckel =153 AND langd IS NULL
		  ORDER BY elfisk_nyckel) 
		  AS eel_dens
ON ed.ELFISK_NYCKEL = eel_dens.ELFISK_NYCKEL
LEFT JOIN 
		(SELECT DISTINCT ON (elflok_nyckel,elfisk_nyckel, vattendrag_nr) elflok_nyckel, elfisk_nyckel, vattendrag_nr  
		 FROM sers.elfiske_fangst_fact eff) 
		 AS keys -- keys for location and distinct electrofishing operation
ON ed.elfisk_nyckel=keys.elfisk_nyckel
LEFT JOIN 
	    sers.elfiske_lokal_dim eld 
ON keys.elflok_nyckel=eld.elflok_nyckel
LEFT JOIN 
		sers.vattendrag_dim vd 
ON keys.vattendrag_nr=vd.vattendrag_nr 
);

-- add primary key
ALTER TABLE rawdata.eel_dens 
ADD PRIMARY KEY(id);

-- add spatial index
CREATE INDEX elfish_geom_ix ON rawdata.eel_dens USING gist (geom);

-------------------------------------------
-- Snap electrofishing data to sweden.rn --
-------------------------------------------

-- Add columns idsegment(from sweden.rn) and snapped_geom(geometries snapped to the rivernetwork) to rawdata.sers table
ALTER TABLE rawdata.eel_dens 
ADD COLUMN snapped_geom geometry,
ADD COLUMN idsegment varchar(12); -- we want to connect points segments in sweden.rn table 

-- Snap electrofishing points to rivernetwork
WITH snapped_geom AS ( 
SELECT id, shortest_dist_point.idsegment, ST_Closestpoint(shortest_dist_point.geom, st_transform(rs.geom,3035)) snapped_geom
FROM rawdata.eel_dens rs  
CROSS JOIN LATERAL (
SELECT 
geom, 
idsegment
FROM sweden.rn srn
ORDER BY st_transform(rs.geom,3035) <-> srn.geom
LIMIT 1) shortest_dist_point)
UPDATE rawdata.eel_dens rs2 
SET 
snapped_geom = snapped_geom.snapped_geom, 
idsegment = snapped_geom.idsegment
FROM snapped_geom
WHERE 
rs2.id = snapped_geom.id;  

-----------------------------------------------------------
-----------------------------------------------------------
--         				eel_length    					 --
--														 --
-- A table contain all eel lengths estimates from        --
-- the sers database. If eel_dens is null infers         --
-- that no eels were caught during that electrofishing   --
-- operation.                                            --
-----------------------------------------------------------
-----------------------------------------------------------
DROP TABLE IF EXISTS rawdata.eel_length;
CREATE TABLE rawdata.eel_length AS(
SELECT elfisk_nyckel id,
	   langd "length"
FROM sers.elfiske_fangst_fact eff2
WHERE arter_nyckel =153 AND langd IS NOT NULL
);

-- Add a foreign key referensing eel_dens table
ALTER TABLE rawdata.eel_length 
      ADD CONSTRAINT fk_id_eel_dens FOREIGN KEY (id) 
          REFERENCES rawdata.eel_dens (id);

--------------------------------------
--------------------------------------
-- Recreate views from aqua database -
--------------------------------------
--------------------------------------         
DROP VIEW IF EXISTS sers.sers_elfangst;
CREATE VIEW sers.sers_elfangst AS(
SELECT     SERS.ELFISKE_ALTITUD_DIM.ALTITUD_BESKRIVNING AS Altitudintervall, SERS.ELFISKE_LOKAL_DIM.ANDSJOPR AS "Andel sj� i avrinningsomr", 
                     ANSVARIG.PRFUTF_BESKRIVNING AS "Ansvarig utf�rare", SERS.ELPROVFISKE_DIM.ANTUTFIS AS "Antal utfisken", SERS.SOTVATTENARTER_DIM.ARTER_NAMN AS Art, 
                      SERS.SOTVATTENARTER_DIM.ARTER_NYCKEL AS ArtPK, SERS.ELPROVFISKE_DIM.AREA AS "Avfiskad yta", 
                      SERS.ELFISKE_LOKAL_DIM.AVRIOMRK AS "Avrinningsomr�des klass", SERS.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NAMN AS Avrinningsomr�desklass, 
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_XKOORLOK AS "Elfiskelokal xkoord", SERS.ELFISKE_LOKAL_DIM.ELFLOK_YKOORLOK AS "Elfiskelokal ykoord", 
                      SERS.DAG_DATUM_DIM.DAG_DATUM AS Fiskedatum, SERS.DAG_DATUM_DIM.AAR_NR AS Fiske�r, 
                      SERS.FISKREGION_DIM.FISKREGION_KOD AS Fiskregionnr, SERS.HUVUDAVROMR_DIM.HUVUDAVROMR_NR AS Huvudavrinningsnr, 
                      SERS.HUVUDAVROMR_DIM.HUVUDAVROMR_NAMN AS Huvudavrinningsomr, SERS.ELFISKE_LOKAL_DIM.HOH AS "H�jd �ver havet", 
                      SERS.ELFISKE_LOKAL_DIM.LAENK_KOMMNAMN AS Kommun, SERS.ELFISKE_LOKAL_DIM.LAENK_KOMMKOD AS Kommunnr, 
                      SERS.FISKREGION_DIM.FISKREGION_BESKRIVNING AS "Limnologisk ekoregion", SERS.ELPROVFISKE_DIM.LOKALBRE AS Lokalbredd, 
                      SERS.ELPROVFISKE_DIM.LANGD AS Lokall�ngd, SERS.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNAMN AS Lokalnamn, 
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNR AS Lokalnr, SERS.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL AS LokalPK, 
                      SERS.ELFISKE_LOKAL_DIM.LAEN_LAENNAMN AS L�n, SERS.ELFISKE_FANGST_FACT.LANGD AS L�ngd, 
                      SERS.LANGDINTERVALL_DIM.LANGDINTERV_BESKRIVNING AS L�ngdintervall, SERS.ELFISKE_LOKAL_DIM.LAEN_LAENSKOD AS L�nskod, 
                      SERS.DAG_DATUM_DIM.MANAD_TEXT AS M�nad, SERS.DAG_DATUM_DIM.MANAD_NR AS M�nadsnr, SERS.ELFISKE_FANGST_FACT.ELFISK_NYCKEL AS ProvfiskePK, 
                      SERS.ELFISKE_FANGST_FACT.ELFFANGST_NYCKEL AS Sj�f�ngstPK, SERS.ELPROVFISKE_DIM.SUBSTR1 AS Substrat1, 
                      SERS.ELPROVFISKE_DIM.SUBSTR2 AS Substrat2, SERS.ELPROVFISKE_DIM.SUBSTR3 AS Substrat3, SERS.PROVFISKE_SYFTE_DIM.PRFSYFTE_BESKRIVNING AS Syfte, 
                      SERS.PROVFISKE_SYFTE_DIM.PRFSYFTE_KOD AS Syfteskod, SERS.TYPAVPOP_DIM.TYPAVPOP_BESKRIVNING AS "Typ av population", 
                      SERS.ELFISKE_FANGST_FACT.OPLUS AS "T�thet �rsungar", 
                      SERS.ELFISKE_FANGST_FACT.STORFISK AS "T�thet �ldre �n �rsungar", 
                      SERS.ELFISKE_FANGST_FACT.ELFFANGST_SKAPAD AS UppdateradDatum, SERS.PROVFISKE_UTFORARE_DIM.PRFUTF_BESKRIVNING AS Urf�rare, 
                      SERS.PROVFISKE_UTFORARE_DIM.PRFUTF_KOD AS "Utf�rare kod", SERS.VATTENDRAG_DIM.VATTENDRAG_NR AS VattendragPK, 
                      SERS.ELFISKE_LOKAL_DIM.XKOORVDR AS "Vattendrag xkoord", SERS.ELFISKE_LOKAL_DIM.YKOORVDR AS "Vattendrag ykoord", 
                      SERS.VATTENDRAG_DIM.VATTENDRAG_NAMN AS Vattendragsnamn, SERS.ELPROVFISKE_DIM.VATTENHA AS Vattenhastighet, 
                      SERS.ELPROVFISKE_DIM.VATTENNI AS Vattenniv�, SERS.VATTENDRAGBREDD_DIM.VDRAGBREDD_BESKRIVNING AS "Vattendragsbredd klasser", 
                      SERS.ELPROVFISKE_DIM.BREDD AS "Vattendragsbredd lokal", SERS.VATTENDRAGBREDD_DIM.VDRAGBREDD_SORTERING AS "Vattendragsbredd sortering", 
                      SERS.ELFISKE_LOKAL_DIM.IKEU, 
                      SERS.ELFISKE_LOKAL_DIM.NMO,
                                       Case 
                                                                           when  "oplus" is null and "storfisk" is null then 0
                                                                           else 1
                                                                           end
                                                              as Finns_t�theter
                                                              ,Case 
                                                                           when  "oplus" is null and "storfisk" is null then 0
                                                                           when "oplus" = 0 and  "storfisk" is null then 0
                                                                           when "oplus" is null and  "storfisk" = 0 then 0
                                                                           else 1
                                                                           end
                                                              as Finns_t�theter_x,
                                                              SERS.ELFISKE_LOKAL_DIM.ELFLOK_LOKALID AS Lokalid, 
                                                               SERS.ELFISKE_LOKAL_DIM.ELFLOK_VATTENDRAGSID AS Vattendragsid, 
                                                               SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_LOKAL AS "SSTM99_N Elfiskelokal",
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_LOKAL AS "SSTM99_E Elfiskelokal", 
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_VDR AS "SSTM99_N Vattendrag" ,
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_VDR AS "SSTM99_E Vattendrag",
                      SERS.ELFISKE_LOKAL_DIM.ELFLOK_EU_CD_LOK AS "EU_CD lokal", 
                      SERS.ELPROVFISKE_DIM.UTFORARE AS "Utf�rare",  
                                                                  CASE ANSVARIG.PRFUTF_KOD WHEN '?' THEN '' ELSE ANSVARIG.PRFUTF_BESKRIVNING END AS  Ansvarig,
                      SERS.ELFISKE_LOKAL_DIM.XKOORVDR AS "Vattendrag xkoord_RT90", SERS.ELFISKE_LOKAL_DIM.YKOORVDR AS "Vattendrag ykoord_RT90",
                                                                SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_VDR AS "Vattendrag S99TM_N", SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_VDR AS "Vattendrag S99TM_E", 
                                                                SERS.ELFISKE_LOKAL_DIM.ELFLOK_XKOORLOK AS "Elfiskelokal xkoord_RT90", SERS.ELFISKE_LOKAL_DIM.ELFLOK_YKOORLOK AS "Elfiskelokal ykoord_RT90", 
                                                                SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_LOKAL AS "Elfiskelokal S99TM_N", SERS.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_LOKAL AS "Elfiskelokal S99TM_E"
                                                                ,"poptyp"      ,"nsp"      ,"n�lax"      ,"predn�lax"      ,"p_vix_n�lax"      ,"nandtol"      ,"pred_nandtol"      ,"p_vix_nandtol"      ,"nandlith"
      ,"pred_nandlith"      ,"p_vix_nandlith"      ,"spproptol"      ,"pred_spproptol"      ,"p_vix_spproptol"      ,"sppropint"      ,"pred_sppropint"      ,"p_vix_sppropint"
      ,"kvot"      ,"pred_kvot"      ,"p_vix_kvot"      ,"simpson"      ,"pred_simpson"      ,"p_vixh_simpson"      ,"vixsm"      ,"vixh"      ,"vix_v�rde" , SERS.ELPROVFISKE_DIM.FIXV AS "vix klass"
,"vixmorf_v�rde"      ,"p�verkat_vixmorf"      ,"kommentar_vixmorf", ELFFANGST_NYCKEL, FISKEDAT_KEY, SERS.HUVUDAVROMR_DIM.HUVUDAVROMR_NYCKEL 
FROM         SERS.ELFISKE_FANGST_FACT INNER JOIN
                      SERS.PROVFISKE_UTFORARE_DIM ON SERS.ELFISKE_FANGST_FACT.PRFUTF_NYCKEL = SERS.PROVFISKE_UTFORARE_DIM.PRFUTF_NYCKEL INNER JOIN
                      SERS.SOTVATTENARTER_DIM ON SERS.ELFISKE_FANGST_FACT.ARTER_NYCKEL = SERS.SOTVATTENARTER_DIM.ARTER_NYCKEL INNER JOIN
                      SERS.HUVUDAVROMR_DIM ON SERS.ELFISKE_FANGST_FACT.HUVUDAVROMR_NYCKEL = SERS.HUVUDAVROMR_DIM.HUVUDAVROMR_NYCKEL INNER JOIN
                      SERS.FISKREGION_DIM ON SERS.ELFISKE_FANGST_FACT.FISKREG_NYCKEL = SERS.FISKREGION_DIM.FISKREGION_NYCKEL INNER JOIN
                      SERS.PROVFISKE_SYFTE_DIM ON SERS.ELFISKE_FANGST_FACT.PRFSYFTE_NYCKEL = SERS.PROVFISKE_SYFTE_DIM.PRFSYFTE_NYCKEL INNER JOIN
                      SERS.VATTENDRAG_DIM ON SERS.ELFISKE_FANGST_FACT.VATTENDRAG_NR = SERS.VATTENDRAG_DIM.VATTENDRAG_NR INNER JOIN
                      SERS.ELPROVFISKE_DIM ON SERS.ELFISKE_FANGST_FACT.ELFISK_NYCKEL = SERS.ELPROVFISKE_DIM.ELFISK_NYCKEL INNER JOIN
                      SERS.TYPAVPOP_DIM ON SERS.ELFISKE_FANGST_FACT.TYPAVPOP_NYCKEL = SERS.TYPAVPOP_DIM.TYPAVPOP_NYCKEL INNER JOIN
                      SERS.ELFISKE_AVROMRKLASS_DIM ON 
                      SERS.ELFISKE_FANGST_FACT.AVROMRKLASS_NYCKEL = SERS.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NYCKEL INNER JOIN
                      SERS.LANGDINTERVALL_DIM ON SERS.ELFISKE_FANGST_FACT.LANGDINTERV_NYCKEL = SERS.LANGDINTERVALL_DIM.LANGDINTERV_NYCKEL INNER JOIN
                      SERS.ELFISKE_ALTITUD_DIM ON SERS.ELFISKE_FANGST_FACT.ALTITUD_NYCKEL = SERS.ELFISKE_ALTITUD_DIM.ALTITUD_NYCKEL INNER JOIN
                      SERS.ELFISKE_LOKAL_DIM ON SERS.ELFISKE_FANGST_FACT.ELFLOK_NYCKEL = SERS.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL INNER JOIN
                      SERS.DAG_DATUM_DIM ON SERS.ELFISKE_FANGST_FACT.FISKEDAT_KEY = SERS.DAG_DATUM_DIM.DAG_NYCKEL INNER JOIN
                      SERS.VATTENDRAGBREDD_DIM ON SERS.ELFISKE_FANGST_FACT.VDRAGBREDD_NYCKEL = SERS.VATTENDRAGBREDD_DIM.VDRAGBREDD_NYCKEL LEFT OUTER JOIN
                      SERS.PROVFISKE_UTFORARE_DIM   ANSVARIG ON ANSVARIG.PRFUTF_KOD = SERS.ELPROVFISKE_DIM.ANSVARIG 
WHERE     (SERS.ELFISKE_FANGST_FACT.LANGD IS NULL)
)


DROP VIEW IF EXISTS sers.sers_elfangst_individ;
CREATE VIEW sers.sers_elfangst_individ AS(
SELECT    			  sers.ELFISKE_ALTITUD_DIM.ALTITUD_BESKRIVNING AS Altitudintervall, 
                      sers.ELFISKE_LOKAL_DIM.ANDSJOPR AS "Andel sj� i avrinningsomr", 
                      ANSVARIG.PRFUTF_BESKRIVNING AS "Ansvarig utf�rare", 
                      sers.ELPROVFISKE_DIM.ANTUTFIS AS "Antal utfisken", 
					  sers.SOTVATTENARTER_DIM.ARTER_NAMN AS Art, 
                      sers.SOTVATTENARTER_DIM.ARTER_NYCKEL AS ArtPK, 
                      sers.ELPROVFISKE_DIM.AREA AS "Avfiskad yta", 
                      sers.ELFISKE_LOKAL_DIM.AVRIOMRK AS "Avrinningsomr�des klass", 
                      sers.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NAMN AS Avrinningsomr�desklass, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_YKOORLOK AS "Elfiskelokal ykoord", 
                      sers.DAG_DATUM_DIM.DAG_DATUM AS Fiskedatum, 
                      sers.DAG_DATUM_DIM.AAR_NR AS Fiske�r, 
                      sers.FISKREGION_DIM.FISKREGION_KOD AS Fiskregionnr, 
                      sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NR AS Huvudavrinningsnr,
                      sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NAMN AS Huvudavrinningsomr, 
                      sers.ELFISKE_LOKAL_DIM.HOH AS "H�jd �ver havet", 
                      sers.ELFISKE_LOKAL_DIM.LAENK_KOMMNAMN AS Kommun, 
                      sers.ELFISKE_LOKAL_DIM.LAENK_KOMMKOD AS Kommunnr, 
                      sers.FISKREGION_DIM.FISKREGION_BESKRIVNING AS "Limnologisk ekoregion", 
                      sers.ELPROVFISKE_DIM.LOKALBRE AS Lokalbredd, 
                      sers.ELPROVFISKE_DIM.LANGD AS Lokall�ngd, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNAMN AS Lokalnamn, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNR AS Lokalnr, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL AS LokalPK, 
                      sers.ELFISKE_LOKAL_DIM.LAEN_LAENNAMN AS L�n, 
                      sers.ELFISKE_FANGST_FACT.LANGD AS L�ngd, 
                      sers.LANGDINTERVALL_DIM.LANGDINTERV_BESKRIVNING AS L�ngdintervall, 
                      sers.ELFISKE_LOKAL_DIM.LAEN_LAENSKOD AS L�nskod, 
                      sers.DAG_DATUM_DIM.MANAD_TEXT AS M�nad, 
                      sers.DAG_DATUM_DIM.MANAD_NR AS M�nadsnr, 
                      sers.ELFISKE_FANGST_FACT.ELFISK_NYCKEL AS ProvfiskePK, 
					  sers.ELFISKE_FANGST_FACT.ELFFANGST_NYCKEL AS Sj�f�ngstPK, 
                      sers.ELPROVFISKE_DIM.SUBSTR1 AS Substrat1, 
                      sers.ELPROVFISKE_DIM.SUBSTR2 AS Substrat2, 
                      sers.ELPROVFISKE_DIM.SUBSTR3 AS Substrat3, 
                      sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_BESKRIVNING AS Syfte, 
                      sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_KOD AS Syfteskod, 
                      sers.TYPAVPOP_DIM.TYPAVPOP_BESKRIVNING AS "Typ av population", 
                      sers.ELFISKE_FANGST_FACT.OPLUS AS "T�thet �rsungar", 
                      sers.ELFISKE_FANGST_FACT.STORFISK AS "T�thet �ldre �n �rsungar", 
					  sers.ELFISKE_FANGST_FACT.ELFFANGST_SKAPAD AS UppdateradDatum, 
                      sers.PROVFISKE_UTFORARE_DIM.PRFUTF_BESKRIVNING AS Urf�rare, 
					  sers.PROVFISKE_UTFORARE_DIM.PRFUTF_KOD AS "Utf�rare kod", 
                      sers.VATTENDRAG_DIM.VATTENDRAG_NR AS VattendragPK, 
                      sers.ELFISKE_LOKAL_DIM.XKOORVDR AS "Vattendrag xkoord", 
                      sers.ELFISKE_LOKAL_DIM.YKOORVDR AS "Vattendrag ykoord", 
                      sers.VATTENDRAG_DIM.VATTENDRAG_NAMN AS Vattendragsnamn, 
                      sers.ELPROVFISKE_DIM.VATTENHA AS Vattenhastighet, 
                      sers.ELPROVFISKE_DIM.VATTENNI AS Vattenniv�, 
                      sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_BESKRIVNING AS "Vattendragsbredd klasser", 
                      sers.ELPROVFISKE_DIM.BREDD AS "Vattendragsbredd lokal", 
                      sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_SORTERING AS "Vattendragsbredd sortering", 
                      sers.ELPROVFISKE_DIM.FIXV AS "VIX klass",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALID AS Lokalid, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_VATTENDRAGSID AS Vattendragsid, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_LOKAL AS "SSTM99_N Elfiskelokal",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_LOKAL AS "SSTM99_E Elfiskelokal", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_VDR AS "SSTM99_N Vattendrag" ,
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_VDR AS "SSTM99_E Vattendrag",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_EU_CD_LOK AS "EU_CD lokal", 
                      sers.ELPROVFISKE_DIM.UTFORARE AS "Utf�rare", 
                      CASE ANSVARIG.PRFUTF_KOD WHEN '?' THEN '' ELSE ANSVARIG.PRFUTF_BESKRIVNING END AS  Ansvarig,
                      sers.ELFISKE_LOKAL_DIM.XKOORVDR AS "Vattendrag xkoord_RT90", 
                      sers.ELFISKE_LOKAL_DIM.YKOORVDR AS "Vattendrag ykoord_RT90",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_VDR AS "Vattendrag S99TM_N", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_VDR AS "Vattendrag S99TM_E", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_XKOORLOK AS "Elfiskelokal xkoord_RT90", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_YKOORLOK AS "Elfiskelokal ykoord_RT90", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_LOKAL AS "Elfiskelokal S99TM_N", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_LOKAL AS "Elfiskelokal S99TM_E"
FROM         		  sers.ELFISKE_FANGST_FACT INNER JOIN
                      sers.PROVFISKE_UTFORARE_DIM ON sers.ELFISKE_FANGST_FACT.PRFUTF_NYCKEL = sers.PROVFISKE_UTFORARE_DIM.PRFUTF_NYCKEL INNER JOIN
                      sers.SOTVATTENARTER_DIM ON sers.ELFISKE_FANGST_FACT.ARTER_NYCKEL = sers.SOTVATTENARTER_DIM.ARTER_NYCKEL INNER JOIN
                      sers.HUVUDAVROMR_DIM ON sers.ELFISKE_FANGST_FACT.HUVUDAVROMR_NYCKEL = sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NYCKEL INNER JOIN
                      sers.FISKREGION_DIM ON sers.ELFISKE_FANGST_FACT.FISKREG_NYCKEL = sers.FISKREGION_DIM.FISKREGION_NYCKEL INNER JOIN
                      sers.PROVFISKE_SYFTE_DIM ON sers.ELFISKE_FANGST_FACT.PRFSYFTE_NYCKEL = sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_NYCKEL INNER JOIN
                      sers.VATTENDRAG_DIM ON sers.ELFISKE_FANGST_FACT.VATTENDRAG_NR = sers.VATTENDRAG_DIM.VATTENDRAG_NR INNER JOIN
                      sers.ELPROVFISKE_DIM ON sers.ELFISKE_FANGST_FACT.ELFISK_NYCKEL = sers.ELPROVFISKE_DIM.ELFISK_NYCKEL INNER JOIN
                      sers.TYPAVPOP_DIM ON sers.ELFISKE_FANGST_FACT.TYPAVPOP_NYCKEL = sers.TYPAVPOP_DIM.TYPAVPOP_NYCKEL INNER JOIN
                      sers.ELFISKE_AVROMRKLASS_DIM ON 
                      sers.ELFISKE_FANGST_FACT.AVROMRKLASS_NYCKEL = sers.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NYCKEL INNER JOIN
                      sers.LANGDINTERVALL_DIM ON sers.ELFISKE_FANGST_FACT.LANGDINTERV_NYCKEL = sers.LANGDINTERVALL_DIM.LANGDINTERV_NYCKEL INNER JOIN
                      sers.ELFISKE_ALTITUD_DIM ON sers.ELFISKE_FANGST_FACT.ALTITUD_NYCKEL = sers.ELFISKE_ALTITUD_DIM.ALTITUD_NYCKEL INNER JOIN
                      sers.ELFISKE_LOKAL_DIM ON sers.ELFISKE_FANGST_FACT.ELFLOK_NYCKEL = sers.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL INNER JOIN
                      sers.DAG_DATUM_DIM ON sers.ELFISKE_FANGST_FACT.FISKEDAT_KEY = sers.DAG_DATUM_DIM.DAG_NYCKEL INNER JOIN
                      sers.VATTENDRAGBREDD_DIM ON sers.ELFISKE_FANGST_FACT.VDRAGBREDD_NYCKEL = sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_NYCKEL LEFT OUTER JOIN
                      sers.PROVFISKE_UTFORARE_DIM   ANSVARIG ON ANSVARIG.PRFUTF_KOD = sers.ELPROVFISKE_DIM.ANSVARIG 
WHERE     (NOT (sers.ELFISKE_FANGST_FACT.LANGD IS NULL))
);

DROP VIEW IF EXISTS sers.sers_elutfiske;
CREATE VIEW sers.sers_elutfiske AS (
SELECT     
 CASE sers.ELFISKE_UTFISKE_FACT.ALDERSTA 
            WHEN -9 THEN ' '
            WHEN 0 THEN '0+'
            WHEN 1 THEN '>0+'
            ELSE CAST(sers.ELFISKE_UTFISKE_FACT.ALDERSTA AS varchar(3)) 
            END AS "�lder", 
            		  sers.ELFISKE_UTFISKE_FACT.UTFISKE1 AS "Antal utfiske 1", 
                      sers.ELFISKE_UTFISKE_FACT.UTFISKE2 AS  "Antal utfiske 2", 
                      sers.ELFISKE_UTFISKE_FACT.UTFISKE3 AS  "Antal utfiske 3", 
                      sers.ELFISKE_UTFISKE_FACT.UTFISKE4 AS  "Antal utfiske 4",             
 					  sers.ELFISKE_ALTITUD_DIM.ALTITUD_BESKRIVNING AS Altitudintervall, sers.ELFISKE_LOKAL_DIM.ANDSJOPR AS "Andel sj� i avrinningsomr", 
                      sers.ELPROVFISKE_DIM.ANSVARIG AS "Ansvarig utf�rare", 
                      sers.ELPROVFISKE_DIM.ANTUTFIS AS "Antal utfisken", 
                      sers.SOTVATTENARTER_DIM.ARTER_NAMN AS Art, 
                      sers.SOTVATTENARTER_DIM.ARTER_NYCKEL AS ArtPK, 
                      sers.ELPROVFISKE_DIM.AREA AS "Avfiskad yta", 
                      sers.ELFISKE_LOKAL_DIM.AVRIOMRK AS "Avrinningsomr�des klass", 
                      sers.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NAMN AS Avrinningsomr�desklass, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_XKOORLOK AS "Elfiskelokal xkoord_RT90", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_YKOORLOK AS "Elfiskelokal ykoord_RT90", 
                      sers.DAG_DATUM_DIM.DAG_DATUM AS Fiskedatum, 
                      sers.DAG_DATUM_DIM.AAR_NR AS Fiske�r, 
                      sers.FISKREGION_DIM.FISKREGION_KOD AS Fiskregionnr, 
                      sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NR AS Huvudavrinningsnr, 
                      sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NAMN AS Huvudavrinningsomr, 
                      sers.ELFISKE_LOKAL_DIM.HOH AS "H�jd �ver havet", 
                      sers.ELFISKE_LOKAL_DIM.LAENK_KOMMNAMN AS Kommun, 
                      sers.ELFISKE_LOKAL_DIM.LAENK_KOMMKOD AS Kommunnr, 
                      sers.FISKREGION_DIM.FISKREGION_BESKRIVNING AS "Limnologisk ekoregion", 
                      sers.ELPROVFISKE_DIM.LOKALBRE AS Lokalbredd, 
                      sers.ELPROVFISKE_DIM.LANGD AS Lokall�ngd, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNAMN AS Lokalnamn, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALNR AS Lokalnr, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL AS LokalPK, 
                      sers.ELFISKE_LOKAL_DIM.LAEN_LAENNAMN AS L�n, 
                      sers.ELFISKE_LOKAL_DIM.LAEN_LAENSKOD AS L�nskod, 
                      sers.DAG_DATUM_DIM.MANAD_TEXT AS M�nad, 
                      sers.DAG_DATUM_DIM.MANAD_NR AS M�nadsnr, 
                      sers.ELFISKE_UTFISKE_FACT.ELFISK_NYCKEL AS ProvfiskePK, 
                      sers.ELPROVFISKE_DIM.SUBSTR1 AS Substrat1, 
                      sers.ELPROVFISKE_DIM.SUBSTR2 AS Substrat2, 
                      sers.ELPROVFISKE_DIM.SUBSTR3 AS Substrat3, 
                      sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_BESKRIVNING AS Syfte, 
                      sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_KOD AS Syfteskod, 
                      sers.TYPAVPOP_DIM.TYPAVPOP_BESKRIVNING AS "Typ av population",
                      sers.ELFISKE_UTFISKE_FACT.ELFUTF_SKAPAD AS UppdateradDatum, 
                      sers.PROVFISKE_UTFORARE_DIM.PRFUTF_BESKRIVNING AS Urf�rare, 
                      sers.PROVFISKE_UTFORARE_DIM.PRFUTF_KOD AS "Utf�rare kod", 
                      sers.VATTENDRAG_DIM.VATTENDRAG_NR AS VattendragPK, 
                      sers.ELFISKE_LOKAL_DIM.XKOORVDR AS "Vattendrag xkoord_RT90", 
                      sers.ELFISKE_LOKAL_DIM.YKOORVDR AS "Vattendrag ykoord_RT90", 
                      sers.VATTENDRAG_DIM.VATTENDRAG_NAMN AS Vattendragsnamn, 
                      sers.ELPROVFISKE_DIM.VATTENHA AS Vattenhastighet, 
                      sers.ELPROVFISKE_DIM.VATTENNI AS Vattenniv�, 
                      sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_BESKRIVNING AS "Vattendragsbredd klasser", 
                      sers.ELPROVFISKE_DIM.BREDD AS "Vattendragsbredd lokal", 
                      sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_SORTERING AS "Vattendragsbredd sortering", 
                      sers.ELPROVFISKE_DIM.FIXV AS "VIX klass", 
                      sers.ELFISKE_LOKAL_DIM.IKEU, 
                      sers.ELFISKE_LOKAL_DIM.NMO,
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_LOKALID AS Lokalid, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_VATTENDRAGSID AS Vattendragsid, 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_LOKAL AS "Elfiskelokal S99TM_N",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_LOKAL AS "Elfiskelokal S99TM_E", 
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_N_VDR AS "Vattendrag S99TM_N" ,
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_S99TM_E_VDR AS "Vattendrag S99TM_E",
                      sers.ELFISKE_LOKAL_DIM.ELFLOK_EU_CD_LOK AS "EU_CD lokal", 
                      sers.ELPROVFISKE_DIM.UTFORARE AS "Utf�rare",
                      CASE  sers.ELPROVFISKE_DIM.ANSVARIG WHEN '?' THEN ' ' ELSE sers.ELPROVFISKE_DIM.ANSVARIG END AS Ansvarig
                                     
FROM         sers.ELFISKE_UTFISKE_FACT INNER JOIN
                      sers.DAG_DATUM_DIM ON sers.ELFISKE_UTFISKE_FACT.FISKEDAT_KEY = sers.DAG_DATUM_DIM.DAG_NYCKEL INNER JOIN
                      sers.ELFISKE_ALTITUD_DIM ON sers.ELFISKE_UTFISKE_FACT.ALTITUD_NYCKEL = sers.ELFISKE_ALTITUD_DIM.ALTITUD_NYCKEL INNER JOIN
                      sers.ELFISKE_AVROMRKLASS_DIM ON 
                      sers.ELFISKE_UTFISKE_FACT.AVROMRKLASS_NYCKEL = sers.ELFISKE_AVROMRKLASS_DIM.AVROMRKLASS_NYCKEL INNER JOIN
                      sers.ELFISKE_LOKAL_DIM ON sers.ELFISKE_UTFISKE_FACT.ELFLOK_NYCKEL = sers.ELFISKE_LOKAL_DIM.ELFLOK_NYCKEL INNER JOIN
                      sers.ELPROVFISKE_DIM ON sers.ELFISKE_UTFISKE_FACT.ELFISK_NYCKEL = sers.ELPROVFISKE_DIM.ELFISK_NYCKEL INNER JOIN
                      sers.FISKREGION_DIM ON sers.ELFISKE_UTFISKE_FACT.FISKREG_NYCKEL = sers.FISKREGION_DIM.FISKREGION_NYCKEL INNER JOIN
                      sers.HUVUDAVROMR_DIM ON sers.ELFISKE_UTFISKE_FACT.HUVUDAVROMR_NYCKEL = sers.HUVUDAVROMR_DIM.HUVUDAVROMR_NYCKEL INNER JOIN
                      sers.PROVFISKE_SYFTE_DIM ON sers.ELFISKE_UTFISKE_FACT.PRFSYFTE_NYCKEL = sers.PROVFISKE_SYFTE_DIM.PRFSYFTE_NYCKEL INNER JOIN
                      sers.PROVFISKE_UTFORARE_DIM ON sers.ELFISKE_UTFISKE_FACT.PRFUTF_NYCKEL = sers.PROVFISKE_UTFORARE_DIM.PRFUTF_NYCKEL INNER JOIN
                      sers.SOTVATTENARTER_DIM ON sers.ELFISKE_UTFISKE_FACT.ARTER_NYCKEL = sers.SOTVATTENARTER_DIM.ARTER_NYCKEL INNER JOIN
                      sers.TYPAVPOP_DIM ON sers.ELFISKE_UTFISKE_FACT.TYPAVPOP_NYCKEL = sers.TYPAVPOP_DIM.TYPAVPOP_NYCKEL INNER JOIN
                      sers.VATTENDRAG_DIM ON sers.ELFISKE_UTFISKE_FACT.VATTENDRAG_NR = sers.VATTENDRAG_DIM.VATTENDRAG_NR INNER JOIN
                      sers.VATTENDRAGBREDD_DIM ON sers.ELFISKE_UTFISKE_FACT.VDRAGBREDD_NYCKEL = sers.VATTENDRAGBREDD_DIM.VDRAGBREDD_NYCKEL
)

/*
DROP VIEW IF EXISTS raw_width 
WITH 
create temporary table raw_width as(
SELECT srn.idsegment, srn.geom, srna.shreeve, srna.strahler, srna.basin,
       CASE 
       WHEN rs1.river_width_loc IS NOT NULL THEN rs1.river_width_loc
       WHEN rs1.river_width_loc IS NULL AND rs1.full_width_pass<>'N' AND rs1.river_width IS NOT NULL THEN rs1.river_width
       END width_ef, 
       rs2.breddmedel width_bk 
       FROM sweden.rn srn
       LEFT JOIN sweden.rna srna 
       ON srn.idsegment=srna.idsegment
       LEFT JOIN 
       (select *
       from 
       rawdata.eel_dens
       except -- exclude Electrofishing points where we already have polygons
       select rs.* 
		from 
		(select *
		from
		rawdata.watercourse w 
		where geometrytype(w.geometry)='POLYGON') as wc, rawdata.eel_dens rs
		where ST_Intersects(st_transform(rs.snapped_geom,3006),wc.geometry)) as rs1 -- points from electrofishing data were we do not have polygon data
       on srn.idsegment=rs1.idsegment -- OK  
       left join
       (select *
       from 
       rawdata.prota_inv_str_punkt
       except -- exclude river inventory points where we already have polygons
       select rs.* 
		from 
		(select *
		from
		rawdata.watercourse w 
		where geometrytype(w.geometry)='POLYGON') as wc, rawdata.prota_inv_str_punkt rs
		where ST_Intersects(st_transform(rs.snapped_geom,3006),wc.geometry)) as rs2 
       ON srn.idsegment=rs2.idsegment), --OK
avg_width as( -- this part calculates average river widths
SELECT idsegment, AVG(width_ef) width_ef, AVG(width_bk) width_bk
       FROM raw_width
       GROUP BY idsegment),
"width" as(
SELECT DISTINCT ON (rw.idsegment) rw.idsegment, rw.geom, ST_X(ST_centroid(rw.geom)) X, ST_Y(ST_centroid(rw.geom)) Y, rw.shreeve, rw.strahler, rw.basin, aw.width_ef, aw.width_bk 
FROM 
raw_width rw
LEFT JOIN avg_width aw
ON rw.idsegment=aw.idsegment)
SELECT w.*
FROM 
width w
left join sweden.rn srn
on w.idsegment=srn.idsegment 
where w.width_ef is not null or w.width_bk is not null and srn.fictitious is not true;

WITH
avg_width as( -- this part calculates average river widths
SELECT idsegment, AVG(width_ef) width_ef, AVG(width_bk) width_bk
       FROM raw_width
       GROUP BY idsegment),
"width" as(
SELECT DISTINCT ON (rw.idsegment) rw.idsegment, rw.geom, ST_X(ST_centroid(rw.geom)) X, ST_Y(ST_centroid(rw.geom)) Y, rw.shreeve, rw.strahler, rw.basin, aw.width_ef, aw.width_bk 
FROM 
raw_width rw
LEFT JOIN avg_width aw
ON rw.idsegment=aw.idsegment)
SELECT w.*
FROM 
width w
left join sweden.rn srn
on w.idsegment=srn.idsegment 
where w.width_ef is not null or w.width_bk is not null and srn.fictitious is not true;

select idsegment, COUNT(*)
FROM
(SELECT idsegment, AVG(width_ef) width_ef, AVG(width_bk) width_bk
       FROM raw_width
       GROUP BY idsegment) A
GROUP BY idsegment       

SELECT breddmedel, COUNT(*)
FROM
rawdata.prota_inv_str_punkt pisp
GROUP BY breddmedel; 
       /*
CREATE TABLE eel_length AS(
SELECT ed.elfisk_nyckel id,
	   eel_length.langd "length",
	   eel_utf.utfiske1 pass1,
	   eel_utf.utfiske2 pass2,
	   eel_utf.utfiske3 pass3,
	   eel_utf.utfiske4 pass4,
	   ed.area as "area",
	   ed.bredd AS river_width,
	   ed.fiskedat AS fishing_date,
	   ed.lokalbre AS river_width_loc,
	   ed.substr1 AS substr1,
	   ed.substr2 AS substr2,
	   ed.substr3 AS substr3,
	   ed.vattenha AS water_flow,
	   ed.vattenni AS water_level,
	   ed.grumligh AS water_turbidity,
	   ed.vtnfarg AS water_colour,
	   ed.maxdjup AS water_depth_max,
	   ed.medeldju AS water_depth_mean,
	   ed.bottento AS topography,
	   ed.vattente AS water_temp,
	   ed.lufttemp AS air_temp,
	   ed.beskuggn AS shade_level, 
	   ed.vandhind AS migration_obstacle,
	   ed.utforare AS responsible_for_inventory,
	   vd.vattendrag_namn AS watercourse_name
	   
	   
FROM 
sers.ELPROVFISKE_DIM ed -- ALL Electrofishing operations in the SERS database
LEFT JOIN 
		 (SELECT DISTINCT ON(elfisk_nyckel) *
		  FROM
		  sers.ELFISKE_UTFISKE_FACT 
		  WHERE sers.ELFISKE_UTFISKE_FACT.arter_nyckel =153 
		  ORDER BY elfisk_nyckel) 
		  AS eel_utf -- PASSES where eels have been caught
ON ed.ELFISK_NYCKEL = EEL_UTF.ELFISK_NYCKEL
LEFT JOIN 
		 (SELECT *
		  FROM
		  sers.elfiske_fangst_fact
		  WHERE arter_nyckel =153 AND langd IS NOT NULL
		  ORDER BY elfisk_nyckel) 
		  AS eel_length
ON ed.ELFISK_NYCKEL = eel_length.ELFISK_NYCKEL
LEFT JOIN 
		(SELECT DISTINCT ON (elflok_nyckel,elfisk_nyckel, vattendrag_nr) elflok_nyckel, elfisk_nyckel, vattendrag_nr  
		 FROM elfiske_fangst_fact eff) 
		 AS keys -- keys for location and distinct electrofishing operation
ON ed.elfisk_nyckel=keys.elfisk_nyckel
LEFT JOIN 
	    elfiske_lokal_dim eld 
ON keys.elflok_nyckel=eld.elflok_nyckel
LEFT JOIN 
		vattendrag_dim vd 
ON keys.vattendrag_nr=vd.vattendrag_nr 
);

-- add primary key
ALTER TABLE eel_length 
ADD PRIMARY KEY(id);

-- add spatial index
CREATE INDEX elfish_length_geom_ix ON sers.eel_length USING gist (geom);
*/

/*
SELECT *
FROM eel_dens ed 
where watercourse_name = 'K�vlinge�n'

SELECT COUNT(*)
FROM
(SELECT DISTINCT ON(elfisk_nyckel) *---COUNT(*)
FROM
sers.ELFISKE_UTFISKE_FACT 
WHERE sers.ELFISKE_UTFISKE_FACT.arter_nyckel =153 
ORDER BY elfisk_nyckel) A; 

SELECT COUNT(*) FROM 
		 (SELECT *-- DISTINCT ON(elfisk_nyckel) *
		  FROM
		  sers.elfiske_fangst_fact
		  WHERE arter_nyckel =153 AND langd IS NULL
		  ORDER BY elfisk_nyckel) A


-- 1. create a unique list of ids(lokalid and fiskedat) with electrofishing operations
drop table if exists rawdata.eel_dens_elfish;
create table rawdata.eel_dens_elfish as
with el_key as( -- keys for each electrofishing
select distinct xkoorlok, ykoorlok, lokalid, fiskedat
from rawdata.elfiskefangst_isa),
eel_dens as ( -- eel densities where eel have been caught
select 
*
from 
rawdata.elfiskefangst_isa ei2 
where 
fiskart='�l'),
eel_utf as ( -- electrofishing operations where eels have been caught
select 
*
from 
rawdata.elfiskeutftotal_isa ei3
where 
fiskart = '�l'),
eel_env as ( -- environmental variables for electrofishing
select 
*
from
rawdata.elprovfiske_isa
),
site as ( --information on electrofishing site
select 
*
from
rawdata.elfiskelokal_isa ei 
)
select distinct -- change here to include other variables
ek.xkoorlok, ek.ykoorlok, s.laen, s.hflodomr, s.vdragnam, 
ST_POINT(ek.ykoorlok,ek.xkoorlok,3021) geom, ek.lokalid, ek.fiskedat, 
ed.storfisk eel_dens, ee.antutfis, eu.utf1, eu.utf2, eu.utf3, eu.utf4, eu.aldersta,
ee.bredd, ee.langd, ee.lokalbre, ee."area", ee.avfiskbr, ee.grumligh, 
ee.vtnfarg, ee.maxdjup, ee.medeldju, ee.bottento, ee.substr1, ee.substr2, 
ee.substr3, ee.vattenha, ee.vattente, ee.lufttemp, ee.syfte 
from
el_key ek
left join site s on ek.lokalid=s.lokalid
left join eel_dens ed on ek.lokalid = ed.lokalid and ek.fiskedat=ed.fiskedat
left join eel_env ee on ek.lokalid = ee.lokalid and ek.fiskedat=ee.fiskedat
left join eel_utf eu on ek.lokalid = eu.lokalid and ek.fiskedat=eu.fiskedat;

*/
--------------------------------------
-- Check electrofishing data tables --
--------------------------------------

/*
-- check key in density table (xkoorlok, ykoorlok, fiskedat) #79574
select count(*)
from 
(select distinct lokalid, fiskedat 
from
rawdata.elfiskefangst_isa ei) dens;  

-- check key in length table (xkoorlok, ykoorlok, fiskedat) #74187
select count(*)
from 
(select distinct lokalid, fiskedat 
from
rawdata.elfiskelangder_isa ei) dens;  

-- check key in utf table #79177
select count(*)
from 
(select distinct lokalid, fiskedat 
from
rawdata.elfiskeutftotal_isa ei) dens;  

-- check key in environmental table #79574
select count(*)
from 
(select distinct lokalid, fiskedat 
from
rawdata.elprovfiske_isa ei) dens;  


-- check key difference #5387 ( there is thus 5387 electrofishing operations without lengths)
select count(*)
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskelangder_isa ei) A

-- zero occations with keys in elfiskel�ndger that do not exist in density table
select count(*)
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskelangder_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei) A

-- no difference in keys between density and environmental tables
select count(*)
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elprovfiske_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei) A


-- check key difference #397 ( there is thus 397 electrofishing operations where we do not have utfisken)
select count(*)
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskeutftotal_isa ei) A

-- check key difference #0 (No occations where we have utfisken and not density data)
select count(*)
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskeutftotal_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei) A

-- find data where there is no utfiske but density estimates
with el_key as
(select *
from
(select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskefangst_isa ei
except
select distinct xkoorlok, ykoorlok, fiskedat 
from
rawdata.elfiskeutftotal_isa ei) A)
select ei2.*
from 
rawdata.elfiskefangst_isa ei2, el_key
where 
ei2.xkoorlok in (el_key.xkoorlok) and
ei2.ykoorlok in (el_key.ykoorlok) and 
ei2.fiskedat in (el_key.fiskedat);

-- Check if there are no nulls in lokalid in any electro fishing table
select count(*)
from rawdata.elprovfiske_isa ei  
where lokalid is null
*/


/*
-- It is a double id in the utf table
select *
from 
rawdata.elfiskeutftotal_isa ei  
where lokalid='654280-129370' and fiskedat='1991-09-19 00:00:00.000';

-- There is a double id for one electrofishing
select lokalid, fiskedat, count(*)
from
rawdata.elfiskeutftotal_isa ei
where fiskart ='�l'
group by lokalid, fiskedat
order by count(*) desc
*/
-------------------------------
-- Check data in mart tables --
-------------------------------
-- 79 521
select count(*), MAX(A.ELFISK_NYCKEL), MIN(A.ELFISK_NYCKEL) 
from 
(SELECT distinct ELFISK_NYCKEL
FROM AquaDW_prod.mart.ELFISKE_FANGST_FACT eff ) A; 

-- 79223
select count(*), max(A.ELFISK_NYCKEL), MIN(A.ELFISK_NYCKEL)
from 
(SELECT DISTINCT ELFISK_NYCKEL
FROM AquaDW_prod.mart.ELFISKE_UTFISKE_FACT eff ) A; 



SELECT DISTINCT

SELECT distinct ELFISK_NYCKEL
FROM AquaDW_prod.mart.ELFISKE_FANGST_FACT eff
EXCEPT
SELECT DISTINCT ELFISK_NYCKEL
FROM AquaDW_prod.mart.ELFISKE_UTFISKE_FACT eff

*/

SELECT COUNT(*)
FROM
(SELECT DISTINCT ON(elfisk_nyckel) *
		  FROM
		  sers.ELFISKE_UTFISKE_FACT 
		  WHERE sers.ELFISKE_UTFISKE_FACT.arter_nyckel =153 
		  ORDER BY elfisk_nyckel) A
		  
SELECT COUNT(*)
FROM (SELECT DISTINCT *
		  FROM
		  sers.ELFISKE_UTFISKE_FACT 
		  WHERE sers.ELFISKE_UTFISKE_FACT.arter_nyckel =153 
		  ORDER BY elfisk_nyckel) A
		  
SELECT COUNT(*)
FROM (SELECT DISTINCT ON (elflok_nyckel,elfisk_nyckel, vattendrag_nr) elflok_nyckel, elfisk_nyckel, vattendrag_nr  
		 FROM sers.elfiske_fangst_fact eff) A		  
		 
SELECT COUNT(*)
FROM (SELECT DISTINCT elflok_nyckel, elfisk_nyckel, vattendrag_nr  
		 FROM sers.elfiske_fangst_fact eff) A		  		 