------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- Snap riverwidth data                                                                             --
--  																								--
-- This script is used to merge a GIS-layer from LST and											--
-- its associated rawdata retrieved from LST J�nk�ping(see mail from Joel Jansson Lst J�nk�ping)	--
--																									--
-- @ Torbj�rn S�terberg(torbjorn.saterberg@slu.se) 2022-12-15										--
------------------------------------------------------------------------------------------------------

-- make a table with rawdata retrieved from Joel Jansson Lst J�nk�ping
drop table if exists rawdata.Biotopkartering_ProtokollA;
CREATE TABLE rawdata.Biotopkartering_ProtokollA(
LanNamn varchar(500),
VattendragNamn varchar(500),
"Vattendrag mynningskoordinat N" int,
"Vattendrag mynningskoordinat E" int,
HARO varchar(500),
"dbo_Vattendrag.VattendragsID" int,
Organisation varchar(500),
InmOrgID int,
InmDatum date,
"Protokoll A" int,
"dbo_ProtokollA.VattendragsID" int,
UndID int,
StrNr int,
Maxf�rInvDatum	date,
LokaltVdrag varchar(500),
DelomrOringProd int,
Langd int,
LangdManuell int,
BreddMax real, 
BreddMin real,
BreddMedel real,
Areal real,
VattendjupMax real,
VattendjupMedel real,	
Grovdetritus smallint,
Findetritus	smallint, 
Lera smallint,
Sand smallint,
Grus smallint,	
Sten smallint,	
Block smallint,	
Hall smallint,	
Overvattensvaxter smallint,	
RotadeOEAmfibiska smallint,	
FlytbladOEFriflytande smallint,
Undervattensvaxter smallint,
UndervattenHelaBlad smallint,	
UndervattenFingrenadeBlad smallint,
Rosettvaxter smallint,
Tradalger smallint,
PavaxtalgerOvriga smallint,
Mossor smallint,
Fontinalis smallint,	
MossorKuddlika smallint,
Sotvattensvamp smallint,
DominerandeArter varchar(500),
TackningTotal smallint,
DominerandeVegetation varchar(500),
LugnFlytande smallint,
SvagtStrommande smallint,
Strommande smallint,	
Forsande smallint,
Skuggning smallint,
DodVed smallint,
FlodeUppskattat	real, 
FlodeKlass	smallint,
VattenforingKlass char(1),
Rakt boolean,
Ringlande boolean,
Meandrande boolean,
Torrfara boolean,
Utfyllnad boolean,
Kulverterat boolean,
Damm boolean,
Rensning smallint,
KlassLekomrade smallint,
Uppvaxtomrade smallint,	
TillgangStandplatser smallint,
TillrinnandeVdrag smallint,
Dike smallint,
Tackdike smallint,	
Avloppsror smallint,	
Vattenuttag smallint,
Ravin smallint,
Brant smallint,
KorsandeVag smallint,
Nacke smallint,
Holja smallint,
Sjoutlopp smallint,	
Sjoinlopp smallint,
Sammanflode smallint,
Korvsjo smallint,
Kvillomrade boolean,
Sidofara boolean,
Delta smallint,
NipaBrinkSkredarr smallint,
UtstromnOmrKalla smallint,	
StenbroRestAv smallint,
DammbyggnadAvSten smallint,
StensattningAnnan smallint,
DammrestAnnan smallint,	
BlockrikaStrackor boolean,
MeandrandeVSiOdlingslandskapet boolean,
SjoStracka boolean,	
OversiladeKlippor smallint,
OppnaStrander smallint,	
Sandstrander smallint,
HavdStrandAngar smallint,
Oversvamningsskog smallint,
ForsarFall smallint,	
VdUnderJord boolean,
StrukturelementAnnat varchar(300),
StrukturelementAnnatAntal smallint,
Kommentar varchar(5000),	
Kulturmiljo	smallint,
VattenforekomstEUID	varchar(500),
Atgardsbehov boolean,
Atgarder varchar(500),
UrsprungsID varchar(500),
KopiaKommentar varchar(500),
Andringar boolean,
AndringBeskr varchar(500),
AndringOrsak varchar(200),
StrStartX bigint,
StrStartY bigint,
StrStoppX bigint,
StrStoppY bigint,
StrStartNorthing bigint,	
StrStartEasting bigint,	
StrStoppNorthing  bigint,
StrStoppEasting bigint,
MinTappDamm boolean,
UpplNatSten smallint,
UpplSprSten smallint,	
AvstangdSidofara boolean,
LekgrusAterst boolean,
Diken boolean,
KarteringsAndrDatum	date,
NyInlagd boolean,
Lopp varchar(500),
KommentarNBiotop varchar(500),
NyckelBiotop boolean,
Indamt boolean,	
Oversvamningsskydd boolean,
DummyXid bigint,	
DummyYid bigint,
DummyKommun bigint,
DummyUndId varchar(300),
NarmasteVattenforekomstEUID	varchar(20),
LstID varchar(200),
AvvikAmbNiva smallint,
HyMoTyp varchar(5),
HyMoTypTillagg varchar(10),
UrsprHyMotyp varchar(10),
UrsprHyMoTypTillagg varchar(10),
Planform varchar(10),
DalgangInneslutn varchar(10),
UrspInneslutn varchar(10),	
Bottensub4000 smallint,
Bottensub200 smallint,
Bottensub63 smallint,
Bottensub2 smallint,	
Bottensub063 smallint,
Bottensub002 smallint,
BottensubUnder002 smallint,
BottensubFindetrit smallint,
BottensubGrovdetrit smallint,
BottensubArtifMat smallint,
Flytbladsvaxter smallint,
Friflytandevaxter smallint,
BasDodVedGrovAntal smallint,
BasDodVedGrovAntal100m smallint,
OversvamningsskyddRP smallint,
PaverkanUppstroms smallint,
OversvamnFrekvensGV smallint,
FysPaverkOversvYta smallint,
Baverdamme smallint,
FrammVaxtHydraul smallint,
FrammDjurHydraul smallint,
FrammBladvass smallint,	
FluvialProcessDom varchar(5),	
AtgAtgardsbehov smallint,	
AtgUpplNatStenJaNej smallint,
AtgUpplNatStenVolym	real,
AtgUpplSprStenJaNej	smallint, 
AtgUpplSprStenVolym	real,
AtgUtforda smallint,
SPlanAktivt smallint,
SPlanRecTerass smallint,
SPlanSekundara smallint,	
SPlanInskarningskvot real,
UtvecklingsFas varchar(30),
FrammandeVaxtart smallint,
FrammandeDjurart smallint,
AlderRensatPav smallint,
SedimentkallorAntal smallint,
SedimentfallorAntal smallint,	
DodVedGrovVolym	real,
DodVedKlenVolym real,
DodVedOver2BFW smallint,
DodVedGrovEjNedbrut smallint,
DodVedMattlNedbrut smallint,
DodVedPartNedbrut smallint,
DodVedNedbrut smallint,
DodVedGrovBroPos smallint,
DodVedGrovPartPos smallint,
DodVedGrovLos smallint,
DodVedBegravd smallint,
DodVedSvamplan smallint,
BankfullflodeBredd smallint,
BankfullflodeDjup smallint,
BankfullflodeBDKvot smallint,	
DominNarmiljoH varchar(7),	
Lutning_m100m real,
LutningSvamplan	real,
StromJamnDelstrack smallint,
StromSlumpVariation smallint,
StromRifflePool smallint,
FaraTyp smallint,	
MarkslagSvamplanH varchar(7),	
ForandrMarkanvSvamplan varchar(200),	
HistorikSvamplan varchar(500),
MeanderVagLFas1 smallint,
AmplitudFas1 smallint,
MeanderVagLFas2 smallint,
AmplitudFas2 smallint,
SvamplanBredd smallint,
SekSvamplanBredd smallint,
SelsjoarAntal smallint,
Lagunsjoar smallint,
OvrSmavattenFara smallint,
StabilFara smallint,
MeanderflyttNedstr smallint,
OkandeAmplitud smallint,
MeanderflyttAvskarning smallint,
OjamnErosion smallint,
Avulsion smallint,
LeveerInga smallint,
LeveerGenombrott smallint,
LeveerTydlVKant smallint,
LeveerTydlHKant smallint,
LeveerTydlVHKantBadaSid smallint,
DepIngaTecken smallint,
DepSmaFlackarKant smallint,
DepLangaStrackorFaransKant smallint,
DepSkiktSvamplan smallint,
StabilitetsIndex smallint,	
TerrangformKorvsjo smallint,
FuktigaSankor smallint,
Svamkaglor smallint,
HogGrundvytaFukt smallint,
UtstrOmrGrundv smallint,
AndraSmavatten smallint,
SolexpBarSandSluttn smallint,
KortbetStrandang smallint,
FuktigBarLerJord smallint,	
SkuggigSkog smallint,
TerrangformRikligDodVed smallint,
GamlaLovtrad smallint,
PeriodOversvLagor smallint,
SkuggigSkogEjProd smallint,
RikedomBlommandeOrter smallint,
NaturligSalix smallint,
KantLinjar smallint,
KantKonkav smallint,
KantKonvex smallint,
KantSformad smallint,
KantKomplexStegv smallint,
KantVertikal smallint,
KantDepSluttnFot smallint,
KantErosSluttnFot smallint,
Ras smallint,
FlakSkred smallint,
DjupRotationsSkred smallint,
Flaskskred smallint,
Slamstrom smallint,
KroktaTrad smallint,
KanthojdV smallint,
KanthojdH smallint,
KantlutningsluttnV smallint,
KantlutningsluttnH smallint,
BarMinJordV smallint,
BarMinJordH smallint,
GrasvegetV smallint,
GrasvegetH smallint,
BuskarV smallint,
BuskarH smallint,
SmatradV smallint,
SmatradH smallint,
TradV smallint,
TradH smallint,
TradartV smallint,
TradartH smallint,
StamDiamMedelV smallint,
StamDiamMedelH smallint,
ForekomstTradV smallint,
ForekomstTradH smallint,
LutandeTrad smallint,
ExpTradrotsys smallint,
Fickbildning smallint,
FrammandeArter smallint,
OvrigaMossor smallint,
ReglVattenforing smallint,	
SkyddszonH varchar(5),
SkyddszonV varchar(5),
FluvialStabilitet smallint,
DominNarmiljoV varchar(5),
MarkslagSvamplanV varchar(5),
BSUtanfAvgransning smallint,
BSSanktBNivaFastStr	real,
BSForandrBNivaTotalt real
);

-- add data to table
COPY rawdata.Biotopkartering_ProtokollA
FROM 'C:\hydrografi_i_natverk\Protokoll A alla metoder etc\Protokoll_A_alla_metoder.csv'
(FORMAT CSV, HEADER TRUE, DELIMITER ';', ENCODING 'iso-8859-1')
;

-- add columns to rawdata.prota_inv_str_punkt
alter table rawdata.prota_inv_str_punkt 
add column "Protokoll A" int4,
add column langdmanuell int4, 
add column breddmax float4,
add column breddmin float4,
add column breddmedel float4,
add column areal float4,
add column vattendjupmax float4,
add column vattendjupmedel float4,
add column grovdetritus int2,
add column findetritus int2,
add column lera int2,
add column sand int2,
add column grus int2,
add column sten int2,
add column block int2,
add column tackningtotal int2,
add column vattenforingklass bpchar(1);

-- Add data from excel-file (Joel Andersson LST J�nk�ping) to prota_inv_str GIS-layer 
UPDATE rawdata.prota_inv_str_punkt 
SET 
--"Protokoll A" = predictors."Protokoll A",
langdmanuell =  predictors.langdmanuell, 
breddmax = predictors.breddmax,
breddmin = predictors.breddmin,
breddmedel = predictors.breddmedel,
areal = predictors.areal,
vattendjupmax = predictors.vattendjupmax,
vattendjupmedel = predictors.vattendjupmedel,
grovdetritus = predictors.grovdetritus,
findetritus = predictors.findetritus,
lera = predictors.lera,
sand = predictors.sand,
grus = predictors.grus,
sten = predictors.sten,
block = predictors.block,
tackningtotal = predictors.tackningtotal,
vattenforingklass = predictors.vattenforingklass 
FROM 
(select distinct on ("Protokoll A") -- only select unique observations from .Biotopkartering_ProtokollA(see mail from Joel Jansson Lst J�nk�ping)
"Protokoll A",
langdmanuell, 
breddmax,
breddmin,
breddmedel,
areal,
vattendjupmax,
vattendjupmedel,
grovdetritus,
findetritus,
lera,
sand,
grus,
sten,
block,
tackningtotal,
vattenforingklass
from rawdata.Biotopkartering_ProtokollA) predictors 
WHERE prota_inv_str_punkt.protaid  = predictors."Protokoll A";

-- delete rows where there is no data on river width (566 rows)
delete from rawdata.prota_inv_str_punkt
where "Protokoll A" is null;

-- delete column Protokoll A (it is the same as protaid)
alter table rawdata.prota_inv_str_punkt 
drop column "Protokoll A"

-- add geometry column with inventory data merged to rivernetwork
alter table rawdata.prota_inv_str_punkt 
add column snapped_geom geometry(point, 3035),
add column idsegment text;

-- find closest point in rivernetwork
with snapped_inventory as ( 
select pisp.protaid, pisp.punkttyp, shortest_dist_point.idsegment, ST_Closestpoint(shortest_dist_point.geom, st_transform(pisp.geom,3035)) snapped_geom
from rawdata.prota_inv_str_punkt pisp  
cross join lateral (
select 
geom, 
idsegment
from sweden.rn srn
order by st_transform(pisp.geom,3035) <-> srn.geom
limit 1) shortest_dist_point)
update rawdata.prota_inv_str_punkt pisp2 
set 
snapped_geom = snapped_inventory.snapped_geom, 
idsegment = snapped_inventory.idsegment
from snapped_inventory
where 
pisp2.protaid = snapped_inventory.protaid 
and 
pisp2.punkttyp = snapped_inventory.punkttyp;

-- delete some values that are completely off
delete from rawdata.prota_inv_str_punkt 
where gid in (22905, 101444, 53820, 32723, 80815);

/*
-- 
select *
from sweden.rn r 
left join rawdata.prota_inv_str_punkt pisp 
on r.idsegment =pisp.idsegment
where pisp.idsegment is not null
;

select count(*)
from
(select 
protaid--, idsegment 
from
rawdata.prota_inv_str_punkt pisp
where punkttyp='Start'
except
select 
protaid--, idsegment 
from
rawdata.prota_inv_str_punkt pisp
where punkttyp='Stopp') A;

select --53927
idsegment,avg(breddmedel)
from
rawdata.prota_inv_str_punkt pisp
group by idsegment;
where punkttyp='Start'

select --47837
count(*)
from
rawdata.prota_inv_str_punkt pisp
where punkttyp='Stopp'

select count(*) --101764
from rawdata.prota_inv_str_punkt pisp 

select *
from rawdata.prota_inv_str_punkt pisp */