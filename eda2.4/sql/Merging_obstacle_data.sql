-- Tables needed
select *
from rawdata.damorweir d ;

select * 
from rawdata.dammregistret;

select *
from rawdata.hydronode_altered ha; 

select *
from rawdata.hydropower;

select count(*) -- 34015
from rawdata.lst_obstacles lo; 

select count(*) --10702
from rawdata.dammregistret d; 

select distinct count(lo.gid) -- 0; no intersection between lantm�teriet hydronodes and lst data
from rawdata.hydronode_altered d, rawdata.lst_obstacles lo 
where ST_Intersects(lo.geom, d.geometry);

-- First identify intersecting points
select distinct count(lo.gid) -- 3354
from rawdata.dammregistret d, rawdata.lst_obstacles lo 
where ST_Intersects(d.geom,lo.geom);

-- Check which points are intersecting
select *
from rawdata.dammregistret d, rawdata.lst_obstacles lo 
where ST_Intersects(d.geom,lo.geom);

-- Check if there is any ids in the respective datasets that could be merged
SELECT COUNT(*) --0
FROM
(SELECT LOWER(TRIM(BOTH '{}' FROM dammanlid)) localid 
FROM 
rawdata.dammregistret d2
INTERSECT
SELECT localid
FROM
rawdata.damorweir d) diff

SELECT COUNT(*) -- 10702
FROM
(SELECT LOWER(TRIM(BOTH '{}' FROM dammanlid)) 
FROM 
rawdata.dammregistret d2) D

SELECT COUNT(*) -- 4865
FROM
(SELECT localid
FROM
rawdata.damorweir d) E

SELECT COUNT(*) --0
FROM
(SELECT LOWER(TRIM(BOTH '{}' FROM dammid)) localid 
FROM 
rawdata.dammregistret d2
INTERSECT
SELECT localid
FROM
rawdata.hydronode h) diff

select *
FROM 
rawdata.hydronode_altered ha 
------------------------------------
-- check columns in lst_obstacles --
------------------------------------
/*
important columns: 
* vhtyp - type of obstacle
|vhtyp           |count |
|----------------|------|
|naturligt hinder|5,938 |
|�lkista         |97    |
|                |7     |
|trumma          |8,605 |
|fiskgaller      |110   |
|sj�utlopp       |160   |
|�vrigt hinder   |3,578 |
|damm            |14,369|
|v�gpassage      |1,151 |

* fallhtot - total drop height of the obstacle. Ange vandringshindrets totala fallh�jd (m�ts/uppskattas) i meter. Om
det finns flera avsatser r�knas de ihop till en total h�jd. Den totala fallh�jden
ska ge ledning om hur h�g h�jden �r som ska byggas f�rbi
vid eventuellt fiskv�gsbygge.

|is this info included?|count |
|----------------------|------|
|false                 |19,062|
|true                  |14,953|

* invdatum - date of inventory.

* fallutn -  total drop height that is currently used. Vid dammanl�ggningar d�r vattnet leds f�rbi en del av vattendraget
(g�ller fr�mst kraftverk) anges den utnyttjade fallh�jden. Med utnyttjad
fallh�jd menas hur stor fallh�jden �r f�r den str�cka som vattnet
har letts bort ifr�n.

|is not null|count |
|--------|------|
|false   |31,437|
|true    |2,578 |


-- * naturl_vh - if the obstacle originally(naturally) was a migration barrier(nej - no, ja - yes, os�ker uncertain).
|naturl_vh   |count |
|------------|------|
|null        |9,031 |
|nej         |46    |
|Ja - os�ker |722   |
|Ja          |5,913 |
|Nej - os�ker|1,500 |
|NEJ         |2     |
|Nej         |16,791|
|ja          |10    |

* flodeuppsk - estimated discharge.
|is not null|count |
|--------|------|
|false   |23,763|
|true    |10,252|

* vforklass - class of discharge(low, medium, high). En bed�mning av om vattenf�ringen �r l�g, medel eller h�g p� �rsbasis
g�rs och noteras som L�g, Medel eller H�g. Gr�nsen f�r att klassas som l�g eller medel
�r fl�det som motsvarar ett v�rde precis mellan medell�gvattenf�ring och medelvattenf�ring
(om medell�gvattenf�ring �r 0.1 m3/s och medelvattenf�ring �r 1 m3/s �r gr�nsen s�ledes
0.55 m3/s). Gr�nsen mellan medel och h�g ber�knas p� motsvarande vis.

|vforklass|count |
|---------|------|
|         |20,114|
|H        |1,711 |
|M        |6,664 |
|L        |5,526 |


* trumfallh - height of drop from culvert
|is not null|count |
|--------|------|
|false   |27,521|
|true    |6,494 |


* formort - possible for roach to pass obstacle (Definitivt - not passable; Partiellt - partial passage; Passerbart - passable)
|formort   |count |
|----------|------|
|Definitivt|19,239|
|Partiellt |4,080 |
|Passerbart|1,918 |
|          |8,778 |

* fororing - possible for trout to pass obstacle?(Definitivt - not passable; Partiellt - partial passage; Passerbart - passable)

|fororing  |count |
|----------|------|
|Definitivt|13,767|
|Partiellt |8,413 |
|Passerbart|4,342 |
|          |7,493 |

* foralyngel - possible for young eel to pass obstacle? (Definitivt - not passable; Partiellt - partial passage; Passerbart - passable)

|foralyngel|count |
|----------|------|
|Definitivt|1,599 |
|Partiellt |1,185 |
|Passerbart|2,902 |
|          |28,329|


* foral - possible for eel to pass obstacle? (Definitivt - not passable; Partiellt - partial passage; Passerbart - passable)

|foral     |count |
|----------|------|
|Definitivt|1,586 |
|Partiellt |997   |
|Passerbart|2,947 |
|          |28,485|

* skador - Will fish be injured when passing downstreams? (Ja - yes, Nej - no) Bed�m om fisk skadas vid passage f�rbi hindret i nedstr�msriktning. T
			ex om vattnet faller med h�g fart mot en h�ll. Ett vattenkraftverk utan fungerande fingrindar medf�r ett Ja.

|skador|count |
|------|------|
|      |24,164|
|Ja    |1,339 |
|Nej   |8,512 |


* fiskvag- if there is a fishpass. (Ja- yes, Nej-no)

|fiskvag|count |
|-------|------|
|       |21,547|
|Ja     |517   |
|Nej    |11,951|

* inmdatum - don�t know what this is
* andrdatum - don�t know what this is
*/

------------------------------------
-- check columns in dammregistret --
----------------------------------
/*
important columns: 
* status (1 - existing dam, 2 - previously existing dam)

|status|count|
|------|-----|
|1     |9,298|
|2     |1,404|

* regl_typ (how the dam is regulated) (0 - no info; 1 - Set threshold; 2 - regulated through a sluice; 3 - intake; 4 - unregulated)  
            for previously existing dam 1 - demolished; 2 - removed; 3- unknown  
|regl_typ|count|
|--------|-----|
|0       |6,681|
|1       |1,017|
|2       |2,458|
|3       |385  |
|4       |161  |

* dammhojd - the height(m) between the highest and the deepest part of the dam(10702)

|is zero |count|
|--------|-----|
|false   |2,799|
|true    |7,903|

* kron - length of the dam (m)  

|is zero |count|
|--------|-----|
|false   |3,106|
|true    |7,596|
 
* fiskvag; type of fish pass(1. Bass�ngtrappa; 2. Denilr�nna; 3. Slitsr�nna; 4. Oml�p; 5. Inl�p; 6. �lledare; 7. Smoltr�nna; 8. Ok�nd typ; 9. Ingen fiskv�g; 10. Annan)        

|fiskvag|count|
|-------|-----|
|0      |6,390|
|1      |89   |
|2      |19   |
|3      |4    |
|4      |96   |
|5      |9    |
|6      |33   |
|8      |161  |
|9      |3,890|
|10     |11   |

* fiskavl (is there a fishdiverter to the fish pass?) 0- unkwnown; 1 - yes; 2 - no; 3 - unknown  

|fiskavl|count|
|-------|-----|
|0      |9,576|
|1      |16   |
|2      |675  |
|3      |435  |

* vhinder (Is the dam an obstacle for migrating fish? 0 - unknown; 1 - yes; 2 - no)

|vhinder|count|
|-------|-----|
|0      |9,547|
|1      |528  |
|2      |627  |

* dammanlid - id of the whole set of dams (multiple dams belong to the same dam -- 5863 dams belong to 5045 dam units) 4839 nulls. 594 dam units have 2 or more dams

* verksmht (0. unkown; 1. Kraftproduktion; 2. Industri; 3. Sj�fart; 4. Invallning; 5. Vattenf�rs�rjning; 6. Spegeldamm; 7. Historisk; 8. �vrigt)    

|verksmht|count|
|--------|-----|
|0       |7,374|
|1       |1,455|
|2       |77   |
|3       |4    |
|4       |26   |
|5       |54   |
|6       |576  |
|7       |290  |
|8       |846  |

-----------------------------------------------------------------
-- check columns in hydropower(data from hydropower dams only) --
-----------------------------------------------------------------

* fallh - the height(m) between the highest and the deepest part of the dam(10702) 

*/
SELECT *
FROM 
rawdata.hydropower;

select d.dammhojd, lo.fallhtot, d.geom, d.vhinder, lo.formort, lo.fororing, lo.foralyngel, lo.foral 
from 
rawdata.dammregistret d, rawdata.lst_obstacles lo 
where ST_Intersects(d.geom, lo.geom)

SELECT status, regl_typ, COUNT(*) 
FROM 
rawdata.dammregistret d
GROUP BY status, regl_typ 

/*
important columns: 
* status (1 - existing dam, 2 - previously existing dam)

|status|count|
|------|-----|
|1     |9,298|
|2     |1,404|

* regl_typ (how the dam is regulated) (0 - no info; 1 - Set threshold; 2 - regulated through a sluice; 3 - intake; 4 - unregulated)  
            for previously existing dam 1 - demolished; 2 - removed; 3- unknown  
|regl_typ|count|
|--------|-----|
|0       |6,681|
|1       |1,017|
|2       |2,458|
|3       |385  |
|4       |161  |
* vhtyp - type of obstacle
|vhtyp           |count |
|----------------|------|
|naturligt hinder|5,938 |
|�lkista         |97    |
|                |7     |
|trumma          |8,605 |
|fiskgaller      |110   |
|sj�utlopp       |160   |
|�vrigt hinder   |3,578 |
|damm            |14,369|
|v�gpassage      |1,151 |
**/

WITH 
filtered_dams AS( -- use this layer to place dams in their right position. Also av
SELECT *
FROM
(
SELECT DISTINCT ON (d.ogc_fid) d.*
FROM
sweden.rn srn, rawdata.damorweir d 
WHERE ST_Intersects(ST_Transform(d.geometry,3035),srn.geom)) A),
filtered_dr AS( -- dams registred as up running
SELECT *
FROM
rawdata.dammregistret d2 
WHERE status=1 OR (status=2 AND regl_typ=0 OR regl_typ=3)),
water_obstacle AS (
SELECT *
FROM 
rawdata.lst_obstacles lo 
WHERE vhtyp IN ('�lkista','fiskgaller',)
)

DROP VIEW IF EXISTS rawdata.dam_overlap;
CREATE VIEW rawdata.dam_overlap AS(
SELECT count(*)
FROM
rawdata.dammregistret d ,rawdata.lst_obstacles lo 
WHERE ST_DWithin(d.geom,lo.geom,2)
)

-- find closest point between dammregistret and lst_obstacle 
DROP VIEW IF EXISTS rawdata.dam_overlap;
CREATE VIEW rawdata.dam_overlap AS(
SELECT d.*, lst.dist, lst.vhtyp, lst.fallhtot, lst.fallhutn, lst.naturl_vh, lst.lokalbeskr, 
lst.formort, lst.fororing, lst.foralyngel, lst.foral, lst.fiskvag fiskvag_lst, lst.fiskvfunkt, lst.atgarder,
lst.kommentar kommentar_lst
FROM rawdata.dammregistret d  
cross join lateral (
select 
*,
d.geom <-> lo.geom dist
from (SELECT * FROM rawdata.lst_obstacles) lo -- WHERE vhtyp='damm') lo 
order by d.geom <-> lo.geom
limit 1) lst
WHERE dist <10);

SELECT *
FROM
rawdata.dam_overlap do2 
GROUP BY vhtyp