# functions used ind EDACCM\prediction_1_dbeel.R
# 
# EDA2.0 
# ONEMA, IAV, IRSTEA
# Author: C�dric Briand, C�line Jouanin
###############################################################################

fn_pred.set<-function(mod,data=ccm,annee=2010,month=6,anneeasfactor=FALSE,monthasfactor=FALSE){
	the_terms<-labels(terms(mod))
	the_terms<-gsub(pattern="s[(]",replacement="",x=the_terms)
	the_terms<-gsub(pattern="[)]",replacement="",x=the_terms)
	the_terms<-gsub(pattern=" ",replacement="",x=the_terms)
	the_terms<-gsub(pattern="[,][[:digit:]]",replacement="",x=the_terms)
	the_terms<-gsub(pattern="as.factor[(]",replacement="",x=the_terms)  #pour le cas des mod�les avec int�raction UGA
	full_terms<-the_terms
	if ("annee"%in%the_terms){
		testannee<-TRUE
		the_terms<-the_terms[the_terms!="annee"]
	} else testannee<-FALSE
	if ("month"%in%the_terms){
		testmonth<-TRUE
		the_terms<-the_terms[the_terms!="month"]
	} else testmonth<-FALSE
	pred.set<-data[,the_terms]
	
	if (testannee){
		if (anneeasfactor) annee<-as.factor(annee)
		pred.set<-cbind("annee"=annee,pred.set)
	}
	if (testmonth){
		if (monthasfactor) month<-as.factor(month)
		pred.set<-cbind("month"=month,pred.set)
	}
	return(list("pred.set"=pred.set,"the_terms"=the_terms,"full_terms"=full_terms))
}

# this functions provides a summary for density model
gamcheck<-function(density_model){
	ugaList=levels(as.factor(ers$uga))
	print(form_md)
	print(summary(density_model))
	x11();
	jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Response_curve_MD_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
	par(mfrow=c(2,min(4,round(length(labels(terms(density_model)))/2))));plot.gam(density_model,se=T,ask=FALSE)
	dev.off()
	x11();par(mfrow=c(2,min(4,round(length(labels(terms(density_model)))/2))));plot(density_model,residuals=T)
	#x11();test_distribution(density_model,graph=1)
	#x11();test_distribution(density_model,graph=2)
	
	#QQplot or histogram of the residuals for normality
	x11();hist(residuals(density_model))
	#Residuals versus fitted values to verify homogeneity
	
	x11();plot(fitted(density_model),residuals(density_model))
	
	x11();plot(density_model$y,residuals(density_model))
	x11();plot(density_model$data$d,density_model$data$d-fitted(density_model))
	#Spearman correlation between fitted and observed values
	print(cor.test(density_model$data$d,fitted(density_model),method="spearman",exact=FALSE))	
	print(cor.test(density_model$data$d,density_model$data$d-fitted(density_model),method="spearman",exact=FALSE))
	
	jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Fitted_Density_MD_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
	mdlm<-lm(density_model$data$d~fitted(density_model))
	mdlm2<-lm(fitted(density_model)~density_model$data$d)
	plot(density_model$data$d~fitted(density_model),xlab=expression(paste("Predicted",hat(Y))),ylab="Observed Y")
	points(predict(mdlm)~fitted(density_model),type="l",col="red")
	dev.off()
	#AIC
	
	#Percentage of deviance
	df=density_model$df.residual
	#D�vianceExpliqu�e=1-D�vianceR�siduelle/D�vianceNulle
	#Le degr� de concordance entre les observations et les valeurs ajust�es par le mod�le (Pearce & Ferrier 2000) est mesur�
	#Plus un mod�le explique convenablement les variations de la r�ponse et plus la proportion de d�viance expliqu�e est proche de 1.
	pdev=1-(deviance(density_model)/density_model$null.deviance)
	print(c("percentage deviance explained",round(pdev,2)))
}

# this functions provides a summary for presence absence model
gamcheck2<-function(mod,threshold=0.5){
	ugaList=levels(as.factor(ers$uga))
	jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Response_curve_MPA_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
	par(mfrow=c(2,min(4,round(length(labels(terms(mod)))/2))))
	cat(paste("graphique :",datawd,"/dataEDAccm/graphiques/Response_curve_MPA_",ugaList,".jpeg",sep=""))
	plot.gam(mod,se=T)
	dev.off()
	x11();par(mfrow=c(3,min(4,round(length(labels(terms(mod)))/2))))
	plot(mod,residuals=T)
	#x11();test_distribution(mod,graph=1)
	#x11();test_distribution(mod,graph=2)
	
	#QQplot or histogram of the residuals for normality
	x11();hist(residuals(mod))
	print(form_mpa)
	print(summary(mod))
	#Residuals versus fitted values to verify homogeneity
	DATA=data.frame(ers[,"op_id"],ers[,"d"]>0,predict(mod,type="response"))
	jpeg(filename=paste(datawd,"/dataEDAccm/graphiques/Model_quality_MPA_",ugaList,".jpeg",sep=""), width = 480, height = 480,quality=100)
	presence.absence.summary(DATA)
	dev.off()
	presence.absence.accuracy(DATA)
	matrice_confusion=cmx(DATA,threshold)
	cat(str_c("threshold chosen"=threshold))
	cat("matrice confusion \n")
	print(matrice_confusion)
	aic=AIC(mod)
	print(str_c("aic=",round(aic,2),"\n"))
	(pdev=(1-deviance(mod)/mod$null.deviance))
	cat(str_c("percentage deviance =",round(pdev,2)," \n"))
	pbcl=sum(diag(matrice_confusion))/sum(matrice_confusion)# correctement pr�dits
	print(paste("correctly predicted",round(pbcl,2)))	
	pcp=matrice_confusion[1,1]/sum (matrice_confusion[,1]) # present correctement pr�dits
	print(paste("present correctly predicted",round(pcp,2)))
	acp=matrice_confusion[2,2]/sum (matrice_confusion[,2]) # absent correctement pr�dit
	print(paste("absents correctly predicted",round(acp,2)))
	(K=Kappa(matrice_confusion))
	print("kappa=")
	print(K)
}

# this functions provides a summary for lm model
gamchecklm<-function(modlm,mydata){
	summary(modlm)
	print("Model coefficients")
	print(coefficients(modlm))
	print("CIs for model parameters")
	print(confint(modlm,level=0.95))
	fitted(modlm)  #predicted values
	residuals(modlm) #residuals
	print(anova(modlm))
	print("Covariance matrix for model parameters")
	print(vcov(modlm)) 
	print("Regression diagnostics")
	print(influence(modlm)) #regression diagnostics
	layout(matrix(c(1,2,3,4),2,2))
	plot(modlm)
	#K-fold cross validation
	library(DAAG)
	print(cv.lm(df=mydata, modlm, m=3))
}


funpredmodele=function(data=ccm, # just to get all the variables right, be transformed in don, a new dataset later
		var="annee", # this is the variable to test
		model_var, # the variables of the model that are in the ccm
		add_model_var,	# additional variables not in the ccm	
		niveautest=1982:2010, # range of values to test on
		default_model_var=tapply(colMeans(ccm[,model_var])),
		default_add_model_var=c("annee"=2008,"month"=6),
		newvar1="pres-abs model x density model", # new name for the combined prediction
		newvar2="year", # name for the year variable
		mpa,
		md){
	stopifnot(all(names(default_model_var)==model_var))
	l=length(niveautest)
	data=data[1:l,model_var] 
	for (i in 1:length(model_var)){
		data[,model_var[i]]<-default_model_var[i]
	}	
	if ("annee"%in% add_model_var) data$annee=default_add_model_var["annee"]  # might be modified later
	if ("month"%in%add_model_var) data$month=default_add_model_var["month"] # might be modified later
	data[,var]=niveautest
	data$pred=predict(mpa,newdata=data,type="response")*predict(md,newdata=data,type="response")
	# I want new fancy names
	data=chnames(object=data,                  
			old_variable_name=c(var,"pred"),          
			new_variable_name=c(
					newvar2,
					newvar1
			))
	data=data[,c(newvar1,newvar2)] 
	return(data)
}

# function that produces the response curve. Never print the output otherwise gam model go awry. So just print in the end....
funresponsecurve<-function(
		var,
		niveautest=NULL,
		preprint=FALSE
){
	#load(str_c(datawd,"/dataEDAccm/predictions/",secteur,"md.Rdata"))
	#load(str_c(datawd,"/dataEDAccm/predictions/",secteur,"mpa.Rdata"))
	add_model_var_md<-fn_pred.set(mod=md,data=ccm)$full_terms[!fn_pred.set(mod=md,data=ccm)$full_terms%in%fn_pred.set(mod=md,data=ccm)$the_terms]
	model_var_md<-fn_pred.set(mod=md,data=ccm)$the_terms
	add_model_var_mpa<-fn_pred.set(mod=mpa,data=ccm)$full_terms[!fn_pred.set(mod=mpa,data=ccm)$full_terms%in%fn_pred.set(mod=mpa,data=ccm)$the_terms]
	model_var_mpa<-fn_pred.set(mod=mpa,data=ccm)$the_terms
	model_var<-unique(c(model_var_md,model_var_mpa))
	add_model_var<-unique(c(add_model_var_md,add_model_var_mpa))
	# the default below must correspond to c(model_var,add_model_var)
	dmv<-apply(ccm[,model_var],2,median) # default model variable
	fnsetdefault<-function(var,default,dmv){
		if (var%in%names(dmv)) dmv[var]<-default
		dmv
	}
	# "alt_gradie" mean
	# "slope_mean" mean
	# "temp_mean" mean
	# "rain_mean" mean
	# below all variables are set to default value
	dmv<-fnsetdefault("distance_sea",0,dmv)
	dmv<-fnsetdefault("elev_mean",0,dmv)	
	dmv<-fnsetdefault("cs_score",0,dmv)
	dmv<-fnsetdefault("cs_nbdams",0,dmv)
	dmv<-fnsetdefault("nbdams",0,dmv)
	dmv<-fnsetdefault("cnbdams_dsea",0,dmv)
	dmv<-fnsetdefault("distance_source",max(ccm$"distance_source",na.rm=TRUE),dmv)
	#dmv<-fnsetdefault("up_area",max(ccm$"up_area",na.rm=TRUE),dmv)
	dmv<-fnsetdefault("relative_distance",0,dmv)
	dmv<-fnsetdefault("p_up_agricultural",0,dmv)
	dmv<-fnsetdefault("p_up_urban",0,dmv)	
	dmv<-fnsetdefault("p_up_no_impact",1,dmv)
	dmv<-fnsetdefault("p_agricultural",0,dmv)
	dmv<-fnsetdefault("p_urban",0,dmv)	
	dmv<-fnsetdefault("p_no_impact",1,dmv)
	damv<-c("annee"=2008,"month"=6) # default_additional_model_variable
	whichdamv<-match(var,names(damv))
	if (! is.na(whichdamv)) {
		damvsub<-damv[-match(var,names(damv))] # this is done for labels later
	} else damvsub<-damv
	
	if (is.null(niveautest) & var%in%names(dmv)) {
		niveautest<-round(seq(min(ccm[,var]),max(ccm[,var]),length.out=25),2)
	}	else if (is.null(niveautest) & var%in%names(damv)) niveautest<-seq(min(ers[,var]),max(ers[,var]),by=1)
	
# this one is in source("EDACCM/prediction_functions.R")
# cr�e un tableau de donn�e avec les variables par d�faut cr�� ci dessous et l'�crit dans don1
# une seule variable bouge, c'est var, et la variation est donn�e par niveautest
	don1<-funpredmodele(data=ccm,
			var,
			model_var,
			add_model_var,
			niveautest=niveautest,
			default_model_var=dmv,
			default_add_model_var=damv,
			newvar1="mdxmpa", 
			newvar2=var, # the name could be different but not there
			mpa=get("mpa",".GlobalEnv"),
			md=get("md",".GlobalEnv")
	
	)
# text to be pasted in the graph later
	datdef<-data.frame(nam=names(dmv),val=round(dmv,2))
	datdef1<-data.frame(nam=names(damvsub),val=damvsub)
	datdef<-rbind(datdef,datdef1)
	dat<-paste("predictions for : \n",paste(paste(datdef$nam,"=",datdef$val),collapse="\n"))
	x11();
	old_theme <- theme(
			axis.text.x = element_text(hjust=1,colour="grey10",angle=90),
			axis.text.y = element_text(colour="grey10",hjust=1)
	)
	p<-ggplot(don1,aes_string(y="mdxmpa",x=var))+ylab("densities model delta-gamma")

	if (var%in%names(damv)) {
		if (secteur!="Crepe"){
		ers1<-merge(ers[,c("wso1_id",var)],ccm[c("wso1_id","mdxmpa")],by="wso1_id",all.x=TRUE,all.y=FALSE)
		} else if (secteur=="Crepe"){
		ers$mdxmpa<-predict(mpa,newdata=ers,type="response")*predict(md,newdata=ers,type="response")
		ers1<-ers[,c(var,"mdxmpa")]
		}
		#===========================================================
		# cr�ation d'un nouveau data frame juste pour �crire le texte
		datdef$nam<-gsub("annee","year",datdef$nam)
		dat<-paste("predictions for :\n",paste(paste(datdef$nam[!datdef$nam==var],"=",datdef$val[!datdef$nam==var]),collapse="\n"))
		don2<-don1[1,]
		don2[1,1]<-max(min(ers1$mdxmpa)+diff(range(ers1$mdxmpa))*2/3,min(don1$mdxmpa)+diff(range(don1$mdxmpa))*2/3)
		don2[1,2]<-min(ers[,var])+diff(range(ers[,var]))*2/3
		don2[1,"label"]<-dat
		p<-p+geom_text(aes(label=label),size=4,col="firebrick1",hjust=0,data=don2)
		#===========================================================
		p<-p + geom_point(col="tomato") +geom_line(group=1,col="firebrick1")+
				geom_rug(data=ers1[,colnames(don2)[-3]],position="jitter",alpha=0.5) +
				geom_point(data=ers1[,colnames(don2)[-3]],col="forestgreen",alpha=0.8)+
				ggtitle(paste(secteur,"model predictions :",gsub("annee","year",var)))
		p1<-p+geom_smooth(col="darkgreen",data=ers1[,colnames(don2)[-3]])
	} else {
		#===========================================================
		# cr�ation d'un nouveau data frame juste pour �crire le texte
		datdef$nam<-gsub("annee","year",datdef$nam)
		dat<-paste("predictions for :\n",paste(paste(datdef$nam[!datdef$nam==var],"=",datdef$val[!datdef$nam==var]),collapse="\n"))
		don2<-don1[1,]
		don2[1,1]<-max(min(ccm$mdxmpa)+diff(range(ccm$mdxmpa))*2/3,min(don1$mdxmpa)+diff(range(don1$mdxmpa))*2/3)
		don2[1,2]<-min(ccm[,var])+diff(range(ccm[,var]))*2/3
		don2[1,"label"]<-dat
		p<-p+geom_text(aes(label=label),size=4,col="firebrick1",hjust=0,data=don2)
#===========================================================
		p<-p + geom_point(col="tomato") +geom_line(group=1,col="firebrick1")+
				geom_rug(data=ccm[,colnames(don2)[-3]],position="jitter",alpha=0.5) +
				geom_point(data=ccm[,colnames(don2)[-3]],col="forestgreen",alpha=0.8)+
				ggtitle(paste(secteur,"model predictions :",var))
		p1<-p+geom_smooth(col="darkgreen",data=ccm[,colnames(don2)[-3]])
	}
	if (preprint)print(p) 
	
	
	return(p1)
}

# laurent function
make_interval = function (classe)
{
	n=length(classe)
	paste("[",classe[1:(n-1)],"-", classe[2:n],"[", sep="")
}

graph_silver_eel = function(var_to_be_tested="distance_sea", name_to_display=NULL, factor_yellow=1, factor_silver=1, graph_side="right")
{
	if(is.null(name_to_display))
		name_to_display=var_to_be_tested
# use 'hist' function to get a pretty cut of the explaining variable (named bin thereafter)
	h=hist(ccm[,var_to_be_tested], plot=FALSE)
	if (h$breaks[length(h$breaks)-1]<1) roundseuil<-2 else roundseuil<-1
	# si on arroundit des valeurs faibles, il y a des breaks non unique et la fonction plante
	
# calculate the sum of yellow eel according this explaining variable and using these bins
	sum_yellow_eel = tapply(ccm$nb_eel_per_segment,cut(ccm[,var_to_be_tested],round(h$breaks,roundseuil), labels=make_interval(round(h$breaks,roundseuil))),sum,na.rm=TRUE)
# calculate the sum of wetted area according this explaining variable and using these bins
	sum_ccm_area = tapply(ccm$riverarea/10^4,cut(ccm[,var_to_be_tested],round(h$breaks,roundseuil), labels=make_interval(round(h$breaks,roundseuil))),sum,na.rm=TRUE) # in ha
	
# produce the graph giving the production of yellow and silver eel per explaining variable bins as well as the the distribution of cumulated wetted area in the upper right corner
	par(las = 2, mar = c(6, 5, 2, 5) + 0.1)
	if(graph_side=="right")
		layout(matrix(c(1,2,1,1),nrow=2, byrow=TRUE)) else
		layout(matrix(c(2,1,1,1),nrow=2, byrow=TRUE))
	barplot(sum_yellow_eel, xlab="", ylab="", ylim=c(0, max(sum_yellow_eel, na.rm=TRUE)*1.05), yaxt="n")
	mtext(name_to_display, side=1, las=0, cex = .85, adj=.5, line=4.5)
	axis(2, at = pretty(sum_yellow_eel) ,label = pretty(sum_yellow_eel)/factor_yellow)
	if(factor_yellow==1)
		mtext("Yellow eel production", side=2, las=0, cex = .85, adj=.5, line=3.5) else
		mtext(paste("Yellow eel production (x", factor_yellow, ")", sep="") , side=2, las=0, cex = .85, adj=.5, line=3.5)
	axis(4, at=pretty(sum_yellow_eel), label = pretty(sum_yellow_eel)*0.05/factor_silver)
	if(factor_silver==1)
		mtext("Silver eel production", side=4, las=0, cex = .8, adj=.5, line=3.5) else
		mtext(paste("Silver eel production (x", factor_silver, ")", sep=""), side=4, las=0, cex = .85, adj=.5, line=3.5)
	box()
	
	# upper right corner graph
	barplot(sum_ccm_area, xlab="", ylab="", ylim=c(0, max(sum_ccm_area, na.rm=TRUE)*1.05), xaxt="n",yaxt="n",col="navy")
	title("Cumulated wetted area (ha)", font.main=par("font.axis"), cex.main=0.85)
	if(graph_side=="right")
		axis(2) else axis(4)
	box()
}

funresponsecurvemodelyears<-function(){
	stopifnot(exists(str_c("ModelYear"),envir=.GlobalEnv))
	M<-as.data.frame(ModelYear)
	colnames(M)<-as.character(years)
	M<-cbind("wso1_id"=ccm[,1],M)
	colnames(M)
	M1<-melt(M,id.vars="wso1_id")
	colnames(M1)<-c("wso1_id","annee","densite")
	save(M1,file=paste(datawd,"/dataEDArht/predictions/M1",".RData",sep=""))
	#load(file=paste(datawd,"/dataEDArht/predictions/",rapport,"M1.RData",sep=""))
	
	fun<-function(dat) return(c(boxplot.stats(dat$densite, coef = 1.5, do.conf = TRUE, do.out = FALSE)$stats,
						boxplot.stats(dat$densite, coef = 1.5, do.conf = TRUE, do.out = FALSE)$conf))
	RES<-ddply(M1,"annee",fun)
	
	colnames(RES)<-	c("annee","ymin","lower","middle","upper","ymax","lowernotch","uppernotch")
	width<-tapply(ers$annee,ers$annee,length)
	width<-width[(names(width)%in%unique(M1$annee))]
	width<-as.data.frame(width)
	RES<-cbind(RES,width)
	(p<-ggplot(RES,aes(x=annee,lower=lower,upper=upper,middle=middle,ymin=ymin,ymax=ymax,label=width))+
				geom_boxplot(stat="identity")+
				theme(	axis.text.x = element_text(angle = 90,hjust=1))+
				xlab("")+
				ylab("Density (nb/100m2) - delta gamma model"))
				
}
