# Nom fichier :        fn_sql_0
# Projet :             controle migrateur / traitement
# Organisme :          IAV
# Auteur :             C�dric Briand
# Contact :            cedric.briand@lavilaine.com
# Date de creation :   06/02/2007 10:58:37
# Compatibilite :      R 2.4.0
# Etat :               OK
# Description          fonction g�n�rique de connection � la base sans date
#**********************************************************************


# la fonction essaye d'�tablir une connection
# revoit une erreur en l'absence de connection
# puis teste la requ�te et renvoit une erreur en cas de pb de requete
# pour test   baseODBC="BD_CONTMIG.centraldata"

fn_sql_0<-function(sql,baseODBC, uid,pwd,silent=TRUE){
     
        e=expression(channel <-odbcConnect(baseODBC,
                                        uid = uid,
                                        pwd = pwd,
                                        case = "tolower",
                                        believeNRows = FALSE))
        if (!silent) print(paste("Try to connect ",baseODBC))
        # renvoit du resultat d'un try catch expression dans
        #l'ojet Connexion courante, soit un vecteur caract�re
        connexionCourante<-tryCatch(eval(e),
                        error=paste("unable to connect ",baseODBC))
        
          if (class(connexionCourante)=="RODBC") {if (!silent)print("Connection successfull")} else print(connexionCourante)
        
        if (!silent) print(paste("Try query"))
        e=expression(query<-sqlQuery(connexionCourante,sql,errors=TRUE))
        resultatRequete<-tryCatch(eval(e),error = function(e) e,
                finally=odbcClose(connexionCourante))
        # teste le resultat, si il y a erreur
        #classe avec plusieurs variables erreur , condition..
        # d'ou le [1]
        #sinon classe data.frame
        if ((class(resultatRequete)=="data.frame")[1]) { if (!silent) print("query ok")} else
        print(resultatRequete)
        return(list("query"=query,"sql"=sql))
        }



