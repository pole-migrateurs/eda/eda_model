# BaseEdaCCMriversegmentsmodel.r  class for EDA
# done to load data directly from the dbeel
#  Author: C�dric Briand, C�line Jouanin
###############################################################################

#' @title BaseEdamodel class 
#' @note daughter class of BaseEda
#' the joinschema should be processed ahead the class to contain all usefull informations
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' @slot data="data.frame" inherited from BaseEda
#' @slot baseODBC="character" inherited from BaseEda
#' @slot table the name of the table to load inherited from BaseEda
#' @slot prkey the primary key of the table to load inherited from BaseEda
#' @slot zonegeo="character" the area of the rht selected for the analysis (should contain all sea outlets (id_drain corresponding to the area)
#' @slot namemd the name of the density model
#' @slot namempa the name of the presence abscence model
#' @slot md=an object of class model for density
#' @slot mpa= an object of class model for presence absence
#' @slot predmd=predicted densities for density model 
#' @slot predmpa=predicted densities for presence absence model
#' @slot pred= predicted densities for the multiplication of presence abscence and density model
#' @slot resid the residuals for the pred model
#' @slot annee= year of the prediction
#' @slot mapfond the name(s) of the map to put in background
#' @slot mapstation the name of the shapefilecontaining the stations
#' @slot prkeystation the key file for stations (to join with the shapefile)
#' @slot datasp a spatialpointdataframe corresponding to the stations
#' @example object=new("BaseEdamodel")
#'object<-model_mod
#'object@namemd="md198"
#'object@namempa="mpa174"
setClass(Class="BaseEdamodel",
		representation=representation(zonegeo="character",
				namemd="character",
				namempa="character",
				md="ANY",
				mpa="ANY",
				predmd="numeric",
				predmpa="numeric",				
				pred="numeric",
				resid="numeric", # r�sidus du produit des deux mod�les
				annee="numeric",
				mapfond="character",
				mapstation="character",
				prkeystation="character",
				datasp="SpatialPointsDataFrame"				
		) ,
		prototype=prototype(annee=2005,
				datasp=SpatialPointsDataFrame(matrix(c(0,0),nrow=1,ncol=2),as.data.frame(NA))),
		contains="BaseEda")

setValidity("BaseEdamodel",function(object)
		{			
			rep1=all(class(object@md)==c("gam","glm","lm"))|is.null(object@md)
			tx1="the model md must be of class gam"
			rep2=all(class(object@mpa)==c("gam","glm","lm"))|is.null(object@mpa)
			tx2="the presence absence model must be of class gam"			
			return(ifelse(rep1 & rep2  , TRUE ,c(tx1,tx2)[!c(rep1,rep2)]))	
		})   


#' load method for BaseEdamodel 
#' @note this method will load the models in dataEDArht/predictions/ according to their name in namemd or namempa
#' @returnType object of class BaseEdamodel
#' @return BaseEdamodel
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' for debug object<-model_mod
setMethod("loadrdata",signature=signature("BaseEdamodel"),definition=function(object) {
			loadm<-function(mod){
				file<-str_c(mod,".Rdata")
				path<-str_c(datawd,"/dataEDArht/predictions/",file)
				load(path, envir = .GlobalEnv)
				
			}
			if (length(object@namemd)!=0) {
				loadm(object@namemd)
				if (exists(object@namemd)) object@md<-get(object@namemd) else object@md<-md				
			}
			if (length(object@namempa)!=0) {
				loadm(object@namempa)
				if (exists(object@namempa)) object@mpa<-get(object@namempa) else object@mpa<-mpa
			}
			return(object)
		})

#' load method for BaseEdamodel 
#' @note this method will load the shapefiles and reduce it to the size of the presence absence model data
#' according to the primary key for stations in object@prkeystation 
#' the shape for fond are also loaded and stored in the main environment
#' @returnType object of class BaseEdamodel
#' @return BaseEdamodel
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' for debug object<-model_mod
setMethod("loadshp",signature=signature("BaseEdamodel"),definition=function(object) {
			necessary = c('sp', 'maptools','rgdal')
			if(!all(necessary %in% installed.packages()[, 'Package']))
				install.packages(c('sp', 'maptools','rgdal'), dep = T)
			library(sp)
			library(maptools)
			library(rgdal)
			source("EDACCM/fonctions_graphiques.r")
			library(lattice)
			cat("1- loading data for background object@dataspfond assigned to main environment\n")
			for (i in 1 :length(object@mapfond)){
				dsn=str_c(shpwd,object@mapfond[i],".shp")
				fond<-readOGR(dsn,object@mapfond)
				assign(x=object@mapfond[i],value=fond, envir=.GlobalEnv)
			}
			cat("2- loading data for stations object@mapstation \n")
			dsn=str_c(shpwd,object@mapstation,".shp")
			object@datasp<-readOGR(dsn,object@mapstation)
			# reducing to the number of electrofished analysed in the model
			if(names(object@datasp@data)[1]%in%"st_codecsp")object@datasp@data$st_id<-object@datasp@data$st_codecsp
			if (length(object@mpa)!=0) {
				cat("3- reducing to the number of electrofished analysed in the model presence absence \n")
				object@datasp<-object@datasp[object@datasp@data[,object@prkeystation]%in%object@mpa$data[,object@prkeystation],]
			} else {
				cat("3- reducing to the number of electrofished analysed in the density model  \n")
				object@datasp<-object@datasp[object@datasp@data[,object@prkeystation]%in%object@md$data[,object@prkeystation],]
				
			}
			return(object)
		})
# predictions
# les methodses predict et resid sont des d�fauts dans R, elles doivent prendre comme argument object et pas object
#' predict method for BaseEdamodel 
#' the predict function returns one result per year if annee is set to numeric() in the object
#' @returnType object of class BaseEdamodel
#' @return BaseEdamodel
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' for debug object<-new("BaseEdamodel")
setMethod("predict",signature=signature("BaseEdamodel"),definition=function(object) {
			fn_pred.set<-function(mod,data=rht,annee=object@annee){
				the_terms<-labels(terms(mod))
				the_terms<-gsub(pattern="s[(]",replacement="",x=the_terms)
				the_terms<-gsub(pattern="[)]",replacement="",x=the_terms)
				the_terms<-gsub(pattern=" ",replacement="",x=the_terms)
				the_terms<-gsub(pattern="[,][[:digit:]]",replacement="",x=the_terms)	
				if (length(annee)!=0){
					the_terms<-the_terms[the_terms!="annee"]
					pred.set<-data[,the_terms]
					pred.set<-cbind("annee"=annee,pred.set)
				} else {
					pred.set<-data[,the_terms]
				}
				return(pred.set)
			}
# les predictions sont faites sur le jeu de donn�es du mod�le de pr�sence abscence
			if (length(object@md)!=0) object@predmd<-predict(object@md,fn_pred.set(mod=object@md,data=object@md$data,annee=object@annee),type='response')
			if (length(object@mpa)!=0) object@predmpa<-predict(object@mpa,fn_pred.set(object@mpa,object@mpa$data,object@annee),type='response')
			if (length(object@mpa)!=0 & length(object@md)!=0){
				object@predmd<-predict(object@md,fn_pred.set(mod=object@md,data=object@mpa$data,annee=object@annee),type='response')
				object@predmpa<-predict(object@mpa,fn_pred.set(object@mpa,object@mpa$data,object@annee),type='response')
				object@pred<-object@predmd*object@predmpa
			} else if (length(object@md)!=0) {
				object@pred<-object@predmd
			} else if (length(object@mpa)!=0) {
				object@pred<-object@predmpa
			} else {
				stop("no model for md or mpa either")
			}
			return(object)
		})

setMethod("resid",signature=signature("BaseEdamodel"),definition=function(object) {
			object<-predict(object)			
			if (length(object@predmpa)!=0 & length(object@predmd)!=0){			
				variable_reponse=strsplit(as.character(formula(object@md)), split="~")[[2]]
				object@resid<-object@mpa$data[,variable_reponse]-object@pred
			} else if  (length(object@predmd)!=0)  {
				object@resid<-residuals(object@md)
			} else if  (length(object@predmpa)!=0){
				object@resid<-residuals(object@mpa)				
			} else {
				stop ("error, there should be at least one md or mpa model")
			}
			
			return(object)			
		})
#' decale method for BaseEdamodel adapter from cluster.overplot in package plotrix
#' since we have 20 elect stations i adapted to 25 but the method could be changed by adding to the dist vector
#' the predict function returns one result per year if annee is set to numeric() in the object
#' @returnType object of class BaseEdamodel
#' @return BaseEdamodel
#' @author Cedric Briand \email{cedric.briand@@lavilaine.com}
#' for debug object<-new("BaseEdamodel")
setMethod("decale",signature=signature("BaseEdamodel"),definition=function(object,cex, away = NULL, ...) {
			x=object@datasp@coords[,1] 	#object@datasp@data[,"x"]
			y=object@datasp@coords[,2]   # object@datasp@data[,"Y"]
			if (is.null(away)) {
				away <- c(strwidth(s="o",cex=cex), 5 * strheight(s="o",cex=cex)/8)
			}
			nbyears=max(object@datasp@data$annee)-min(object@datasp@data$annee)+1
			if (nbyears <=16 )dist=c(0,1,2,3,4) else 	dist=c(0,1,2,3,4,5) 
			grd<-expand.grid(x=dist*away[1],y=-dist*away[2])/2
			stopifnot(nrow(grd)>=nbyears)
			grd$year=NA
			grd$year[1]<-min(object@datasp@data$annee)
			# on remplit avec des NA � la fin
			for (j in 2:nrow(grd)){
				grd$year[j]<-grd$year[j-1]+1
			}
#			ordx<-as.numeric(as.factor(abs(grd[,1])))
#			ordy<-as.numeric(as.factor(abs(grd[,2])))
#			ordx[ordx==min(ordx)]<-min(ordx)-1
#			ordx[ordx==max(ordx)]<-max(ordx)+2
#			ordy[ordy==min(ordy)]<-min(ordy)-1
#			ordy[ordy==max(ordy)]<-max(ordy)+2	
#			ord<-order(ordx+ordy)
#			grd<-grd[ord,]
			count<-tapply(object@datasp@data[,object@prkeystation],object@datasp@data[,object@prkeystation],function(X)length(X))
			count<-count[count>1]
			stationsoverplot<-names(count)
			overplots <- object@datasp@data[,object@prkeystation]%in%names(count)
			if (sum(overplots) > 1) {
				cat(str_c(sum(overplots)," stations en overplot\n"))
				
				for (i in stationsoverplot) {
					# debug i<-stationsoverplot[1]
					index<-which(object@datasp@data[,object@prkeystation]%in%i)
					# index is the line of the repeaded station i
					for (j in 1:length(index)){
						x[index[j]] <- x[index[j]] + grd[match(object@datasp@data[index[j],"annee"],grd$year),1]
						y[index[j]] <- y[index[j]] + grd[match(object@datasp@data[index[j],"annee"],grd$year),2]
					}
				}
			}			
			object@datasp@coords[,1]<-x
			object@datasp@coords[,2]<-y
			#object@datasp@data[,"x"]<-x
			#object@datasp@data[,"y"]<-y
			return(object)
		})

setMethod("map",
		signature=signature("BaseEdamodel"),
		definition=function(object,
				type="residus",    #residus, predites
				couleurs,
				probs,
				cutn=NULL,         # ignore les quantiles
				cex=0.8,
				space="Lab",
				title,
				xlim =  NULL,                      
				ylim = NULL ,
				couleur_fond="grey95",
				point=19,
				textbas=FALSE,
				surface=NULL,
				couleursurface=NULL,
				legendlab=NULL,
				legendval=NULL,
				group="size", # "size",les points changent de taille "cte" les points sont de taille constante mais les moyennes sont quand m�me calcul�es, "decale" 
				load_opt="load",
				...) {
			# first need to call calculation of resid and hence predict method
			if(load_opt=="load") object<-loadrdata(object) # sometimes we will want to load less than the full dataset
			if(load_opt=="load") object<-resid(object)
			# this can be either density model or presence abscence or the combination see resid
			# loading shape data
			object<-loadshp(object)
			# jeudi 23 juin, je vais rechercher les coordonnees
			object@datasp@data[,"x"]<-coordinates(object@datasp)[,1]
			object@datasp@data[,"y"]<-coordinates(object@datasp)[,2]
			#stopifnot(object@datasp@data[,object@prkeystation]==object@mpa$data[,object@prkeystation])
			# le switch ci dessous renvoit un tapply pour les r�sidus, les pr�dites ou une variable quelconque
			
			switch(type,
					"residus"=dens<-object@resid,
					"predites"=dens<-object@pred,
					{   #par d�faut last si rien n'est trouv� avant type="nomcolonne"
						# v�rification
						if (!(type %in% names(object@mpa$data))) stop("le nom de la variable n\'est pas dans le jeu de donn\u00e9es du mod\u00e8le")
						#calcul
						dens<-object@mpa$data[,type]						
					}
			)
			if (group=="size"|group=="cte") {
				if (length(object@predmpa)!=0) dens=tapply(dens,object@mpa$data[,object@prkeystation],mean) else
					dens=tapply(dens,object@md$data[,object@prkeystation],mean)	
				index<-match(names(dens),object@datasp@data[,object@prkeystation])
				object@datasp@data[index,"donnee_affichee"]<-dens 
				if (length(object@predmpa)!=0) {
					nb=tapply(object@mpa$data[,object@prkeystation],object@mpa$data[,object@prkeystation],length)
				} else {
					nb=tapply(object@md$data[,object@prkeystation],object@md$data[,object@prkeystation],length)
				}
				object@datasp@data[index,"nb"]<-nb
			} else if (group=="decale") {
				# il y moins de stations que d'op�rations il faut r�int�grer les x et y des stations pour chaque op�ration.
				if (length(object@predmpa)!=0){
					object@mpa$data[,"donnee_affichee"]<-dens
					operations<-merge(object@datasp@data[,c("x","y",object@prkeystation)],object@mpa$data,by=object@prkeystation)
				} else {# values only for density model
					object@md$data[,"donnee_affichee"]<-dens
					operations<-merge(object@datasp@data[,c("x","y",object@prkeystation)],object@md$data,by=object@prkeystation)
					
				}
				object@datasp@data<-operations
				dimnam<-dimnames(object@datasp@coords)
				object@datasp@coords<-as.matrix(operations[,c("x","y")])
				dimnames(object@datasp@coords)<-dimnam
			} else { # il s'agit alors de la variable dont on veut afficher la taille
				dens=tapply(dens,object@mpa$data[,object@prkeystation],mean)
				index<-match(names(dens),object@datasp@data[,object@prkeystation])
				object@datasp@data[index,"donnee_affichee"]<-dens 
				nb=tapply(object@mpa$data[,group],object@mpa$data[,object@prkeystation],mean)
				object@datasp@data[index,"nb"]<-nb
			}
			# object@datasp@data[,"donnee_affichee"]
			# calcul des effectifs par station, peu
			
			text=""
			
			# il doit y avoir un object graphique ouvert pour calculer decale
			if (dev.cur()==1) {
				x11()
				if (!is.null(xlim)&!is.null(ylim)){
					plot(get(object@mapfond),xlim=xlim,ylim=ylim)
				} else {
					plot(get(object@mapfond))
				}
			}
# ci dessous appel � la m�thode decale pour d�placer les points
			if (group=="decale"){
				cat("application d\'un decale\n")
				object<-decale(object,cex=cex)	
			}
			assign("object",object,envir=.GlobalEnv)
			plot_SpPtsDF(fond=object@mapfond,
					xlim=xlim,
					ylim=ylim,
					donnee=object@datasp,  #un object de classe SpPtsDF
					colonne="donnee_affichee",     # la colonne qu'on souhaite afficher
					couleurs=couleurs,
					space=space,  #c("rgb", "Lab")
					probs=probs,
					cutn=cutn,
					cex=cex, 
					title=title,
					text=text,
					couleur_fond=couleur_fond,
					point=point,
					textbas=textbas,
					surface=surface,
					couleursurface=couleursurface,
					legendlab=legendlab,
					legendval=legendval,
					group=group
			)
			# return(object)# use this for debug
		})