# BaseEdaRHTpatedeval.R class for EDARHT
# DEPRECATED (method moved to baseEDARHTpate)
# This class is daughter class for EDaRHTpate which connects to the devalpomi 
# database
#
# EDA2.1 
# Created 02/04/2013
# IAV
# Author: Cedric
###############################################################################

#' @title BaseEdaRHTpatedeval class 
#' @note daughter class of BaseEdaRHTpate same functions but replaces the loadb function
#' @author cedric Briand
#' @slot data="data.frame" inherited from BaseEda 
#' @slot baseODBC="character" inherited from BaseEda
#' @slot table the name of the table to load inherited from BaseEda
#' @slot prkey the primary key of the table to load inherited from BaseEda
#' @slot schema="character" the schema name of the table inherited from BaseEda
#' @slot zonegeo="character" the area of the rht selected for the analysis (should contain all sea outlets (wso_id corresponding to the area)
#' @prkey the primary key of the table to load inherited from BaseEda
#' @example object=new("BaseEdaRHTpatedeval")
setClass(Class="BaseEdaRHTpatedeval",
		prototype=prototype(),
		contains="BaseEdaRHTpate")

setValidity("BaseEdaRHTpatedeval",function(object)
		{
			# todo develop if necessary	
		})   

#' load method for BaseEdaRHTpatedeval
#' loads the data to the datapatedebit slot
#' @note the table 
#' @returnType Object of class BaseEda
#' @return BaseEda
#' @author Celine Jouanin \email{celine.jouanin@@irstea.fr}
#' for debug object<-BaseEdaRHTpatedeval (from main_rht_result.R)
#' for test write 	assign("showmerequest",1,envir=.GlobalEnv)
setMethod("loaddb",signature=signature("BaseEdaRHTpatedeval"),definition=function(object) {
	  requete<-new("RequeteODBCwhere")
	  requete@baseODBC<-baseODBC # should be the one for dams
	  requete@select= 		"select o.ouv_id,
		  ouv_libelle,
		  libelle as libelle_sous_secteur_hydro,
		  lib_sect as libelle_secteur_hydro,
		  rht_roe.id_drain,
		  vs.module,
		  eno_presenceexutoire,
		  eno_debittotalexutoire,
		  eno_presencegrille25mm,
		  eno_commentaireturbine,
		  eno_debitequipement,
		  eno_puissancemax,
		  typt_libelle,
		  typt_id,
		  turb_enservice,
		  turb_espaceinterbar,	
		  turb_puissance,	
		  turb_debitarmement,
		  turb_hauteurchute,
		  turb_diametre,
		  turb_vitesserotation,
		  turb_nbpales,
		  turb_debitmaxturbine,
		  turb_description,
		  a_conserver
		  from lb.t_ouvrage_ouv o 
		  join lb.sous_secteurlb ss on ouv_csssect=c_ss_sect 
		  join lb.t_turbine_turb on turb_ouv_id=ouv_id
		  join lb.t_ensembleouvrage_eno on eno_ouv_id=ouv_id
		  join lb.tr_typeturbine_typt on turb_typt_id=typt_id
		  left join rht.rht_roe on rht_roe.ouv_id=o.ouv_id
		  left join rht.a_conserver on a_conserver.ouv_id=o.ouv_id
		  left join rht.attributs_rht_fev_2011_vs2 vs on rht_roe.id_drain=vs.id_drain"
	  requete@select<-gsub("\\n"," ",requete@select)
	  requete@select<-gsub("\\t","",requete@select)
	  requete@where=" where a_conserver=TRUE"
	  requete@order_by=" order by libelle_secteur_hydro,ouv_id"
	  requete<-connect(requete) 
	  object@datapate<-requete@query
	  #cat(paste("data from the pate file loaded:",nrow(object@datapatedebit),"lines\n")	
	  return(object)
			
		})
