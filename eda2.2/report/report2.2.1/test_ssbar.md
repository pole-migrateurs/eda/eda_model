---
title: "Recherche d'une erreur dans les prédictions sans barrage"
author: "Cédric"
date: "19 avril 2018"
output: html_document
---

# init




# Creation d'un jeu de données pour la Bretagne


```r
library(ggmap)
library(sp)
library(rpostgis)
```

```r
# chargement des prédictions (une ligne par stade x id_drain)
load(file=str_c(datawd,"/dataEDArht/predictions/",secteur,rapport,"dffss.Rdata"))
dffss$mdxmpa<-dffss$md*dffss$mpa
sum(is.na(dffss$mdxmpa)) # 108
dcdfss<-dcast(dffss[,c("id_drain","taille","mdxmpa")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "mdxmpa")
# caclul de la densité totale
dcdfss$d<-rowSums(dcdfss[,2:7])
dcdfss[,2:8]<-round(dcdfss[,2:8],4)
colnames(dcdfss)[2:7]<-str_c("mdxmpass",colnames(mpass)[2:7])
mdss<-dcast(dffss[,c("id_drain","taille","md")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "md")
colnames(mdss)[2:7]<-str_c("mdss",colnames(mdss)[2:7])
mpass<-dcast(dffss[,c("id_drain","taille","mpa")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "mpa")
colnames(mpass)[2:7]<-str_c("mpass",colnames(mpass)[2:7])
#########################"
# chargement postgis
#########################



#import d'une table postgis dans R via le package rpostgis
conn<-dbConnect("PostgreSQL",dbname='eda2.2',host='localhost',port='5432',user='postgres',password='postgres')
my_spdf<-pgGetGeom(conn, name=c("rht","rhtvs2"), geom = "the_geom")
dbDisconnect(conn)
my_spdf@data$gid <- 1:nrow(my_spdf@data)
my_spdf@data <- merge(my_spdf@data,rht[,c("cs_heightp15","distance_sea","fUGA","id_drain")],by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <-merge(my_spdf@data,dcdfss,by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <-merge(my_spdf@data,mdss,by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <-merge(my_spdf@data,mpass,by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <- my_spdf@data[order(my_spdf@data$gid),]

################# 
# DONNEES MODELE ORIGINAL
#####################

load(file=str_c(datawd,"/dataEDArht/predictions/",secteur,rapport,"dff.Rdata"))
str(dff)
dff$mdxmpa<-dff$md*dff$mpa
sum(is.na(dff$mdxmpa)) # 108
dcdf<-dcast(dff[,c("id_drain","taille","mdxmpa")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "mdxmpa")
# caclul de la densité totale
dcdf$d<-rowSums(dcdf[,2:7])
dcdf[,2:8]<-round(dcdf[,2:8],4)

md<-dcast(dff[,c("id_drain","taille","md")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "md")
colnames(md)[2:7]<-str_c("md",colnames(md)[2:7])
mpa<-dcast(dff[,c("id_drain","taille","mpa")] ,formula = id_drain~taille,fun.aggregate = sum,value.var = "mpa")
colnames(mpa)[2:7]<-str_c("mpa",colnames(mpa)[2:7])
my_spdf@data <-merge(my_spdf@data,dcdf,by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <-merge(my_spdf@data,md,by="id_drain",all.x=TRUE,all.y=FALSE)
my_spdf@data <-merge(my_spdf@data,mpa,by="id_drain",all.x=TRUE,all.y=FALSE)
brit<-my_spdf[my_spdf@data$fUGA=="Bre",]
save(brit,file=str_c(datawd,"/dataEDArht/predictions/",secteur,rapport,"britss_spatial.Rdata"))


save(my_spdf,file=str_c(datawd,"/dataEDArht/predictions/",secteur,rapport,"dffss_spatial.Rdata"))
```



```r
# colnames(brit@data)
#[1] "id_drain"      "gid"           "__gid"         "fnode"        
#[5] "tnode"         "length"        "noeudmer"      "nextdownid"   
#[9] "noeudsource"   "chemin"        "dmer"          "dsource"      
#[13] "cumnbbar"      "id_drainmer"   "temp_cru_2000" "cs_heightp15" 
#[17] "distance_sea"  "fUGA"          "150"           "150_300"      
#[21] "300_450"       "450_600"       "600_750"       "750"          
#[25] "d"             "md150"         "md150_300"     "md300_450"    
#[29] "md450_600"     "md600_750"     "md750"         "mpa150"       
#[33] "mpa150_300"    "mpa300_450"    "mpa450_600"    "mpa600_750"   
#[37] "mpa750" 
spplot(brit,zcol="cs_heightp15",main="cs_heightp15")
```

```
## Error in spplot(brit, zcol = "cs_heightp15", main = "cs_heightp15"): object 'brit' not found
```

```r
spplot(brit,zcol="distance_sea",main="distance_sea")
```

```
## Error in spplot(brit, zcol = "distance_sea", main = "distance_sea"): object 'brit' not found
```

```r
spplot(brit,zcol="d",main="densite")
```

```
## Error in spplot(brit, zcol = "d", main = "densite"): object 'brit' not found
```
