\select@language {british}
\select@language {french}
\contentsline {part}{I\hspace {1em}R\'esum\'e op\'erationnel (en Fran\c cais)}{7}{part.1}
\select@language {british}
\contentsline {part}{II\hspace {1em}Report}{13}{part.2}
\contentsline {chapter}{\numberline {1}Introduction}{14}{chapter.1}
\contentsline {chapter}{\numberline {2}Material and methods}{16}{chapter.2}
\contentsline {section}{\numberline {2.1}A short historical overview of EDA}{16}{section.2.1}
\contentsline {section}{\numberline {2.2}Modelling strategy}{16}{section.2.2}
\contentsline {section}{\numberline {2.3}Dataset construction}{17}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Dam data}{17}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Electrofishing data}{18}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}BDMAP data}{18}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}Eel specific electrofishing}{19}{subsubsection.2.3.2.2}
\contentsline {subsubsection}{\numberline {2.3.2.3}Water surface}{19}{subsubsection.2.3.2.3}
\contentsline {subsubsection}{\numberline {2.3.2.4}Estimation of total number in a station}{19}{subsubsection.2.3.2.4}
\contentsline {subsubsection}{\numberline {2.3.2.5}Operations removed from the dataset}{21}{subsubsection.2.3.2.5}
\contentsline {section}{\numberline {2.4}Other variables}{23}{section.2.4}
\contentsline {section}{\numberline {2.5}Model calibration}{23}{section.2.5}
\contentsline {section}{\numberline {2.6}Silvering}{23}{section.2.6}
\contentsline {section}{\numberline {2.7}Predictions}{25}{section.2.7}
\contentsline {section}{\numberline {2.8}Statistic and database tools used to calibrate the model}{25}{section.2.8}
\contentsline {chapter}{\numberline {3}Results}{27}{chapter.3}
\contentsline {section}{\numberline {3.1}Topological variables}{27}{section.3.1}
\contentsline {section}{\numberline {3.2}EDA adjustment}{29}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Delta model}{29}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Gamma model}{35}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Model diagnostic and prediction}{40}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Delta model}{40}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Delta-Gamma model}{40}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Temporal trends}{48}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Trend of yellow eel abundance per size class}{48}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Trends in silver eel abundance}{49}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Comparison to the recruitment series}{49}{subsection.3.4.3}
\contentsline {section}{\numberline {3.5}Analysis of model responses}{50}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Fishing type}{50}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Difficulty of access}{52}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}EMU}{54}{subsection.3.5.3}
\contentsline {section}{\numberline {3.6}Silvering rates}{56}{section.3.6}
\contentsline {section}{\numberline {3.7}Silver eel numbers}{59}{section.3.7}
\contentsline {section}{\numberline {3.8}Synthesis of results per EMU}{61}{section.3.8}
\contentsline {section}{\numberline {3.9}Comparison with known productions}{63}{section.3.9}
\contentsline {chapter}{\numberline {4}Discussion}{66}{chapter.4}
\contentsline {section}{\numberline {4.1}Temporal trend in escapement}{66}{section.4.1}
\contentsline {section}{\numberline {4.2}Search for bias in the time series}{66}{section.4.2}
\contentsline {section}{\numberline {4.3}Electrofishing type}{67}{section.4.3}
\contentsline {section}{\numberline {4.4}Spatial repartition of eels.}{68}{section.4.4}
\contentsline {section}{\numberline {4.5}Under estimation of eel production by EDA model}{69}{section.4.5}
\contentsline {section}{\numberline {4.6}Importance of large eel in the reproductive stock}{70}{section.4.6}
\contentsline {section}{\numberline {4.7}Perspectives}{70}{section.4.7}
\contentsline {chapter}{\numberline {5}Glossary}{76}{chapter.5}
\contentsline {part}{III\hspace {1em}Annexes}{79}{part.3}
\contentsline {chapter}{Annexe 1: Equations}{80}{section*.55}
\contentsline {chapter}{Annexe 2: : Comparison ERS-RHT}{82}{section*.58}
\contentsline {chapter}{Annexe 3: Data source for eel specific surveys}{86}{section*.63}
\contentsline {chapter}{Annexe 4: Donn\'ees concernant les ouvrages}{89}{section*.67}
\contentsline {chapter}{Annexe 5: Model response variables}{90}{section*.71}
\contentsline {chapter}{Annexe 6: Predictions without dams}{92}{section*.76}
