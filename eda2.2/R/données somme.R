# 
# 
# Author: cedric.briand
###############################################################################

load_library=function(necessary) {
	if(!all(necessary %in% installed.packages()[, 'Package']))
		install.packages(necessary[!necessary %in% installed.packages()[, 'Package']], dep = T)
	for(i in 1:length(necessary))
		library(necessary[i], character.only = TRUE)
}
#load_library('RODBC') # odbc connector ==> no more used
load_library('stacomirtools') # connection tools
load_library('stringr') # text handling
load_library('sqldf') # mimict sql queries in a data.frame
load_library('RPostgreSQL') # one can use RODBC, here I'm using direct connection via the sqldf package
# loading RPostgresSQL ensures that postgres is used as a default driver by sqldf
load_library("XLConnect")

wb2 = loadWorkbook("C:/Users/cedric.briand/Desktop/eda/somme.xlsx", create = FALSE)
# ci dessous je force les types de turbines.
ang=readWorksheet(wb2, sheet = "ang")
suivi=readWorksheet(wb2, sheet = "suivi")
colnames(ang) # "Station" "saison"  "id"      "date"    "mois"    "taille"  "classe"  "poids"   "dh"      "dv"      "pecto"   "patho"  
colnames(suivi) #"annee"     "saison"    "station"   "nb"        "biom"      "dateinit"  "hinit"     "datefin"   "hfin"      "duree"     "dysfct"    "Remarques"
ta<-as.data.frame.matrix(table(ang$classe, ang$saison))
sum(ta["600",])/sum(rowSums(ta[c("750","900",1050),])) # 0.036 
1-0.036
xtabs(nb~saison+station,data=suivi)