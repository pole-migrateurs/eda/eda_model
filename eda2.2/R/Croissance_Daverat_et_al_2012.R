# Implementation de la fonction de croissance de Daverat et al. 2012 (EFF)
# 
# Author: lbeaulaton
###############################################################################

growth_rate_global = function(TempSUP13 = 31, sex = "M", class_depthsup1m = FALSE, class_selMarine = FALSE, age = 1, ratiodistsea = 0.2)
{
	class_sex = switch(sex, M = -0.1660791, F = 0, U = -0.5569070)
	classage = c(0,-0.1909239,-0.2638326,-0.4604791,-0.5524782,-0.5906220,-0.6618712,-0.7791524,-0.8373239,-0.8313118,-0.8968473,-0.9369006,-1.0367159,-1.0981445,-1.1272327,-1.1301553,-1.1252112,-1.2355196,-1.2649469,-1.2780214,-1.3304789,-1.3547388,-1.4452010,-1.3774239,-1.3779581,-1.5759406,-1.4389961,-1.6000603,-1.6860092,-1.6578861)[age]
	GR = exp(4.5215606 + 0.0056942 * TempSUP13 + class_sex + 0.0409277 * class_depthsup1m + 0.0508390 * class_selMarine + -0.2641504 * ratiodistsea + classage) 
	return(GR)
}

taille = function(Li = 70,...)
{
	GR = growth_rate_global(TempSUP13, sex, class_depthsup1m, class_selMarine, age, ratiodistsea)
	return(GR * age + Li)
}

growth_rate_global(sex = "U", TempSUP13 = 30, age = 1:30)

x11()
par(mar = c(4, 4, 0.5, 0.5))
plot(0:30, c(70, taille(sex = "F", TempSUP13 = 10, age = 1:30)), type = "l", ylim = c(0, 1000), xlim = c(0,30), col = "blue", xlab = "Age", ylab = "Taille", las = 2, yaxs = "i", xaxs = "i")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30)), type = "l", col = "yellow")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 30, age = 1:30)), type = "l", col = "orange")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 40, age = 1:30)), type = "l", col = "red")
legend("topleft", legend = c(seq(10,40,10)), title = "TEMP13", col = c("blue", "yellow", "orange", "red"), lty = 1)
savePlot("temp13.png", "png")

plot(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30)), type = "l", ylim = c(0, 1000), xlim = c(0,30), col = "magenta", xlab = "Age", ylab = "Taille", las = 2, yaxs = "i", xaxs = "i")
points(0:30, c(70, taille(sex = "M", TempSUP13 = 20, age = 1:30)), type = "l", col = "blue")
points(0:30, c(70, taille(sex = "U", TempSUP13 = 20, age = 1:30)), type = "l", col = "grey")
legend("topleft", legend = c("F", "M", "U"), col = c("magenta", "blue", "grey"), lty = 1)
savePlot("sex.png", "png")

plot(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, class_selMarine = TRUE)), type = "l", ylim = c(0, 1000), xlim = c(0,30), col = "blue", xlab = "Age", ylab = "Taille", las = 2, yaxs = "i", xaxs = "i")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, class_selMarine = FALSE)), type = "l", col = "green")
legend("topleft", legend = c("Marin", "Douce"), col = c("blue", "green"), lty = 1)
savePlot("marine.png", "png")

plot(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, ratiodistsea = 0)), type = "l", ylim = c(0, 1000), xlim = c(0,30), col = "blue", xlab = "Age", ylab = "Taille", las = 2, yaxs = "i", xaxs = "i")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, ratiodistsea = 0.3)), type = "l", col = "green")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, ratiodistsea = 0.6)), type = "l", col = "orange")
points(0:30, c(70, taille(sex = "F", TempSUP13 = 20, age = 1:30, ratiodistsea = 1)), type = "l", col = "black")
legend("topleft", legend = c(0,0.3,0.6,1), title = "d relative", col = c("blue", "green", "orange", "black"), lty = 1)
savePlot("drel.png", "png")